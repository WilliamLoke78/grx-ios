//
//  OrderHistoryTableViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 16/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import UIKit

private let orderViewCellId = "orderViewCellId"
private let loadingCell = "loadingCell"

class OrderHistoryTableViewController: BaseTableViewController {

    var market: Market?
    private var orders: [Order] = []
    
    var isFetching = false
    var isLast = false
    var pagination: Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        adjustUI()
        tableView.register(OrderTableViewCell.self, forCellReuseIdentifier: orderViewCellId)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: loadingCell)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
//        getOrderHistoryData(page: pagination)
        getOrderHistory()
    }
}

// MARK: User Interface
extension OrderHistoryTableViewController {
    
    func adjustUI() {
        
        title = "\(market?.coin ?? "")/\(market?.marketCoin ?? "") " + "Order History".localized()
        
        tableView.backgroundColor = .background
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView()
    }
}

// MARK: Table
extension OrderHistoryTableViewController {

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orders.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: orderViewCellId, for: indexPath) as! OrderTableViewCell
        
        cell.configure(orders[indexPath.row], market: market!)
//        cell.button.setTitle(orders[indexPath.row].state?.capitalized.localized().uppercased(), for: .normal)
        cell.button.setTitleColor(.textColor, for: .normal)
        cell.button.backgroundColor = .clear
        cell.button.layer.borderWidth = 0
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = orders.count - 1
//        if indexPath.row == lastElement - 5 {
//            getOrderHistoryData(page: pagination)
//
//        }
    }
    
    // Estimated height for row at is needed to avoid jumping cells
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
//        let vc = OrderDetailsTableViewController()
        let vc = ChildOrderHistoryTableViewController()
        vc.order = orders[indexPath.row]
        vc.market = market
        vc.category = orders[indexPath.row].category
        vc.orderId = orders[indexPath.row].id
        self.show(vc, sender: nil)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
}

// MARK: Networking
extension OrderHistoryTableViewController {
 
    func getOrderHistory() {
        
        print("get orders")
        
        var params: [String: Any] = [:]
        params["current"] = 1
        params["size"] = 50
        params["status"] = 1
        params["pair"] = market?.symbol ?? ""
        
        print(params)
        
        let networking = Networking()
        networking.getOrderList(params: params, controller: self) { orderData in
            
            print("orde reco")
            print(orderData.record)
            self.orders = orderData.record!
            self.tableView.reloadData()
        }
    }
}
