//
//  TradePriceTableViewCell.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 17/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

class PriceButton: UIButton {
    var price: String?
}

class TradePriceTableViewCell: BaseTableViewCell {
    
    let button: PriceButton = {
        let button = PriceButton()
        button.addTarget(self, action: #selector(TradeViewController.tapPrice(_:)), for: .touchUpInside)
        return button
    }()
    
    let priceShadowView: UIView = {
        let view = UIView()
        view.isUserInteractionEnabled = false
        return view
    }()
    
    let priceLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: regular, size: 12)
        label.textColor = .greenColor
        label.text = "- -"
        return label
    }()
    
    let amountLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: regular, size: 12)
        label.textColor = .textColor
        label.text = "- -"
        return label
    }()
    
    let separatorLine: UIView = {
        let view = UIView()
        view.backgroundColor = .textColor
        view.isHidden = true
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = .clear
        
        addSubview(button)
        button.addSubview(priceShadowView)
        button.addSubview(priceLabel)
        button.addSubview(amountLabel)
        button.addSubview(separatorLine)
        
        button.snp.makeConstraints({ (make) in
            make.edges.equalToSuperview()
        })
        
        priceShadowView.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(2)
            make.bottom.equalToSuperview().offset(-2)
            make.left.equalToSuperview().offset(6)
            make.width.equalTo(10)
        })
        
        priceLabel.snp.makeConstraints({ (make) in
            make.left.equalToSuperview().offset(11)
            make.centerY.equalToSuperview()
        })
        
        amountLabel.snp.makeConstraints({ (make) in
            make.right.equalToSuperview().offset(-11)
            make.centerY.equalToSuperview()
        })
        
        separatorLine.snp.makeConstraints({ (make) in
            make.height.equalTo(0.5)
            make.width.equalToSuperview()
            make.centerY.equalToSuperview()
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
//    override func resetTheme() {
//        backgroundColor = .container
//    }
}
