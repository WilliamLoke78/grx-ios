//
//  SimpleMarketTableViewCell.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 19/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import UIKit

private let marketCellHeight: CGFloat = 44

class SimpleMarketTableViewCell: BaseTableViewCell {
    
    lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .container
        return view
    }()

    lazy var currencyPairLabel: UILabel = {
        let label = UILabel()
        label.text = "BTC/USDT"
        label.textColor = .grayColor
        return label
    }()
    
    lazy var priceLabel: UILabel = {
        let label = UILabel()
        label.text = "9380.23"
        label.font = UIFont(name: bold, size: 14)
        label.textColor = .textColor
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    lazy var percentageLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: bold, size: 14)
        return label
    }()
    
    lazy var bottomLine: UIView = {
        let view = UIView()
        view.backgroundColor = .background
        return view
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .background
        
        addSubview(containerView)
        containerView.addSubview(currencyPairLabel)
//        containerView.addSubview(volumeLabel)
        containerView.addSubview(priceLabel)
//        containerView.addSubview(convertedPriceLabel)
        containerView.addSubview(percentageLabel)
        containerView.addSubview(bottomLine)
        
        let columnWidth = (screenWidth*2/3)/3
        
        containerView.snp.makeConstraints({ (make) in
            make.edges.equalToSuperview()
        })
        
        currencyPairLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(percentageLabel.snp.top)
            make.left.equalToSuperview().offset(11)
            make.width.equalTo(columnWidth)
        })
        
//        volumeLabel.snp.makeConstraints({ (make) in
//            make.top.equalTo(currencyPairLabel.snp.bottom)
//            make.left.equalTo(currencyPairLabel.snp.left)
//        })
        
        priceLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(currencyPairLabel)
            make.left.equalTo(currencyPairLabel.snp.right)
            make.width.equalTo(columnWidth)
        })
        
//        convertedPriceLabel.snp.makeConstraints({ (make) in
//            make.top.equalTo(volumeLabel)
//            make.left.equalTo(priceLabel.snp.left)
//        })
        
        percentageLabel.snp.makeConstraints({ (make) in
            make.right.equalToSuperview().offset(-11)
            make.centerY.equalToSuperview()
        })
        
        bottomLine.snp.makeConstraints({ (make) in
            make.height.equalTo(1)
            make.width.equalToSuperview()
            make.bottom.equalToSuperview()
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func configure(_ market: MarketProtocol) {
                
        let marketSymbol = "\(market.coin!)/\(market.marketCoin!)"
        currencyPairLabel.text = marketSymbol
        currencyPairLabel.pairBeautifier(.textColor)
        priceLabel.text = market.close != "" ? market.close?.forceDecimalDigits(market.priceDec!) : "--"
        if market.change!.contains("-") {
            percentageLabel.text = market.change != "" ? (market.change?.forceDecimalDigits(2))! + "%" : "--"
            percentageLabel.textColor = .redColor
        } else {
            percentageLabel.text = market.change != "" ? "+" + (market.change?.forceDecimalDigits(2))! + "%" : "--"
            percentageLabel.textColor = .greenColor
        }
    }

}

