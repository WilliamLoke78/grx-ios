//
//  OrderTableViewCell.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 17/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

class OrderTableViewCell: BaseTableViewCell {
    
    let markets: [Market] = []
    
    lazy var orderIdLabel: UILabel = {
        let label = UILabel()
        label.textColor = .grayColor
        label.font = UIFont(name: regular, size: 12)
        return label
    }()
    
    lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .container
        view.layer.cornerRadius = 4
        return view
    }()
    
    lazy var sideLabel: UILabel = {
        let label = UILabel()
        label.textColor = .textColor
        label.font = UIFont(name: bold, size: 14)
        return label
    }()
    
    lazy var orderTypeLabel: UILabel = {
        let label = UILabel()
        label.textColor = .grayColor
        label.backgroundColor = .background
        label.font = UIFont(name: bold, size: 11)
        label.textAlignment = .center
        label.layer.cornerRadius = 2
        label.backgroundColor = .background
        label.layer.borderWidth = darkTheme ? 1 : 0
        label.layer.borderColor = darkTheme ? UIColor.grayColor.cgColor : UIColor.clear.cgColor
        return label
    }()
    
    lazy var currencyLabel: UILabel = {
        let label = UILabel()
        label.text = "ETH/BTC"
        label.textColor = .grayColor
        label.backgroundColor = .background
        label.font = UIFont(name: bold, size: 11)
        label.textAlignment = .center
        label.layer.cornerRadius = 2
        label.backgroundColor = .background
        label.layer.borderWidth = darkTheme ? 1 : 0
        label.layer.borderColor = darkTheme ? UIColor.grayColor.cgColor : UIColor.clear.cgColor
        return label
    }()
    
    lazy var datetimeLabel: UILabel = {
        let label = UILabel()
        label.textColor = .grayColor
        label.font = UIFont(name: regular, size: 12)
        return label
    }()
    
    lazy var separatorLine: UIView = {
        let view = UIView()
        view.backgroundColor = .background
        return view
    }()
    
    lazy var filledPercentageView: UILabel = {
        let label = UILabel()
        label.text = "0%"
        label.textColor = .primary
        label.font = UIFont(name: bold, size: 20)
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    lazy var volumeLabel: UILabel = {
        let label = UILabel()
        label.text = "0.000123 BTC"
        label.textColor = .textColor
        label.font = UIFont(name: bold, size: 14)
        return label
    }()
    
    lazy var priceLabel: UILabel = {
        let label = UILabel()
        label.text = "206.78 USD"
        label.textColor = .grayColor
        label.font = UIFont(name: regular, size: 11)
        return label
    }()
    
    lazy var button: UIButton = {
        let button = UIButton()
        button.setTitle("Cancel".localized(), for: .normal)
        button.setTitleColor(.redColor, for: .normal)
        button.titleLabel?.font = UIFont(name: bold, size: 12)
        button.backgroundColor = UIColor.redColor.withAlphaComponent(0.2)
        button.layer.cornerRadius = 4
        button.layer.borderColor = UIColor.redColor.cgColor
        button.layer.borderWidth = 1
        return button
    }()
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .darkTheme, object: nil)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .background
        
        addSubview(orderIdLabel)
        addSubview(containerView)
        
        containerView.addSubview(sideLabel)
        containerView.addSubview(orderTypeLabel)
        containerView.addSubview(currencyLabel)
        containerView.addSubview(datetimeLabel)
        
        containerView.addSubview(separatorLine)
        
        containerView.addSubview(filledPercentageView)
        containerView.addSubview(volumeLabel)
        containerView.addSubview(priceLabel)
        containerView.addSubview(button)
        
        orderIdLabel.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(11)
            make.left.equalToSuperview().offset(12)
        })
        
        containerView.snp.makeConstraints({ (make) in
            make.top.equalTo(orderIdLabel.snp.bottom).offset(5)
            make.left.equalToSuperview().offset(12)
            make.right.equalToSuperview().offset(-12)
            make.height.equalTo(100)
        })
        
        sideLabel.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(11)
            make.left.equalToSuperview().offset(11)
        })
        
        orderTypeLabel.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(11)
            make.left.equalTo(sideLabel.snp.right).offset(10)
            make.height.equalTo(20)
            make.width.equalTo(50)
        })
        
        currencyLabel.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(11)
            make.left.equalTo(orderTypeLabel.snp.right).offset(10)
            make.height.equalTo(20)
            make.width.equalTo(60)
        })
        
        datetimeLabel.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(13)
            make.right.equalToSuperview().offset(-11)
        })
        
        separatorLine.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(40)
            make.left.equalToSuperview().offset(11)
            make.right.equalToSuperview().offset(-11)
            make.height.equalTo(1)
        })
        
        filledPercentageView.snp.makeConstraints({ (make) in
            make.top.equalTo(separatorLine.snp.bottom).offset(15)
            make.left.equalToSuperview().offset(15)
            make.right.equalTo(volumeLabel.snp.left).offset(-15)
        })
        
        volumeLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(separatorLine.snp.bottom).offset(14)
            make.left.equalToSuperview().offset(100)
        })
        
        priceLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(volumeLabel.snp.bottom)
            make.left.equalTo(volumeLabel.snp.left)
        })
        
        button.snp.makeConstraints({ (make) in
            make.top.equalTo(separatorLine.snp.bottom).offset(15)
            make.right.equalToSuperview().offset(-11)
            make.height.equalTo(34)
            make.width.equalTo(70)
        })
    }
    
    func configure(_ order: OrderProtocol, market: Market) {
        
        orderIdLabel.text = "Order ID".localized() + ": \(order.id!)"
        sideLabel.text = (order.category == 1) ? "Buy".localized() : "Sell".localized()
        sideLabel.textColor = (order.category == 1) ? .greenColor : .redColor
        
        orderTypeLabel.text = (order.orderType == 1) ? "Limit".localized() : "Market".localized()
        datetimeLabel.text = order.created?.serverToLocal()
        currencyLabel.text = "\(market.coin ?? "")/\(market.marketCoin ?? "")"
        
        var averagePrice: Double?
        if let dealAmount = order.dealAmount?.toDouble(), let dealVolume = order.dealVolume?.toDouble() {
            averagePrice = dealAmount/dealVolume
        }
        
        volumeLabel.text = "\(order.volume?.forceDecimalDigits(market.volDec!) ?? "0") \(market.coin ?? "")"
        
        if averagePrice!.isNaN {
            priceLabel.text = "\(order.price!.forceDecimalDigits(market.priceDec!)) \(market.marketCoin ?? "")"
        } else {
            priceLabel.text = "\(averagePrice ?? 0.0)".forceDecimalDigits(market.priceDec!) + " \(market.marketCoin ?? "")"
        }
        
        var filled: Double?
        if let executedVolume = order.dealVolume?.toDouble(), let originVolume = order.volume?.toDouble() {
            filled = executedVolume/originVolume*100
        }
        
        if filled!.isNaN {
            filledPercentageView.text = "--"
        } else {
            filledPercentageView.text = String(format: "%.2f", filled!)+"%"
        }
        
        var status: String?
        switch order.status {
        case 0:
            status = "Cancel".localized().uppercased()
            button.isEnabled = true
        case 1:
            status = "Done".localized().uppercased()
            button.isEnabled = false
        default:
            status = "Canceled".localized().uppercased()
            button.isEnabled = false
        }
        
        button.setTitle(status, for: .normal)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
