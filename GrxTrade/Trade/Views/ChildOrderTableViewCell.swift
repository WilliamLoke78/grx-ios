//
//  OrderTableViewCell.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 17/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

class ChildOrderTableViewCell: BaseTableViewCell {
    
    let markets: [Market] = []
    
    lazy var orderIdLabel: UILabel = {
        let label = UILabel()
        label.textColor = .grayColor
        label.font = UIFont(name: regular, size: 12)
        return label
    }()
    
    lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .container
        view.layer.cornerRadius = 4
        return view
    }()
    
    lazy var datetimeLabel: UILabel = {
        let label = UILabel()
        label.textColor = .grayColor
        label.font = UIFont(name: regular, size: 12)
        return label
    }()
    
    lazy var filledPercentageView: UILabel = {
        let label = UILabel()
        label.text = "0%"
        label.textColor = .primary
        label.font = UIFont(name: bold, size: 20)
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    lazy var volumeLabel: UILabel = {
        let label = UILabel()
        label.text = "0.000123 BTC"
        label.textColor = .textColor
        label.font = UIFont(name: bold, size: 14)
        return label
    }()
    
    lazy var priceLabel: UILabel = {
        let label = UILabel()
        label.text = "206.78 USD"
        label.textColor = .grayColor
        label.font = UIFont(name: regular, size: 11)
        return label
    }()
    
    lazy var button: UIButton = {
        let button = UIButton()
        button.setTitle("Cancel".localized(), for: .normal)
        button.setTitleColor(.redColor, for: .normal)
        button.titleLabel?.font = UIFont(name: bold, size: 12)
        button.backgroundColor = UIColor.redColor.withAlphaComponent(0.2)
        button.layer.cornerRadius = 4
        button.layer.borderColor = UIColor.redColor.cgColor
        button.layer.borderWidth = 1
        return button
    }()
    
    lazy var bottomLine: UIView = {
        let view = UIView()
        view.backgroundColor = .background
        return view
    }()
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .darkTheme, object: nil)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .background
        
        addSubview(orderIdLabel)
        addSubview(containerView)
        
        containerView.addSubview(datetimeLabel)
        containerView.addSubview(filledPercentageView)
        containerView.addSubview(volumeLabel)
        containerView.addSubview(priceLabel)
        
        orderIdLabel.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(5)
            make.left.equalToSuperview().offset(12)
        })
        
        containerView.snp.makeConstraints({ (make) in
            make.top.equalTo(orderIdLabel.snp.bottom).offset(5)
            make.left.equalToSuperview().offset(12)
            make.right.equalToSuperview().offset(-12)
            make.height.equalTo(59)
        })
        
        filledPercentageView.snp.makeConstraints({ (make) in
            make.top.equalTo(containerView.snp.top).offset(15)
            make.left.equalToSuperview().offset(15)
            make.right.equalTo(volumeLabel.snp.left).offset(-15)
        })
        
        volumeLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(filledPercentageView)
            make.left.equalToSuperview().offset(100)
        })
        
        priceLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(volumeLabel.snp.bottom)
            make.left.equalTo(volumeLabel.snp.left)
        })
        
        datetimeLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(volumeLabel)
            make.right.equalToSuperview().offset(-11)
        })
        
//        bottomLine.snp.makeConstraints({ (make) in
//            make.bottom.equalToSuperview()
//            make.width.equalToSuperview()
//            make.height.equalTo(10)
//        })
    }
    
    func configure(_ childOrder: ChildOrderProtocol, order: OrderProtocol, market: Market) {
        
        orderIdLabel.text = "Sub-Order ID".localized() + ": \(childOrder.id!)"
        datetimeLabel.text = childOrder.created?.serverToLocal()
        
        volumeLabel.text = "\(childOrder.vol!.forceDecimalDigits(market.volDec!)) \(market.coin ?? "")"
        priceLabel.text = "\(childOrder.price!.forceDecimalDigits(market.priceDec!)) \(market.marketCoin ?? "")"
        
        var filled: Double?
        if let executedVolume = childOrder.vol?.toDouble(), let originVolume = order.volume?.toDouble() {
            filled = executedVolume/originVolume*100
        }
        
        if filled!.isNaN {
            filledPercentageView.text = "--"
        } else {
            filledPercentageView.text = String(format: "%.2f", filled!)+"%"
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
