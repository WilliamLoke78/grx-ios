//
//  TradeTopBarView.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 17/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

class TradeTopBarView: BaseView {
    
    lazy var buyTab: UIButton = {
        let button = UIButton()
        button.setTitleColor(.primary, for: .normal)
        button.setTitle("Buy".localized(), for: .normal)
        button.titleLabel?.font = UIFont(name: bold, size: 14)
        return button
    }()
    
    lazy var buyBottomLine: UIView = {
        let view = UIView()
        view.backgroundColor = .primary
        return view
    }()
    
    lazy var sellTab: UIButton = {
        let button = UIButton()
        button.setTitleColor(.grayColor, for: .normal)
        button.setTitle("Sell".localized(), for: .normal)
        button.titleLabel?.font = UIFont(name: bold, size: 14)
        return button
    }()
    
    lazy var sellBottomLine: UIView = {
        let view = UIView()
        return view
    }()
    
    lazy var orderTab: UIButton = {
        let button = UIButton()
        button.setTitleColor(.grayColor, for: .normal)
        button.setTitle("Order History".localized(), for: .normal)
        button.titleLabel?.font = UIFont(name: bold, size: 14)
        return button
    }()
    
    lazy var orderBottomLine: UIView = {
        let view = UIView()
        return view
    }()
    
    override func setupViews() {
        
        backgroundColor = .container
        
        addSubview(buyTab)
        addSubview(buyBottomLine)
        addSubview(sellTab)
        addSubview(sellBottomLine)
        addSubview(orderTab)
        addSubview(orderBottomLine)
        
        buyTab.snp.makeConstraints({ (make) in
            make.width.equalTo(100)
            make.left.equalToSuperview()
            make.height.equalToSuperview()
        })
        
        buyBottomLine.snp.makeConstraints({ (make) in
            make.bottom.equalTo(buyTab.snp.bottom)
            make.height.equalTo(2)
            make.width.equalTo(buyTab.snp.width)
            make.left.equalTo(buyTab.snp.left)
            make.right.equalTo(buyTab.snp.right)
        })
        
        sellTab.snp.makeConstraints({ (make) in
            make.width.equalTo(100)
            make.left.equalTo(buyTab.snp.right)
            make.height.equalToSuperview()
        })
        
        sellBottomLine.snp.makeConstraints({ (make) in
            make.bottom.equalTo(sellTab.snp.bottom)
            make.height.equalTo(2)
            make.width.equalTo(sellTab.snp.width)
            make.left.equalTo(sellTab.snp.left)
            make.right.equalTo(sellTab.snp.right)
        })
        
        orderTab.snp.makeConstraints({ (make) in
            make.width.equalTo(100)
            make.left.equalTo(sellTab.snp.right)
            make.height.equalToSuperview()
        })
        
        orderBottomLine.snp.makeConstraints({ (make) in
            make.bottom.equalTo(orderTab.snp.bottom)
            make.height.equalTo(2)
            make.width.equalTo(orderTab.snp.width)
            make.left.equalTo(orderTab.snp.left)
            make.right.equalTo(orderTab.snp.right)
        })
    }
    
}

