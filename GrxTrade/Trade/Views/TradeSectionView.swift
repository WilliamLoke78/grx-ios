//
//  TradeSectionView.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 17/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

class TradeSectionView: BaseView {
    
    lazy var topView: UIView = {
        let view = UIView()
        return view
    }()
    
    lazy var priceLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: bold, size: 20)
        label.textColor = .greenColor
//        label.text = "150.122345"
        return label
    }()
    
    lazy var convertedPriceLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: regular, size: 12)
        label.textColor = .grayColor
//        label.text = "≈ 1003034056.83 USD"
        return label
    }()
    
    lazy var depthButton: UIButton = {
        let button = UIButton()
        button.setTitle("D1".localized(), for: .normal)
        let image = UIImage(named: "caret")
        button.setImage(image, for: .normal)
        button.titleLabel?.font = UIFont(name: bold, size: 11)
        button.setTitleColor(.grayColor, for: .normal)
        button.backgroundColor = .background
        button.layer.cornerRadius = 2
        button.transform = CGAffineTransform(scaleX: -1.0, y: 1.0);
        button.titleLabel?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0);
        button.imageView?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0);
        button.imageEdgeInsets = UIEdgeInsets(top: 7, left: 0, bottom: 7, right: 7)
        button.imageView?.contentMode = .scaleAspectFit
        return button
    }()
    
    lazy var modifyPriceLayoutButton: UIButton = {
        let button = UIButton()
        let image = UIImage(named: darkTheme ? "split_mode_dark" : "split_mode_light")
        button.setImage(image, for: .normal)
        return button
    }()
    
    lazy var leftView: UIView = {
        let view = UIView()
        return view
    }()
    
    lazy var tradeTypeButton: UILabel = {
        let label = UILabel()
        label.text = "Limit".localized()
        label.font = UIFont(name: bold, size: 12)
        label.textColor = .grayColor
        return label
    }()
    
    lazy var caretImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "caret")
        return iv
    }()
    
    lazy var stopTextField: UITextField = {
        let tf = UITextField()
        tf.backgroundColor = .white
        tf.layer.cornerRadius = 4
        tf.keyboardType = .decimalPad
        tf.leftPadding(11)
        tf.placeholder = "Stop".localized()
        tf.layer.borderWidth = 1
        return tf
    }()
    
    lazy var priceTextField: UITextField = {
        let tf = UITextField()
        tf.backgroundColor = .textFieldContainer
        tf.layer.cornerRadius = 4
        tf.keyboardType = .decimalPad
        tf.leftPadding(11)
        tf.keyboardAppearance = darkTheme ? .dark : .light
        
        var placeholder = NSMutableAttributedString()
        let name  = "Price".localized()
        placeholder = NSMutableAttributedString(string: name, attributes: [NSAttributedString.Key.font: UIFont(name: regular, size: 12)!])
        placeholder.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.grayColor, range: NSRange(location: 0, length: name.count))
        tf.attributedPlaceholder = placeholder
        
        tf.textColor = .textColor
        return tf
    }()
    
    lazy var minMaxPriceLabel: UILabel = {
        let label = UILabel()
        label.textColor = .grayColor
        label.font = UIFont(name: regular, size: 11)
        label.textAlignment = .right
//        label.text = "0.000001 - 0.100000"
        return label
    }()
    
    lazy var convertedInputPriceLabel: UILabel = {
        let label = UILabel()
        label.textColor = .grayColor
        label.font = UIFont(name: regular, size: 11)
//        label.text = "≈ 04.83 USD"
        label.isHidden = true
        return label
    }()
    
    lazy var amountTextField: UITextField = {
        let tf = UITextField()
        tf.backgroundColor = .textFieldContainer
        tf.layer.cornerRadius = 4
        tf.keyboardType = .decimalPad
        tf.leftPadding(11)
        tf.rightPadding(40)
        tf.layer.borderColor = UIColor.textFieldBorder.cgColor
        tf.keyboardAppearance = darkTheme ? .dark : .light
        
        var placeholder = NSMutableAttributedString()
        let name  = "Volume".localized()
        placeholder = NSMutableAttributedString(string: name, attributes: [NSAttributedString.Key.font: UIFont(name: regular, size: 12)!])
        placeholder.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.grayColor, range: NSRange(location: 0, length: name.count))
        tf.attributedPlaceholder = placeholder
        
        tf.textColor = .textColor
        return tf
    }()
    
    lazy var amountTextFieldLabel: UILabel = {
        let label = UILabel()
        label.textColor = .textColor
//        label.text = "BTC"
        return label
    }()
    
    lazy var availableTitleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .grayColor
        label.font = UIFont(name: regular, size: 11)
        label.text = "Available".localized()
        return label
    }()
    
    lazy var availableAmountLabel: UILabel = {
        let label = UILabel()
        label.textColor = .grayColor
        label.font = UIFont(name: regular, size: 11)
        label.text = "--"
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    lazy var totalTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Total".localized()
        label.textColor = .primary
        label.font = UIFont(name: bold, size: 14)
        return label
    }()
    
    lazy var totalAmountLabel: UILabel = {
        let label = UILabel()
        label.text = "--"
        label.textColor = .primary
        label.font = UIFont(name: bold, size: 14)
        return label
    }()
    
    lazy var buySellButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .greenColor
        button.titleLabel?.font = UIFont(name: bold, size: 16)
        button.layer.cornerRadius = 4
        return button
    }()
    
    lazy var rightView: UIView = {
        let view = UIView()
        return view
    }()
    
    lazy var priceTableView: PriceTableView = {
        let tv = PriceTableView()
        return tv
    }()
    
    override func setupViews() {
        
        backgroundColor = .container
        
        addSubview(topView)
        topView.addSubview(priceLabel)
        topView.addSubview(convertedPriceLabel)
        
        topView.snp.makeConstraints({ (make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(40)
        })
        
        priceLabel.snp.makeConstraints({ (make) in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(13)
        })
        
        convertedPriceLabel.snp.makeConstraints({ (make) in
            make.bottom.equalTo(priceLabel.snp.bottom).offset(-2)
            make.left.equalTo(priceLabel.snp.right).offset(4)
        })
        
        addSubview(leftView)
        leftView.addSubview(tradeTypeButton)
        leftView.addSubview(caretImageView)
        leftView.addSubview(stopTextField)
        leftView.addSubview(priceTextField)
        leftView.addSubview(minMaxPriceLabel)
        leftView.addSubview(convertedInputPriceLabel)
        leftView.addSubview(amountTextField)
        leftView.addSubview(amountTextFieldLabel)
        leftView.addSubview(availableTitleLabel)
        leftView.addSubview(availableAmountLabel)
        leftView.addSubview(totalTitleLabel)
        leftView.addSubview(totalAmountLabel)
        leftView.addSubview(buySellButton)
        
        addSubview(rightView)
        addSubview(modifyPriceLayoutButton)
        addSubview(depthButton)
        rightView.addSubview(priceTableView)
        
        let viewWidth = screenWidth/2
        
        leftView.snp.makeConstraints({ (make) in
            make.top.equalTo(topView.snp.bottom).offset(5)
            make.left.equalToSuperview()
            make.bottom.equalToSuperview()
            make.width.equalTo(viewWidth)
        })
        
        tradeTypeButton.snp.makeConstraints({ (make) in
            make.top.equalTo(rightView.snp.top)
            make.left.equalToSuperview().offset(14)
            make.right.equalToSuperview().offset(-13)
            make.height.equalTo(20)
        })
        
        caretImageView.snp.makeConstraints({ (make) in
            make.centerY.equalTo(tradeTypeButton.snp.centerY)
            make.right.equalToSuperview().offset(-13)
            make.height.equalTo(6)
            make.width.equalTo(10)
        })
        
        stopTextField.snp.makeConstraints({ (make) in
            make.top.equalTo(tradeTypeButton.snp.bottom).offset(0)
            make.left.equalToSuperview().offset(13)
            make.right.equalToSuperview().offset(-13)
            make.height.equalTo(0)
        })
        
        priceTextField.snp.makeConstraints({ (make) in
            make.top.equalTo(stopTextField.snp.bottom).offset(13)
            make.left.equalToSuperview().offset(13)
            make.right.equalToSuperview().offset(-13)
            make.height.equalTo(44)
        })
        
        minMaxPriceLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(priceTextField.snp.bottom).offset(3)
            make.right.equalTo(priceTextField.snp.right)
            make.width.equalTo(priceTextField)
        })
        
        convertedInputPriceLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(minMaxPriceLabel.snp.bottom).offset(3)
            make.left.equalToSuperview().offset(13)
            make.right.equalToSuperview().offset(-13)
        })
        
        amountTextField.snp.makeConstraints({ (make) in
//            make.top.equalTo(priceTextField.snp.bottom).offset(3)
            make.top.equalTo(minMaxPriceLabel.snp.bottom).offset(3)
            make.left.equalToSuperview().offset(13)
            make.right.equalToSuperview().offset(-13)
            make.height.equalTo(44)
        })
        
        amountTextFieldLabel.snp.makeConstraints({ (make) in
            make.right.equalTo(amountTextField).offset(-8)
            make.centerY.equalTo(amountTextField)
        })
        
        availableTitleLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(amountTextField.snp.bottom).offset(3)
            make.left.equalToSuperview().offset(13)
            make.right.equalToSuperview().offset(-13)
        })
        
        availableAmountLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(availableTitleLabel.snp.top)
            make.right.equalTo(buySellButton.snp.right)
            make.left.equalTo(availableTitleLabel.snp.right).offset(10)
            make.width.equalTo(120)
        })
        
        totalTitleLabel.snp.makeConstraints({ (make) in
            make.bottom.equalTo(buySellButton.snp.top).offset(-30)
            make.left.equalToSuperview().offset(13)
            make.right.equalToSuperview().offset(-13)
        })
        
        totalAmountLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(totalTitleLabel.snp.top)
            make.right.equalTo(buySellButton.snp.right)
            make.left.equalTo(totalTitleLabel.snp.right).offset(10)
            make.width.equalTo(screenWidth*0.34)
        })
        
        buySellButton.snp.makeConstraints({ (make) in
            make.left.equalToSuperview().offset(13)
            make.right.equalToSuperview().offset(-13)
            make.bottom.equalToSuperview().offset(-43)
            make.height.equalTo(44)
            make.width.equalToSuperview().offset(-26)
        })
        
        rightView.snp.makeConstraints({ (make) in
            make.top.equalTo(topView.snp.bottom).offset(5)
            make.bottom.equalToSuperview()
            make.right.equalToSuperview()
            make.width.equalTo(viewWidth)
        })
        
        priceTableView.snp.makeConstraints({ (make) in
            make.height.equalToSuperview()
            make.width.equalToSuperview()
            make.bottom.equalTo(leftView.snp.bottom)
        })
        
        modifyPriceLayoutButton.snp.makeConstraints({ (make) in
            make.bottom.equalToSuperview().offset(-10)
            make.right.equalToSuperview().offset(-10)
            make.height.equalTo(24)
            make.width.equalTo(24)
        })
        
        depthButton.snp.makeConstraints({ (make) in
            make.top.equalTo(modifyPriceLayoutButton)
            make.right.equalTo(modifyPriceLayoutButton.snp.left).offset(-10)
            make.height.equalTo(24)
            make.width.equalTo(72)
        })
    }
}
