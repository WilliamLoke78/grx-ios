//
//  PriceTableView.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 17/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

private let priceCellId = "priceCellId"

class PriceTableView: BaseView {
    
    var displayMode: String = "split"
    
    var market = Market()
    var bids = [[String]]()
    var asks = [[String]]()
    var highestVolume = Double()
    
    fileprivate lazy var tableHeader: UIView = {
        let th = UIView.init(frame: CGRect.init(x: 0, y: 0, width: screenWidth, height: 22))
        return th
    }()
    
    let priceTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Price".localized()
        label.font = UIFont(name: regular, size: 11)
        label.textColor = .grayColor
        return label
    }()
    
    let amountTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Volume".localized()
        label.font = UIFont(name: regular, size: 11)
        label.textColor = .grayColor
        return label
    }()
    
    let tableView: UITableView = {
        let tv = UITableView()
        tv.backgroundColor = .clear
        tv.separatorStyle = .none
        return tv
    }()
    
    override func setupViews() {
        
        addSubview(tableHeader)
        tableHeader.addSubview(priceTitleLabel)
        tableHeader.addSubview(amountTitleLabel)
        addSubview(tableView)
        
        tableView.tableHeaderView = tableHeader
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(TradePriceTableViewCell.self, forCellReuseIdentifier: priceCellId)
        
        tableView.isScrollEnabled = false
        
        priceTitleLabel.snp.makeConstraints({ (make) in
            make.left.equalToSuperview().offset(11)
            make.width.equalTo(50)
        })
        
        amountTitleLabel.snp.makeConstraints({ (make) in
            make.right.equalToSuperview()
            make.width.equalTo(50)
        })
        
        tableView.snp.makeConstraints({ (make) in
            make.edges.equalToSuperview()
        })
    }
    
//    override func resetTheme() {
//        tableView.backgroundColor = .container
//        tableView.reloadData()
//    }
}

extension PriceTableView: UITableViewDelegate {
    
}

extension PriceTableView: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 11
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: priceCellId) as! TradePriceTableViewCell
        
        cell.amountLabel.textColor = .textColor
        
        if displayMode == "split" {
            
            if indexPath.item < 5 {
                cell.priceLabel.textColor = .redColor
                let number = 4
                if asks.indices.contains(number-indexPath.row) {
                    cell.priceLabel.text = asks[number-indexPath.row].first?.forceDecimalDigits(market.priceDec ?? 0)
                    cell.amountLabel.text = asks[number-indexPath.row][1].forceDecimalDigits(market.volDec ?? 0)
                    cell.button.price = asks[number-indexPath.row].first
                    
                    cell.priceShadowView.backgroundColor = UIColor.redColor.withAlphaComponent(0.16)
                    
                    cell.priceShadowView.snp.updateConstraints({ (make) in
                        make.width.equalTo( (asks[number-indexPath.row].first!.toDouble() * asks[number-indexPath.row].last!.toDouble())/highestVolume * Double(screenWidth * 0.35))
                    })
                } else {
                    cell.priceLabel.text = "- -"
                    cell.amountLabel.text = "- -"
                    cell.button.price = ""
                    cell.priceShadowView.backgroundColor = .clear
                    
                    cell.priceShadowView.snp.updateConstraints({ (make) in
                        make.width.equalTo(0)
                    })
                }
            }
            
            if indexPath.item == 5 {
                cell.priceLabel.text = ""
                cell.amountLabel.text = ""
                cell.button.price = ""
                cell.priceShadowView.backgroundColor = .clear
                
                cell.priceShadowView.snp.updateConstraints({ (make) in
                    make.width.equalTo(0)
                })
            }
            
            if indexPath.item > 5 {
                cell.priceLabel.textColor = .greenColor
                if bids.indices.contains(indexPath.row-6) {
                    cell.priceLabel.text = bids[indexPath.row-6].first?.forceDecimalDigits(market.priceDec ?? 0)
                    cell.amountLabel.text = bids[indexPath.row-6][1].forceDecimalDigits(market.volDec ?? 0)
                    cell.button.price = bids[indexPath.row-6].first
                    
                    cell.priceShadowView.backgroundColor = UIColor.greenColor.withAlphaComponent(0.16)
                    
                    cell.priceShadowView.snp.updateConstraints({ (make) in
                        make.width.equalTo( (bids[indexPath.row-6].first!.toDouble() * bids[indexPath.row-6].last!.toDouble())/highestVolume * Double(screenWidth * 0.35))
                    })
                } else {
                    cell.priceLabel.text = "- -"
                    cell.amountLabel.text = "- -"
                    cell.button.price = ""
                    cell.priceShadowView.backgroundColor = .clear
                    
                    cell.priceShadowView.snp.updateConstraints({ (make) in
                        make.width.equalTo(0)
                    })
                }
            }
        }
        
        if displayMode == "buy" {
            cell.priceLabel.textColor = .greenColor
            if bids.indices.contains(indexPath.row) {
                cell.priceLabel.text = bids[indexPath.row].first?.forceDecimalDigits(market.priceDec ?? 0)
                cell.amountLabel.text = bids[indexPath.row][1].forceDecimalDigits(market.volDec ?? 0)
                cell.button.price = bids[indexPath.row].first
                
                cell.priceShadowView.backgroundColor = UIColor.greenColor.withAlphaComponent(0.16)
                
                cell.priceShadowView.snp.updateConstraints({ (make) in
                    make.width.equalTo( (bids[indexPath.row].first!.toDouble() * bids[indexPath.row].last!.toDouble())/highestVolume * Double(screenWidth * 0.35))
                })
            } else {
                cell.priceLabel.text = "- -"
                cell.amountLabel.text = "- -"
                cell.button.price = ""
                cell.priceShadowView.backgroundColor = .clear
                
                cell.priceShadowView.snp.updateConstraints({ (make) in
                    make.width.equalTo(0)
                })
            }
            
            if indexPath.item == 10 {
                cell.priceLabel.text = ""
                cell.amountLabel.text = ""
                cell.button.price = ""
                cell.priceShadowView.backgroundColor = .clear
                
                cell.priceShadowView.snp.updateConstraints({ (make) in
                    make.width.equalTo(0)
                })
            }
        }
        
        if displayMode == "sell" {
            cell.priceLabel.textColor = .redColor
            let number = 9
            if asks.indices.contains(number-indexPath.row) {
                cell.priceLabel.text = asks[number-indexPath.row].first?.forceDecimalDigits(market.priceDec ?? 0)
                cell.amountLabel.text = asks[number-indexPath.row][1].forceDecimalDigits(market.volDec ?? 0)
                cell.button.price = asks[number-indexPath.row].first
                
                cell.priceShadowView.backgroundColor = UIColor.redColor.withAlphaComponent(0.16)
                
                cell.priceShadowView.snp.updateConstraints({ (make) in
                    make.width.equalTo( (asks[number-indexPath.row].first!.toDouble() * asks[number-indexPath.row].last!.toDouble())/highestVolume * Double(screenWidth * 0.35))
                })
            } else {
                cell.priceLabel.text = "- -"
                cell.amountLabel.text = "- -"
                cell.button.price = ""
                cell.priceShadowView.backgroundColor = .clear
                
                cell.priceShadowView.snp.updateConstraints({ (make) in
                    make.width.equalTo(0)
                })
            }
            
            if indexPath.item == 10 {
                cell.priceLabel.text = ""
                cell.amountLabel.text = ""
                cell.button.price = ""
                cell.priceShadowView.backgroundColor = .clear
                
                cell.priceShadowView.snp.updateConstraints({ (make) in
                    make.width.equalTo(0)
                })
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 22
    }
}

