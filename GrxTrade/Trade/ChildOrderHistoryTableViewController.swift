//
//  OrderHistoryTableViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 16/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import UIKit

private let childOrderViewCellId = "childOrderViewCellId"
private let loadingCell = "loadingCell"

class ChildOrderHistoryTableViewController: BaseTableViewController {

    var market: Market?
    var category: Int?
    var orderId: String?
    var order: Order?
    private var childOrders: [ChildOrder] = []
    
    var isFetching = false
    var isLast = false
    var pagination: Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        adjustUI()
        tableView.register(ChildOrderTableViewCell.self, forCellReuseIdentifier: childOrderViewCellId)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: loadingCell)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
//        getOrderHistoryData(page: pagination)
        getOrderHistory()
    }
}

// MARK: User Interface
extension ChildOrderHistoryTableViewController {
    
    func adjustUI() {
        
        title = orderId
        
        tableView.backgroundColor = .background
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView()
    }
}

// MARK: Table
extension ChildOrderHistoryTableViewController {

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return childOrders.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: childOrderViewCellId, for: indexPath) as! ChildOrderTableViewCell
        
        cell.configure(childOrders[indexPath.row], order: order!, market: market!)
//        cell.button.setTitle(orders[indexPath.row].state?.capitalized.localized().uppercased(), for: .normal)
        cell.button.setTitleColor(.textColor, for: .normal)
        cell.button.backgroundColor = .clear
        cell.button.layer.borderWidth = 0
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = childOrders.count - 1
//        if indexPath.row == lastElement - 5 {
//            getOrderHistoryData(page: pagination)
//
//        }
    }
    
    // Estimated height for row at is needed to avoid jumping cells
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 92
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let vc = OrderDetailsTableViewController()
        vc.order = order!
        vc.market = market!
        self.show(vc, sender: nil)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 92
    }
}

// MARK: Networking
extension ChildOrderHistoryTableViewController {
 
    func getOrderHistory() {
        
        var params: [String: String] = [:]
        params["category"] = "\(category!)"
        params["orderId"] = orderId
        
        print(params)
        
        let networking = Networking()
        networking.getChildOrderList(params: params, controller: self) { childOrderData in
            
            print("child order data:")
            print(childOrderData.record!.first?.id)
            
            self.childOrders = childOrderData.record!
            self.tableView.reloadData()
        }
    }
}
