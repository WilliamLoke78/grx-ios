//
//  OrderDetailsTableViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 16/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import UIKit

private let orderDetailsCellId = "orderDetailsCellId"

class OrderDetailsTableViewController: BaseTableViewController {

    let markets: [Market] = BaseSingleton.shared.markets!
    
    var order = Order()
    var market = Market()
    
    private var cellModelArray: [CellModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        adjustUI()
        setupData(order)
        
        tableView.register(AccountSimpleTableViewCell.self, forCellReuseIdentifier: orderDetailsCellId)
    }
}

// MARK: User Interface
extension OrderDetailsTableViewController {
    
    func adjustUI() {
        title = "Order Details".localized()
        tableView.backgroundColor = .background
        tableView.tableFooterView = UIView()
        tableView.separatorColor = .background
    }
}

// MARK: Table
extension OrderDetailsTableViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellModelArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: orderDetailsCellId, for: indexPath) as! AccountSimpleTableViewCell
        
        let model = cellModelArray[indexPath.row]
        cell.label.text = model.title
        cell.subLabel.text = model.subTitle
        cell.arrowImageView.isHidden = true
        
        cell.subLabel.snp.remakeConstraints({ (make) in
            make.right.equalToSuperview().offset(-21)
            make.centerY.equalToSuperview()
        })
        
        if model.tag == "buysell" {
            if cellModelArray[0].subTitle == "Buy" {
                cell.subLabel.textColor = .greenColor
            } else {
                cell.subLabel.textColor = .redColor
            }
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56
    }
}

// MARK: Networking
extension OrderDetailsTableViewController {
    
    func setupData(_ order: Order) {
        
        cellModelArray.removeAll()
        
//        var market = Market()
        if let model = markets.first(where: {$0.symbol == order.symbol}) {
            market = model
        }

        var filled: Double?
        if let executedVolume = Double(order.dealVolume!), let originVolume = Double(order.volume!) {
            filled = executedVolume/originVolume*100
        }

        var price: String?
        var averagePrice: Double?
        if let dealAmount = order.dealAmount?.toDouble(), let dealVolume = order.dealVolume?.toDouble() {
            averagePrice = dealAmount/dealVolume
        }
        
        if averagePrice!.isNaN {
            price = "\(order.price!)"
        } else {
            price = "\(averagePrice ?? 0.0)"
        }
        
        var status: String?
        switch order.status {
        case 0:
            status = "Pending".localized()
        case 1:
            status = "Done".localized()
        default:
            status = "Canceled".localized()
        }
        
//        var value: Double?
//        value = order.price! * order.volume!
        
        print("deal volume: \(order.dealVolume)")
        print("deal amount: \(order.dealAmount)")

        let model0 = CellModel(icon: "", title: "Order Side".localized(), subTitle: order.category == 1 ? "Buy".localized() : "Sell".localized(), accIcon: "", tag: "buysell")
        let model1 = CellModel(icon: "", title: "Order Type".localized(), subTitle: order.orderType == 1 ? "Limit".localized() : "Market".localized(), accIcon: "", tag: "")
        let model2 = CellModel(icon: "", title: "Order Pair".localized(), subTitle: "\(market.coin!)/\(market.marketCoin!)", accIcon: "", tag: "")
        let model3 = CellModel(icon: "", title: "Status".localized(), subTitle: status!, accIcon: "", tag: "")
        let model4 = CellModel(icon: "", title: "Filled".localized(), subTitle: "\(filled!)".forceDecimalDigits(2) + "%", accIcon: "", tag: "")
        let model5 = CellModel(icon: "", title: "Time".localized(), subTitle: order.created!.serverToLocal(), accIcon: "", tag: "")
        let model6 = CellModel(icon: "", title: "Average Price".localized(), subTitle: price!.forceDecimalDigits(8) + " \(market.marketCoin!.uppercased())", accIcon: "", tag: "")
        let model7 = CellModel(icon: "", title: "Volume".localized(), subTitle: order.volume!.forceDecimalDigits(market.volDec ?? 0) + " \(market.coin!.uppercased())", accIcon: "", tag: "")
        let model8 = CellModel(icon: "", title: "Amount".localized(), subTitle: "\(order.dealAmount ?? "0")".forceDecimalDigits(8) + " \(market.marketCoin!.uppercased())", accIcon: "", tag: "")

        cellModelArray = [model0, model1, model2, model3, model4, model5, model6, model7, model8]
    }
}
