////
////  KLineViewController.swift
////  GrxTrade
////
////  Created by Leon Mah Kean Loon on 31/01/2020.
////  Copyright © 2020 Leon Mah. All rights reserved.
////
//
//import UIKit
//import CHKLineChartKit
//import SwiftyJSON
//
//class KLineViewController: UIViewController {
//
//    var priceIncrement: Double? = 0.00
//    
//    /// 数据源
//    var klineDatas = [KlineChartData]()
//    
//    static let Hide: String = ""
//    
//    /// 已选主图指标
//    var selectedMasterIndex: Int = 0
//    
//    /// 主图指标
//    let masterIndex: [String] = [
//        CHSeriesKey.ma, CHSeriesKey.ema, CHSeriesKey.sar, CHSeriesKey.boll, CHSeriesKey.sam, Hide
//    ]
//
//    /// 图表
//    lazy var chartView: CHKLineChartView = {
//        let chartView = CHKLineChartView(frame: CGRect.zero)
//        chartView.style = .base       //默认样式
//        chartView.delegate = self
//        return chartView
//    }()
//    
//    /// 顶部数据
//    lazy var topView: TickerTopView = {
//        let view = TickerTopView(frame: CGRect.zero)
//        return view
//    }()
//    
//    lazy var loadingView: UIActivityIndicatorView = {
//        let v = UIActivityIndicatorView(style: .whiteLarge)
//        return v
//    }()
//    
//    var chartXAxisPrevDay: String = ""
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        self.view.addSubview(self.chartView)
//        self.view.addSubview(self.topView)
//        self.view.addSubview(self.loadingView)
//        
//        pumpKLine()
//        setupUI()
//    }
//    
//    override func viewDidLayoutSubviews() {
//        self.chartView.frame = self.view.bounds
//    }
//    
//    func setupUI() {
//        
//        self.topView.snp.makeConstraints { (make) in
//            if #available(iOS 11.0, *) {
//                make.top.equalTo(self.view.safeAreaLayoutGuide.snp.top).offset(4)
//            } else {
//                // Fallback on earlier versions
//            }
//            make.bottom.equalTo(self.chartView.snp.top).offset(-4)
//            make.left.right.equalToSuperview().inset(8)
//            make.height.equalTo(60)
//        }
//        
//        self.chartView.snp.makeConstraints { (make) in
////            make.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottom)
//            make.left.right.equalToSuperview()
//        }
//        
//        self.loadingView.snp.makeConstraints { (make) in
//            make.center.equalTo(self.chartView)
//        }
//    }
//    
//    func pumpKLine() {
//            
//        self.loadingView.startAnimating()
//        self.loadingView.isHidden = false
//        
//        var kline: [String] = []
//        
//        priceIncrement! += Double.random(in: 0 ..< 40)
//        
//        var highPrice: Double? = 15.00 + priceIncrement!
//        var lowPrice: Double? = 1.00 + priceIncrement!
//        var openPrice: Double? = 12.00 + priceIncrement!
//        var closePrice: Double? = 1.00 + priceIncrement!
//        var vol: Double? = 5000.0
//        
//        kline = ["\(NSDate().timeIntervalSince1970)", "\(openPrice ?? 0.0)", "\(closePrice ?? 0.0)", "\(highPrice ?? 0.0)", "\(closePrice ?? 0.0)", "\(vol ?? 0.0)"]
//        
//        klineDatas.append(KlineChartData.init(json: kline))
//            
//        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
//            print("pump pimp")
//            self.pumpKLine()
//            self.chartView.reloadData()
//            
//            //显示最后一条数据
//            self.topView.update(data: self.klineDatas.last!)
//            
//            self.loadingView.stopAnimating()
//            self.loadingView.isHidden = true
//        }
//    }
//}
//
//// MARK: - 实现K线图表的委托方法
//extension KLineViewController: CHKLineChartDelegate {
//
//    /// 图表显示数据总数
//    func numberOfPointsInKLineChart(chart: CHKLineChartView) -> Int {
//        return self.klineDatas.count
//    }
//    
//    /// 提供图表数据源
//    func kLineChart(chart: CHKLineChartView, valueForPointAtIndex index: Int) -> CHChartItem {
//        let data = self.klineDatas[index]
//        let item = CHChartItem()
//        item.time = data.time
//        item.openPrice = CGFloat(data.openPrice)
//        item.highPrice = CGFloat(data.highPrice)
//        item.lowPrice = CGFloat(data.lowPrice)
//        item.closePrice = CGFloat(data.closePrice)
//        item.vol = CGFloat(data.vol)
//        return item
//    }
//    
//    /// 自定义Y轴坐标值显示内容
//    func kLineChart(chart: CHKLineChartView, labelOnYAxisForValue value: CGFloat, atIndex index: Int, section: CHSection) -> String {
//        var strValue = ""
//        if section.key == "volume" {
//            if value / 1000 > 1 {
//                strValue = (value / 1000).ch_toString(maxF: section.decimal) + "K"
//            } else {
//                strValue = value.ch_toString(maxF: section.decimal)
//            }
//        } else {
//            strValue = value.ch_toString(maxF: section.decimal)
//        }
//        
//        return strValue
//    }
//    
//    /// 自定义X轴坐标值显示内容
//    func kLineChart(chart: CHKLineChartView, labelOnXAxisForIndex index: Int) -> String {
//        let data = self.klineDatas[index]
//        let timestamp = data.time
//        let dayText = Date.ch_getTimeByStamp(timestamp, format: "MM-dd")
//        let timeText = Date.ch_getTimeByStamp(timestamp, format: "HH:mm")
//        var text = ""
//        //跨日，显示日期
//        if dayText != self.chartXAxisPrevDay && index > 0 {
//            text = dayText
//        } else {
//            text = timeText
//        }
//        self.chartXAxisPrevDay = dayText
//        return text
//    }
//    
//    
//    /// 调整每个分区的小数位保留数
//    ///
//    /// - parameter chart:
//    /// - parameter section:
//    ///
//    /// - returns:
//    func kLineChart(chart: CHKLineChartView, decimalAt section: Int) -> Int {
//        if section == 0 {
//            return 4
//        } else {
//            return 2
//        }
//    }
//    
//    func widthForYAxisLabelInKLineChart(in chart: CHKLineChartView) -> CGFloat {
//        return 60
//    }
//    
//    func kLineChart(chart: CHKLineChartView, titleForHeaderInSection section: CHSection, index: Int, item: CHChartItem) -> NSAttributedString? {
//        var start = 0
//        let titleString = NSMutableAttributedString()
//        var key = ""
//        switch section.index {
//        case 0:
//            key = self.masterIndex[self.selectedMasterIndex]
//        default:
//            key = section.series[section.selectedIndex].key
//        }
//        
//        //获取该线段的标题值及颜色，可以继续自定义
//        guard let attributes = section.getTitleAttributesByIndex(index, seriesKey: key) else {
//            return nil
//        }
//        
//        //合并为完整字符串
//        for (title, color) in attributes {
//            titleString.append(NSAttributedString(string: title))
//            let range = NSMakeRange(start, title.ch_length)
//            let colorAttribute = [NSAttributedString.Key.foregroundColor: color]
//            titleString.addAttributes(colorAttribute, range: range)
//            start += title.ch_length
//        }
//        
//        return titleString
//    }
//    
//    func kLineChart(chart: CHKLineChartView, didSelectAt index: Int, item: CHChartItem) {
//        let data = self.klineDatas[index]
//        self.topView.update(data: data)
//    }
//    
//    
//}
