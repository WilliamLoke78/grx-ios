//
//  KLineChartData.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 31/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import UIKit
import SwiftyJSON

class KlineChartData: NSObject, Codable {

    var time: Int = 0
    var lowPrice: Double = 0
    var highPrice: Double = 0
    var openPrice: Double = 0
    var closePrice: Double = 0
    var vol: Double = 0
    var symbol: String = ""
    var platfom: String = ""
    var rise: Double = 0
    var timeType: String = ""
    var usd: String = ""
    //振幅
    var amplitude: Double = 0
    var amplitudeRatio: Double = 0
    
    convenience init(json: [String]) {
        self.init()
        self.time = Int(json[0].dropLast(3)) ?? 0
        self.highPrice = json[3].toDouble()
        self.lowPrice = json[4].toDouble()
        self.openPrice = json[1].toDouble()
        self.closePrice = json[2].toDouble() 
        self.vol = json[5].toDouble()
        self.usd = json[8]
        
        //振幅
        if self.openPrice > 0 {
            self.amplitude = self.closePrice - self.openPrice
            self.amplitudeRatio = self.amplitude / self.openPrice * 100
        }
        
    }
    
}
