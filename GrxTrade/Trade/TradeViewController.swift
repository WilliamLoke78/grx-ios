//
//  TradeViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 01/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit
import Presentr
import SideMenu
import SwiftKeychainWrapper
import SocketIO
import SwiftyJSON

private let headerCellId = "headerCellId"
private let priceCellId = "priceCellId"
private let orderCellId = "orderCellId"

class TradeViewController: BaseTableViewController, UITextFieldDelegate {

    var isSplitMode = true
    var isBuyMode = false
    var isSellMode = false
    
    var market: Market?
    var markets: [Market] = []
    var wallets: [Wallet] = []
    var orders: [Order] = []
    var side: String = "buy"
    var orderType: String = "limit"
    
    var secondCount: Int = 0
    var waitingDuration: Double = 1.0
    var bids = [[String]]()
    var asks = [[String]]()
    
    var mainBids = [[String]]()
    var mainAsks = [[String]]()
    
    var isBidIn: Bool? = true
    var isAskIn: Bool? = true
    
    var isSplit = true
    var isBuyOnly = false
    var isSellOnly = false
    
    var depthInt: Int = 1
    var depthString: String = "D1"
    
    var manager: SocketManager!
    var socket: SocketIOClient!
    
    var socketSingleton = Socket.shared
    
    lazy var drawerItem = UIBarButtonItem(image: UIImage(named: "drawer_open"), style: .plain, target: self, action: #selector(tapMenu))
    
    fileprivate lazy var tableHeader: UIView = {
        let th = UIView.init(frame: CGRect.init(x: 0, y: 0, width: screenWidth, height: 425))
        return th
    }()
    
    fileprivate lazy var topBarView: TradeTopBarView = {
        let view = TradeTopBarView()
        view.buyTab.addTarget(self, action: #selector(tapBuy(_:)), for: .touchUpInside)
        view.sellTab.addTarget(self, action: #selector(tapSell(_:)), for: .touchUpInside)
        view.orderTab.addTarget(self, action: #selector(tapOrder(_:)), for: .touchUpInside)
        return view
    }()
    
    fileprivate lazy var tradeSectionView: TradeSectionView = {
        let view = TradeSectionView()
        view.priceTextField.tag = 0
        view.priceTextField.delegate = self
        view.amountTextField.tag = 1
        view.amountTextField.delegate = self
        view.buySellButton.addTarget(self, action: #selector(tapCreateOrder(_:)), for: .touchUpInside)
        view.modifyPriceLayoutButton.addTarget(self, action: #selector(tapSplit(_:)), for: .touchUpInside)
        view.depthButton.addTarget(self, action: #selector(tapDepth(_:)), for: .touchUpInside)
        return view
    }()
    
    fileprivate lazy var secondTableHeaderView: UIView = {
        let view = UIView()
        view.backgroundColor = .background
        return view
    }()
    
    fileprivate lazy var openOrderLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: bold, size: 16)
        label.textColor = .grayColor
        label.text = "Open Order".localized()
        return label
    }()
    
    fileprivate lazy var emptyImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: darkTheme ? "record_empty_dark" : "record_empty_light")
        return iv
    }()
    
    fileprivate lazy var emptyLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: regular, size: 14)
        label.textColor = .grayColor
        label.text = "No record".localized()
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addSubviews()
        adjustUI()
        addConstraints()
        addEvents()
        
        tableView.register(OrderTableViewCell.self, forCellReuseIdentifier: orderCellId)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "emptyTableCellId")
        
        hideKeyboardWhenTappedAround()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        secondCount = 0
        waitingDuration = 1.0
        loopTimer()
        
        getMarket()
        
        if BaseSingleton.shared.token != "" {
            getWalletsData()
            getOpenOrderData()
        }
        
        if side == "buy" {
            switchToBuy()
        } else if side == "sell" {
            switchToSell()
        }
        
        tradeSectionView.priceTextField.text = market?.close
        
        getDepthData()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        disconnectSocket()
        waitingDuration = 100.0
    }
    
    func loopTimer() {
        DispatchQueue.main.asyncAfter(deadline: .now() + waitingDuration) {
            self.secondCount += 1000
            
            if self.waitingDuration == 1.0 {
                self.loopTimer()
            }
        }
        
        sortBidAsk()
    }
    
    func sortBidAsk() {
        if self.secondCount >= 1000 {
        
            if !self.bids.isEmpty {
                self.mainBids = self.bids.sorted(by: { $0[0].toDouble() > $1[0].toDouble() })
                self.bids.removeAll()
                self.isBidIn = true
                
                let bidsSlice = mainBids.prefix(300)
                self.mainBids = Array(bidsSlice)
            }
            
            if !self.asks.isEmpty {
                self.mainAsks = self.asks.sorted(by: { $0[0].toDouble() < $1[0].toDouble() })
                self.asks.removeAll()
                self.isAskIn = true
                
                let asksSlice = mainAsks.prefix(300)
                self.mainAsks = Array(asksSlice)
            }

            self.populatePriceTable()
            self.secondCount = 0
        }
    }
    
    func populatePriceTable() {
        
//        getWalletsData()
        
//        tradeSectionView.priceLabel.text = "B:\(mainBids.count), A:\(mainAsks.count)"
        tradeSectionView.priceLabel.text = market?.close != "" ? (market?.close!.forceDecimalDigits(market?.priceDec ?? 2))! : "--"
        tradeSectionView.priceLabel.textColor = ((market?.change!.contains("-"))!) ? .redColor : .greenColor

        tradeSectionView.minMaxPriceLabel.text = (market?.priceMin?.forceDecimalDigits(market?.priceDec ?? 2))! + " ~ " + (market?.priceMax?.forceDecimalDigits(market?.priceDec ?? 2))! + " \(market?.marketCoin ?? "")"
        
        // Do not pre-populate price text field
//        tradeSectionView.priceTextField.text = market?.close
        
        let currencies = BaseSingleton.shared.currencies
        let selectedCurrency = currencies?.first(where: {$0.abbreviation == CurrencyManager.userCurrency()})
        let convertedPrice = (market?.usd?.toDouble() ?? 0) * (selectedCurrency?.rate ?? 0.0)
        let convertedPriceText = (convertedPrice == 0.0) ? "" : "\(convertedPrice)".forceDecimalDigits(2)
        tradeSectionView.convertedPriceLabel.text = market?.close != "" ? "≈ \(selectedCurrency?.symbol ?? "") \(convertedPriceText)" : ""
        
        tradeSectionView.amountTextFieldLabel.text = market?.coin
        
        if BaseSingleton.shared.token == "" {
            tradeSectionView.availableAmountLabel.text = " \(market?.marketCoin ?? "")"
        } else {
            if let wallet = wallets.first(where: {$0.coinName == market?.marketCoin}) {
                tradeSectionView.availableAmountLabel.text = "\(wallet.available ?? 0)".forceDecimalDigits(market?.priceDec ?? 0) + " \(market?.marketCoin ?? "")"
            }
        }
        
        tradeSectionView.buySellButton.setTitle("\(side.capitalized.localized()) \(market?.coin ?? "")", for: .normal)
        tradeSectionView.priceTableView.market = market!
        
        if market?.tradeStatus == 0 {
            disableMarketTrade()
        }
        
        var allVolume: [Double] = []
        
        for (index, elem) in mainBids.enumerated() {
            if index < 10 {
                allVolume.append(elem.first!.toDouble() * elem.last!.toDouble())
            }
        }
        for (index, elem) in mainAsks.enumerated() {
            if index < 10 {
                allVolume.append(elem.first!.toDouble() * elem.last!.toDouble())
            }
        }
        
        let sortedBidVolume = allVolume.sorted {
            $0 > $1
        }
        
        tradeSectionView.priceTableView.highestVolume = sortedBidVolume.first ?? 0
        
        if isSplit == true {
            tradeSectionView.priceTableView.displayMode = "split"
            tradeSectionView.priceTableView.asks = mainAsks
            tradeSectionView.priceTableView.bids = mainBids
            
            tradeSectionView.priceTableView.tableView.reloadData()
        }
        
        if isBuyOnly == true {
            tradeSectionView.priceTableView.displayMode = "buy"
            tradeSectionView.priceTableView.asks = []
            tradeSectionView.priceTableView.bids = mainBids
            
            tradeSectionView.priceTableView.tableView.reloadData()
        }
        
        if isSellOnly == true {
            tradeSectionView.priceTableView.displayMode = "sell"
            tradeSectionView.priceTableView.asks = mainAsks
            tradeSectionView.priceTableView.bids = []
            
            tradeSectionView.priceTableView.tableView.reloadData()
        }
    }
}

// MARK: Table
extension TradeViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if orders.count > 0 {
            return orders.count
        }
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if orders.count > 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: orderCellId, for: indexPath) as! OrderTableViewCell

            let tap = OrderTap(target: self, action: #selector(tapOrderDetail(_:)))
            tap.order = orders[indexPath.row]
            cell.addGestureRecognizer(tap)
            cell.isUserInteractionEnabled = true
            
            cell.configure(orders[indexPath.row], market: market!)
            cell.button.addTarget(self, action: #selector(tapCancelOrder(_:)), for: .touchUpInside)
            cell.button.tag = Int(orders[indexPath.row].id!)!
            
            return cell
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "emptyTableCellId", for: indexPath)
            cell.selectionStyle = .none
            cell.backgroundColor = .background
            
            cell.addSubview(emptyImageView)
            cell.addSubview(emptyLabel)
            
            emptyImageView.snp.makeConstraints({ (make) in
                make.centerX.equalToSuperview()
                make.centerY.equalToSuperview()
            })
            
            emptyLabel.snp.makeConstraints({ (make) in
                make.top.equalTo(emptyImageView.snp.bottom).offset(3)
                make.centerX.equalToSuperview()
            })
            
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if orders.count > 0 {
            return 130
        }
        return 300
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var sectionHeader = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerCellId)
        
        sectionHeader = UITableViewHeaderFooterView.init(reuseIdentifier: headerCellId)
        sectionHeader?.contentView.addSubview(secondTableHeaderView)
        
        secondTableHeaderView.snp.makeConstraints({ (make) in
            make.height.equalToSuperview()
            make.width.equalToSuperview()
        })
        
        secondTableHeaderView.addSubview(openOrderLabel)
        
        openOrderLabel.snp.makeConstraints({ (make) in
            make.left.equalToSuperview().offset(13)
            make.centerY.equalToSuperview()
        })
        
        return sectionHeader
    }
    
//    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        let lastElement = orders.count - 1
//        if indexPath.row == lastElement - 5 {
//            getOpenOrder(page: pagination)
//        }
//    }
    
    // Estimated height for row at is needed to avoid jumping cells
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        if orders.count > 0 {
            let vc = OrderDetailsTableViewController()
            vc.market = market!
            vc.order = orders[indexPath.row]
            self.show(vc, sender: nil)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }

}

extension TradeViewController {

    func addSubviews() {
        view.addSubview(tableHeader)
        tableHeader.addSubview(topBarView)
        tableHeader.addSubview(tradeSectionView)
        tableView.tableHeaderView = tableHeader
        tableView.tableFooterView = UIView()
    }
    
    func adjustUI() {
        let item = UIBarButtonItem(image: UIImage(named: "drawer_open"), style: .plain, target: self, action: #selector(tapMenu))
        navigationItem.leftBarButtonItem = item
        
        let chartItem = UIBarButtonItem(image: UIImage(named: "chart"), style: .plain, target: self, action: #selector(tapChart(_:)))
        navigationItem.rightBarButtonItem = chartItem
        
        tableView.backgroundColor = .background
        tableView.separatorStyle = .none
    }
    
    func addConstraints() {
        
        topBarView.snp.makeConstraints({ (make) in
            make.top.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalTo(40)
        })
        
        tradeSectionView.snp.makeConstraints({ (make) in
            make.top.equalTo(topBarView.snp.bottom).offset(6)
            make.height.equalTo(360)
            make.left.right.equalToSuperview()
        })
    }
    
    func addEvents() {
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapType(_:)))
        tap.cancelsTouchesInView = true
        tap.numberOfTapsRequired = 1
        tradeSectionView.tradeTypeButton.addGestureRecognizer(tap)
        tradeSectionView.tradeTypeButton.isUserInteractionEnabled = true
        
        tradeSectionView.priceTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        tradeSectionView.amountTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        tradeSectionView.modifyPriceLayoutButton.addTarget(self, action: #selector(tapSplit(_:)), for: .touchUpInside)
    }
}

// MARK: Action
extension TradeViewController {
    
    @objc func tapMenu() {
       let menu = UISideMenuNavigationController(rootViewController: MenuViewController())
       SideMenuManager.default.menuLeftNavigationController = menu
       SideMenuManager.default.menuWidth = screenWidth * 2 / 3
       SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
       SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
       SideMenuManager.default.menuFadeStatusBar = false
       present(menu, animated: true, completion: nil)
    }
    
    @objc func tapChart(_ sender: UIBarButtonItem) {
        let vc = ChartCustomViewController()
        vc.market = market
        show(vc, sender: nil)
    }
    
    @objc func tapBuy(_ sender: UIButton) {
        switchToBuy()
    }
    
    @objc func tapSell(_ sender: UIButton) {
        switchToSell()
    }
    
    func switchToBuy() {
        side = "buy"
        switchBuySellUI()

        if let wallet = wallets.first(where: {$0.coinName == market?.marketCoin}) {
            tradeSectionView.availableAmountLabel.text = "\(wallet.available ?? 0.0)".forceDecimalDigits(market!.priceDec!) + " \(market?.marketCoin ?? "")"
        }
    }
    
    func switchToSell() {
        side = "sell"
        switchBuySellUI()

        if let wallet = wallets.first(where: {$0.coinName == market?.coin}) {
            tradeSectionView.availableAmountLabel.text = "\(wallet.available ?? 0.0)".forceDecimalDigits(market!.priceDec!) + " \(market?.coin ?? "")"
        }
    }
    
    func switchBuySellUI() {
        topBarView.buyTab.setTitleColor((side == "buy") ? .primary : .grayColor, for: .normal)
        topBarView.sellTab.setTitleColor((side == "buy") ? .grayColor : .primary, for: .normal)
        topBarView.buyBottomLine.backgroundColor = (side == "buy") ? .primary : .clear
        topBarView.sellBottomLine.backgroundColor = (side == "buy") ? .clear : .primary
        tradeSectionView.amountTextField.text = ""
        tradeSectionView.buySellButton.setTitle((side == "buy") ? "Buy".localized() + " \(market?.coin ?? "")" : "Sell".localized() + " \(market?.coin ?? "")", for: .normal)
        tradeSectionView.buySellButton.backgroundColor = (side == "buy") ? .greenColor : .redColor
        
        if market?.tradeStatus == 0 {
            disableMarketTrade()
        }
    }
    
    @objc func tapOrder(_ sender: UIButton) {
        if BaseSingleton.shared.token == "" {
            showSimpleToast("Please login to view history".localized())
            return
        }
        let vc = OrderHistoryTableViewController()
        vc.market = market
        self.show(vc, sender: nil)
    }
    
    @objc func tapCreateOrder(_ sender: UIButton) {
        postOrderPlacing()
    }
    
    @objc func tapType(_ sender: UITapGestureRecognizer) {
            
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.view.subviews.first?.subviews.first?.subviews.first?.backgroundColor = .container
        alert.view.subviews.first?.subviews.first?.subviews.last?.backgroundColor = .container
        alert.view.tintColor = .grayColor
        
        alert.addAction(UIAlertAction(title: "Limit".localized(), style: .default , handler:{ (UIAlertAction)in
            
            self.tradeSectionView.tradeTypeButton.text = "Limit".localized()
            self.tradeSectionView.stopTextField.snp.updateConstraints({ (make) in
                make.top.equalTo(self.tradeSectionView.tradeTypeButton.snp.bottom).offset(0)
                make.height.equalTo(0)
            })
            self.tradeSectionView.priceTextField.isUserInteractionEnabled = true
            self.tradeSectionView.priceTextField.backgroundColor = UIColor.textFieldContainer.withAlphaComponent(1)
            self.orderType = "limit"
        }))
        
//        alert.addAction(UIAlertAction(title: "Market".localized(), style: .default , handler:{ (UIAlertAction)in
//            
//            self.tradeSectionView.tradeTypeButton.text = "Market".localized()
//            self.tradeSectionView.stopTextField.snp.updateConstraints({ (make) in
//                make.top.equalTo(self.tradeSectionView.tradeTypeButton.snp.bottom).offset(0)
//                make.height.equalTo(0)
//            })
//            self.tradeSectionView.priceTextField.isUserInteractionEnabled = false
//            self.tradeSectionView.priceTextField.backgroundColor = UIColor.textFieldContainer.withAlphaComponent(0.3)
//            self.orderType = "market"
//            // Remove price from text field as per requested
////            self.tradeSectionView.priceTextField.text = self.market.last?.forceDecimalDigits(self.market.price_precision ?? 0)
//            self.tradeSectionView.priceTextField.text = ""
//        }))
        
        alert.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func tapPrice(_ sender: PriceButton) {
        if sender.price != "" && orderType == "limit" {
            tradeSectionView.priceTextField.text = sender.price
        }
    }
    
    @objc func tapDepth(_ sender: UIButton) {
    
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let depths = market?.mergeDepth!.components(separatedBy: ",")
        for depth in depths! {
            alert.addAction(UIAlertAction(title: "D\(depth)", style: .default , handler:{ (UIAlertAction) in
                self.tradeSectionView.depthButton.setTitle("D\(depth)", for: .normal)
                self.depthInt = depth.toInt()
                self.depthString = "D\(depth)"
                self.getDepthData()
                self.reEmit()
            }))
        }
//        for index in stride(from: 1, to: market!.priceDec! + 1, by: 1) {
//            alert.addAction(UIAlertAction(title: "D\(index)", style: .default , handler:{ (UIAlertAction) in
//                self.tradeSectionView.depthButton.setTitle("D\(index)", for: .normal)
//                self.depthInt = index
//                self.depthString = "D\(index)"
//                self.getDepthData()
//                self.reEmit()
//            }))
//        }
        alert.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func tapSplit(_ sender: UIButton) {
        
        if isSplit == true {
            isSplit = false
            isBuyOnly = true
            isSellOnly = false
            
            let image = UIImage(named: darkTheme ? "bid_mode_dark" : "bid_mode_light")
            tradeSectionView.modifyPriceLayoutButton.setImage(image, for: .normal)
            
        } else if isBuyOnly == true {
            isSplit = false
            isBuyOnly = false
            isSellOnly = true
            
            let image = UIImage(named: darkTheme ? "ask_mode_dark" : "ask_mode_light")
            tradeSectionView.modifyPriceLayoutButton.setImage(image, for: .normal)
            
        } else if isSellOnly == true {
            isSplit = true
            isBuyOnly = false
            isSellOnly = false
            
            let image = UIImage(named: darkTheme ? "split_mode_dark" : "split_mode_light")
            tradeSectionView.modifyPriceLayoutButton.setImage(image, for: .normal)
        }
        
        getDepthData()
    }
    
    @objc func tapOrderDetail(_ sender: OrderTap) {
        let vc = OrderDetailsTableViewController()
        vc.market = market!
        vc.order = sender.order
        show(vc, sender: nil)
    }
    
    @objc func tapCancelOrder(_ sender: UIButton) {
        let alert = UIAlertController(title: "Cancel Order".localized(), message: "Are you sure you want to cancel this order?".localized(), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "No".localized(), style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Yes".localized(), style: .default, handler: { action in
            self.cancelOrder(id: sender.tag)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        let price = tradeSectionView.priceTextField.text?.toDouble() ?? 0
        let amount = tradeSectionView.amountTextField.text?.toDouble() ?? 0
        
        let totalAmount = price * amount
        if totalAmount > 0 {
            tradeSectionView.totalAmountLabel.text = "\(totalAmount)".forceDecimalDigits(8)
        } else {
            tradeSectionView.totalAmountLabel.text = "--"
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var decimalPlace = Int()
        if textField.tag == 0 {
            decimalPlace = market!.priceDec!
        } else if textField.tag == 1 {
            decimalPlace = market!.volDec!
        }
        
        let dotString = "."
        if let text = textField.text {
           let isDeleteKey = string.isEmpty

           if !isDeleteKey {
               if text.contains(dotString) {
                   if text.components(separatedBy: dotString)[1].count == decimalPlace {
                       return false
                   }
               }
           }
        }
        return true
    }
    
    func disableMarketTrade() {
        tradeSectionView.buySellButton.backgroundColor = UIColor.grayColor.withAlphaComponent(0.5)
        tradeSectionView.buySellButton.isUserInteractionEnabled = false
    }
}

// MARK: Networking
extension TradeViewController {
    
    func getMarket() {
        
        if market == nil {
            market = BaseSingleton.shared.markets?.first
            if market == nil {
                let emptyMarket = Market.init(aJSON: [])
                market = emptyMarket
            }
        }
        
        depthString = "D\(market?.priceDec ?? 1)"
        depthInt = market!.priceDec!
        tradeSectionView.depthButton.setTitle(depthString, for: .normal)
        
        var pairTitle = String()
        if market?.coin == "" {
            pairTitle = ""
        } else {
            pairTitle = "\(market?.coin ?? "")/\(market?.marketCoin ?? "")"
        }
        
        let nameItem = UIBarButtonItem(title: pairTitle, style: .plain, target: self, action: nil)
        nameItem.setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: bold, size: 18)!], for: .normal)
        navigationItem.leftBarButtonItems = [drawerItem, nameItem]
        
        populatePriceTable()
    }
    
    func getDepthData() {
        
        print("get market depth data again")
        
        mainAsks.removeAll()
        mainBids.removeAll()
        
        let networking = Networking()
        var params: [String: String] = [:]
        params["pair"] = market?.symbol
        params["depthGroup"] = depthString
        params["filter"] = "010"
        
        networking.getMarketData(params: params, controller: self) { marketData in
            
            let asksSlice = marketData.depth!.sell?.prefix(300)
            let bidsSlice = marketData.depth!.buy?.prefix(300)
            
            self.mainAsks = Array(asksSlice ?? [])
            self.mainBids = Array(bidsSlice ?? [])
            
            self.populatePriceTable()
            
            self.socketSingleton.connectToSocket(token: "")
            self.connectToSocket()
        }
    }
    
    func getWalletsData() {
        let networking = Networking()
        networking.getUserAssets(params: [:], controller: self) { wallets in
            
            let marketCoin = (self.side == "buy") ? self.market?.marketCoin : self.market?.coin
            self.wallets = wallets
            if let wallet = wallets.first(where: {$0.coinName == marketCoin}) {
                let walletAvailable = "\(wallet.available ?? 0)"
                self.tradeSectionView.availableAmountLabel.text = "\(walletAvailable.forceDecimalDigits(self.market?.priceDec ?? 0)) \(marketCoin ?? "")"
            }
        }
    }
    
    func getOpenOrderData() {
        
        var params: [String: Any] = [:]
        params["current"] = 1
        params["size"] = 50
        params["status"] = 0
        params["pair"] = market?.symbol ?? ""
        
        let networking = Networking()
        networking.getOrderList(params: params, controller: self) { orderData in
            self.orders = orderData.record!
            self.tableView.reloadData()
        }
    }
    
    func postOrderPlacing() {
        
        guard (BaseSingleton.shared.token != "") else {
            showSimpleToast("Please login to perform trade".localized())
            return
        }
        
        guard (BaseSingleton.shared.user.gaStatus == 1 || BaseSingleton.shared.user.paymentPasswordStatus == 1) else {
            showSimpleToast("Please complete your security settings before trading".localized())
            return
        }
        
        guard (tradeSectionView.priceTextField.text != "") else {
            showSimpleToast("Please enter price to continue".localized())
            return
        }
        
        guard (tradeSectionView.amountTextField.text != "") else {
            showSimpleToast("Please enter volume to continue".localized())
            return
        }
        
        var priceValidation: Double = 0.0
        var isPriceExceeded: Bool = false
        
        if side == "buy" {
            priceValidation = 1.5
            if mainAsks.first?.first != nil {
                if "\(tradeSectionView.priceTextField.text ?? "0")".toDouble() > (priceValidation * (mainAsks.first?.first ?? "0").toDouble()) {
                    isPriceExceeded = true
                } else {
                   isPriceExceeded = false
                }
            } else {
                isPriceExceeded = false
            }
        } else {
            priceValidation = 0.5
            if mainBids.first?.first != nil {
                if "\(tradeSectionView.priceTextField.text ?? "0")".toDouble() < (priceValidation * (mainBids.first?.first ?? "0").toDouble()) {
                    isPriceExceeded = true
                } else {
                    isPriceExceeded = false
                }
            } else {
                isPriceExceeded = false
            }
        }
        
        var params: [String: Any] = [:]
        params["userId"] = BaseSingleton.shared.user.id
        params["pair"] = market?.symbol
        params["side"] = side == "buy" ? 1 : 2 // 1: buy, 2: sell
        params["price"] = tradeSectionView.priceTextField.text
        params["volume"] = tradeSectionView.amountTextField.text
        params["orderType"] = orderType == "limit" ? 1 : 2 // 1: limit, 2: market
        
        if isPriceExceeded == false {
            if let transactionPassword: String = KeychainWrapper.standard.string(forKey: "transactionPassword") {
                
                print("has txn pwd")
                print(transactionPassword)
                
                params["txnPwd"] = transactionPassword
                
                let networking = Networking()
                networking.postOrderPlacing(params: params, controller: self) { response in
                    
                    print("response: \(response)")
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                        showSimpleToast("Order has been placed".localized())
                        self.tradeSectionView.priceTextField.text = ""
                        self.tradeSectionView.amountTextField.text = ""
                    }
                }
                
                return
            }
        }
        
        let vc = TransactionPasswordViewController()
        vc.isPriceExceeded = isPriceExceeded
        vc.params = params
        let presenter = Presentr(presentationType: .custom(width: .full, height: .custom(size: 580), center: .bottomCenter))
        self.customPresentViewController(presenter, viewController: vc, animated: true, completion: nil)
    }
    
    func cancelOrder(id: Int) {
        
        var params: [String: Any] = [:]
        params["orderId"] = "\(id)"
        params["pair"] = market?.symbol ?? ""
        params["volume"] = "0"
        
        let networking = Networking()
        networking.cancelOrder(params: params, controller: self) { response in
            showSimpleToast("Order has been canceled".localized())
        }
    }
    
    func reEmit() {
        
        getDepthData()
        
        manager = socketSingleton.manager
        socket = manager.defaultSocket
            
        var params: [String: String] = [:]
        params["pair"] = self.market?.symbol ?? ""
        params["depthGroup"] = self.depthString
        
        print("depthGroup")
        print(params)
        
        self.socket.emit("setDepth", params)

    }
    
    func connectToSocket() {
            
        manager = socketSingleton.manager
        socket = manager.defaultSocket
        
        self.socket.on(clientEvent: .connect) {data, ack in
            print("message socket connected")
            
            var params: [String: String] = [:]
            params["pair"] = self.market?.symbol ?? ""
            params["depthGroup"] = self.depthString
            
            print("depthGroup")
            print(params)
            
            self.socket.on("marketList") { (data, eck) in
                print("market list socket data: \(data)")

                let result = JSON(data.first!)
                let market = Market.init(aJSON: result["data"])

                if self.market?.symbol == market.symbol {
                    self.market = market
                }
            }
            
            self.socket.emit("setDepth", params)
            
            self.socket.on("getDepth") { (data, eck) in
    
//                let asksSlice = self.mainAsks.prefix(300)
//                let bidsSlice = self.mainBids.prefix(300)
//
//                self.asks = Array(asksSlice)
//                self.bids = Array(bidsSlice)
                
                if self.isAskIn == true {
                    self.asks = self.mainAsks
                    self.isAskIn = false
                } else {
                    
                }
                
                if self.isBidIn == true {
                    self.bids = self.mainBids
                    self.isBidIn = false
                }
                
                let response = JSON(data.first!)
                let result = response["data"]
                let depthGroup = result["depthGroup"]
                let depth = Depth.init(aJSON: result["depth"])
                let bids = depth.buy ?? [[]]
                let asks = depth.sell ?? [[]]
                
                if depthGroup.stringValue != self.depthString { // Not belongs to same depth group
                    return
                }
                
                for elem in bids {
                    if let index = self.bids.firstIndex(where: {$0.first == elem.first}) { // if price exists in older list
                        self.bids[index] = elem
                        if self.bids[index].last == "0" {
                            self.bids.remove(at: index)
                        }
                    } else if elem.first != nil { // if price doesnt not exist, but not empty
                        if elem.last != "0" {
                            
                            print("append bid: \(elem.first ?? "")")
                            
                            self.bids.append(elem)
                        }
                    }
                }
//
                for elem in asks {
                    if let index = self.asks.firstIndex(where: {$0.first == elem.first}) {
                        self.asks[index] = elem // replace existing row
                        if self.asks[index].last == "0" {
                            self.asks.remove(at: index) // remove existing row
                        }
                    } else if elem.first != nil {
                        if elem.last != "0" {
                            
                            print("append ask: \(elem.first ?? "")")
                            
                            self.asks.append(elem) // add new row
                        }
                    }
                }
                
//                self.loopTimer()
                self.sortBidAsk()
            }
            
//            self.socket.emit("authorize", BaseSingleton.shared.token)
            self.socket.emit("authorize-test", BaseSingleton.shared.user.id ?? "")

            self.socket.on("orderList") { (data, eck) in
                print("show order history data: \(data)")
                
                self.getOpenOrderData()
            }
        }
    }
    
    func disconnectSocket() {
        print("socket disconnected")
        socket.disconnect()
    }
}

class OrderTap: UITapGestureRecognizer {
    var order = Order()
}
