//
//  TransactionPasswordViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 16/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import UIKit
import M13Checkbox
import SwiftKeychainWrapper

class TransactionPasswordViewController: BaseViewController {
    
    var isPriceExceeded: Bool?
    var params: [String: Any] = [:]
    
    lazy var priceExceededLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: regular, size: 12)
        label.textColor = .yellowColor
        label.text = "Your price has exceeded the current price by 50%".localized()
        label.isHidden = true
        return label
    }()
    
    lazy var passwordView: TextFieldView = {
        let view = TextFieldView()
        view.label.text = "Transaction Password".localized()
        view.textField.isSecureTextEntry = true
        view.lastButton.isHidden = false
        view.lastButton.addTarget(self, action: #selector(togglePassword(_:)), for: .touchUpInside)
        return view
    }()
    
    lazy var checkbox: M13Checkbox = {
        let checkbox = M13Checkbox()
        checkbox.checkedValue = "1"
        return checkbox
    }()
    
    lazy var checkboxLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: regular, size: 12)
        label.textColor = .grayColor
        label.text = "Remember my transaction password?".localized()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapRememberPassword(_:)))
        tap.cancelsTouchesInView = false
        label.addGestureRecognizer(tap)
        label.isUserInteractionEnabled = true
        return label
    }()
    
    lazy var button: PrimaryButton = {
        let button = PrimaryButton()
        button.setTitle("Place Order".localized(), for: .normal)
        button.addTarget(self, action: #selector(tapPlaceOrder(_:)), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        
        view.backgroundColor = .container
        
        view.addSubview(priceExceededLabel)
        priceExceededLabel.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(15)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.height.equalTo(15)
        })
        
        view.addSubview(passwordView)
        passwordView.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(35)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.height.equalTo(54)
        })
        
        view.addSubview(checkbox)
        checkbox.snp.makeConstraints({ (make) in
            make.top.equalTo(passwordView.snp.bottom).offset(10)
            make.left.equalTo(passwordView)
            make.height.equalTo(15)
            make.width.equalTo(15)
        })
        
        view.addSubview(checkboxLabel)
        checkboxLabel.snp.makeConstraints({ (make) in
            make.centerY.equalTo(checkbox)
            make.left.equalTo(checkbox.snp.right).offset(10)
        })
        
        view.addSubview(button)
        button.snp.makeConstraints({ (make) in
            make.top.equalTo(checkbox.snp.bottom).offset(10)
            make.left.right.equalTo(passwordView)
            make.height.equalTo(54)
        })
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
            self.passwordView.textField.becomeFirstResponder()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if isPriceExceeded == true {
            print("is exceeded")
            priceExceededLabel.isHidden = false
        } else {
            print("is not exceeded")
            priceExceededLabel.isHidden = true
        }
    }
}

// MARK: Action
extension TransactionPasswordViewController {
    
    @objc private func togglePassword(_ sender: UIButton) {
        passwordView.textField.isSecureTextEntry.toggle()
        if passwordView.textField.isSecureTextEntry == true {
            passwordView.lastButton.setImage(UIImage(named: "invisible"), for: .normal)
        } else {
            passwordView.lastButton.setImage(UIImage(named: "visible"), for: .normal)
        }
    }
    
    @objc func tapRememberPassword(_ sender: UITapGestureRecognizer) {
        
        print("tap remember")
        
        if checkbox.checkState == .checked {
            checkbox.checkState = .unchecked
            return
        }
        checkbox.checkState = .checked
    }
    
    @objc func tapPlaceOrder(_ sender: UIButton) {
        postOrderPlacing()
    }
    
    func postOrderPlacing() {
        
        guard (passwordView.textField.text != "") else {
            showSimpleToast("Transaction password cannot be empty".localized())
            return
        }
        
        let transactionPassword = passwordView.textField.text!.md5()
        
        if checkbox.value != nil {
            print("transaction value")
            let saveSuccessful: Bool = KeychainWrapper.standard.set(transactionPassword, forKey: "transactionPassword")
        }
        
//        "a906449d5769fa7361d7ecc6aa3f6d28"
        
        print(transactionPassword)
        
        params["txnPwd"] = transactionPassword
        
        let networking = Networking()
        networking.postOrderPlacing(params: params, controller: self) { response in
            
            print("response: \(response)")
            
            self.dismiss(animated: true, completion: nil)
        }
    }
}
