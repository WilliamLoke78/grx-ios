//
//  Color.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 01/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import Foundation
import UIKit

public var darkTheme = Storage.boolValue(key: Storage.Name.darkTheme)

extension UIColor {
 
    static var primary: UIColor {
        return Theme.light.primary!
    }
    
    static var secondary: UIColor {
        return Theme.light.secondary!
    }
    
    static var mxwGold: UIColor {
        return Theme.light.mxwGold!
    }
    
    static var grayColor: UIColor {
        return Theme.light.gray!
    }
    
    static var greenColor: UIColor {
        return Theme.light.green!
    }
    
    static var redColor: UIColor {
        return Theme.light.red!
    }
    
    static var yellowColor: UIColor {
        return Theme.light.yellow!
    }
    
    static var blackTextColor: UIColor {
        return Theme.light.black!
    }
    
    static var navBackgroundColor: UIColor {
        return Theme.light.theme!
    }
    
    // Dark/Light Theme
    static var backgroundTheme: UIColor {
        return darkTheme ? Theme.dark.theme! : Theme.light.theme!
    }
    
    static var container: UIColor {
        return darkTheme ? Theme.dark.container! : Theme.light.container!
    }
    
    static var background: UIColor {
        return darkTheme ? Theme.dark.background! : Theme.light.background!
    }
    
    static var containerAlternative: UIColor {
        return darkTheme ? Theme.dark.background! : Theme.light.background!
    }
    
    static var backgroundAlternative: UIColor {
        return darkTheme ? Theme.dark.container! : Theme.light.container!
    }
    
    static var textColor: UIColor {
        return darkTheme ? Theme.dark.white : Theme.light.black
    }
    
    static var textFieldContainer: UIColor {
        return darkTheme ? Theme.dark.background : Theme.light.text
    }
    
    static var textFieldBorder: UIColor {
        return darkTheme ? Theme.dark.textFieldBorder : Theme.light.textFieldBorder
    }
}

class Theme {
    
    static let current = darkTheme ? Theme.dark : Theme.light
    
    static let light: Theme = {
        
        let instance = Theme.init()
        instance.primary = "#385CAD".hex()
        instance.secondary = "#307BF2".hex()
        instance.gray = "#889DBF".hex()
        instance.green = "#1DCE8A".hex()
        instance.red = "#E24747".hex()
        instance.yellow = "#FFB205".hex()
        instance.black = "#333333".hex()
        instance.white = "#EFEFF8".hex()
        instance.text = "#F5F5F8".hex()
        instance.theme = "#385CAD".hex()
        instance.mxwGold = "#D4AF37".hex()
        
        instance.container = .white
        instance.background = "#EFEFF8".hex()
        instance.textFieldBorder = "#DDDDDD".hex()
        
        return instance
    }()
    
    static let dark: Theme = {
        
        let instance = Theme.init()
        instance.primary = "#385CAD".hex()
        instance.secondary = "#307BF2".hex()
        instance.gray = "#889DBF".hex()
        instance.green = "#1DCE8A".hex()
        instance.red = "#E24747".hex()
        instance.yellow = "#FFB205".hex()
        instance.black = "#333333".hex()
        instance.white = "#EFEFF8".hex()
        instance.text = "#F5F5F8".hex()
        instance.theme = "#282A2E".hex()
        
        instance.container = "#1A1F33".hex()
        instance.background = "#111522".hex()
        instance.textFieldBorder = .clear
        
        return instance
    }()
    
    private(set) var primary: UIColor!
    private(set) var secondary: UIColor!
    private(set) var gray: UIColor!
    private(set) var green: UIColor!
    private(set) var red: UIColor!
    private(set) var yellow: UIColor!
    private(set) var black: UIColor!
    private(set) var white: UIColor!
    private(set) var text: UIColor!
    private(set) var theme: UIColor!
    private(set) var mxwGold: UIColor!
    
    private(set) var container: UIColor!
    private(set) var background: UIColor!
    private(set) var textFieldBorder: UIColor!
    
    private init() {
        
    }
}

extension String {
    
    public func hex() -> UIColor {
        
        var hexColorString = self
        
        if hexColorString.contains("#") {
            hexColorString = hexColorString[1..<7]
        }
        
        // 存储转换后的数值
        var red:UInt32 = 0, green:UInt32 = 0, blue:UInt32 = 0
        
        // 分别转换进行转换
        Scanner(string: hexColorString[0..<2]).scanHexInt32(&red)
        
        Scanner(string: hexColorString[2..<4]).scanHexInt32(&green)
        
        Scanner(string: hexColorString[4..<6]).scanHexInt32(&blue)
        
        return UIColor(red: CGFloat(red)/255.0, green: CGFloat(green)/255.0, blue: CGFloat(blue)/255.0, alpha: 1.0)
    }
}

extension String {
    
    // MARK: - String使用下标截取字符串， 例: "示例字符串"[0..<2] 结果是 "示例"
    public subscript (r: Range<Int>) -> String {
        get {
            let startIndex = self.index(self.startIndex, offsetBy: r.lowerBound)
            let endIndex = self.index(self.startIndex, offsetBy: r.upperBound)
            
            return String(self[startIndex..<endIndex])
        }
    }
}

extension UIColor {
    
    /**
     16进制表示颜色
     
     - parameter hex:
     
     - returns:
     */
    public convenience init(hex: UInt, alpha: CGFloat = 1.0) {
        self.init(red: CGFloat((hex & 0xFF0000) >> 16) / 255.0,
                  green: CGFloat((hex & 0x00FF00) >> 8) / 255.0,
                  blue: CGFloat(hex & 0x0000FF) / 255.0,
                  alpha: alpha)
    }
    
    /// 用十六进制颜色创建UIColor
    ///
    /// - Parameter hexColor: 十六进制颜色 (0F0F0F)
    public convenience init(hexColor: String) {
        
        var hexColorString = hexColor
        
        if hexColorString.contains("#") {
            hexColorString = hexColorString[1..<7]
        }
        
        // 存储转换后的数值
        var red:UInt32 = 0, green:UInt32 = 0, blue:UInt32 = 0
        
        // 分别转换进行转换
        Scanner(string: hexColorString[0..<2]).scanHexInt32(&red)
        
        Scanner(string: hexColorString[2..<4]).scanHexInt32(&green)
        
        Scanner(string: hexColorString[4..<6]).scanHexInt32(&blue)
        
        self.init(red: CGFloat(red)/255.0, green: CGFloat(green)/255.0, blue: CGFloat(blue)/255.0, alpha: 1.0)
    }
}
