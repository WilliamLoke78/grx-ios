//
//  Protocols.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 01/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

typealias Properties = Dictionary<String, Any>
protocol Modelable {
    
    /// 转化成Dictionary<String, Any>格式，用于save操作
    ///
    /// - Returns: 需要保存Model的属性列表
    func properties() -> Properties
    
    /// 读取数据（读出来就是Properties），然后生成新的实例
    ///
    /// - Parameter properties: 属性
    /// - Returns: 返回实例
    static func model(properties: Properties) -> Modelable
}

protocol JSONParse {
    init(aJSON: JSON)
    init(fromDictionary dictionary: [String: Any])
    func toDictionary() -> [String: Any]
}

@objc protocol ResetTheme: class {
    @objc optional func resetTheme()
}

public protocol UI {
    
    /// 用于调整UI的接口
    func adjustUI()
    
    /// UI添加事件
    func addEvents()
    
    /// 用来添加子视图的接口
    func addSubviews()
    
    /// 用来给子视图添加约束的接口
    func addConstraints()
    
    /// 设置数据源
    func configure<T>(model: T)
}
