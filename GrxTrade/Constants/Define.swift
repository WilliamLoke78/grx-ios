//
//  Define.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 01/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

let screenWidth = UIScreen.main.bounds.size.width
let screenHeight = UIScreen.main.bounds.size.height

let regular = "OpenSans"
let medium = "OpenSans"
let bold = "OpenSans-Bold"

// Ayong's shit
let isLiuHaiScreen = false
let navTopHeight: CGFloat = isLiuHaiScreen ? 88 : 64
let tabBarHeight: CGFloat = isLiuHaiScreen ? 83 : 49
let safeBottom: CGFloat = isLiuHaiScreen ? 34 : 0

extension Storage.Name {
    static let darkTheme = Storage.Name.init(rawValue: "darkTheme")
    static let user = Storage.Name.init(rawValue: "user")
    static let token = Storage.Name.init(rawValue: "token")
}

extension Notification.Name {
    static let darkTheme = Notification.Name.init("darkTheme")
}

let isSafeAreaDevice = UIDevice.current.isSafeAreaDevice//(isIPhoneX||isIPhoneXs||isIPhoneXr||isIPhoneXsMax||isSimulatorX)
