/* 
  Localizable.strings
  GrxTrade

  Created by Leon Mah Kean Loon on 01/10/2019.
  Copyright © 2019 Leon Mah. All rights reserved.
*/

// Tab Bar
"Home" = "首頁";
"Markets" = "行情";
"Trades" = "交易";
"Wallets" = "資產";
"Account" = "我的";

// Common
"Session Timeout" = "會話超時";
"Your session has expired, please log in to continue." = "會話超時，請登錄以訪問更多內容。";
"You're already an existing user, please try to login." = "您已經是現有用戶，請嘗試登錄。";
"No record" = "無記錄";
"Yes" = "是";
"No" = "否";
"Submit" = "提交";
"Save" = "保存";
"Error" = "錯誤";
"New Version Available" = "新版本更新";
"Update" = "更新";
"Camera" = "相機";
"Delete" = "删除";
"Camera is needed to scan QR code" = "需要相機掃描QR碼";
"Camera is needed to take photos" = "需要相機拍照";

// Home
"Digital Asset" = "幣幣行情";
"My Balance" = "我的余額";
"Balance of Exchange Account" = "總賬戶資産折合";
"Refer a Friend" = "推薦好友";
"Gain additional benefit." = "獲取收益";
"Popular Trades" = "熱門幣幣交易";

// Notification
"Notification" = "通知";
"Are you sure you want to delete this notification?" = "确定删除通知?";

// Market
"Coin Name" = "幣種名稱";
"Coin Price" = "最新價";
"Change %" = "漲跌幅";
"24H Vol" = "24H 量";
"Search" = "搜索";

// Trade
"Buy" = "買";
"Sell" = "賣";
"Open Order" = "當前委托";
"Order History" = "曆史訂單";
"Price" = "價格";
"Limit" = "限制";
"Market" = "市場";
"Stop-Limit" = "限價";
"Market Price" = "市場價錢";
"Stop" = "停止";
"Cancel" = "取消";
"Canceled" = "取消";
"Volume" = "數量";
"Available" = "可用";
"Total" = "總額";
"Fee" = "收費";
"Cancel Order" = "取消訂單";
"Order ID" = "訂單編號";
"Sub-Order ID" = "子訂單編號";
"Done" = "完成";
"Your price has exceeded the current price by 50%" = "您的價格超出當前價格50%";
"Remember my transaction password?" = "記住交易密碼？";
"Place Order" = "下單";

// KLine
"KLine Open" = "開";
"KLine Close" = "收";
"KLine High" = "高";
"KLine Low" = "低";
"KLine Volume" = "量";
"KLine Turnover" = "成";

"Please login to perform trade" = "請登錄進行交易";
"Please login to view history" = "請登錄以查看曆史記錄";
"Are you sure you want to cancel this order?" = "確定取消訂單？";
"Please complete your security settings before trading" = "請在交易前完成您的安全設置";
"Please enter price to continue" = "請輸入價格繼續";
"Please enter volume to continue" = "請輸入金額繼續";
"Order has been placed" = "訂單成功創建";
"Order has been canceled" = "訂單取消成功";

"Order Details" = "訂單詳細信息";
"Order Side" = "訂單方";
"Order Type" = "訂單類型";
"Status" = "狀態";
"Order Pair" = "訂單對";
"Filled" = "填充";
"Time" = "時間";
"Average Price" = "平均價錢";
"Amount" = "合計";
"Value" = "價值";

// Wallet
"Deposit" = "存款";
"Withdraw" = "提幣";
"Transaction" = "交易";
"Transaction History" = "交易曆史";
"Locked" = "凍結";
"Transaction Details" = "交易明細";

"Choose coin" = "選擇貨幣";
"Deposit Address" = "存款地址";
"Copy Address" = "複制地址";
"Memo" = "備忘錄";
"Deposit address has been copied to clipboard" = "存款地址已複制到剪貼板";
"Please make sure the wallet address and memo address provided are accurate and be sure to verify the wallet address of the type of token or coin. Any funds deposited in the wrong token/coin address cannot be recovered." = "請確認錢包地址和備忘錄地址是否准確，並確認各種代幣或貨幣的錢包地址。 存入非對應代幣/貨幣地址的任何資金將無法收回。";

"Please make sure whether the wallet address and memo address are accurate and also confirm wallet address of various type of token or coin. Any funds deposited in non-corresponding token/coin address will not be recovered." = "請確認錢包地址和備忘錄地址是否准確，並確認各種代幣或貨幣的錢包地址。 存入非對應代幣/貨幣地址的任何資金將無法收回。";
"Once you have submitted your withdrawal request, we will send a confirmation email. Please clock on the confirmation link in your email." = "提交您的提幣申請後，我們會發送確認電郵。 請單擊電郵中的確認鏈接。";
"Withdraw Address" = "提幣地址";
"Receive Amount" = "收獲金額";

"Add New Beneficiary" = "添加新受益人";
"Withdrawal Address" = "提幣地址";
"Paste" = "粘貼";
"Beneficiary Name" = "受益人姓名";
"Memo (optional)" = "備忘錄（可選）";
"Beneficiary has been added" = "成功添加受益人";
"Delete Beneficiary" = "刪除受益人";
"Are you sure you want to delete this beneficiary?" = "您確定要刪除此受益人嗎？";

"Deposits" = "存款";
"Withdraws" = "提幣";
"All" = "全部";

"Transaction Type" = "交易類型";
"Action" = "操作";
"Account ID" = "帳戶編號";
"Before Amount" = "金額前";
"Change Amount" = "改換金額";
"After Amount" = "金額後";
"Remark" = "備注";

"Please login to access your wallets" = "請登錄以進接您的錢包";

// Account
"Login / Register" = "登錄/注冊";
"Welcome to GRXTrade" = "歡迎來到GRXTrade";
"Verify Your Account" = "完成身份驗證";
"Unlock more features now" = "即可享有更多特權福利";

"Login" = "登錄";
"Log In" = "登錄";
"Register" = "注冊";
"Your Email" = "你的電郵";
"Password" = "密碼";
"Forgot Password?" = "忘記登錄密碼？";
"OR" = "或";
"Referral ID?" = "推薦ID？";
"Referral ID" = "推薦ID";
"Create Account" = "創建用戶";
"Login with MAX-Wallet" = "使用MAX-Wallet登錄";
"Register with MAX-Wallet" = "向MAX-Wallet注冊";
"By creating an account, I agree to GRXTrade’s " = "通過創建用戶，我同意GRXTrade的";
"Terms of Use" = "使用條款";
" and " = "和";
"Privacy Policy" = "隱私政策";
"Do you wish to perform identity verification upon registration?" = "您要在註冊時執行身份驗證嗎？";
"Sure" = "好的";
"Later" = "稍後";
"Authorize" = "授權";

"Forgot Password" = "忘記密碼";
"Send code" = "發送代碼";

"My Info" = "我的資料";
"Name" = "真實姓名";
"Country" = "國家";
"UID" = "UID";
"Email Address" = "郵箱";
"Not set" = "未設置";
"Log out" = "退出";
"Are you sure you want to log out?" = "確定退出？";

"Email cannot be empty" = "電郵不允許留空";
"Invalid email address" = "電郵地址無效";
"Password must contain at least 6 alphanumerical characters with uppercase and lowercase" = "密碼必須至少包含6個字母數字字符，並帶有大寫與小寫字母";
"Password does not match" = "密碼不匹配";
"Password has been reset" = "密碼修改成功";
"Binding success" = "綁定成功";

"Identity Verification" = "身份驗證";
"Announcement" = "公告";
"Guide" = "指南";
"Tutorial" = "教程";
"Help & Support" = "支援服務";
"Security Center" = "安全中心";
"About GRXTrade" = "聯系GRXTrade";
"Referral" = "推薦好友";
"Settings" = "系統設置";

"SMS Authenticator" = "短信身份驗證器";
"Google Authenticator" = "谷歌身份驗證器";
"Login Password" = "登錄密碼";
"Transaction Password" = "交易密碼";
"Reset" = "修改";
"Change" = "";
"Cancel SMS Authenticator" = "取消短信驗證器";
"Cancel Google Authenticator" = "取消谷歌身份驗證器";
"Confirm" = "確認";

"Please enter the 6-digit security verification code." = "請輸入6位數的安全驗證。";

"Current Password" = "當前密碼";
"New Password" = "新密碼";
"Re-type Password" = "重新輸入密碼";
"Password must contain at least 6 alphanumerical characters with uppercase and lowercase" = "密碼必須至少包含6個字母數字字符，並帶有大寫與小寫字母";

"Your Invite Code" = "您的邀請代碼";
"Your Referral Code" = "您的推薦代碼";
"Copy" = "複制";
"Members" = "會員";
"Referral code has been copied to clipboard" = "推薦代碼複制到剪貼板";
"Share" = "分享";

"Currency" = "計價货币";
"Language" = "語言";

"Level 1" = "級別1";
"Level 2" = "級別2";

"Verified" = "已驗證";
"Pending Review" = "等待審核";
"Unverified" = "未驗證";
"Rejected" = "驗證拒絕";
"Continue" = "繼續";
"L1 Verified" = "級別1已驗證";
"L2 Verified" = "級別2已驗證";
"Basic verification process with proof of Identity Card or Passport." = "具有身份證或護照證明的基本驗證流程。";
"Advanced verification process with proof of Residence documents, Employment document and etc." = "預先核實程序，包括居留證件，就業文件等證明。";

// About Us
"Version" = "當前版本 %@";
"Website" = "網站";
"Facebook" = "臉書";
"Twitter" = "推特";
"Telegram" = "電報";
"Email" = "官方郵箱";

// KYC
"Residential Information" = "住宅信息";
"Fill up your Residential Information for secure purpose" = "請填寫您的住宅資料以確保安全";
"Building" = "大廈";
"Street" = "街名";
"City" = "城市";
"District" = "地區";
"Zip Code" = "郵政編號";
"Province" = "省（可選）";
"Proof of Residential" = "居住證明";
"Edit Residential Info" = "修改住宅資料";
"Identity successfully verified" = "身份已成功驗證";

"Employment Information" = "就業資料";
"Fill up your Employment Information for secure purpose" = "請填寫您的就業資料以確保安全";
"Employment Status" = "就業狀況";
"Income Source" = "收入來源";
"Industry" = "行業";
"Job Title" = "職稱";
"Company Name" = "公司名";
"Proof of Income" = "收入證明";
"Edit Employment Info" = "修改就業資料";

"You paid a fee." = "你支付了 %@ 的費用。";

"Version" = "當前版本 %@";
"Terms of Use and Conditions" = "用戶協議";
"All Rights Reserved." = "© %@ GROWDEX。版權所有";
