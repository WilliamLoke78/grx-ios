/* 
  Localizable.strings
  GrxTrade

  Created by Leon Mah Kean Loon on 01/10/2019.
  Copyright © 2019 Leon Mah. All rights reserved.
*/

// Tab Bar
"Home" = "首页";
"Markets" = "行情";
"Trades" = "交易";
"Wallets" = "资产";
"Account" = "我的";

// Common
"Session Timeout" = "会话超时";
"Your session has expired, please log in to continue." = "会话超时，请登录以访问更多内容。";
"You're already an existing user, please try to login." = "您已经是现有用户，请尝试登录。";
"No record" = "无记录";
"Yes" = "是";
"No" = "否";
"Submit" = "提交";
"Save" = "保存";
"Error" = "错误";
"New Version Available" = "新版本更新";
"Update" = "更新";
"Camera" = "相机";
"Delete" = "删除";
"Camera is needed to scan QR code" = "需要相机扫描QR码";
"Camera is needed to take photos" = "需要相机拍照";

// Home
"Digital Asset" = "币币行情";
"My Balance" = "我的余额";
"Balance of Exchange Account" = "总账户资产折合";
"Refer a Friend" = "推荐好友";
"Gain additional benefit." = "获取收益";
"Popular Trades" = "热门币币交易";

// Notification
"Notification" = "通知";
"Are you sure you want to delete this notification?" = "确定删除通知?";

// Market
"Coin Name" = "币种名称";
"Coin Price" = "最新价";
"Change %" = "涨跌幅";
"24H Vol" = "24H 量";
"Search" = "搜索";

// Trade
"Buy" = "买";
"Sell" = "卖";
"Open Order" = "当前委托";
"Order History" = "历史订单";
"Price" = "价格";
"Limit" = "限制";
"Market" = "市场";
"Stop-Limit" = "限价";
"Market Price" = "市场价钱";
"Stop" = "停止";
"Cancel" = "取消";
"Canceled" = "取消";
"Volume" = "数量";
"Available" = "可用";
"Total" = "总额";
"Fee" = "收费";
"Cancel Order" = "取消订单";
"Order ID" = "订单编号";
"Sub-Order ID" = "子订单编号";
"Done" = "完成";
"Your price has exceeded the current price by 50%" = "您的价格超出当前价格50％";
"Remember my transaction password?" = "记住交易密码？";
"Place Order" = "下单";

// KLine
"KLine Open" = "开";
"KLine Close" = "收";
"KLine High" = "高";
"KLine Low" = "低";
"KLine Volume" = "量";
"KLine Turnover" = "成";

"Please login to perform trade" = "请登录进行交易";
"Please login to view history" = "请登录以查看历史记录";
"Are you sure you want to cancel this order?" = "确定取消订单？";
"Please complete your security settings before trading" = "请在交易前完成您的安全设置";
"Please enter price to continue" = "请输入价格继续";
"Please enter volume to continue" = "请输入金额继续";
"Order has been placed" = "订单成功创建";
"Order has been canceled" = "订单取消成功";

"Order Details" = "订单详细信息";
"Order Side" = "订单方";
"Order Type" = "订单类型";
"Status" = "状态";
"Order Pair" = "订单对";
"Filled" = "填充";
"Time" = "时间";
"Average Price" = "平均价钱";
"Amount" = "合计";
"Value" = "价值";

// Wallet
"Deposit" = "存款";
"Withdraw" = "提币";
"Transaction" = "交易";
"Transaction History" = "交易历史";
"Locked" = "冻结";
"Transaction Details" = "交易明细";

"Choose coin" = "选择货币";
"Deposit Address" = "存款地址";
"Copy Address" = "复制地址";
"Memo" = "备忘录";
"Deposit address has been copied to clipboard" = "存款地址已复制到剪贴板";
"Please make sure the wallet address and memo address provided are accurate and be sure to verify the wallet address of the type of token or coin. Any funds deposited in the wrong token/coin address cannot be recovered." = "请确认钱包地址和备忘录地址是否准确，并确认各种代币或货币的钱包地址。 存入非对应代币/货币地址的任何资金将无法收回。";

"Please make sure whether the wallet address and memo address are accurate and also confirm wallet address of various type of token or coin. Any funds deposited in non-corresponding token/coin address will not be recovered." = "请确认钱包地址和备忘录地址是否准确，并确认各种代币或货币的钱包地址。 存入非对应代币/货币地址的任何资金将无法收回。";
"Once you have submitted your withdrawal request, we will send a confirmation email. Please clock on the confirmation link in your email." = "提交您的提币申请后，我们会发送确认电邮。 请单击电邮中的确认链接。";
"Withdraw Address" = "提币地址";
"Receive Amount" = "收获金额";

"Add New Beneficiary" = "添加新受益人";
"Withdrawal Address" = "提币地址";
"Paste" = "粘贴";
"Beneficiary Name" = "受益人姓名";
"Memo (optional)" = "备忘录（可选）";
"Beneficiary has been added" = "成功添加受益人";
"Delete Beneficiary" = "删除受益人";
"Are you sure you want to delete this beneficiary?" = "您确定要删除此受益人吗？";

"Deposits" = "存款";
"Withdraws" = "提币";
"All" = "全部";

"Transaction Type" = "交易类型";
"Action" = "操作";
"Account ID" = "帐户编号";
"Before Amount" = "金额前";
"Change Amount" = "改换金额";
"After Amount" = "金额后";
"Remark" = "备注";

"Please login to access your wallets" = "请登录以进接您的钱包";

// Account
"Login / Register" = "登录/注册";
"Welcome to GRXTrade" = "欢迎来到GRXTrade";
"Verify Your Account" = "完成身份验证";
"Unlock more features now" = "即可享有更多特权福利";

"Login" = "登录";
"Log In" = "登录";
"Register" = "注册";
"Your Email" = "你的电邮";
"Password" = "密码";
"Forgot Password?" = "忘记登录密码？";
"OR" = "或";
"Referral ID?" = "推荐ID？";
"Referral ID" = "推荐ID";
"Create Account" = "创建用户";
"Login with MAX-Wallet" = "使用MAX-Wallet登录";
"Register with MAX-Wallet" = "向MAX-Wallet注册";
"By creating an account, I agree to GRXTrade’s " = "通过创建用户，我同意GRXTrade的";
"Terms of Use" = "使用条款";
" and " = "和";
"Privacy Policy" = "隐私政策";
"Do you wish to perform identity verification upon registration?" = "您要在注册时执行身份验证吗？";
"Sure" = "好的";
"Later" = "稍后";
"Authorize" = "授权";

"Forgot Password" = "忘记密码";
"Send code" = "发送代码";

"My Info" = "我的资料";
"Name" = "真实姓名";
"Country" = "国家";
"UID" = "UID";
"Email Address" = "邮箱";
"Not set" = "未设置";
"Log out" = "退出";
"Are you sure you want to log out?" = "确定退出？";

"Email cannot be empty" = "电邮不允许留空";
"Invalid email address" = "电邮地址无效";
"Password must contain at least 6 alphanumerical characters with uppercase and lowercase" = "密码必须至少包含6个字母数字字符，并带有大写与小写字母";
"Password does not match" = "密码不匹配";
"Password has been reset" = "密码修改成功";
"Binding success" = "绑定成功";

"Identity Verification" = "身份验证";
"Announcement" = "公告";
"Guide" = "指南";
"Tutorial" = "教程";
"Help & Support" = "支援服务";
"Security Center" = "安全中心";
"About GRXTrade" = "联系GRXTrade";
"Referral" = "推荐好友";
"Settings" = "系统设置";

"SMS Authenticator" = "短信身份验证器";
"Google Authenticator" = "谷歌身份验证器";
"Login Password" = "登录密码";
"Transaction Password" = "交易密码";
"Reset" = "修改";
"Cancel SMS Authenticator" = "取消短信验证器";
"Cancel Google Authenticator" = "取消谷歌身份验证器";
"Confirm" = "确认";

"Please enter the 6-digit security verification code." = "请输入6位数的安全验证。";

"Current Password" = "当前密码";
"New Password" = "新密码";
"Re-type Password" = "重新输入密码";
"Password must contain at least 6 alphanumerical characters with uppercase and lowercase" = "密码必须至少包含6个字母数字字符，并带有大写与小写字母";

"Your Invite Code" = "您的邀请代码";
"Your Referral Code" = "您的推荐代码";
"Copy" = "复制";
"Members" = "会员";
"Referral code has been copied to clipboard" = "推荐代码复制到剪贴板";
"Share" = "分享";

"Currency" = "计价货币";
"Language" = "语言";

"Level 1" = "级别1";
"Level 2" = "级别2";

"Verified" = "已验证";
"Pending Review" = "等待审核";
"Unverified" = "未验证";
"Rejected" = "验证拒绝";
"Continue" = "继续";
"L1 Verified" = "级别1已验证";
"L2 Verified" = "级别2已验证";
"Basic verification process with proof of Identity Card or Passport." = "具有身份证或护照证明的基本验证流程。";
"Advanced verification process with proof of Residence documents, Employment document and etc." = "预先核实程序，包括居留证件，就业文件等证明。";

// About Us
"Version" = "当前版本 %@";
"Website" = "网站";
"Facebook" = "脸书";
"Twitter" = "推特";
"Telegram" = "电报";
"Email" = "官方邮箱";

// KYC
"Residential Information" = "住宅信息";
"Fill up your Residential Information for secure purpose" = "请填写您的住宅资料以确保安全";
"Building" = "大厦";
"Street" = "街名";
"City" = "城市";
"District" = "地区";
"Zip Code" = "邮政编码";
"Province" = "省（可选）";
"Proof of Residential" = "居住证明";
"Edit Residential Info" = "修改住宅资料";
"Identity successfully verified" = "身份已成功验证";

"Employment Information" = "就业资料";
"Fill up your Employment Information for secure purpose" = "请填写您的就业资料以确保安全";
"Employment Status" = "就业状况";
"Income Source" = "收入来源";
"Industry" = "行业";
"Job Title" = "职称";
"Company Name" = "公司名";
"Proof of Income" = "收入证明";
"Edit Employment Info" = "修改就业资料";

"You paid a fee." = "你支付了 %@ 的费用。";

"Version" = "当前版本 %@";
"Terms of Use and Conditions" = "用户协议";
"All Rights Reserved." = "© %@ GRXTrade。版权所有";
