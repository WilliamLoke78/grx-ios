//
//  SearchBarView.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 14/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import UIKit

class SearchBarView: BaseView {
    
    lazy var searchBar: UIView = {
        let view = UIView()
        return view
    }()
    
    override func setupViews() {
        
        addSubview(searchBar)
        
        searchBar.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(-1)
            make.left.equalToSuperview().offset(11)
            make.right.equalToSuperview().offset(-11)
        })
    }
}
