//
//  PhoneTextFieldView.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 04/02/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import UIKit

class PhoneTextFieldView: BaseView {
    
    lazy var prefixView: UIView = {
        let view = UIView()
        view.backgroundColor = .background
        view.layer.cornerRadius = 4
        return view
    }()
    
    lazy var label: UILabel = {
        let label = UILabel()
        label.text = "Phone No.".localized()
        label.font = UIFont(name: regular, size: 12)
        label.textColor = .grayColor
        return label
    }()
    
    lazy var plusLabel: UILabel = {
        let label = UILabel()
        label.text = "+"
        return label
    }()
    
    lazy var prefixTextField: UITextField = {
        let tf = UITextField()
        tf.backgroundColor = .clear
        tf.textColor = .textColor
        tf.keyboardType = .numberPad
        tf.keyboardAppearance = darkTheme ? .dark : .light
        tf.leftPadding(11)
        return tf
    }()
    
    lazy var numberView: UIView = {
        let view = UIView()
        view.backgroundColor = .background
        view.layer.cornerRadius = 4
        return view
    }()
    
    lazy var separatorLine: UIView = {
        let view = UIView()
        view.backgroundColor = .grayColor
        return view
    }()
    
    lazy var numberTextField: UITextField = {
        let tf = UITextField()
        tf.backgroundColor = .clear
        tf.textColor = .textColor
        tf.keyboardType = .numberPad
        tf.keyboardAppearance = darkTheme ? .dark : .light
        tf.leftPadding(11)
        return tf
    }()
    
    override func setupViews() {
        
        self.addSubview(prefixView)
        prefixView.addSubview(label)
        prefixView.addSubview(plusLabel)
        prefixView.addSubview(prefixTextField)
        
        self.addSubview(numberView)
        numberView.addSubview(separatorLine)
        numberView.addSubview(numberTextField)
        
        prefixView.snp.makeConstraints({ (make) in
            make.left.equalToSuperview()
            make.width.equalTo(88)
            make.height.equalToSuperview()
        })
        
        label.snp.makeConstraints({ (make) in
            make.top.equalTo(prefixView.snp.top).offset(3)
            make.left.equalTo(prefixView.snp.left).offset(11)
        })
        
        plusLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(label.snp.bottom).offset(4)
            make.left.equalToSuperview().offset(11)
        })
        
        prefixTextField.snp.makeConstraints({ (make) in
            make.top.equalTo(label.snp.bottom).offset(5)
            make.left.equalTo(plusLabel.snp.right).offset(-11)
            make.width.equalToSuperview()
        })
        
        numberView.snp.makeConstraints({ (make) in
            make.top.equalTo(prefixView.snp.top)
            make.left.equalTo(prefixView.snp.right).offset(-2)
            make.right.equalToSuperview()
            make.height.equalToSuperview()
        })
        
        separatorLine.snp.makeConstraints({ (make) in
            make.top.equalTo(prefixTextField.snp.top)
            make.bottom.equalTo(prefixTextField.snp.bottom)
            make.width.equalTo(1)
        })
        
        numberTextField.snp.makeConstraints({ (make) in
            make.top.equalTo(prefixTextField.snp.top)
            make.width.equalToSuperview()
        })
    }

}
