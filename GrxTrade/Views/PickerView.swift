//
//  PickerView.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 10/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import UIKit

class PickerView: BaseView {
    
    lazy var label: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: regular, size: 12)
        label.textColor = .grayColor
        return label
    }()
    
    lazy var textField: UITextField = {
        let tf = UITextField()
        tf.backgroundColor = .clear
        tf.textColor = .textColor
        tf.keyboardAppearance = darkTheme ? .dark : .light
        return tf
    }()
    
    lazy var valueField: UITextField = {
        let tf = UITextField()
        tf.isHidden = true
        return tf
    }()
    
    lazy var lastButton: UIButton = {
        let button = UIButton()
        let image = UIImage.init(named: "caret")
        button.setImage(image, for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        return button
    }()
    
    lazy var button: UIButton = {
        let button = UIButton()
        return button
    }()

    override func setupViews() {
        
        self.layer.cornerRadius = 4
        self.backgroundColor = .background
        
        self.addSubview(label)
        self.addSubview(textField)
        self.addSubview(lastButton)
        self.addSubview(button)
        
        label.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(3)
            make.left.equalToSuperview().offset(6)
        })
        
        textField.snp.makeConstraints({ (make) in
            make.top.equalTo(label.snp.bottom).offset(5)
            make.left.equalToSuperview().offset(6)
            make.right.equalTo(lastButton.snp.left)
        })
        
        lastButton.snp.makeConstraints({ (make) in
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(-11)
            make.height.equalTo(20)
            make.width.equalTo(20)
        })
        
        button.snp.makeConstraints({ (make) in
            make.edges.equalToSuperview()
        })
    }
}

