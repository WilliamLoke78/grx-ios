//
//  ImageButton.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 17/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

class ImageButton: UIButton {
    
    let iconView: UIImageView = {
        let iv = UIImageView()
        return iv
    }()
    
    let label: UILabel = {
        let label = UILabel()
        label.text = "Deposit".localized()
        label.textColor = .grayColor
        label.font = UIFont(name: regular, size: 11)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(iconView)
        addSubview(label)
        
        iconView.snp.makeConstraints({ (make) in
            make.height.equalTo(25)
            make.width.equalTo(25)
            make.centerX.equalToSuperview()
        })
        
        label.snp.makeConstraints({ (make) in
            make.top.equalTo(iconView.snp.bottom).offset(20)
            make.centerX.equalToSuperview()
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

