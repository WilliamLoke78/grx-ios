//
//  BannerView.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 16/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

class BannerView: BaseView {
    
    lazy var imageView: UIImageView = {
        let iv = UIImageView()
        iv.layer.cornerRadius = 4
        iv.clipsToBounds = true
        return iv
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont(name: bold, size: 16)
        return label
    }()
    
    lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont(name: regular, size: 12)
        return label
    }()
    
    override func setupViews() {
        
        layer.cornerRadius = 4
        clipsToBounds = true
        
        addSubview(imageView)
        imageView.addSubview(titleLabel)
        imageView.addSubview(subtitleLabel)
        
        self.snp.makeConstraints({ (make) in
            make.height.equalTo(90)
        })
        
        imageView.snp.makeConstraints({ (make) in
            make.height.equalTo(90)
            make.width.equalTo(screenWidth-22)
            make.left.equalToSuperview().offset(11)
            make.right.equalToSuperview().offset(-11)
        })
        
        titleLabel.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(26)
            make.left.equalToSuperview().offset(14)
        })
        
        subtitleLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(titleLabel.snp.bottom)
            make.left.equalToSuperview().offset(14)
        })
        
        bringSubviewToFront(titleLabel)
        bringSubviewToFront(subtitleLabel)
    }
}
