//
//  TextFieldView.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 01/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

class TextFieldView: BaseView {
    
    lazy var label: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: regular, size: 12)
        label.textColor = .grayColor
        return label
    }()
    
    lazy var textField: UITextField = {
        let tf = UITextField()
        tf.backgroundColor = .clear
        tf.textColor = .textColor
        tf.keyboardAppearance = darkTheme ? .dark : .light
        return tf
    }()
    
    lazy var firstButton: UIButton = {
        let button = UIButton()
        button.isHidden = true
        return button
    }()
    
    lazy var lastButton: UIButton = {
        let button = UIButton()
        let image = UIImage.init(named: "invisible")
        button.setImage(image, for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        button.isHidden = true
        return button
    }()

    override func setupViews() {
        
        self.layer.cornerRadius = 4
        self.backgroundColor = .background
        
        self.addSubview(label)
        self.addSubview(textField)
        self.addSubview(firstButton)
        self.addSubview(lastButton)
        
        label.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(5)
            make.left.equalToSuperview().offset(10)
        })
        
        textField.snp.makeConstraints({ (make) in
            make.top.equalTo(label.snp.bottom).offset(5)
            make.left.equalToSuperview().offset(10)
            make.right.equalTo(firstButton.snp.left)
        })
        
        firstButton.snp.makeConstraints({ (make) in
            make.centerY.equalToSuperview()
            make.right.equalTo(lastButton.snp.left).offset(-15)
            make.height.equalTo(20)
            make.width.equalTo(20)
        })
        
        lastButton.snp.makeConstraints({ (make) in
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(-11)
            make.height.equalTo(20)
            make.width.equalTo(20)
        })
    }
}
