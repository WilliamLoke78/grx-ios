//
//  PrimaryButton.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 24/12/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

class PrimaryButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = .primary
        self.layer.cornerRadius = 4
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
