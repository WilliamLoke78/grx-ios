//
//  TitleRightLabel.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 09/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import UIKit

class TitleRightLabel: UILabel {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        textAlignment = .right
        textColor = .textColor
        font = UIFont(name: regular, size: 12)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
