//
//  SelectFieldView.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 06/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import UIKit

class SelectFieldView: BaseView {
    
    lazy var label: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: regular, size: 12)
        label.textColor = .grayColor
        return label
    }()
    
    lazy var textField: UITextField = {
        let tf = UITextField()
        tf.textColor = .textColor
        return tf
    }()
    
    lazy var blockButton: UIButton = {
        let button = UIButton()
        return button
    }()

    override func setupViews() {
        
        self.layer.cornerRadius = 4
        self.backgroundColor = .background
        
        self.addSubview(label)
        self.addSubview(textField)
        self.addSubview(blockButton)
        
        label.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(5)
            make.left.equalToSuperview().offset(10)
        })
        
        textField.snp.makeConstraints({ (make) in
            make.top.equalTo(label.snp.bottom)
            make.left.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-6)
        })
        
        blockButton.snp.makeConstraints({ (make) in
            make.edges.equalTo(textField)
        })
        
        bringSubviewToFront(blockButton)
        blockButton.layer.zPosition = 1
    }
}

