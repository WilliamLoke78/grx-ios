//
//  EmptyView.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 23/12/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

class EmptyView: BaseView {

    let iconImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: darkTheme ? "record_empty_dark" : "record_empty_light")
        return iv
    }()
    
    let label: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: regular, size: 14)
        label.textColor = .grayColor
        label.text = "No record".localized()
        return label
    }()
    
    override func setupViews() {
        
        addSubview(iconImageView)
        addSubview(label)
        
        iconImageView.snp.makeConstraints({ (make) in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
            make.height.equalTo(74)
            make.width.equalTo(74)
        })
        
        label.snp.makeConstraints({ (make) in
            make.top.equalTo(iconImageView.snp.bottom).offset(3)
            make.centerX.equalToSuperview()
        })
    }

}
