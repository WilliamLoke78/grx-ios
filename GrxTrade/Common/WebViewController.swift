//
//  WebViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 16/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController, WKNavigationDelegate, WKUIDelegate {
    
    var webView: WKWebView!
    var webViewTitle: String = ""
    var isScrollEnabled: Bool = true
    var urlString = String()
    var isChart: Bool = false
    
    lazy var tradingBar: UIView = {
        let view = UIView()
        
        let buyButton = UIButton()
        buyButton.layer.cornerRadius = 4
        buyButton.backgroundColor = .greenColor
        buyButton.setTitle("Buy".localized(), for: .normal)
        buyButton.addTarget(self, action: #selector(tapBuy(_:)), for: .touchUpInside)
        
        let sellButton = UIButton()
        sellButton.layer.cornerRadius = 4
        sellButton.backgroundColor = .redColor
        sellButton.setTitle("Sell".localized(), for: .normal)
        sellButton.addTarget(self, action: #selector(tapSell(_:)), for: .touchUpInside)
        
        view.addSubview(buyButton)
        buyButton.snp.makeConstraints({ (make) in
            make.left.equalToSuperview()
            make.width.equalTo((screenWidth/2)-15)
            make.height.equalTo(54)
        })
        view.addSubview(sellButton)
        sellButton.snp.makeConstraints({ (make) in
            make.left.equalTo(buyButton.snp.right).offset(10)
            make.width.equalTo((screenWidth/2)-15)
            make.height.equalTo(54)
        })
        
        return view
    }()
    
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.navigationDelegate = self
        webView.uiDelegate = self
        
        if isScrollEnabled == false {
            webView.scrollView.isScrollEnabled = false
        }
        view = webView
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = webViewTitle
        
        let url = URL(string: urlString)
        let request = URLRequest(url: url!)
        webView.load(request)
        
        if isChart == true {
            webView.addSubview(tradingBar)
            tradingBar.snp.makeConstraints({ (make) in
                make.height.equalTo(54)
                make.left.equalToSuperview().offset(10)
                make.right.equalToSuperview().offset(-10)
                make.bottom.equalToSuperview()
            })
        }
    }
    
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        if navigationAction.targetFrame?.isMainFrame != true {
            webView.load(navigationAction.request)
        }
        return nil
    }
    
    @objc func tapBuy(_ sender: UIButton) {
        
    }
    
    @objc func tapSell(_ sender: UIButton) {
        
    }
}
