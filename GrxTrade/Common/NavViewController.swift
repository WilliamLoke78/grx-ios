//
//  NavViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 01/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

class NavViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationBar.tintColor = .white
        self.interactivePopGestureRecognizer?.isEnabled = true
        self.interactivePopGestureRecognizer?.delegate = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationBar.barTintColor = .navBackgroundColor
        self.navigationBar.setBackground(.navBackgroundColor)
    }
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        if viewControllers.count > 0 {
            viewController.hidesBottomBarWhenPushed = true
            viewController.addNavLeftItem()
        }
        super.pushViewController(viewController, animated: true)
    }

    @objc override func navigationBack() {
        popViewController(animated: true)
    }

}

extension UIViewController {
    @objc func addNavLeftItem(_ color: UIColor = .white) {
        
        let leftBarButtonItem: UIBarButtonItem = {
            let item = UIBarButtonItem.init(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(navigationBack))
            item.tintColor = color
            return item
        }()
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
    }
    
    @objc func navigationBack() {
        if self.navigationController?.viewControllers.count == 1, self.navigationController?.viewControllers.last == self {
            self.navigationController?.dismiss(animated: true, completion: nil)
            return
        }
        self.navigationController?.popViewController(animated: true)
    }
}

