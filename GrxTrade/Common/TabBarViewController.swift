//
//  TabBarViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 01/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController, UITabBarControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBar.barTintColor = .container
        self.tabBar.isTranslucent = false
        UITabBar.appearance().tintColor = .primary
        UITabBar.appearance().layer.borderWidth = 0
        UITabBar.appearance().clipsToBounds = true
        
        if #available(iOS 10.0, *) {
            self.tabBar.unselectedItemTintColor = .grayColor
        }
        
        let homeTab = NavViewController(rootViewController: HomeViewController.init())
        homeTab.tabBarItem = UITabBarItem(title: "Home".localized(), image: UIImage(named: "home"), tag: 0)
        homeTab.tabBarItem.titlePositionAdjustment.vertical = -2
        
        let marketTab = NavViewController(rootViewController: MarketHomeViewController.init())
        marketTab.tabBarItem = UITabBarItem(title: "Markets".localized(), image: UIImage(named: "market"), tag: 1)
        marketTab.tabBarItem.titlePositionAdjustment.vertical = -2
        
        let tradeTab = NavViewController(rootViewController: TradeViewController.init())
        tradeTab.tabBarItem = UITabBarItem(title: "Trades".localized(), image: UIImage(named: "trade"), tag: 2)
        tradeTab.tabBarItem.titlePositionAdjustment.vertical = -2
        
        let walletTab = NavViewController(rootViewController: WalletViewController())
        walletTab.tabBarItem = UITabBarItem(title: "Wallets".localized(), image: UIImage(named: "wallet"), tag: 3)
        walletTab.tabBarItem.titlePositionAdjustment.vertical = -2
        
        let accountTab = NavViewController(rootViewController: AccountViewController())
        accountTab.tabBarItem = UITabBarItem(title: "Account".localized(), image: UIImage(named: "account"), tag: 4)
        accountTab.tabBarItem.titlePositionAdjustment.vertical = -2
        
        let controllers = [homeTab, marketTab, tradeTab, walletTab, accountTab]
        self.viewControllers = controllers
    }
}
