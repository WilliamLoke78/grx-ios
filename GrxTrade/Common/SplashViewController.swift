//
//  SplashViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 13/02/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import UIKit

class SplashViewController: BaseViewController {
    
    let networking = Networking()
    let appBuild = "\(Bundle.main.infoDictionary?["CFBundleVersion"] ?? "")"
    
    lazy var logoView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "logo")
        return iv
    }()
    
    lazy var versionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: regular, size: 11)
        label.textColor = .grayColor
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .primary
        
        view.addSubview(logoView)
        logoView.snp.makeConstraints({ (make) in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
        })
        
        view.addSubview(versionLabel)
        versionLabel.snp.makeConstraints({ (make) in
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().offset(-10)
        })
        
        versionLabel.text = "v\(Bundle.main.infoDictionary!["CFBundleShortVersionString"]!)" + " (\(Bundle.main.infoDictionary?["CFBundleVersion"] ?? ""))"
    
        guard UserDefaults.standard.string(forKey: "appBuild") != nil else {
            postErrorLog(isUpdate: false)
            return
        }
        
        if let savedAppBuild = UserDefaults.standard.string(forKey: "appBuild") {
            if savedAppBuild != appBuild {
                postErrorLog(isUpdate: true)
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        checkAppVersion()
    }
    
    func postErrorLog(isUpdate: Bool) {
        var params: [String: Any] = [:]
        params["app"] = "2"
        params["brand"] = "Apple"
        params["buildNo"] = "\(Bundle.main.infoDictionary?["CFBundleVersion"] ?? "")"
        params["tag"] = isUpdate ? "200" : "100"
        params["device"] = UIDevice.current.model
        params["level"] = "1"
        params["model"] = UIDevice.current.modelName
        params["os"] = UIDevice.current.systemName
        params["os_ver"] = UIDevice.current.systemVersion
        params["imei"] = UIDevice.current.identifierForVendor!.uuidString
        
        print("device")
        print(params)
        
        let networking = Networking()
        networking.postErrorLog(params: params, controller: self) { result in
            UserDefaults.standard.set(self.appBuild, forKey: "appBuild")
        }
    }
    
    func checkAppVersion() {
        var params: [String: Any] = [:]
        params["appId"] = "grx-ios-store"
        params["requestVersion"] = appBuild
        
        let networking = Networking()
        networking.getVersionLookup(params: params, controller: self) { versionData in
            
            print("version build:")
            print(versionData.build)
            print(versionData.did)
            
            if versionData.build == 0 {
                print("proceed to home")
                self.proceedToHomeView()
                return
            }
            
            let alert = UIAlertController(title: "New Version Available".localized() + " (v\(Bundle.main.infoDictionary!["CFBundleShortVersionString"]!))", message: versionData.did, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Update".localized(), style: .default, handler: { action in
                guard let url = URL(string: networking.staticHost) else {
                    return
                }
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
                
                self.checkAppVersion()
            }))
            
            if versionData.isForceUpdate == 0 {
                alert.addAction(UIAlertAction(title: "Later".localized(), style: .cancel, handler: { action in
                    self.proceedToHomeView()
                }))
            }
            
            self.present(alert, animated: true)
            return
        }
    }
    
    func proceedToHomeView() {
        restartApplication(pageIndex: 0)
        return
    }
}
