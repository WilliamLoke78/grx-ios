//
//  SuccessViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 07/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import UIKit

class SuccessViewController: BaseViewController {

    lazy var backgroundView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 10
        view.backgroundColor = .backgroundAlternative
        return view
    }()
    
    lazy var imageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage.init(named: "success")
        return iv
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: bold, size: 20)
        label.text = "Successful!"
        label.textColor = .textColor
        return label
    }()
    
    lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: regular, size: 14)
        label.text = "Your email address has been successful verified."
        label.textColor = .grayColor
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addSubviews()
        adjustUI()
        addConstraints()
    }

//    override func resetTheme() {
//        backgroundView.backgroundColor = .backgroundAlternative
//        titleLabel.textColor = .textColor
//    }
}

// MARK: User Interface
extension SuccessViewController {
    
    func addSubviews() {
        view.addSubview(backgroundView)
        backgroundView.addSubview(imageView)
        backgroundView.addSubview(titleLabel)
        backgroundView.addSubview(subtitleLabel)
    }
    
    func adjustUI() {
        view.backgroundColor = .clear
        backgroundView.layer.cornerRadius = 10
    }
    
    func addConstraints() {
        view.addConstraintsWithFormat(format: "H:|-0-[v0]-0-|", views: backgroundView)
        view.addConstraintsWithFormat(format: "V:|-0-[v0]-0-|", views: backgroundView)
        
        backgroundView.addConstraintsWithFormat(format: "H:|[v0(60)]|", views: imageView)
        imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        titleLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        backgroundView.addConstraintsWithFormat(format: "V:|-49-[v0(60)]-12-[v1]-15-[v2]", views: imageView, titleLabel, subtitleLabel)
        
        subtitleLabel.snp.makeConstraints({ (make) in
            make.left.equalToSuperview().offset(80)
            make.right.equalToSuperview().offset(-80)
            make.centerX.equalToSuperview()
        })
    }
}

