//
//  AppDelegate.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 01/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit
import SobotKit
import SideMenu
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
//        UIApplication.shared.statusBarStyle = .lightContent
        window = UIWindow(frame: UIScreen.main.bounds)
        
//        let tabBarViewController = TabBarViewController()
//        tabBarViewController.selectedIndex = 0
//        window?.rootViewController = tabBarViewController
        let vc = SplashViewController()
        window?.rootViewController = vc
        window?.makeKeyAndVisible()
        
        configureSobot()

        #if Production
            configureUMeng()
            configureFirebase()
        #else
            print("this is testing")
        #endif
        
        return true
    }

    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        let alert = UIAlertController(title: url.absoluteString, message: url.queryParameters["data"], preferredStyle: .alert)

        // url.queryParameters["data"] 
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        
        print("show type")
        let type = url.queryParameters["type"]!

        if type == "login" {
            
            print("type: \(type)")
            
            if UIApplication.topViewController() != nil {
    //            topController.present(alert, animated: true)
                let vc = LoginViewController()
                vc.postMXWLogin(data: url.queryParameters["data"]!)
            }
        } else if type == "register" {
            
            print("type: \(type)")
            
            if UIApplication.topViewController() != nil {
                let vc = SignupViewController()
                vc.postMXWRegister(data: url.queryParameters["data"]!)
            }
        } else if type == "bind" {
            
            print("type: \(type)")
            
            if UIApplication.topViewController() != nil {
                let vc = SecurityViewController()
                vc.postBindMaxWallet(data: url.queryParameters["data"]!)
            }
        } else {
            
            print("type: \(type)")
            
            if UIApplication.topViewController() != nil {
                let vc = IdentityVerificationViewController()
                vc.postMxwKyc(data: url.queryParameters["data"]!)
            }
        }
        
        return true
    }
    
    var backgroundTask: UIBackgroundTaskIdentifier = .invalid

    func applicationWillResignActive(_ application: UIApplication) {
        backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
            self?.endBackgroundTask()
        }
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func endBackgroundTask() {
        print("Background task ended.")
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask = .invalid
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func configureSobot() {
        let sobotClient = ZCLibClient.init()
        sobotClient.initSobotSDK("2ebf86fb9fd04250acdc734a1b4d2b57")
    }
    
    func configureUMeng() {
//        UMCommonLogSwift.setUpUMCommonLogManager()
        UMCommonSwift.setLogEnabled(bFlag: true)
        UMCommonSwift.initWithAppkey(appKey: "5e4675134ca3573523000257", channel: "grx")
    }
    
    func configureFirebase() {
        FirebaseApp.configure()
        Fabric.sharedSDK().debug = true
    }
}


extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}
extension URL {
    var queryParameters: QueryParameters { return QueryParameters(url: self) }
}

class QueryParameters {
    let queryItems: [URLQueryItem]
    init(url: URL?) {
        queryItems = URLComponents(string: url?.absoluteString ?? "")?.queryItems ?? []
        print(queryItems)
    }
    subscript(name: String) -> String? {
        return queryItems.first(where: { $0.name == name })?.value
    }
}
