//
//  GuideTableViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 13/02/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import UIKit

private let guidesCellId = "guidesCellId"

class GuidesViewController: BaseTableViewController {

    private var cellModelArray: [CellModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.adjustUI()
        self.setupData()
        tableView.register(AccountSimpleTableViewCell.self, forCellReuseIdentifier: guidesCellId)
    }
}

// MARK: User Interface
extension GuidesViewController {
    func adjustUI() {
        title = "Guide".localized()
        tableView.backgroundColor = .background
        tableView.separatorColor = .background
        tableView.tableFooterView = UIView()
    }
}

// MARK: Table
extension GuidesViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellModelArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: guidesCellId, for: indexPath) as! AccountSimpleTableViewCell
        
        let model = cellModelArray[indexPath.row]
        cell.label.text = model.title
        cell.subLabel.text = model.subTitle
        
        if model.tag == "tutorial" {
            cell.button.addTarget(self, action: #selector(tapTutorial(_:)), for: .touchUpInside)
        }
        
        if model.tag == "fee" {
            cell.button.addTarget(self, action: #selector(tapFee(_:)), for: .touchUpInside)
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

// MARK: Data
extension GuidesViewController {
    
    @objc func tapTutorial(_ sender: UIButton) {
        let vc = WebViewController()
        let networking = Networking()
        vc.webViewTitle = "Tutorial".localized()
        vc.isScrollEnabled = true
        vc.urlString = "\(networking.staticHost)/help?isMobile=1"
        show(vc, sender: nil)
    }
    
    @objc func tapFee(_ sender: UIButton) {
        let vc = WebViewController()
        let networking = Networking()
        vc.webViewTitle = "Fee".localized()
        vc.isScrollEnabled = true
        var endpoint = String()
        switch LocalizationManager.serverLang() {
        case "zh_CN":
            endpoint = "https://grxtrade.s2.udesk.cn/hc/sections/20629"
        case "zh_TW":
            endpoint = "https://grxtrade.s2.udesk.cn/hc/sections/20630"
        default:
            endpoint = "https://grxtrade.s2.udesk.cn/hc/sections/20631"
        }
        vc.urlString = endpoint
        show(vc, sender: nil)
    }
    
    private func setupData() {
        
        let model0 = CellModel.init(icon: "", title: "Tutorial".localized(), subTitle: "", accIcon: "", tag: "tutorial")
//        let model1 = CellModel.init(icon: "", title: "Fee".localized(), subTitle: "", accIcon: "", tag: "fee")
        
//        cellModelArray = [model0, model1]
        cellModelArray = [model0]
    }
}
