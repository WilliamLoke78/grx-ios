//
//  AccountViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 01/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit
import SobotKit
import UdeskSDK

private let headerCellId = "headerCellId"
private let accountCellId = "accountCellId"

class AccountViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    private var cellModels: [CellModel] = []
    let user: User = BaseSingleton.shared.user
    let uuid = UUID().uuidString
    var vipData: VipData?
    
    fileprivate lazy var headerView: AccountHeaderView = {
        let hv = AccountHeaderView()
        hv.backgroundColor = .black
        hv.button.addTarget(self, action: #selector(tapProfile(_:)), for: .touchUpInside)
        return hv
    }()
    
    fileprivate lazy var verifyAccountBannerView: BannerView = {
        let bv = BannerView(frame: CGRect(x: 0, y: 0, width: screenWidth - 22, height: 0))
        bv.imageView.image = UIImage(named: "verify_account")
        bv.titleLabel.text = "Verify Your Account".localized()
        bv.subtitleLabel.text = "Unlock more features now".localized()
        return bv
    }()
    
    fileprivate lazy var tableView: UITableView = {
        let tv = UITableView(frame: .zero, style: .grouped)
        tv.contentInset = UIEdgeInsets(top: -25, left: 0, bottom: 0, right: 0)
        tv.backgroundColor = .background
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addSubviews()
        addConstraints()
        adjustUI()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(AccountTableViewCell.self, forCellReuseIdentifier: accountCellId)
        
        if BaseSingleton.shared.token != "" {
            getUserInfo()
            setupUIAfterLogin()
        } else {
            setupUIBeforeLogin()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if BaseSingleton.shared.token != "" {
            getUserInfo()
            setupUIAfterLogin()
        } else {
            setupUIBeforeLogin()
        }
        
        tableView.reloadData()
    }
    
    func resetTheme() {
        // Sun and moon switches
        let icon = darkTheme ? "light" : "dark"
        let themeItem = UIBarButtonItem(image: UIImage(named: icon), style: .plain, target: self, action: #selector(tapTheme(_:)))
        navigationItem.rightBarButtonItem = themeItem
    }
}

// MARK: User Interface
extension AccountViewController {
    
    func addSubviews() {
        view.addSubview(headerView)
        view.addSubview(tableView)
    }
    
    func addConstraints() {
        headerView.snp.makeConstraints({ (make) in
            make.top.equalToSuperview()
//            make.height.equalTo(80)
            make.height.equalTo(100)
            make.width.equalToSuperview()
        })
        
        tableView.snp.makeConstraints({ (make) in
            make.top.equalTo(headerView.snp.bottom)
            make.left.equalTo(view.snp.left)
            make.right.equalTo(view.snp.right)
            make.bottom.equalTo(view.snp.bottom)
            make.width.equalToSuperview()
        })
    }
    
    func adjustUI() {
        let icon = darkTheme ? "light" : "dark"
        let themeItem = UIBarButtonItem(image: UIImage(named: icon), style: .plain, target: self, action: #selector(tapTheme(_:)))
        self.navigationItem.rightBarButtonItem = themeItem
        self.tableView.separatorColor = .background
        view.backgroundColor = .background
        
        if user.kycLevel == 2 {
            headerView.profileImageView.image = UIImage(named: "profile_verified")
        } else {
            headerView.profileImageView.image = UIImage(named: "profile")
        }
    }

}

// MARK: Table
extension AccountViewController {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: accountCellId, for: indexPath) as! AccountTableViewCell
        
        let model = cellModels[indexPath.row]
        cell.configure(model: model)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let model = cellModels[indexPath.row]
        if model.tag == "empty" {
            return 10
        }
        if model.tag == "banner" {
            return 100
        }
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if BaseSingleton.shared.token == "" {
            return 10
        }
        
        if BaseSingleton.shared.user.kycLevel! < 2 {
            return 75
        }
        
        return 10
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        let model = cellModels[indexPath.row]
        
        switch model.tag {
        case "banner":
            let vc = IdentityVerificationViewController()
            show(vc, sender: nil)
        case "authentication":
            let vc = IdentityVerificationViewController()
            show(vc, sender: nil)
        case "security":
            let vc = SecurityViewController()
            show(vc, sender: nil)
        case "referral":
            let vc = ReferralViewController()
            show(vc, sender: nil)
        case "announcement":
            let vc = WebViewController()
            vc.webViewTitle = "Announcement".localized()
            vc.isScrollEnabled = true
            vc.urlString = "\(Networking().staticHost)/announcement?isMobile=1"
            show(vc, sender: nil)
        case "guide":
            let vc = GuidesViewController()
            show(vc, sender: nil)
        case "help":
            let chineseLangs = ["zh-Hant", "zh-Hans"]
            if chineseLangs.contains(LocalizationManager.userLang()) {
                let sobotLib = ZCLibInitInfo.init()
                sobotLib.nickName = BaseSingleton.shared.user.userName
                sobotLib.email = BaseSingleton.shared.user.email
                let vc = ZCChatController()
                vc.isPush = true
                show(vc, sender: nil)
            } else {
                
                let organization = UdeskOrganization.init(domain: "grxtrade.s2.udesk.cn", appKey: "712f69765cbf626a29f3ad1ce5e81d2d", appId: "82b36b766d60c119")
                let customer = UdeskCustomer()
                customer.sdkToken = BaseSingleton.shared.user.id ?? uuid
                customer.nickName = BaseSingleton.shared.user.userName
                customer.email = BaseSingleton.shared.user.email
                customer.cellphone = "\(BaseSingleton.shared.user.phoneCode ?? "")\(BaseSingleton.shared.user.mobile ?? "")"
        
                UdeskManager.initWith(organization, customer: customer)
        
                let sdkStyle = UdeskSDKStyle.custom()
                sdkStyle?.navigationColor = .primary
                sdkStyle?.titleColor = .white
                sdkStyle?.navBackButtonColor = .white
                sdkStyle?.navBackButtonImage = UIImage(named: "back")
                sdkStyle?.navRightButtonColor = .white
                sdkStyle?.transferButtonColor = .white
                sdkStyle?.agentTextColor = .textColor
                sdkStyle?.searchCancleButtonColor = .white
                
                let sdkConfig = UdeskSDKConfig.custom()
                sdkConfig?.languageType = UDLanguageType.EN
                
                let sdkManager = UdeskSDKManager.init(sdkStyle: sdkStyle, sdkConfig: sdkConfig)
                sdkManager?.pushUdesk(in: self.navigationController, completion: nil)
            }
        case "about":
            let vc = AboutViewController()
            show(vc, sender: nil)
        case "settings":
            let vc = SettingsViewController()
            show(vc, sender: nil)
        default:
            return
        }
    }
}

// MARK: Action
extension AccountViewController {
    
    @objc func tapProfile(_ sender: UIButton) {
        
        guard BaseSingleton.shared.token != "" else {
            let vc = NavViewController(rootViewController: LoginViewController())
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
            return
        }
        
        let vc = MyInfoTableViewController()
        self.show(vc, sender: nil)
    }
    
    @objc private func tapTheme(_ sender: UIBarButtonItem) {
        let theme = Storage.boolValue(key: Storage.Name.darkTheme)
        Storage.save(key: Storage.Name.darkTheme, value: theme ? false : true)
        darkTheme = theme ? false : true
        NotificationCenter.default.post(name: NSNotification.Name.darkTheme, object: nil)

        restartApplication(pageIndex: 4)
    }
    
    private func setupUIBeforeLogin() {
        
        headerView.titleLabel.text = "Login / Register".localized()
        headerView.subtitleLabel.text = "Welcome to GRXTrade".localized()
        headerView.levelLabel.isHidden = true
        headerView.vipLabel.isHidden = true
        headerView.tradeVolumeLabel.isHidden = true
        
        headerView.snp.remakeConstraints({ (make) in
            make.top.equalToSuperview()
            make.height.equalTo(80)
            make.width.equalToSuperview()
        })
        
        let model0 = CellModel(icon: "announcement", title: "Announcement".localized(), subTitle: "", accIcon: "", tag: "announcement")
        let model1 = CellModel(icon: "guide", title: "Guide".localized(), subTitle: "", accIcon: "", tag: "guide")
        let model2 = CellModel(icon: "help", title: "Help & Support".localized(), subTitle: "", accIcon: "", tag: "help")
        let model3 = CellModel(icon: "info", title: "About GRXTrade".localized(), subTitle: "v\(Bundle.main.infoDictionary!["CFBundleShortVersionString"]!)", accIcon: "", tag: "about")
        let model4 = CellModel(icon: "", title: "", subTitle: "", accIcon: "", tag: "empty")
        let model5 = CellModel(icon: "settings", title: "Settings".localized(), subTitle: "", accIcon: "", tag: "settings")
        
        cellModels = [model0, model1, model2, model3, model4, model5]
//        cellModels = [model2, model3, model4, model5]
    }
    
    private func setupUIAfterLogin() {
        
        headerView.titleLabel.text = BaseSingleton.shared.user.email
        headerView.subtitleLabel.text = "UID: GRX\(String(describing: BaseSingleton.shared.user.id!))"
        headerView.vipLabel.isHidden = false
        headerView.tradeVolumeLabel.isHidden = false
        
        headerView.snp.remakeConstraints({ (make) in
            make.top.equalToSuperview()
            make.height.equalTo(100)
            make.width.equalToSuperview()
        })
        
        if BaseSingleton.shared.user.kycLevel! < 2 {
            
            switch BaseSingleton.shared.user.kycLevel! {
            case 0:
                headerView.levelLabel.text = "Lvl \(0)"
                headerView.levelLabel.isHidden = false
            case 1:
                headerView.levelLabel.text = "Lvl \(1)"
                headerView.levelLabel.isHidden = false
            case 2:
                headerView.levelLabel.text = "Lvl \(2)"
                headerView.levelLabel.isHidden = false
            default:
                headerView.levelLabel.isHidden = true
            }
        } else {
            headerView.profileImageView.image = UIImage(named: "profile_verified")
            headerView.levelLabel.isHidden = true
        }
        
        var authLevel = String()
        if BaseSingleton.shared.user.kycLevel! == 0 {
            authLevel = "Unverified".localized()
        } else if BaseSingleton.shared.user.kycLevel! == 1 {
//            authLevel = "Level \(BaseSingleton.shared.user.kycLevel!)".localized()
            authLevel = "L1 Verified".localized()
        } else {
//            authLevel = "Verified".localized()
            authLevel = "L2 Verified".localized()
        }
        
        let model10 = CellModel(icon: "", title: "", subTitle: "", accIcon: "", tag: "banner")
        let model0 = CellModel(icon: "identity", title: "Identity Verification".localized(), subTitle: authLevel, accIcon: "", tag: "authentication")
        let model1 = CellModel(icon: "security", title: "Security Center".localized(), subTitle: "", accIcon: "", tag: "security")
        let model2 = CellModel(icon: "referral", title: "Referral".localized(), subTitle: "", accIcon: "", tag: "referral")
        let model3 = CellModel(icon: "", title: "", subTitle: "", accIcon: "", tag: "empty")
        let model4 = CellModel(icon: "announcement", title: "Announcement".localized(), subTitle: "", accIcon: "", tag: "announcement")
        let model5 = CellModel(icon: "guide", title: "Guide".localized(), subTitle: "", accIcon: "", tag: "guide")
        let model6 = CellModel(icon: "help", title: "Help & Support".localized(), subTitle: "", accIcon: "", tag: "help")
        let model7 = CellModel(icon: "info", title: "About GRXTrade".localized(), subTitle: "v\(Bundle.main.infoDictionary!["CFBundleShortVersionString"]!)", accIcon: "", tag: "about")
        let model8 = CellModel(icon: "", title: "", subTitle: "", accIcon: "", tag: "empty")
        let model9 = CellModel(icon: "settings", title: "Settings".localized(), subTitle: "", accIcon: "", tag: "settings")
        
        cellModels = [model10, model0, model1, model2, model3, model4, model5, model6, model7, model8, model9]
//        cellModels = [model10, model0, model1, model2, model3, model6, model7, model8, model9]
        
        if BaseSingleton.shared.user.kycLevel! == 2 {
            cellModels.removeFirst()
        }
    }
}

// MARK: Networking
extension AccountViewController {
    
    func getUserInfo() {
        let networking = Networking()
        networking.getUserInfo(params: [:], controller: self) { user in
            
            Storage.save(key: Storage.Name.user, value: user)
            BaseSingleton.shared.user = user
            
            self.tableView.reloadData()
        }
        
        networking.getVipInfo(params: [:], controller: self) { vipData in
            
            self.vipData = vipData
            self.headerView.vipLabel.text = vipData.vipName
            self.headerView.tradeVolumeLabel.text = "\(vipData.tradeVolume!)".forceDecimalDigits(8) + " \(vipData.tradeCoinSymbol!)"
        }
    }
}
