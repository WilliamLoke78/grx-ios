//
//  MyInfoTableViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 27/12/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

private let myInfoCellId = "myInfoCellId"

class MyInfoTableViewController: BaseTableViewController {

    private var cellModelArray: [CellModel] = []
    var isProfileSet: Bool? = false
    var name: String?
    var country: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        adjustUI()
        
        updateUserInfoAfterVerified()
        setupData()
        
        tableView.register(AccountSimpleTableViewCell.self, forCellReuseIdentifier: myInfoCellId)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        updateUserInfoAfterVerified()
        setupData()
    }
}

// MARK: User Interface
extension MyInfoTableViewController {
    
    func adjustUI() {
        title = "My Info".localized()
        tableView.backgroundColor = .background
        tableView.separatorColor = .background
        tableView.tableFooterView = UIView()
    }
}

// MARK: Table
extension MyInfoTableViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellModelArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: myInfoCellId, for: indexPath) as! AccountSimpleTableViewCell
        
        cell.arrowImageView.isHidden = true
        cell.subLabel.textColor = .primary
        cell.subLabel.snp.remakeConstraints({ (make) in
            make.right.equalToSuperview().offset(-14)
            make.centerY.equalToSuperview()
        })
        
        let model = cellModelArray[indexPath.row]
        
        if model.tag == "name" {
            cell.subLabel.textColor = isProfileSet! ? .primary : .grayColor
        }
        
        if model.tag == "country" {
            cell.subLabel.textColor = isProfileSet! ? .primary : .grayColor
        }
        
        if indexPath.row == 2 {
            cell.backgroundColor = .background
        }
        
        if indexPath.row == 5 {
            cell.backgroundColor = .background
        }
        
        if model.tag == "logout" {
            cell.button.addTarget(self, action: #selector(tapLogout(_:)), for: .touchUpInside)
            cell.button.setTitle("Log out".localized(), for: .normal)
            cell.button.setTitleColor(.redColor, for: .normal)
            cell.button.titleLabel?.font = UIFont(name: bold, size: 16)
        }
        
        cell.label.text = model.title
        cell.subLabel.text = model.subTitle
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.row {
        case 2:
            return 10
        case 5:
            return 30
        default:
            return 50
        }
    }
}

// MARK: Data
extension MyInfoTableViewController {
        
    private func setupData() {
        
        let model0 = CellModel.init(icon: "", title: "Name".localized(), subTitle: BaseSingleton.shared.user.nickName ?? "Not set".localized(), accIcon: "", tag: "name")
        let model2 = CellModel.init(icon: "", title: "Country".localized(), subTitle: BaseSingleton.shared.user.country ?? "Not set".localized(), accIcon: "", tag: "country")
        let model3 = CellModel.init(icon: "", title: "".localized(), subTitle: "", accIcon: "", tag: "")
        let model4 = CellModel.init(icon: "", title: "UID".localized(), subTitle: "GRX\(BaseSingleton.shared.user.id!)", accIcon: "", tag: "uid")
        let model5 = CellModel.init(icon: "", title: "Email Address".localized(), subTitle: BaseSingleton.shared.user.email!, accIcon: "", tag: "email")
        let model6 = CellModel.init(icon: "", title: "".localized(), subTitle: "", accIcon: "", tag: "")
        let model7 = CellModel.init(icon: "", title: "", subTitle: "", accIcon: "", tag: "logout")
        
        cellModelArray = [model0, model2, model3, model4, model5, model6, model7]
    }
    
    func updateUserInfoAfterVerified() {
//        Alamofire.request("\(baseHost)/barong/resource/profiles/me", method: .get).validate().responseJSON { response in
//
//            switch response.result {
//            case .success:
//
//                let result = JSON(response.result.value!)
//                let model = User.init(aJSON: result)
//
//                var name: String?
//                var country: String?
//
//                name = "\(model.first_name!) \(model.last_name!)"
//                country = model.country
//
//                self.isProfileSet = true
//                self.name = name
//                self.country = country
//
//                self.tableView.reloadData()
//
//            case .failure:
//                if response.response?.statusCode == 401 {
//                    if BaseSingleton.shared.user.isLogin == true {
//                        self.showSessionExpired()
//                    }
//                } else {
//                    if let data = response.data, let message = String(data: data, encoding: .utf8) {
//                        showSimpleToast(message)
//                    }
//                }
//            }
//        }
    }
    
    func logout() {
        BaseSingleton.shared.user.reset()
        restartApplication(pageIndex: 4)
    }
}

// MARK: Action
extension MyInfoTableViewController {
    
    @objc func tapLogout(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Log out".localized(), message: "Are you sure you want to log out?".localized(), preferredStyle: UIAlertController.Style.alert)
        alert.view.subviews.first?.subviews.first?.subviews.first?.backgroundColor = .container
        alert.view.subviews.first?.subviews.first?.subviews.last?.backgroundColor = .container
        alert.view.tintColor = .grayColor
        
        alert.setValue(NSAttributedString(string: "Log out".localized(), attributes: [NSAttributedString.Key.font : UIFont(name: bold, size: 15) as Any,NSAttributedString.Key.foregroundColor : UIColor.grayColor]), forKey: "attributedTitle")
        alert.setValue(NSAttributedString(string: "Are you sure you want to log out?".localized(), attributes: [NSAttributedString.Key.font: UIFont(name: regular, size: 13) as Any, NSAttributedString.Key.foregroundColor : UIColor.grayColor]), forKey: "attributedMessage")
        
        alert.addAction(UIAlertAction(title: "No".localized(), style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Yes".localized(), style: .default, handler: { action in
            self.logout()
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
}

