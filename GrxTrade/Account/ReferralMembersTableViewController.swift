//
//  ReferralMembersTableViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 09/02/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import UIKit

private let memberCellId = "memberCellId"
private let loadingCell = "loadingCell"

class ReferralMembersTableViewController: BaseTableViewController {

    var members: [ReferralMember] = []
    
    var isFetching = false
    var isLast = false
    var pagination: Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        adjustUI()
        tableView.register(ReferralMemberTableViewCell.self, forCellReuseIdentifier: memberCellId)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: loadingCell)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
//        getOrderHistoryData(page: pagination)
        getReferralMembers()
    }
}

// MARK: User Interface
extension ReferralMembersTableViewController {
    
    func adjustUI() {
        
        title = "Referral Members".localized()
        
        tableView.backgroundColor = .background
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView()
    }
}

// MARK: Table
extension ReferralMembersTableViewController {

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return members.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: memberCellId, for: indexPath) as! ReferralMemberTableViewCell
        
        cell.configure(members[indexPath.row])
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = members.count - 1
//        if indexPath.row == lastElement - 5 {
//            getOrderHistoryData(page: pagination)
//
//        }
    }
    
    // Estimated height for row at is needed to avoid jumping cells
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 112
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 112
    }
}

// MARK: Networking
extension ReferralMembersTableViewController {
 
    func getReferralMembers() {
        
        var params: [String: String] = [:]
        params["current"] = "1"
        params["size"] = "50"
        
        print(params)
        
        let networking = Networking()
        networking.getReferralMembers(params: params, controller: self) { members in
            
            self.members = members
            self.tableView.reloadData()
        }
    }
}

