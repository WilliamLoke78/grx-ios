//
//  ChangePasswordViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 02/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import UIKit
import Presentr

class ChangePasswordViewController: BaseViewController {
    
    var viewTitle: String?
    var type: String?
    var isSet: Int?
    
    var params: [String: String] = [:]

    lazy var currentPassword: TextFieldView = {
        let view = TextFieldView()
        view.label.text = "Current Password".localized()
        view.lastButton.isHidden = false
        view.lastButton.addTarget(self, action: #selector(toggleCurrentPassword(_:)), for: .touchUpInside)
        view.textField.isSecureTextEntry = true
        return view
    }()
    
    lazy var newPassword: TextFieldView = {
        let view = TextFieldView()
        view.label.text = "New Password".localized()
        view.lastButton.isHidden = false
        view.lastButton.addTarget(self, action: #selector(toggleNewPassword(_:)), for: .touchUpInside)
        view.textField.isSecureTextEntry = true
        return view
    }()
    
    lazy var retypePassword: TextFieldView = {
        let view = TextFieldView()
        view.label.text = "Re-type Password".localized()
        view.lastButton.isHidden = false
        view.lastButton.addTarget(self, action: #selector(toggleRetypePassword(_:)), for: .touchUpInside)
        view.textField.isSecureTextEntry = true
        return view
    }()
    
    lazy var hintLabel: UILabel = {
        let label = UILabel()
        label.text = "Password must contain at least 6 alphanumerical characters with uppercase and lowercase".localized()
        label.textColor = .grayColor
        label.font = UIFont(name: regular, size: 12)
        label.numberOfLines = 2
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addSubviews()
        adjustUI()
        addConstraints()
        
        print("isSet: \(isSet)")
        print("login: \(BaseSingleton.shared.user.pwdStatus)")
        print("txn: \(BaseSingleton.shared.user.paymentPasswordStatus)")
        
        NotificationCenter.default.addObserver(self, selector: #selector(popLoginView), name:NSNotification.Name(rawValue: "popChangeLoginPasswordView"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(popTransactionView), name:NSNotification.Name(rawValue: "popChangeTransactionPasswordView"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        addSubviews()
        adjustUI()
        addConstraints()
    }
}

// MARK: User Interface
extension ChangePasswordViewController {
    
    func addSubviews() {
        if isSet != 0 {
            view.addSubview(currentPassword)
        }
        view.addSubview(newPassword)
        view.addSubview(retypePassword)
        view.addSubview(hintLabel)
    }
    
    func adjustUI() {
        title = viewTitle
        view.backgroundColor = .backgroundAlternative
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Confirm".localized(), style: .plain, target: self, action: #selector(tapConfirm(_:)))
    }
    
    func addConstraints() {
        
        if isSet != 0 {
            
            currentPassword.snp.makeConstraints({ (make) in
                make.top.equalToSuperview().offset(9)
                make.left.equalToSuperview().offset(11)
                make.right.equalToSuperview().offset(-11)
                make.height.equalTo(54)
            })
            
            newPassword.snp.makeConstraints({ (make) in
                make.top.equalTo(currentPassword.snp.bottom).offset(10)
                make.left.right.equalTo(currentPassword)
                make.height.equalTo(54)
            })
            
            retypePassword.snp.makeConstraints({ (make) in
                make.top.equalTo(newPassword.snp.bottom).offset(10)
                make.left.right.equalTo(currentPassword)
                make.height.equalTo(54)
            })
            
        } else {
        
            newPassword.snp.makeConstraints({ (make) in
                make.top.equalToSuperview().offset(9)
                make.left.equalToSuperview().offset(11)
                make.right.equalToSuperview().offset(-11)
                make.height.equalTo(54)
            })
            
            retypePassword.snp.makeConstraints({ (make) in
                make.top.equalTo(newPassword.snp.bottom).offset(10)
                make.left.right.equalTo(newPassword)
                make.height.equalTo(54)
            })
        }
        
        hintLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(retypePassword.snp.bottom).offset(10)
            make.left.right.equalTo(currentPassword)
        })
    }
    
    @objc func tapConfirm(_ sender: UIBarButtonItem) {
        
        if newPassword.textField.text!.md5() != retypePassword.textField.text!.md5() {
            showSimpleToast("Password does not match".localized())
            return
        }
        
        guard isValidPassword(newPassword.textField.text!) else {
            showSimpleToast("Password must contain at least 6 alphanumerical characters with uppercase and lowercase".localized())
            return
        }
        
        switch type {
        case "loginPassword":
            if isSet == 0 {
                postLoginPassword()
                return
            }
            putLoginPassword()
        case "transactionPassword":
            if isSet == 0 {
                postTransactionPassword()
                return
            }
            putTransactionPassword()
        default:
            return
        }
    }
    
    @objc func toggleCurrentPassword(_ sender: UIButton) {
        currentPassword.textField.isSecureTextEntry.toggle()
        newPassword.textField.isSecureTextEntry.toggle()
        retypePassword.textField.isSecureTextEntry.toggle()
        if currentPassword.textField.isSecureTextEntry == true {
            currentPassword.lastButton.setImage(UIImage(named: "invisible"), for: .normal)
            newPassword.lastButton.setImage(UIImage(named: "invisible"), for: .normal)
            retypePassword.lastButton.setImage(UIImage(named: "invisible"), for: .normal)
        } else {
            currentPassword.lastButton.setImage(UIImage(named: "visible"), for: .normal)
            newPassword.lastButton.setImage(UIImage(named: "visible"), for: .normal)
            retypePassword.lastButton.setImage(UIImage(named: "visible"), for: .normal)
        }
    }
    
    @objc func toggleNewPassword(_ sender: UIButton) {
        currentPassword.textField.isSecureTextEntry.toggle()
        newPassword.textField.isSecureTextEntry.toggle()
        retypePassword.textField.isSecureTextEntry.toggle()
        if newPassword.textField.isSecureTextEntry == true {
            currentPassword.lastButton.setImage(UIImage(named: "invisible"), for: .normal)
            newPassword.lastButton.setImage(UIImage(named: "invisible"), for: .normal)
            retypePassword.lastButton.setImage(UIImage(named: "invisible"), for: .normal)
        } else {
            currentPassword.lastButton.setImage(UIImage(named: "visible"), for: .normal)
            newPassword.lastButton.setImage(UIImage(named: "visible"), for: .normal)
            retypePassword.lastButton.setImage(UIImage(named: "visible"), for: .normal)
        }
    }
    
    @objc func toggleRetypePassword(_ sender: UIButton) {
        currentPassword.textField.isSecureTextEntry.toggle()
        newPassword.textField.isSecureTextEntry.toggle()
        retypePassword.textField.isSecureTextEntry.toggle()
        if retypePassword.textField.isSecureTextEntry == true {
            currentPassword.lastButton.setImage(UIImage(named: "invisible"), for: .normal)
            newPassword.lastButton.setImage(UIImage(named: "invisible"), for: .normal)
            retypePassword.lastButton.setImage(UIImage(named: "invisible"), for: .normal)
        } else {
            currentPassword.lastButton.setImage(UIImage(named: "visible"), for: .normal)
            newPassword.lastButton.setImage(UIImage(named: "visible"), for: .normal)
            retypePassword.lastButton.setImage(UIImage(named: "visible"), for: .normal)
        }
    }
    
    @objc func popLoginView() {
        showSimpleToast("Login password has been changed".localized())
        BaseSingleton.shared.user.pwdStatus = 1
        navigationController?.popViewController(animated: true)
    }
    
    @objc func popTransactionView() {
        showSimpleToast("Transaction password has been changed".localized())
        BaseSingleton.shared.user.paymentPasswordStatus = 1
        navigationController?.popViewController(animated: true)
    }
}

// MARK: Networking
extension ChangePasswordViewController {
    
    func postLoginPassword() {
        
        guard (newPassword.textField.text != "") else {
            showSimpleToast("Password cannot be empty".localized())
            return
        }
        
        guard isValidPassword(newPassword.textField.text!) else {
            showSimpleToast("Password must contain at least 6 alphanumerical characters with uppercase and lowercase".localized())
            return
        }
        
        validatePasswordMatching()
        
        params["password"] = newPassword.textField.text!.md5()
        
        showOTPView(type: "loginPassword", isSet: false, params: params)
    }
    
    func putLoginPassword() {
        
        guard (currentPassword.textField.text != "") else {
            showSimpleToast("Current password cannot be empty".localized())
            return
        }
        
        guard (newPassword.textField.text != "") else {
            showSimpleToast("New password cannot be empty".localized())
            return
        }
        
        guard isValidPassword(newPassword.textField.text!) else {
            showSimpleToast("Password must contain at least 6 alphanumerical characters with uppercase and lowercase".localized())
            return
        }
        
        validatePasswordMatching()
        
        params["oldPassword"] = currentPassword.textField.text!.md5()
        params["newPassword"] = newPassword.textField.text!.md5()
        
        showOTPView(type: "loginPassword", isSet: true, params: params)
    }
    
    func postTransactionPassword() {
        
        guard (newPassword.textField.text != "") else {
            showSimpleToast("Password cannot be empty".localized())
            return
        }
        
        guard isValidPassword(newPassword.textField.text!) else {
            showSimpleToast("Password must contain at least 6 alphanumerical characters with uppercase and lowercase".localized())
            return
        }
        
        validatePasswordMatching()
        
        params["password"] = newPassword.textField.text!.md5()
        
        showOTPView(type: "transactionPassword", isSet: false, params: params)
    }
    
    func putTransactionPassword() {
        
        guard (currentPassword.textField.text != "") else {
            showSimpleToast("Current password cannot be empty".localized())
            return
        }
        
        guard (newPassword.textField.text != "") else {
            showSimpleToast("New password cannot be empty".localized())
            return
        }
        
        guard isValidPassword(newPassword.textField.text!) else {
            showSimpleToast("Password must contain at least 6 alphanumerical characters with uppercase and lowercase".localized())
            return
        }
        
        validatePasswordMatching()
        
        params["oldPassword"] = currentPassword.textField.text!.md5()
        params["newPassword"] = newPassword.textField.text!.md5()
        
        showOTPView(type: "transactionPassword", isSet: true, params: params)
    }
    
    func showOTPView(type: String, isSet: Bool, params: [String: String]) {
        
        let vc = OneTimePasswordViewController()
        vc.type = type
        vc.isSet = isSet
        vc.params = params
        let presenter = Presentr(presentationType: .custom(width: .full, height: .custom(size: 680), center: .bottomCenter))
        self.customPresentViewController(presenter, viewController: vc, animated: true, completion: nil)
    }
    
    func validatePasswordMatching() {
        if newPassword.textField.text!.md5() != retypePassword.textField.text!.md5() {
            showSimpleToast("Password does not match".localized())
            return
        }
    }
}

