//
//  SecurityViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 31/12/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit
import Presentr

private let securityCellId = "securityCellId"

class SecurityViewController: BaseTableViewController {

    private var cellModels: [CellModel] = []
    
    lazy var smsSwitch: UISwitch = {
        let button = UISwitch()
        button.isOn = false
        button.isUserInteractionEnabled = false
        return button
    }()
    
    lazy var googleSwitch: UISwitch = {
        let button = UISwitch()
        button.isOn = false
        button.isUserInteractionEnabled = false
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        adjustUI()
        setupData()
        
        tableView.register(AccountTableViewCell.self, forCellReuseIdentifier: securityCellId)

        NotificationCenter.default.addObserver(self, selector: #selector(reloadSecurityCenter), name:NSNotification.Name(rawValue: "reloadSecurityCenter"), object: nil)
    }

    override func viewDidAppear(_ animated: Bool) {
        
        tableView.reloadData()
    }
}

// MARK: UI
extension SecurityViewController {
    
    func adjustUI() {
        title = "Security Center".localized()
        tableView.backgroundColor = .background
        tableView.separatorColor = .background
        tableView.tableFooterView = UIView()
    }
    
    func setupData() {
    
        let model0 = CellModel.init(icon: "sms_auth", title: "SMS Authenticator".localized(), subTitle: "", accIcon: "", tag: "sms")
        let model1 = CellModel.init(icon: "lock", title: "Google Authenticator".localized(), subTitle: "", accIcon: "", tag: "google")
        let model2 = CellModel.init(icon: "lock", title: "Login Password".localized(), subTitle: "Not set".localized(), accIcon: "", tag: "password")
        let model3 = CellModel.init(icon: "lock", title: "Transaction Password".localized(), subTitle: "Not set".localized(), accIcon: "", tag: "transaction")
        let model4 = CellModel.init(icon: "mxw", title: "Authorize".localized() + " MAX-Wallet".localized(), subTitle: "Not set".localized(), accIcon: "", tag: "mxw")
        
        cellModels = [model0, model1, model2, model3, model4]
    }
}

// MARK: Table
extension SecurityViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return cellModels.count
        }
        
        override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: securityCellId, for: indexPath) as! AccountTableViewCell
            
            let model = cellModels[indexPath.row]
            
            cell.iconImageView.image = UIImage(named: model.icon)
            cell.label.text = model.title
            cell.subLabel.text = model.subTitle
            
            if model.tag == "sms" {
                
                cell.arrowImageView.isHidden = true
                
                cell.addSubview(smsSwitch)
                smsSwitch.snp.makeConstraints({ (make) in
                    make.right.equalToSuperview().offset(-20)
                    make.centerY.equalToSuperview()
                })
                
                if BaseSingleton.shared.user.mobileStatus == 1 {
                    smsSwitch.isOn = true
                } else {
                    smsSwitch.isOn = false
                }
            }
            
            if model.tag == "google" {
                
                cell.arrowImageView.isHidden = true
                
                cell.addSubview(googleSwitch)
                googleSwitch.snp.makeConstraints({ (make) in
                    make.right.equalToSuperview().offset(-20)
                    make.centerY.equalToSuperview()
                })
                
                if BaseSingleton.shared.user.gaStatus == 1 {
                    googleSwitch.isOn = true
                }  else {
                   googleSwitch.isOn = false
                }
            }
            
            if model.tag == "password" {
                if BaseSingleton.shared.user.pwdStatus == 1 {
                    cell.subLabel.text = "Reset".localized()
                }
            }
            
            if model.tag == "transaction" {
                if BaseSingleton.shared.user.paymentPasswordStatus == 1 {
                    cell.subLabel.text = "Reset".localized()
                }
            }
            
            if model.tag == "mxw" {
                if BaseSingleton.shared.user.regWalletStatus == 1 {
                    cell.subLabel.text = "Enabled".localized()
                    cell.arrowImageView.isHidden = true
                    cell.subLabel.snp.remakeConstraints({ (make) in
                        make.right.equalToSuperview().offset(-14)
                        make.centerY.equalToSuperview()
                    })
                }
            }
            
            return cell
        }
        
        override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            tableView.deselectRow(at: indexPath, animated: true)
            
            let model = cellModels[indexPath.row]
            
            if model.tag == "sms" {
                if BaseSingleton.shared.user.mobileStatus! == 0 {
                    let vc = SmsAuthViewController()
                    show(vc, sender: nil)
                }
                
                tapSms()
            }
            
            if model.tag == "google" {
                if BaseSingleton.shared.user.gaStatus! == 0 {
                    let vc = GoogleAuthViewController()
                    show(vc, sender: nil)
                }
                
                tapGoogle()
            }
            
            if model.tag == "password" {
                if BaseSingleton.shared.user.gaStatus! == 0 {
                    showSimpleToast("Please enable Google Authenticator".localized())
                    return
                }
                
                let vc = ChangePasswordViewController()
                vc.viewTitle = "Login Password".localized()
                vc.isSet = BaseSingleton.shared.user.pwdStatus
                vc.type = "loginPassword"
                show(vc, sender: nil)
            }
            
            if model.tag == "transaction" {
                
                if BaseSingleton.shared.user.gaStatus! == 0 {
                    showSimpleToast("Please enable Google Authenticator".localized())
                    return
                }
                
                let vc = ChangePasswordViewController()
                vc.viewTitle = "Transaction Password".localized()
                vc.isSet = BaseSingleton.shared.user.paymentPasswordStatus
                vc.type = "transactionPassword"
                show(vc, sender: nil)
            }
            
            if model.tag == "mxw" {
                if BaseSingleton.shared.user.regWalletStatus! == 1 {
                    return
                }
                getPackage()
            }
        }
        
        override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 50
        }
}

// MARK: Action
extension SecurityViewController {
    
    func tapSms() {
        if BaseSingleton.shared.user.mobileStatus == 1 {
            
            getSmsOTP()
            
            let vc = OneTimePasswordViewController()
            vc.type = "cancelSMS"
            let presenter = Presentr(presentationType: .custom(width: .full, height: .custom(size: 680), center: .bottomCenter))
            self.customPresentViewController(presenter, viewController: vc, animated: true, completion: nil)
        }
    }
    
    func tapGoogle() {
        if BaseSingleton.shared.user.gaStatus == 1 {
            let vc = OneTimePasswordViewController()
            vc.type = "cancelGA"
            let presenter = Presentr(presentationType: .custom(width: .full, height: .custom(size: 680), center: .bottomCenter))
            self.customPresentViewController(presenter, viewController: vc, animated: true, completion: nil)
        }
    }
    
    func getSmsOTP() {
        
        var params: [String: String] = [:]
        params["phoneCode"] = BaseSingleton.shared.user.phoneCode
        params["mobile"] = BaseSingleton.shared.user.mobile
        
        let networking = Networking()
        networking.postSmsAuthOTP(params: params, controller: self) { response in
            
        }
    }
    
    @objc func reloadSecurityCenter() {
        getUserInfo()
    }
    
    func getUserInfo() {
        let networking = Networking()
        networking.getUserInfo(params: [:], controller: self) { user in
            Storage.save(key: Storage.Name.user, value: user)
            BaseSingleton.shared.user = user
            self.tableView.reloadData()
        }
    }
    
    func getPackage() {
    
        var params: [String: String] = [:]
        params["type"] = "MXW_WALLET_REGISTER"
        
        let networking = Networking()
        networking.getPackageInfo(params: params, controller: self) { response in
            
            print("response:")
            print(response)
            
            let base64 = "\(response)".toBase64()
            
            let host = "com.mxw.wallet://maxRegister?scheme=com.grxtrade.app&type=bind"
            var data = "&data=\(base64)"
            data = data.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
            
            guard let openMxwURL = URL(string: host+data) else {
                print("open MXW URL failed")
                return //be safe
            }
            
            if UIApplication.shared.canOpenURL(openMxwURL) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(openMxwURL, options: [:], completionHandler: { (success) in
                        print("Successfully launched MaxWallet")
                    })
                } else {
                    UIApplication.shared.openURL(openMxwURL)
                    print("Successfully launched MaxWallet")
                }
            } else {
                guard let url = URL(string: networking.mxwHost) else {
                    return
                }
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    
    func postBindMaxWallet(data: Any) {
        
        var params: [String: Any] = [:]
        params["data"] = "\(data)".fromBase64()
        
        print("post mxw bind")
        print(params)
        
        let networking = Networking()
        networking.postBindMaxWallet(params: params as! [String : String], controller: self) { response in
            
            showSimpleToast("Binding success".localized())
            
            restartApplication(pageIndex: 4)
        }
    }
}
