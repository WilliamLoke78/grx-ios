//
//  SmsAuthViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 04/02/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import UIKit

class SmsAuthViewController: BaseViewController, UITextFieldDelegate {

    lazy var phoneView: PhoneTextFieldView = {
        let view = PhoneTextFieldView()
        view.numberTextField.keyboardType = .numberPad
        view.label.text = "Phone No.".localized()
        let button = UIButton()
        button.setTitle("Send Code".localized(), for: .normal)
        button.titleLabel?.font = UIFont(name: bold, size: 12)
        button.setTitleColor(.primary, for: .normal)
        button.addTarget(self, action: #selector(tapSendCode(_:)), for: .touchUpInside)
        button.backgroundColor = UIColor.primary.withAlphaComponent(0.2)
        button.layer.cornerRadius = 4
        button.layer.borderColor = UIColor.primary.cgColor
        button.layer.borderWidth = 1
        view.addSubview(button)
        
        button.snp.makeConstraints({ (make) in
            make.right.equalToSuperview().offset(-10)
            make.centerY.equalToSuperview()
            make.width.equalTo(84)
        })
        return view
    }()
    
    lazy var codeView: TextFieldView = {
        let view = TextFieldView()
        view.textField.keyboardType = .numberPad
        view.label.text = "Verify Code".localized()
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addSubviews()
        adjustUI()
        addConstraints()
        
        phoneView.prefixTextField.delegate = self
        phoneView.numberTextField.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
//        let data = phoneNumber?.toDictionary()
//        if let str = data?["phone_number"] as? String {
//            phoneView.textField.text = str
//        }
    }
}

// MARK: User Interface
extension SmsAuthViewController {
    
    func addSubviews() {
        view.addSubview(phoneView)
        view.addSubview(codeView)
    }
    
    func adjustUI() {
        title = "SMS Authenticator".localized()
        view.backgroundColor = .backgroundAlternative
        
        let confirmItem = UIBarButtonItem(title: "Confirm".localized(), style: .plain, target: self, action: #selector(tapConfirm(_:)))
        navigationItem.rightBarButtonItem = confirmItem
    }
    
    func addConstraints() {
        
        phoneView.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(10)
            make.left.equalToSuperview().offset(11)
            make.right.equalToSuperview().offset(-11)
            make.height.equalTo(54)
        })
        
        codeView.snp.makeConstraints({ (make) in
            make.top.equalTo(phoneView.snp.bottom).offset(10)
            make.left.equalToSuperview().offset(11)
            make.right.equalToSuperview().offset(-11)
            make.height.equalTo(54)
        })
    }
    
    @objc func tapSendCode(_ sender: UIButton) {
        postSmsAuthOTP()
    }
    
    @objc func tapConfirm(_ sender: UIBarButtonItem) {
        setSmsAuth()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let prefixLimitLength = 6
        let numberLimitLength = 15
        switch textField {
        case phoneView.prefixTextField:
            if ((textField.text?.length)! + (string.length - range.length)) > prefixLimitLength {
                return false
            }
        return true
        case phoneView.numberTextField:
            if ((textField.text?.length)! + (string.length - range.length)) > numberLimitLength {
                return false
            }
        return true
        default:
            return true
        }
    }
}

// MARK: Networking
extension SmsAuthViewController {
    
    func postSmsAuthOTP() {
        
        var params: [String: String] = [:]
        params["phoneCode"] = "+\(phoneView.prefixTextField.text!)"
        params["mobile"] = phoneView.numberTextField.text!
        
        let networking = Networking()
        networking.postSmsAuthOTP(params: params, controller: self) { response in
            showSimpleToast("Please check your SMS for verification code".localized())
        }
    }
    
    func setSmsAuth() {
        
        var params: [String: String] = [:]
        params["phoneCode"] = "+\(phoneView.prefixTextField.text!)"
        params["mobile"] = phoneView.numberTextField.text!
        params["otp"] = codeView.textField.text!
        
        let networking = Networking()
        networking.setSmsAuth(params: params, controller: self) { resposne in
            showSimpleToast("SMS Authenticator is now enabled".localized())
            self.getUserInfo()
        }
    }
    
    func getUserInfo() {
        let networking = Networking()
        networking.getUserInfo(params: [:], controller: self) { user in
            
            Storage.save(key: Storage.Name.user, value: user)
            BaseSingleton.shared.user = user
            
            self.navigationBack()
        }
    }
}
