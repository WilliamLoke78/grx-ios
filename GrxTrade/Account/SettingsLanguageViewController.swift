//
//  SettingsLanguageViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 24/12/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

private let langCellId = "langCellId"

class SettingsLanguageViewController: BaseTableViewController {

    var langArray = [("en", "English"), ("zh-Hans", "简体中文"), ("zh-Hant", "繁體中文")]
    var selectedIndex: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.adjustUI()
        tableView.register(AccountSimpleTableViewCell.self, forCellReuseIdentifier: langCellId)
    }
}

// MARK: User Interface
extension SettingsLanguageViewController: UI {
    func addEvents() {
        
    }
    
    func addSubviews() {
        
    }
    
    func addConstraints() {
        
    }
    
    func configure<T>(model: T) {
        
    }
    
    
    func adjustUI() {
        self.title = "Language".localized().uppercased()
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .background
        tableView.separatorColor = .background
    }
}

// MARK: Table
extension SettingsLanguageViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return langArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: langCellId, for: indexPath) as! AccountSimpleTableViewCell
        
        cell.button.isEnabled = false
        cell.subLabel.isHidden = true
        cell.arrowImageView.isHidden = true
        
        cell.label.text = langArray[indexPath.row].1
        
        let allLang = langArray.compactMap {(a: String, b: String) -> String? in
            return a
        }
        var lang = LocalizationManager.userLang()
        if !allLang.contains(lang) {
            lang = "en"
        }
        if lang == langArray[indexPath.row].0 {
            cell.accessoryType = .checkmark
            selectedIndex = indexPath.row
        } else {
            cell.accessoryType = .none
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard selectedIndex != indexPath.row else {
            return
        }
        
        LocalizationManager.setUserLang(langArray[indexPath.row].0)
        UIApplication.shared.keyWindow?.rootViewController = TabBarViewController()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}
