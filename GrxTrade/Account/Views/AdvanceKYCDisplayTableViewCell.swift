//
//  AdvanceKYCDisplayTableViewCell.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 09/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import UIKit

class AdvanceKYCDisplayTableViewCell: BaseTableViewCell {
    
    lazy var label: TitleRightLabel = {
        let label = TitleRightLabel()
        return label
    }()
    
    lazy var content: ContentLeftLabel = {
        let label = ContentLeftLabel()
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(label)
        addSubview(content)
        
        label.snp.makeConstraints({ (make) in
            make.left.equalToSuperview()
            make.width.equalTo(screenWidth/3)
            make.centerY.equalToSuperview()
        })
        
        content.snp.makeConstraints({ (make) in
            make.top.equalTo(label)
            make.left.equalToSuperview().offset(screenWidth/3)
        })
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
