//
//  ResidentialDisplayView.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 09/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import Foundation

class ResidentialDisplayView: BaseView {
    
    lazy var buildingLabel: TitleRightLabel = {
        let label = TitleRightLabel()
        label.text = "Building".localized()
        return label
    }()
    
    lazy var buildingContent: ContentLeftLabel = {
        let label = ContentLeftLabel()
        label.text = "- -"
        return label
    }()
    
    lazy var streetLabel: TitleRightLabel = {
        let label = TitleRightLabel()
        label.text = "Street".localized()
        return label
    }()
    
    lazy var streetContent: ContentLeftLabel = {
        let label = ContentLeftLabel()
        label.text = "- -"
        return label
    }()
    
    lazy var cityLabel: TitleRightLabel = {
        let label = TitleRightLabel()
        label.text = "City".localized()
        return label
    }()
    
    lazy var cityContent: ContentLeftLabel = {
        let label = ContentLeftLabel()
        label.text = "- -"
        return label
    }()
    
    lazy var districtLabel: TitleRightLabel = {
        let label = TitleRightLabel()
        label.text = "District".localized()
        return label
    }()
    
    lazy var districtContent: ContentLeftLabel = {
        let label = ContentLeftLabel()
        label.text = "- -"
        return label
    }()
    
    lazy var zipCodeLabel: TitleRightLabel = {
        let label = TitleRightLabel()
        label.text = "Zip Code".localized()
        return label
    }()
    
    lazy var zipCodeContent: ContentLeftLabel = {
        let label = ContentLeftLabel()
        label.text = "- -"
        return label
    }()
    
    lazy var provinceLabel: TitleRightLabel = {
        let label = TitleRightLabel()
        label.text = "Province".localized()
        return label
    }()
    
    lazy var provinceContent: ContentLeftLabel = {
        let label = ContentLeftLabel()
        label.text = "- -"
        return label
    }()
    
    lazy var countryLabel: TitleRightLabel = {
        let label = TitleRightLabel()
        label.text = "Country".localized()
        return label
    }()
    
    lazy var countryContent: ContentLeftLabel = {
        let label = ContentLeftLabel()
        label.text = "- -"
        return label
    }()
    
    lazy var proofOfResidenceLabel: TitleRightLabel = {
        let label = TitleRightLabel()
        label.text = "Proof of Residential".localized()
        return label
    }()
    
    lazy var proofOfResidenceView: CameraPlaceholderView = {
        let view = CameraPlaceholderView()
        view.photoImageView.contentMode = .scaleToFill
        view.iconImageView.isHidden = true
        return view
    }()
    
    override func setupViews() {
        
        backgroundColor = .container
        
        addSubview(buildingLabel)
        addSubview(buildingContent)
        addSubview(streetLabel)
        addSubview(streetContent)
        addSubview(cityLabel)
        addSubview(cityContent)
        addSubview(districtLabel)
        addSubview(districtContent)
        addSubview(zipCodeLabel)
        addSubview(zipCodeContent)
        addSubview(provinceLabel)
        addSubview(provinceContent)
        addSubview(countryLabel)
        addSubview(countryContent)
        addSubview(proofOfResidenceLabel)
        addSubview(proofOfResidenceView)
        
        buildingLabel.snp.makeConstraints({ (make) in
            make.left.equalToSuperview()
            make.width.equalTo(screenWidth/3)
        })
        
        buildingContent.snp.makeConstraints({ (make) in
            make.top.equalTo(buildingLabel)
            make.left.equalToSuperview().offset((screenWidth/3)+5)
        })
        
        streetLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(buildingLabel.snp.bottom).offset(10)
            make.left.equalToSuperview()
            make.width.equalTo(screenWidth/3)
        })
        
        streetContent.snp.makeConstraints({ (make) in
            make.top.equalTo(streetLabel)
            make.left.equalTo(buildingContent)
        })
        
        cityLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(streetLabel.snp.bottom).offset(10)
            make.left.equalToSuperview()
            make.width.equalTo(screenWidth/3)
        })
        
        cityContent.snp.makeConstraints({ (make) in
            make.top.equalTo(cityLabel)
            make.left.equalTo(buildingContent)
        })
        
        districtLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(cityLabel.snp.bottom).offset(10)
            make.left.equalToSuperview()
            make.width.equalTo(screenWidth/3)
        })
        
        districtContent.snp.makeConstraints({ (make) in
            make.top.equalTo(districtLabel)
            make.left.equalTo(buildingContent)
        })
        
        zipCodeLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(districtLabel.snp.bottom).offset(10)
            make.left.equalToSuperview()
            make.width.equalTo(screenWidth/3)
        })
        
        zipCodeContent.snp.makeConstraints({ (make) in
            make.top.equalTo(zipCodeLabel)
            make.left.equalTo(buildingContent)
        })
        
        provinceLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(zipCodeLabel.snp.bottom).offset(10)
            make.left.equalToSuperview()
            make.width.equalTo(screenWidth/3)
        })
        
        provinceContent.snp.makeConstraints({ (make) in
            make.top.equalTo(provinceLabel)
            make.left.equalTo(buildingContent)
        })
        
        countryLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(provinceLabel.snp.bottom).offset(10)
            make.left.equalToSuperview()
            make.width.equalTo(screenWidth/3)
        })
        
        countryContent.snp.makeConstraints({ (make) in
            make.top.equalTo(countryLabel)
            make.left.equalTo(buildingContent)
        })
        
        proofOfResidenceLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(countryLabel.snp.bottom).offset(10)
            make.left.equalToSuperview()
            make.width.equalTo(screenWidth/3)
        })
        
        proofOfResidenceView.snp.makeConstraints({ (make) in
            make.top.equalTo(proofOfResidenceLabel)
            make.left.equalTo(buildingContent)
            make.height.equalTo(50)
            make.width.equalTo(50)
        })
    }
}
