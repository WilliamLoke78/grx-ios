//
//  InfoButtonView.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 10/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import UIKit

class InfoButtonView: BaseView {
    
    lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = .background
        return view
    }()
    
    lazy var button: UIButton = {
        let button = UIButton()
        button.setTitleColor(.primary, for: .normal)
        button.titleLabel?.font = UIFont(name: bold, size: 16)
        return button
    }()
    
    override func setupViews() {
     
        backgroundColor = .container
        roundCorners(corners: [.bottomLeft, .bottomRight], radius: 8)
        
        addSubview(lineView)
        addSubview(button)
        
        lineView.snp.makeConstraints({ (make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview().offset(11)
            make.right.equalToSuperview().offset(-11)
            make.height.equalTo(1)
        })
        
        button.snp.makeConstraints({ (make) in
            make.top.equalTo(lineView.snp.bottom).offset(10)
            make.bottom.equalToSuperview().offset(-13)
            make.centerX.equalToSuperview()
        })
    }
}
