//
//  InfoContainerView.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 30/12/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

class InfoContainerView: BaseView {
    
    lazy var iconImageView: UIImageView = {
        let iv = UIImageView()
        return iv
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: bold, size: 12)
        label.textColor = .grayColor
        return label
    }()
    
    lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: regular, size: 12)
        label.textColor = .grayColor
        label.numberOfLines = 2
        return label
    }()
    
//    lazy var lineView: UIView = {
//        let view = UIView()
//        view.backgroundColor = .background
//        return view
//    }()
//
//    lazy var button: UIButton = {
//        let button = UIButton()
//        button.setTitleColor(.primary, for: .normal)
//        button.titleLabel?.font = UIFont(name: bold, size: 16)
//        return button
//    }()
    
    override func setupViews() {
        
        backgroundColor = .container
        roundCorners(corners: [.topLeft, .topRight], radius: 8)
        
        addSubview(iconImageView)
        addSubview(titleLabel)
        addSubview(subtitleLabel)
//        addSubview(lineView)
//        addSubview(button)
        
        iconImageView.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(23)
            make.left.equalToSuperview().offset(35)
            make.height.equalTo(74)
            make.width.equalTo(74)
        })
        
        titleLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(iconImageView.snp.top)
            make.left.equalTo(iconImageView.snp.right).offset(19)
        })
        
        subtitleLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(1)
            make.left.equalTo(titleLabel.snp.left)
            make.right.equalToSuperview().offset(-17)
        })
//
//        lineView.snp.makeConstraints({ (make) in
//            make.top.equalTo(iconImageView.snp.bottom).offset(18)
//            make.left.equalToSuperview().offset(11)
//            make.right.equalToSuperview().offset(-11)
//            make.height.equalTo(1)
//        })
        
//        button.snp.makeConstraints({ (make) in
//            make.top.equalTo(lineView.snp.bottom).offset(10)
//            make.bottom.equalToSuperview().offset(-13)
//            make.centerX.equalToSuperview()
//        })
    }
}
