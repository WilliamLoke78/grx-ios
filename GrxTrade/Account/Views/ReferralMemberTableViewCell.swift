//
//  ReferralMemberTableViewCell.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 09/02/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import UIKit

class ReferralMemberTableViewCell: BaseTableViewCell {
    
    lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .container
        view.layer.cornerRadius = 4
        return view
    }()
    
    lazy var partnerLabel: UILabel = {
        let label = UILabel()
        label.textColor = .grayColor
        label.backgroundColor = .background
        label.font = UIFont(name: bold, size: 11)
        label.textAlignment = .center
        label.layer.cornerRadius = 2
        label.backgroundColor = .background
        label.layer.borderWidth = darkTheme ? 1 : 0
        label.layer.borderColor = darkTheme ? UIColor.grayColor.cgColor : UIColor.clear.cgColor
        return label
    }()
    
    lazy var affiliateLabel: UILabel = {
        let label = UILabel()
        label.textColor = .grayColor
        label.backgroundColor = .background
        label.font = UIFont(name: bold, size: 11)
        label.textAlignment = .center
        label.layer.cornerRadius = 2
        label.backgroundColor = .background
        label.layer.borderWidth = darkTheme ? 1 : 0
        label.layer.borderColor = darkTheme ? UIColor.grayColor.cgColor : UIColor.clear.cgColor
        return label
    }()
    
    lazy var datetimeLabel: UILabel = {
        let label = UILabel()
        label.textColor = .grayColor
        label.font = UIFont(name: regular, size: 12)
        return label
    }()
    
    lazy var separatorLine: UIView = {
        let view = UIView()
        view.backgroundColor = .background
        return view
    }()
    
    lazy var emailLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: bold, size: 14)
        return label
    }()
    
    lazy var idLabel: UILabel = {
        let label = UILabel()
        label.textColor = .grayColor
        label.font = UIFont(name: regular, size: 12)
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .background
        
        addSubview(containerView)
        containerView.addSubview(partnerLabel)
        containerView.addSubview(affiliateLabel)
        containerView.addSubview(datetimeLabel)
        containerView.addSubview(separatorLine)
        containerView.addSubview(emailLabel)
        containerView.addSubview(idLabel)
        
        containerView.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(11)
            make.left.equalToSuperview().offset(12)
            make.right.equalToSuperview().offset(-12)
            make.height.equalTo(100)
        })
        
        partnerLabel.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(11)
            make.left.equalToSuperview().offset(11)
            make.height.equalTo(20)
            make.width.equalTo(100)
        })
        
        affiliateLabel.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(11)
            make.left.equalTo(partnerLabel.snp.right).offset(10)
            make.height.equalTo(20)
            make.width.equalTo(100)
        })
        
        datetimeLabel.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(13)
            make.right.equalToSuperview().offset(-11)
        })
        
        separatorLine.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(40)
            make.left.equalToSuperview().offset(11)
            make.right.equalToSuperview().offset(-11)
            make.height.equalTo(1)
        })
        
        emailLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(separatorLine.snp.bottom).offset(10)
            make.left.equalToSuperview().offset(15)
        })
        
        idLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(emailLabel.snp.bottom)
            make.left.equalTo(emailLabel)
        })
    }
    
    func configure(_ member: ReferralMember) {
        
        var ptLabelString = String()
        
        switch member.ptLevel {
        case 1:
            ptLabelString = "Senior".localized()
        case 2:
            ptLabelString = "Top".localized()
        default:
            ptLabelString = "Normal".localized()
        }
        
        var afLabelString = String()
        
        switch member.afLevel {
        case 1:
            afLabelString = "Senior".localized()
        case 2:
            afLabelString = "Top".localized()
        default:
            afLabelString = "Normal".localized()
        }
        
        partnerLabel.text = "Partner".localized() + " \(ptLabelString)"
        affiliateLabel.text = "Affiliate".localized() + " \(afLabelString)"
        datetimeLabel.text = member.created?.serverToLocal()
        emailLabel.text = member.usrLogin
        idLabel.text = "\(member.id ?? 0)"
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
