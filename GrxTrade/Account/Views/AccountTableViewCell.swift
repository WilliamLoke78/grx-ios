//
//  AccountTableViewCell.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 16/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

let accountCellHeight: CGFloat = 50

class AccountTableViewCell: BaseTableViewCell {
    
    lazy var iconImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage.init(named: "loudspeaker")
        iv.setImageColor(color: .primary)
        return iv
    }()
    
    lazy var label: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: regular, size: 14)
        label.textColor = .textColor
        return label
    }()
    
    lazy var subLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: bold, size: 12)
        label.textColor = .primary
        return label
    }()
    
    lazy var arrowImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "arrow_right")
        return iv
    }()
    
    lazy var tickImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "tick")
        return iv
    }()
    
    fileprivate lazy var verifyAccountBannerView: BannerView = {
        let bv = BannerView(frame: CGRect(x: 0, y: 0, width: screenWidth - 22, height: 0))
        bv.imageView.image = UIImage(named: "verify_account")
        bv.titleLabel.text = "Verify Your Account".localized()
        bv.subtitleLabel.text = "Unlock more features now".localized()
        return bv
    }()
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .darkTheme, object: nil)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .container
        
        addSubview(iconImageView)
        addSubview(label)
        addSubview(subLabel)
        addSubview(arrowImageView)
        
        iconImageView.snp.makeConstraints({ (make) in
            make.left.equalTo(self.snp.left).offset(16)
            make.centerY.equalTo(self.snp.centerY)
        })
        
        label.snp.makeConstraints({ (make) in
            make.left.equalTo(self.snp.left).offset(56)
            make.centerY.equalTo(self.snp.centerY)
        })
        
        subLabel.snp.makeConstraints({ (make) in
            make.right.equalTo(arrowImageView.snp.left).offset(-10)
            make.centerY.equalTo(self.snp.centerY)
        })
        
        arrowImageView.snp.makeConstraints({ (make) in
            make.height.equalTo(40)
            make.width.equalTo(10)
            make.right.equalTo(self.snp.right).offset(-20)
            make.centerY.equalTo(self.snp.centerY)
        })
        
//        NotificationCenter.default.addObserver(self, selector: #selector(receivedResetThemeNotification(_:)), name: .darkTheme, object: nil)
    }
    
    func configure(model: CellModel) {
        
        iconImageView.image = UIImage(named: model.icon)
        label.text = model.title
        subLabel.text = model.subTitle
        arrowImageView.image = UIImage(named: "arrow_right")
        
        
        if model.tag == "banner" {
            backgroundColor = .background
            arrowImageView.isHidden = true
            iconImageView.isHidden = true
            selectionStyle = .none
            
            addSubview(verifyAccountBannerView)
            
            verifyAccountBannerView.snp.makeConstraints({ (make) in
                make.top.equalTo(self.snp.top)
                make.height.equalTo(90)
            })
            
        } else if model.tag == "empty" {
            backgroundColor = .background
            arrowImageView.isHidden = true
            iconImageView.isHidden = true
            selectionStyle = .none
        } else {
            backgroundColor = .container
            arrowImageView.isHidden = false
            iconImageView.isHidden = false
            selectionStyle = .default
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
//    override func resetTheme() {
//        backgroundColor = .container
//        label.textColor = .defaultText
//    }
    
}
