//
//  InvestmentFormView.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 30/12/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

class InvestmentFormView: BaseView {
    
    lazy var stockMarketLabel: UILabel = {
        let label = UILabel()
        label.text = "Have you invested stock market?".localized()
        return label
    }()
    
    lazy var stockMarketTextField: TextFieldView = {
        let tf = TextFieldView()
        return tf
    }()
    
    lazy var derivativeMarketLabel: UILabel = {
        let label = UILabel()
        label.text = "Have you understood derivative market?".localized()
        return label
    }()
    
    lazy var derivativeMarketTextField: TextFieldView = {
        let tf = TextFieldView()
        return tf
    }()
    
    lazy var blockchainAssetMarketLabel: UILabel = {
        let label = UILabel()
        label.text = "Have you understood blockchain asset market?".localized()
        return label
    }()
    
    lazy var blockchainAssetMarketTextField: TextFieldView = {
        let tf = TextFieldView()
        return tf
    }()
    
    lazy var totalAssetLabel: UILabel = {
        let label = UILabel()
        label.text = "Total Asset Expected Investment".localized()
        return label
    }()
    
    lazy var totalAssetTextField: TextFieldView = {
        let tf = TextFieldView()
        return tf
    }()
    
    lazy var proofOfAddressLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: regular, size: 12)
        label.textColor = .grayColor
        label.text = "Proof of Address".localized()
        return label
    }()
    
    lazy var proofOfAddressView: CameraPlaceholderView = {
        let view = CameraPlaceholderView()
        view.photoImageView.contentMode = .scaleToFill
        return view
    }()
    
    lazy var proofOfEmploymentLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: regular, size: 12)
        label.textColor = .grayColor
        label.text = "Proof of Address".localized()
        return label
    }()
    
    lazy var proofOfEmploymentView: CameraPlaceholderView = {
        let view = CameraPlaceholderView()
        view.photoImageView.contentMode = .scaleToFill
        return view
    }()
    
    lazy var saveButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .primary
        button.layer.cornerRadius = 4
        button.setTitle("Save".localized(), for: .normal)
        return button
    }()
    
    lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.layer.borderColor = UIColor.grayColor.cgColor
        button.layer.cornerRadius = 4
        button.layer.borderWidth = 1
        button.setTitle("Cancel".localized(), for: .normal)
        return button
    }()
    
    override func setupViews() {
    
        backgroundColor = .container
        
        addSubview(stockMarketLabel)
        addSubview(stockMarketTextField)
        addSubview(derivativeMarketLabel)
        addSubview(derivativeMarketTextField)
        addSubview(blockchainAssetMarketLabel)
        addSubview(blockchainAssetMarketTextField)
        addSubview(totalAssetLabel)
        addSubview(totalAssetTextField)
        addSubview(proofOfAddressLabel)
        addSubview(proofOfAddressView)
        addSubview(proofOfEmploymentLabel)
        addSubview(proofOfEmploymentView)
        addSubview(saveButton)
        addSubview(cancelButton)
        
        stockMarketLabel.snp.makeConstraints({ (make) in
            make.left.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-10)
        })
        
        stockMarketTextField.snp.makeConstraints({ (make) in
            make.top.equalTo(stockMarketLabel.snp.bottom).offset(5)
            make.left.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-10)
            make.height.equalTo(54)
        })
        
        derivativeMarketLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(stockMarketTextField.snp.bottom).offset(14)
            make.left.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-10)
        })
        
        derivativeMarketTextField.snp.makeConstraints({ (make) in
            make.top.equalTo(derivativeMarketLabel.snp.bottom).offset(5)
            make.left.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-10)
            make.height.equalTo(54)
        })
        
        blockchainAssetMarketLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(derivativeMarketTextField.snp.bottom).offset(14)
            make.left.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-10)
        })
        
        blockchainAssetMarketTextField.snp.makeConstraints({ (make) in
            make.top.equalTo(blockchainAssetMarketLabel.snp.bottom).offset(5)
            make.left.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-10)
            make.height.equalTo(54)
        })
        
        totalAssetLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(blockchainAssetMarketTextField.snp.bottom).offset(14)
            make.left.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-10)
        })
        
        totalAssetTextField.snp.makeConstraints({ (make) in
            make.top.equalTo(totalAssetLabel.snp.bottom).offset(5)
            make.left.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-10)
            make.height.equalTo(54)
        })
        
        proofOfAddressLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(totalAssetTextField.snp.bottom).offset(14)
            make.left.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-10)
        })
        
        proofOfAddressView.snp.makeConstraints({ (make) in
            make.top.equalTo(proofOfAddressLabel.snp.bottom).offset(5)
            make.left.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-10)
            make.height.equalTo(54)
        })
        
        proofOfEmploymentLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(proofOfAddressView.snp.bottom).offset(14)
            make.left.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-10)
        })
        
        proofOfEmploymentView.snp.makeConstraints({ (make) in
            make.top.equalTo(proofOfEmploymentLabel.snp.bottom).offset(5)
            make.left.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-10)
            make.height.equalTo(54)
        })
        
        saveButton.snp.makeConstraints({ (make) in
            make.top.equalTo(proofOfEmploymentView.snp.bottom).offset(22)
            make.height.equalTo(54)
        })
        
        cancelButton.snp.makeConstraints({ (make) in
            make.top.equalTo(saveButton.snp.bottom).offset(10)
            make.height.equalTo(54)
        })
    }
}
