//
//  AccountSimpleTableViewCell.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 24/12/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

let accountSimpleCellHeight: CGFloat = 50

class AccountSimpleTableViewCell: BaseTableViewCell {
    
    lazy var button: UIButton = {
        let button = UIButton()
        return button
    }()
    
    lazy var label: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: regular, size: 14)
        label.textColor = .textColor
        return label
    }()
    
    lazy var subLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: bold, size: 12)
        label.textColor = .grayColor
        label.text = "CNY"
        return label
    }()
    
    lazy var arrowImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "arrow_right")
        return iv
    }()
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .darkTheme, object: nil)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .container

        addSubview(button)
        button.addSubview(label)
        button.addSubview(subLabel)
        button.addSubview(arrowImageView)
        
        button.snp.makeConstraints({ (make) in
            make.top.left.right.bottom.equalToSuperview()
        })
        
        label.snp.makeConstraints({ (make) in
            make.centerY.equalTo(self.snp.centerY)
            make.left.equalTo(self.snp.left).offset(16)
        })
        
        subLabel.snp.makeConstraints({ (make) in
            make.centerY.equalTo(self.snp.centerY)
            make.right.equalTo(arrowImageView.snp.left).offset(-9)
        })
        
        arrowImageView.snp.makeConstraints({ (make) in
            make.centerY.equalTo(self.snp.centerY)
            make.height.equalTo(40)
            make.width.equalTo(10)
            make.right.equalTo(self.snp.right).offset(-20)
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
