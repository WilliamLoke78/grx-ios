//
//  AboutHeaderView.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 16/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

class AboutHeaderView: BaseView {
    
    lazy var logoImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "home")
        return iv
    }()
    
    lazy var brandLabel: UILabel = {
        let label = UILabel()
        label.text = "GRXTrade".uppercased()
        label.font = UIFont(name: bold, size: 16)
        label.textColor = .primary
        return label
    }()
    
    lazy var versionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: regular, size: 12)
        label.textColor = .textColor
        return label
    }()
    
    override func setupViews() {
        
        addSubview(logoImageView)
        addSubview(brandLabel)
        addSubview(versionLabel)
        
        logoImageView.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(30)
            make.centerX.equalToSuperview()
            make.height.equalTo(60)
            make.width.equalTo(60)
        })
        
        brandLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(logoImageView.snp.bottom).offset(4)
            make.centerX.equalToSuperview()
        })
        
        versionLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(brandLabel.snp.bottom).offset(2)
            make.centerX.equalToSuperview()
        })
    }
}
