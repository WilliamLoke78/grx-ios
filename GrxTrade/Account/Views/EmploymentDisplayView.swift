//
//  EmploymentDisplayView.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 09/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import Foundation

class EmploymentDisplayView: BaseView {
    
    lazy var employStatusLabel: TitleRightLabel = {
        let label = TitleRightLabel()
        label.text = "Employment Status".localized()
        return label
    }()
    
    lazy var employStatusContent: ContentLeftLabel = {
        let label = ContentLeftLabel()
        label.text = "- -"
        return label
    }()
    
    lazy var incomeLabel: TitleRightLabel = {
        let label = TitleRightLabel()
        label.text = "Income Source".localized()
        return label
    }()
    
    lazy var incomeContent: ContentLeftLabel = {
        let label = ContentLeftLabel()
        label.text = "- -"
        return label
    }()
    
    lazy var industryLabel: TitleRightLabel = {
        let label = TitleRightLabel()
        label.text = "Industry".localized()
        return label
    }()
    
    lazy var industryContent: ContentLeftLabel = {
        let label = ContentLeftLabel()
        label.text = "- -"
        return label
    }()
    
    lazy var jobTitleLabel: TitleRightLabel = {
        let label = TitleRightLabel()
        label.text = "Job Title".localized()
        return label
    }()
    
    lazy var jobTitleContent: ContentLeftLabel = {
        let label = ContentLeftLabel()
        label.text = "- -"
        return label
    }()
    
    lazy var companyNameLabel: TitleRightLabel = {
        let label = TitleRightLabel()
        label.text = "Company Name".localized()
        return label
    }()
    
    lazy var companyNameContent: ContentLeftLabel = {
        let label = ContentLeftLabel()
        label.text = "- -"
        return label
    }()
    
    lazy var buildingLabel: TitleRightLabel = {
        let label = TitleRightLabel()
        label.text = "Building".localized()
        return label
    }()
    
    lazy var buildingContent: ContentLeftLabel = {
        let label = ContentLeftLabel()
        label.text = "- -"
        return label
    }()
    
    lazy var streetLabel: TitleRightLabel = {
        let label = TitleRightLabel()
        label.text = "Street".localized()
        return label
    }()
    
    lazy var streetContent: ContentLeftLabel = {
        let label = ContentLeftLabel()
        label.text = "- -"
        return label
    }()
    
    lazy var cityLabel: TitleRightLabel = {
        let label = TitleRightLabel()
        label.text = "City".localized()
        return label
    }()
    
    lazy var cityContent: ContentLeftLabel = {
        let label = ContentLeftLabel()
        label.text = "- -"
        return label
    }()
    
    lazy var districtLabel: TitleRightLabel = {
        let label = TitleRightLabel()
        label.text = "District".localized()
        return label
    }()
    
    lazy var districtContent: ContentLeftLabel = {
        let label = ContentLeftLabel()
        label.text = "- -"
        return label
    }()
    
    lazy var zipCodeLabel: TitleRightLabel = {
        let label = TitleRightLabel()
        label.text = "Zip Code".localized()
        return label
    }()
    
    lazy var zipCodeContent: ContentLeftLabel = {
        let label = ContentLeftLabel()
        label.text = "- -"
        return label
    }()
    
    lazy var provinceLabel: TitleRightLabel = {
        let label = TitleRightLabel()
        label.text = "Province".localized()
        return label
    }()
    
    lazy var provinceContent: ContentLeftLabel = {
        let label = ContentLeftLabel()
        label.text = "- -"
        return label
    }()
    
    lazy var countryLabel: TitleRightLabel = {
        let label = TitleRightLabel()
        label.text = "Country".localized()
        return label
    }()
    
    lazy var countryContent: ContentLeftLabel = {
        let label = ContentLeftLabel()
        label.text = "- -"
        return label
    }()
    
    lazy var proofOfEmploymentLabel: TitleRightLabel = {
        let label = TitleRightLabel()
        label.text = "Proof of Income".localized()
        return label
    }()
    
    lazy var proofOfEmploymentView: CameraPlaceholderView = {
        let view = CameraPlaceholderView()
        view.photoImageView.contentMode = .scaleToFill
        view.iconImageView.isHidden = true
        return view
    }()
    
    override func setupViews() {
        
        backgroundColor = .container
        
        addSubview(employStatusLabel)
        addSubview(employStatusContent)
        addSubview(incomeLabel)
        addSubview(incomeContent)
        addSubview(industryLabel)
        addSubview(industryContent)
        addSubview(jobTitleLabel)
        addSubview(jobTitleContent)
        addSubview(companyNameLabel)
        addSubview(companyNameContent)
        addSubview(buildingLabel)
        addSubview(buildingContent)
        addSubview(streetLabel)
        addSubview(streetContent)
        addSubview(cityLabel)
        addSubview(cityContent)
        addSubview(districtLabel)
        addSubview(districtContent)
        addSubview(zipCodeLabel)
        addSubview(zipCodeContent)
        addSubview(provinceLabel)
        addSubview(provinceContent)
        addSubview(countryLabel)
        addSubview(countryContent)
        addSubview(proofOfEmploymentLabel)
        addSubview(proofOfEmploymentView)
        
        employStatusLabel.snp.makeConstraints({ (make) in
            make.left.equalToSuperview()
            make.width.equalTo(screenWidth/3)
        })
        
        employStatusContent.snp.makeConstraints({ (make) in
            make.top.equalTo(employStatusLabel)
            make.left.equalToSuperview().offset((screenWidth/3)+5)
        })
        
        incomeLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(employStatusLabel.snp.bottom).offset(10)
            make.left.equalToSuperview()
            make.width.equalTo(screenWidth/3)
        })
        
        incomeContent.snp.makeConstraints({ (make) in
            make.top.equalTo(incomeLabel)
            make.left.equalTo(employStatusContent)
        })
        
        industryLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(incomeLabel.snp.bottom).offset(10)
            make.left.equalToSuperview()
            make.width.equalTo(screenWidth/3)
        })
        
        industryContent.snp.makeConstraints({ (make) in
            make.top.equalTo(industryLabel)
            make.left.equalTo(employStatusContent)
        })
        
        jobTitleLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(industryLabel.snp.bottom).offset(10)
            make.left.equalToSuperview()
            make.width.equalTo(screenWidth/3)
        })
        
        jobTitleContent.snp.makeConstraints({ (make) in
            make.top.equalTo(jobTitleLabel)
            make.left.equalTo(employStatusContent)
        })
        
        companyNameLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(jobTitleLabel.snp.bottom).offset(10)
            make.left.equalToSuperview()
            make.width.equalTo(screenWidth/3)
        })
        
        companyNameContent.snp.makeConstraints({ (make) in
            make.top.equalTo(companyNameLabel)
            make.left.equalTo(employStatusContent)
        })
        
        buildingLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(companyNameLabel.snp.bottom).offset(10)
            make.left.equalToSuperview()
            make.width.equalTo(screenWidth/3)
        })
        
        buildingContent.snp.makeConstraints({ (make) in
            make.top.equalTo(buildingLabel)
            make.left.equalTo(employStatusContent)
        })
        
        streetLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(buildingLabel.snp.bottom).offset(10)
            make.left.equalToSuperview()
            make.width.equalTo(screenWidth/3)
        })
        
        streetContent.snp.makeConstraints({ (make) in
            make.top.equalTo(streetLabel)
            make.left.equalTo(employStatusContent)
        })
        
        cityLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(streetLabel.snp.bottom).offset(10)
            make.left.equalToSuperview()
            make.width.equalTo(screenWidth/3)
        })
        
        cityContent.snp.makeConstraints({ (make) in
            make.top.equalTo(cityLabel)
            make.left.equalTo(employStatusContent)
        })
        
        districtLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(cityLabel.snp.bottom).offset(10)
            make.left.equalToSuperview()
            make.width.equalTo(screenWidth/3)
        })
        
        districtContent.snp.makeConstraints({ (make) in
            make.top.equalTo(districtLabel)
            make.left.equalTo(employStatusContent)
        })
        
        zipCodeLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(districtLabel.snp.bottom).offset(10)
            make.left.equalToSuperview()
            make.width.equalTo(screenWidth/3)
        })
        
        zipCodeContent.snp.makeConstraints({ (make) in
            make.top.equalTo(zipCodeLabel)
            make.left.equalTo(employStatusContent)
        })
        
        provinceLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(zipCodeLabel.snp.bottom).offset(10)
            make.left.equalToSuperview()
            make.width.equalTo(screenWidth/3)
        })
        
        provinceContent.snp.makeConstraints({ (make) in
            make.top.equalTo(provinceLabel)
            make.left.equalTo(employStatusContent)
        })
        
        countryLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(provinceLabel.snp.bottom).offset(10)
            make.left.equalToSuperview()
            make.width.equalTo(screenWidth/3)
        })
        
        countryContent.snp.makeConstraints({ (make) in
            make.top.equalTo(countryLabel)
            make.left.equalTo(employStatusContent)
        })
        
        proofOfEmploymentLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(countryLabel.snp.bottom).offset(10)
            make.left.equalToSuperview()
            make.width.equalTo(screenWidth/3)
        })
        
        proofOfEmploymentView.snp.makeConstraints({ (make) in
            make.top.equalTo(proofOfEmploymentLabel)
            make.left.equalTo(buildingContent)
            make.height.equalTo(50)
            make.width.equalTo(50)
        })
    }
}
