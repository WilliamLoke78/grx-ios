//
//  AccountHeaderView.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 16/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

class AccountHeaderView: UIView {
    
    lazy var button: UIButton = {
        let button = UIButton()
        button.backgroundColor = .primary
        return button
    }()
    
    lazy var profileImageView: UIImageView = {
        let iv = UIImageView()
        iv.tintColor = .white
        iv.image = UIImage(named: "profile")
        return iv
    }()
    
    lazy var levelLabel: UILabel = {
        let label = UILabel()
        label.textColor = .blackTextColor
        label.font = UIFont(name: bold, size: 9)
        label.backgroundColor = .yellowColor
        label.layer.cornerRadius = 4
        label.layer.masksToBounds = true
        label.textAlignment = .center
        return label
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Login / Register".localized()
        label.font = UIFont(name: bold, size: 16)
        label.textColor = .white
        return label
    }()
    
    lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Welcome to GRXTrade".localized()
        label.font = UIFont(name: bold, size: 12)
        label.textColor = .grayColor
        return label
    }()
    
    lazy var arrowImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage.init(named: "arrow_right")
        return iv
    }()
    
    lazy var vipLabel: UILabel = {
        let label = UILabel()
        label.text = "VIP0"
        label.textColor = .white
        label.backgroundColor = .clear
        label.font = UIFont(name: bold, size: 12)
        label.textAlignment = .center
        label.layer.cornerRadius = 2
        label.layer.borderWidth = 1
        label.layer.borderColor = UIColor.white.cgColor
        return label
    }()
    
    lazy var tradeVolumeLabel: UILabel = {
        let label = UILabel()
        label.text = "0 USDT"
        label.textColor = .yellowColor
        label.font = UIFont(name: bold, size: 12)
        return label
    }()
        
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = .backgroundTheme
        
        self.addSubview(button)
        button.addSubview(profileImageView)
        button.addSubview(levelLabel)
        button.addSubview(titleLabel)
        button.addSubview(subtitleLabel)
        button.addSubview(vipLabel)
        button.addSubview(tradeVolumeLabel)
        button.addSubview(arrowImageView)
        
        button.snp.makeConstraints({ (make) in
            make.top.left.right.bottom.equalToSuperview()
        })
        
        profileImageView.snp.makeConstraints({ (make) in
            make.height.equalTo(44)
            make.width.equalTo(44)
            make.left.equalToSuperview().offset(12)
            make.centerY.equalToSuperview()
        })
        
        levelLabel.snp.makeConstraints({ (make) in
            make.bottom.equalTo(profileImageView.snp.bottom)
            make.right.equalTo(profileImageView.snp.right)
            make.height.equalTo(12)
            make.width.equalTo(28)
        })
        
        titleLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(profileImageView)
            make.left.equalTo(profileImageView.snp.right).offset(16)
        })
        
        arrowImageView.snp.makeConstraints({ (make) in
            make.height.equalTo(40)
            make.width.equalTo(10)
            make.right.equalToSuperview().offset(-20)
            make.centerY.equalToSuperview()
        })
        
        subtitleLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(3)
            make.left.equalTo(titleLabel.snp.left)
        })
        
        vipLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(subtitleLabel.snp.bottom).offset(3)
            make.left.equalTo(subtitleLabel)
            make.width.equalTo(40)
        })
        
        tradeVolumeLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(subtitleLabel.snp.bottom).offset(3)
            make.left.equalTo(vipLabel.snp.right).offset(10)
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
