//
//  CameraPlaceholderView.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 30/12/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

class CameraPlaceholderView: BaseView {

    let photoImageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        return iv
    }()
    
    let button: UIButton = {
        let button = UIButton()
        return button
    }()
    
    let iconImageView: UIView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "camera")
        return iv
    }()
    
    let label: UILabel = {
        let label = UILabel()
        label.textColor = .textColor
        label.font = UIFont(name: regular, size: 12)
        return label
    }()

    override func setupViews() {
        
        backgroundColor = .containerAlternative
        layer.cornerRadius = 4
        
        addSubview(button)
        button.addSubview(photoImageView)
        button.addSubview(iconImageView)
        button.addSubview(label)
        
        button.snp.makeConstraints({ (make) in
            make.height.equalToSuperview()
            make.width.equalToSuperview()
        })
        
        photoImageView.snp.makeConstraints({ (make) in
            make.edges.equalToSuperview()
            make.height.equalTo(200)
            make.width.equalTo(200)
        })
        
        iconImageView.snp.makeConstraints({ (make) in
            make.height.equalTo(55)
            make.width.equalTo(60)
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
        })
        
        label.snp.makeConstraints({ (make) in
            make.top.equalTo(iconImageView.snp.bottom).offset(12)
            make.centerX.equalToSuperview()
        })
    }
}

