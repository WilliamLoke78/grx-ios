//
//  ReferralView.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 30/12/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

class ReferralView: BaseView {

    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Your Referral Code".localized()
        label.textColor = .textColor
        label.font = UIFont(name: regular, size: 14)
        return label
    }()
    
    lazy var idView: UIView = {
        let view = UIView()
        view.backgroundColor = .containerAlternative
        view.layer.cornerRadius = 2
        return view
    }()
    
    lazy var idLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: bold, size: 14)
        label.textColor = .textColor
        return label
    }()
    
    lazy var copyButton: UIButton = {
        let button = UIButton()
        button.setTitle("Copy".localized(), for: .normal)
        button.setTitleColor(.primary, for: .normal)
        button.titleLabel!.font = UIFont(name: bold, size: 14)
//        button.addTarget(self, action: #selector(tapCopy(_:)), for: .touchUpInside)
        return button
    }()
    
    lazy var qrView: UIView = {
        let view = UIView()
        view.backgroundColor = .containerAlternative
        view.layer.cornerRadius = 4
        return view
    }()
    
    lazy var qrImageView: UIImageView = {
        let iv = UIImageView()
        return iv
    }()
    
//    @objc func tapCopy(_ sender: UIButton) {
//        UIPasteboard.general.string = idLabel.text
////        showSimpleToast("Referral code copied to clipboard".localized())
//    }
    
    override func setupViews() {
        
        addSubview(titleLabel)
        addSubview(idView)
        idView.addSubview(idLabel)
        idView.addSubview(copyButton)
        
        addSubview(qrView)
        qrView.addSubview(qrImageView)
        
        titleLabel.snp.makeConstraints({ (make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview()
        })
        
        idView.snp.makeConstraints({ (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(8)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(44)
        })
        
        idLabel.snp.makeConstraints({ (make) in
            make.left.equalToSuperview().offset(11)
            make.centerY.equalToSuperview()
        })
        
        copyButton.snp.makeConstraints({ (make) in
            make.right.equalToSuperview().offset(-11)
            make.centerY.equalToSuperview()
        })
        
        qrView.snp.makeConstraints({ (make) in
            make.top.equalTo(idView.snp.bottom).offset(10)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(264) // 180+20+44+20
        })
        
        qrImageView.snp.makeConstraints({ (make) in
            make.height.equalTo(117)
            make.width.equalTo(117)
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
        })
    }

}
