//
//  InstructionStepView.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 31/12/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

class InstructionStepView: BaseView {
    
    lazy var numberLabel: UILabel = {
        let size: CGFloat = 20
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: size, height: size))
        label.layer.cornerRadius = 0.5 * label.bounds.size.width
        label.clipsToBounds = true
        label.text = "1"
        label.textColor = .white
        label.backgroundColor = .primary
        label.font = UIFont(name: bold, size: 12)
        label.textAlignment = .center
        return label
    }()
    
    lazy var remarkLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: regular, size: 12)
        label.textColor = .grayColor
        label.text = "Search and download \"Google Authenticator\" at App Store.".localized()
        return label
    }()
    
    override func setupViews() {
        addSubview(numberLabel)
        addSubview(remarkLabel)
        
        numberLabel.snp.makeConstraints({ (make) in
            make.left.equalToSuperview()
            make.height.equalTo(20)
            make.width.equalTo(20)
        })
        
        remarkLabel.snp.makeConstraints({ (make) in
            make.left.equalTo(numberLabel.snp.right).offset(10)
            make.right.equalToSuperview()
        })
    }
}

