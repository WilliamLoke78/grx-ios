//
//  ResidentialFormView.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 30/12/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

class ResidentialFormView: BaseView {
    
//    lazy var addressTextField: TextFieldView = {
//        let tf = TextFieldView()
//        tf.label.text = "Address".localized()
//        return tf
//    }()
    
    lazy var buildingTextField: TextFieldView = {
        let tf = TextFieldView()
        tf.label.text = "Building (Optional)".localized()
        return tf
    }()
    
    lazy var streetTextField: TextFieldView = {
        let tf = TextFieldView()
        tf.label.text = "Street".localized()
        return tf
    }()
    
    lazy var cityTextField: TextFieldView = {
        let tf = TextFieldView()
        tf.label.text = "City".localized()
        return tf
    }()
    
    lazy var districtTextField: TextFieldView = {
        let tf = TextFieldView()
        tf.label.text = "District".localized()
        return tf
    }()
    
    lazy var zipCodeTextField: TextFieldView = {
        let tf = TextFieldView()
        tf.label.text = "Zip Code".localized()
        return tf
    }()
    
    lazy var provinceTextField: TextFieldView = {
        let tf = TextFieldView()
        tf.label.text = "Province".localized()
        return tf
    }()
    
    lazy var countryView: SelectFieldView = {
        let view = SelectFieldView()
        view.label.text = "Country".localized()
        return view
    }()
    
    lazy var proofOfResidenceLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: regular, size: 12)
        label.textColor = .grayColor
        label.text = "Proof of Residential".localized()
        return label
    }()
    
    lazy var proofOfResidenceView: CameraPlaceholderView = {
        let view = CameraPlaceholderView()
        view.photoImageView.contentMode = .scaleToFill
        return view
    }()
    
    lazy var saveButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .primary
        button.layer.cornerRadius = 4
        button.setTitle("Save".localized(), for: .normal)
        return button
    }()
    
    lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.layer.borderColor = UIColor.grayColor.cgColor
        button.setTitleColor(.grayColor, for: .normal)
        button.layer.cornerRadius = 4
        button.layer.borderWidth = 1
        button.setTitle("Cancel".localized(), for: .normal)
        return button
    }()
    
    override func setupViews() {
        
        backgroundColor = .container
        
//        addSubview(addressTextField)
        addSubview(buildingTextField)
        addSubview(streetTextField)
        addSubview(cityTextField)
        addSubview(districtTextField)
        addSubview(zipCodeTextField)
        addSubview(provinceTextField)
        addSubview(countryView)
        addSubview(proofOfResidenceLabel)
        addSubview(proofOfResidenceView)
        addSubview(saveButton)
        addSubview(cancelButton)
        
//        addressTextField.snp.makeConstraints({ (make) in
//            make.top.equalToSuperview()
//            make.left.equalToSuperview().offset(11)
//            make.right.equalToSuperview().offset(-11)
//            make.height.equalTo(54)
//        })
        
        buildingTextField.snp.makeConstraints({ (make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview().offset(11)
            make.right.equalToSuperview().offset(-11)
            make.height.equalTo(54)
        })
        
        streetTextField.snp.makeConstraints({ (make) in
            make.top.equalTo(buildingTextField.snp.bottom).offset(10)
            make.left.right.equalTo(buildingTextField)
            make.height.equalTo(54)
        })
        
        cityTextField.snp.makeConstraints({ (make) in
            make.top.equalTo(streetTextField.snp.bottom).offset(10)
            make.left.equalTo(buildingTextField)
            make.height.equalTo(54)
            make.width.equalTo((screenWidth-49)/2)
        })
        
        districtTextField.snp.makeConstraints({ (make) in
            make.top.equalTo(cityTextField)
            make.left.equalTo(cityTextField.snp.right).offset(5)
            make.height.equalTo(54)
            make.width.equalTo((screenWidth-49)/2)
        })
        
        zipCodeTextField.snp.makeConstraints({ (make) in
            make.top.equalTo(cityTextField.snp.bottom).offset(10)
            make.left.equalTo(buildingTextField)
            make.height.equalTo(54)
            make.width.equalTo((screenWidth-49)/2)
        })
        
        provinceTextField.snp.makeConstraints({ (make) in
            make.top.equalTo(cityTextField.snp.bottom).offset(10)
            make.left.equalTo(zipCodeTextField.snp.right).offset(5)
            make.height.equalTo(54)
            make.width.equalTo((screenWidth-49)/2)
        })
        
        countryView.snp.makeConstraints({ (make) in
            make.top.equalTo(zipCodeTextField.snp.bottom).offset(10)
            make.left.right.equalTo(buildingTextField)
            make.height.equalTo(58)
        })
        
        proofOfResidenceLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(countryView.snp.bottom).offset(14)
            make.left.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-10)
        })
        
        proofOfResidenceView.snp.makeConstraints({ (make) in
            make.top.equalTo(proofOfResidenceLabel.snp.bottom).offset(5)
            make.left.equalToSuperview().offset(10)
            make.height.equalTo(54)
            make.width.equalTo(54)
        })
        
        saveButton.snp.makeConstraints({ (make) in
            make.top.equalTo(proofOfResidenceView.snp.bottom).offset(10)
            make.left.right.equalTo(buildingTextField)
            make.height.equalTo(46)
        })
        
        cancelButton.snp.makeConstraints({ (make) in
            make.top.equalTo(saveButton.snp.bottom).offset(10)
            make.left.right.equalTo(buildingTextField)
            make.height.equalTo(46)
        })
    }
}
