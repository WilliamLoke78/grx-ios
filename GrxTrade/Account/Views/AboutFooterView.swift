//
//  AboutFooterView.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 16/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

class AboutFooterView: BaseView {
    
    let button: UIButton = {
        let button = UIButton()
        return button
    }()
    
    let termsLabel: UILabel = {
        let label = UILabel()
        label.text = "Terms of Use and Conditions".localized()
        label.textColor = .primary
        label.font = UIFont(name: bold, size: 12)
        return label
    }()
    
    let copyrightLabel: UILabel = {
        let label = UILabel()
        let year = Calendar.current.component(.year, from: Date())
        label.text = "All Rights Reserved.".localized(with: [String(describing: year)])
        label.textColor = .textColor
        label.font = UIFont(name: regular, size: 12)
        return label
    }()
    
    override func setupViews() {
        
        addSubview(button)
        button.addSubview(termsLabel)
        addSubview(copyrightLabel)
        
        button.snp.makeConstraints({ (make) in
            make.centerX.equalToSuperview()
            make.width.equalTo(150)
        })
        
        termsLabel.snp.makeConstraints({ (make) in
            make.centerX.equalToSuperview()
        })
        
        copyrightLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(termsLabel.snp.bottom)
            make.centerX.equalToSuperview()
        })
    }
}
