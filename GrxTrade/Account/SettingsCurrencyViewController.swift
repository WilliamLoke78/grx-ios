//
//  SettingsCurrencyViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 15/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import UIKit

private let currencyCellId = "currencyCellId"

class SettingsCurrencyViewController: BaseTableViewController {

    var currencyArray = BaseSingleton.shared.currencies!
    var selectedIndex: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.adjustUI()
        tableView.register(AccountSimpleTableViewCell.self, forCellReuseIdentifier: currencyCellId)
    }
}

// MARK: User Interface
extension SettingsCurrencyViewController {
        
    func adjustUI() {
        self.title = "Currency".localized()
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .background
        tableView.separatorColor = .background
    }
}

// MARK: Table
extension SettingsCurrencyViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currencyArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: currencyCellId, for: indexPath) as! AccountSimpleTableViewCell
        
        cell.button.isEnabled = false
        cell.subLabel.isHidden = true
        cell.arrowImageView.isHidden = true
        
        cell.label.text = "\(currencyArray[indexPath.row].abbreviation ?? "") \(currencyArray[indexPath.row].symbol ?? "")"
        
//        let allCurrencies = currencyArray.compactMap { (a: String,b: String) -> String? in
//            return a
//        }
        var currency = CurrencyManager.userCurrency()
//        if !allCurrency.contains(currency) {
//            currency = "USD"
//        }
        if currency == currencyArray[indexPath.row].abbreviation {
            cell.accessoryType = .checkmark
            selectedIndex = indexPath.row
        } else {
            cell.accessoryType = .none
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard selectedIndex != indexPath.row else {
            return
        }
        
        CurrencyManager.setUserCurrency(currencyArray[indexPath.row].abbreviation!)
        UIApplication.shared.keyWindow?.rootViewController = TabBarViewController()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

