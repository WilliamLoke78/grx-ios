//
//  ReferralViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 30/12/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit
import AVFoundation

class ReferralViewController: BaseViewController, QrDelegate {
    
    let networking = Networking()
    var shareLink: String?
    
    lazy var referralTextFieldView: TextFieldView = {
        let view = TextFieldView()
        view.isHidden = true
        view.label.text = "Your Invite Code".localized()
        view.lastButton.isHidden = true
        view.lastButton.setImage(UIImage(named: "scan"), for: .normal)
        view.lastButton.addTarget(self, action: #selector(tapScan(_:)), for: .touchUpInside)
        return view
    }()
    
    lazy var referralView: ReferralView = {
        let view = ReferralView()
        view.isHidden = true
//        view.idLabel.text = BaseSingleton.shared.user.id!
//        view.qrImageView.image = BaseSingleton.shared.user.id!.generateQRCode()
        view.copyButton.addTarget(self, action: #selector(tapCopy(_:)), for: .touchUpInside)
        return view
    }()
    
    lazy var noReferralButton: PrimaryButton = {
        let button = PrimaryButton()
        button.backgroundColor = .container
        button.layer.borderColor = UIColor.primary.cgColor
        button.layer.borderWidth = 1
        button.setTitle("No Referral".localized(), for: .normal)
        button.setTitleColor(.primary, for: .normal)
        button.titleLabel?.font = UIFont(name: bold, size: 16)
        button.isHidden = true
        button.addTarget(self, action: #selector(tapNoReferral(_:)), for: .touchUpInside)
        return button
    }()
    
    lazy var confirmButton: PrimaryButton = {
        let button = PrimaryButton()
        button.setTitle("Confirm".localized(), for: .normal)
        button.titleLabel?.font = UIFont(name: bold, size: 16)
        button.isHidden = true
        button.addTarget(self, action: #selector(tapSubmit(_:)), for: .touchUpInside)
        return button
    }()
    
    lazy var shareButton: PrimaryButton = {
        let button = PrimaryButton()
        button.setTitle("Share".localized(), for: .normal)
        button.isHidden = true
        button.addTarget(self, action: #selector(tapShare(_:)), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addSubviews()
        adjustUI()
        addConstraints()
        
        getReferralData()
    }

}

// MARK: User Interface
extension ReferralViewController {
    
    func addSubviews() {
        view.addSubview(referralTextFieldView)
        view.addSubview(referralView)
        view.addSubview(noReferralButton)
        view.addSubview(confirmButton)
        view.addSubview(shareButton)
    }
    
    func adjustUI() {
        title = "Referral".localized()
        view.backgroundColor = .backgroundAlternative
    }
    
    func addConstraints() {
        
        referralTextFieldView.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(10)
            make.left.equalToSuperview().offset(11)
            make.right.equalToSuperview().offset(-11)
            make.height.equalTo(54)
        })
        
//        referralView.snp.makeConstraints({ (make) in
//            make.top.equalTo(referralTextFieldView.snp.bottom).offset(10)
//            make.left.equalToSuperview().offset(11)
//            make.right.equalToSuperview().offset(-11)
//            make.height.equalTo(260)
//        })
        
        noReferralButton.snp.makeConstraints({ (make) in
            make.top.equalTo(referralTextFieldView.snp.bottom).offset(20)
            make.left.equalToSuperview().offset(11)
            make.height.equalTo(44)
            make.width.equalTo((screenWidth-31)/2)
        })
        
        confirmButton.snp.makeConstraints({ (make) in
            make.top.equalTo(noReferralButton)
            make.left.equalTo(noReferralButton.snp.right).offset(9)
            make.height.equalTo(noReferralButton)
            make.width.equalTo(noReferralButton)
        })
    }
    
    @objc func tapCopy(_ sender: UIButton) {
        UIPasteboard.general.string = referralView.idLabel.text
        showSimpleToast("Referral code has been copied to clipboard".localized())
    }
}

// MARK: Networking
extension ReferralViewController {
    
    func getReferralData() {

        networking.getReferralCode(params: [:], controller: self) { referral in
            
//            print("referral: \(referral.referral)")
            
            if referral.referral != nil { // Referral already set
//                self.referralTextFieldView.textField.isEnabled = false
//                self.referralTextFieldView.lastButton.isHidden = true
//                self.referralTextFieldView.textField.text = referral.referral
                
//                self.navigationItem.rightBarButtonItem = nil
                
                self.noReferralButton.isHidden = true
                self.confirmButton.isHidden = true
                
                if referral.referral == "" { // Referral bind to company
                    self.referralTextFieldView.isHidden = true
                    self.referralView.isHidden = false
                    
                    self.referralView.snp.remakeConstraints({ (make) in
                        make.top.equalToSuperview().offset(10)
                        make.left.equalToSuperview().offset(11)
                        make.right.equalToSuperview().offset(-11)
                        make.height.equalTo(260)
                    })
                    
                } else { // Referral bind to other user
                    
                    self.referralTextFieldView.textField.isEnabled = false
                    self.referralTextFieldView.lastButton.isHidden = true
                    self.referralTextFieldView.textField.text = referral.referral
                    self.referralTextFieldView.isHidden = false
                    self.referralView.isHidden = false
                    
                    self.referralView.snp.remakeConstraints({ (make) in
                        make.top.equalTo(self.referralTextFieldView.snp.bottom).offset(10)
                        make.left.equalToSuperview().offset(11)
                        make.right.equalToSuperview().offset(-11)
                        make.height.equalTo(260)
                    })
                }
                
                self.shareButton.snp.remakeConstraints({ (make) in
                    make.top.equalTo(self.referralView.snp.bottom).offset(20)
                    make.centerX.equalToSuperview()
                    make.height.equalTo(44)
                    make.width.equalTo(180)
                })
                
                let membersItem = UIBarButtonItem(title: "Members".localized(), style: .plain, target: self, action: #selector(self.tapMembers(_:)))
                self.navigationItem.rightBarButtonItem = membersItem
                
                self.shareButton.isHidden = false
                self.shareLink = "\(self.networking.staticHost)/register?inviteCode=\(referral.invite ?? "")"
                
            } else { // Referral yet to be set

                self.referralTextFieldView.textField.isEnabled = true
                self.referralTextFieldView.lastButton.isHidden = false
                self.referralTextFieldView.isHidden = false

                self.noReferralButton.isHidden = false
                self.confirmButton.isHidden = false
                self.referralView.isHidden = true
                self.shareButton.isHidden = true
            }
            
            self.referralView.idLabel.text = referral.invite
            self.referralView.qrImageView.image = "\(self.networking.staticHost)/register?inviteCode=\(referral.invite ?? "")".generateQRCode()
        }
    }
}

// MARK: Action
extension ReferralViewController {
    
    @objc func tapNoReferral(_ sender: UIButton) {
        networking.postSetDefaultReferral(params: [:], controller: self) { response in
            print("set default referral: \(response)")
        }
    }
    
    @objc func tapSubmit(_ sender: UIBarButtonItem) {
        
        var params: [String: String] = [:]
        params["refCode"] = referralTextFieldView.textField.text
        
        networking.postReferralCode(params: params, controller: self) { response in
            self.getReferralData()
        }
    }
    
    @objc private func tapScan(_ sender: UIButton) {
        let vc = ScanViewController()
        vc.modalPresentationStyle = .fullScreen
        vc.delegate = self
        
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized:
            
            present(vc, animated: true, completion: nil)
            
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { granted in
                if granted {
                    DispatchQueue.main.async {
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            }
            
        case .denied:
            let alert = UIAlertController(title: "Camera".localized(), message: "Camera is needed to scan QR code".localized(), preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
                } else {
                    // Fallback on earlier versions
                }
            }))
            
            self.present(alert, animated: true)
            return
            
        case .restricted:
            showSimpleToast("Camera is needed to scan QR code".localized())
            return
        }
    }
    
    @objc func tapShare(_ sender: UIButton) {
        if let link = NSURL(string: shareLink ?? "") {
            let vc = UIActivityViewController(activityItems: [link], applicationActivities: [])
            present(vc, animated: true)
        }
    }
    
    @objc func tapMembers(_ sender: UIBarButtonItem) {
        let vc = ReferralMembersTableViewController()
        show(vc, sender: nil)
    }
    
    func backFromCamera(string: String) {
        referralTextFieldView.textField.text = string.replacingOccurrences(of: "\(self.networking.staticHost)/register?inviteCode=", with: "")
    }
}
