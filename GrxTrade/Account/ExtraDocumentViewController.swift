//
//  ExtraDocumentViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 09/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import UIKit
import Presentr
import AVFoundation
import Alamofire
import SwiftyJSON
import CountryPickerView
import Kingfisher

class ExtraDocumentViewController: BaseViewController {
    
    var advanceKYCInfo: AdvanceKYCInfo?
    var scrollViewHeight: CGFloat = screenHeight
    
    let jobless = ["2", "3", "5"]
    var jobStatus: [OptionData] = []
    var revenueSources: [OptionData] = []
    var industry: [OptionData] = []
    
    let networking = Networking()
    
    let imagePicker =  UIImagePickerController()
    var proofImage: UIImage?
    
    var isResidenceImage: Bool = false
    var isResidenceCountry: Bool = false
    var isEmploymentImage: Bool = false
    var isEmploymentCountry: Bool = false
    
    let activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
    
    lazy var cpv = CountryPickerView(frame: CGRect(x: 0, y: 0, width: screenWidth-22, height: 20))
    
    lazy var residentialView: InfoContainerView = {
        let view = InfoContainerView()
        view.iconImageView.image = UIImage(named: "residential")
        view.titleLabel.text = "Residential Information".localized()
        view.subtitleLabel.text = "Fill up your Residential Information for secure purpose".localized()
        return view
    }()
    
    lazy var residentialFormView: ResidentialFormView = {
        let view = ResidentialFormView()
        view.isHidden = true
        view.countryView.blockButton.addTarget(self, action: #selector(tapSelectResidenceCountry(_:)), for: .touchUpInside)
        view.countryView.backgroundColor = .background
        view.proofOfResidenceView.button.addTarget(self, action: #selector(tapResidenceImage(_:)), for: .touchUpInside)
        view.saveButton.addTarget(self, action: #selector(tapSaveResident(_:)), for: .touchUpInside)
        view.cancelButton.addTarget(self, action: #selector(tapCancelResidential(_:)), for: .touchUpInside)
        return view
    }()
    
    lazy var residentialDisplayView: ResidentialDisplayView = {
        let view = ResidentialDisplayView()
        view.isHidden = true
        return view
    }()
    
    lazy var editResidentialButton: InfoButtonView = {
        let view = InfoButtonView()
        view.button.setTitleColor(.primary, for: .normal)
        view.button.titleLabel?.font = UIFont(name: bold, size: 16)
        view.button.setTitle("Edit Residential Info".localized(), for: .normal)
        view.button.addTarget(self, action: #selector(tapResidential(_:)), for: .touchUpInside)
        return view
    }()

    lazy var employmentView: InfoContainerView = {
        let view = InfoContainerView()
        view.iconImageView.image = UIImage(named: "employment")
        view.titleLabel.text = "Employment Information".localized()
        view.subtitleLabel.text = "Fill up your Employment Information for secure purpose".localized()
        return view
    }()
    
    lazy var employmentDisplayView: EmploymentDisplayView = {
        let view = EmploymentDisplayView()
        view.isHidden = true
        return view
    }()
    
    lazy var employmentFormView: EmploymentFormView = {
        let view = EmploymentFormView()
        view.isHidden = true
        view.employmentStatusTextField.button.addTarget(self, action: #selector(tapEmploymentStatus(_:)), for: .touchUpInside)
        
        view.incomeSourcesTextField.button.addTarget(self, action: #selector(tapRevenueSource(_:)), for: .touchUpInside)
        view.incomeSourcesTextField.lastButton.addTarget(self, action: #selector(tapRevenueSource(_:)), for: .touchUpInside)
        
        view.industryTextField.button.addTarget(self, action: #selector(tapIndustry(_:)), for: .touchUpInside)
        view.industryTextField.lastButton.addTarget(self, action: #selector(tapIndustry(_:)), for: .touchUpInside)
        
        view.countryView.blockButton.addTarget(self, action: #selector(tapSelectEmploymentCountry(_:)), for: .touchUpInside)
        view.proofOfEmploymentView.button.addTarget(self, action: #selector(tapEmploymentImage(_:)), for: .touchUpInside)
        
        view.saveButton.addTarget(self, action: #selector(tapSaveEmployment(_:)), for: .touchUpInside)
        view.cancelButton.addTarget(self, action: #selector(tapCancelEmployment(_:)), for: .touchUpInside)
        return view
    }()
    
    lazy var editEmploymentButton: InfoButtonView = {
        let view = InfoButtonView()
        view.button.setTitleColor(.primary, for: .normal)
        view.button.titleLabel?.font = UIFont(name: bold, size: 16)
        view.button.setTitle("Edit Employment Info".localized(), for: .normal)
        view.button.addTarget(self, action: #selector(tapEmployment(_:)), for: .touchUpInside)
        return view
    }()
    
    lazy var scrollView: UIScrollView = {
        let v = UIScrollView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.contentSize = CGSize(width: screenWidth, height: screenHeight + 500)
        return v
    }()
    
    override func viewDidLoad() {
        
        addSubviews()
        adjustUI()
        addConstraints()
        
        getAdvanceKYCInfo()
        getJobStatusOptions()
        getRevenueSourceOptions()
        getIndustryOptions()
        
        hideKeyboardWhenTappedAround()
        
        cpv.delegate = self
        cpv.dataSource = self
        
//        residentialFormView.countryView.textField.leftView = cpv
//        residentialFormView.countryView.textField.leftViewMode = .always
        residentialFormView.countryView.textField.text = cpv.selectedCountry.localizedName()
        
//        employmentFormView.countryView.textField.leftView = cpv
//        employmentFormView.countryView.textField.leftViewMode = .always
        employmentFormView.countryView.textField.text = cpv.selectedCountry.localizedName()
    }
}

// MARK: UI
extension ExtraDocumentViewController {
    
    func addSubviews() {
        view.addSubview(scrollView)
        view.addSubview(activityIndicator)
        scrollView.addSubview(residentialView)
        scrollView.addSubview(residentialDisplayView)
        scrollView.addSubview(residentialFormView)
        scrollView.addSubview(editResidentialButton)
        scrollView.addSubview(employmentView)
        scrollView.addSubview(employmentDisplayView)
        scrollView.addSubview(employmentFormView)
        scrollView.addSubview(editEmploymentButton)
        
    }
    
    func adjustUI() {
        title = "Level 2".localized()
        view.backgroundColor = .background
        
        let submitItem = UIBarButtonItem(title: "Submit".localized(), style: .plain, target: self, action: #selector(tapSubmit(_:)))
        navigationItem.rightBarButtonItem = submitItem
    }
    
    func addConstraints() {
        
        activityIndicator.snp.makeConstraints({ (make) in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
        })
        
        scrollView.snp.makeConstraints({ (make) in
            make.edges.equalToSuperview()
        })
        
        residentialView.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(20)
            make.left.equalTo(view).offset(11)
            make.right.equalTo(view).offset(-11)
            make.height.equalTo(130)
        })
        
        residentialDisplayView.snp.makeConstraints({ (make) in
            make.top.equalTo(residentialView.snp.bottom)
            make.left.right.equalTo(residentialView)
            make.height.equalTo(0)
        })
        
        residentialFormView.snp.makeConstraints({ (make) in
            make.top.equalTo(residentialDisplayView.snp.bottom)
            make.left.right.equalTo(residentialView)
            make.height.equalTo(0)
        })
        
        editResidentialButton.snp.makeConstraints({ (make) in
            make.top.equalTo(residentialFormView.snp.bottom)
            make.left.right.equalTo(residentialView)
        })

        employmentView.snp.makeConstraints({ (make) in
            make.top.equalTo(editResidentialButton.snp.bottom).offset(20)
            make.left.right.equalTo(residentialView)
            make.height.equalTo(130)
        })
        
        employmentDisplayView.snp.makeConstraints({ (make) in
            make.top.equalTo(employmentView.snp.bottom)
            make.left.right.equalTo(employmentView)
            make.height.equalTo(0)
        })
        
        employmentFormView.snp.makeConstraints({ (make) in
            make.top.equalTo(employmentDisplayView.snp.bottom)
            make.left.right.equalTo(residentialView)
            make.height.equalTo(0)
        })
        
        editEmploymentButton.snp.makeConstraints({ (make) in
            make.top.equalTo(employmentFormView.snp.bottom)
            make.left.right.equalTo(residentialView)
        })
    }
}

// MARK: Employment Action
extension ExtraDocumentViewController {
    
    @objc func tapEmploymentStatus(_ sender: UIButton) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.view.subviews.first?.subviews.first?.subviews.first?.backgroundColor = .container
        alert.view.subviews.first?.subviews.first?.subviews.last?.backgroundColor = .container
        alert.view.tintColor = .grayColor
        
        for elem in jobStatus {
            
            alert.addAction(UIAlertAction(title: elem.display, style: .default , handler:{ (UIAlertAction) in
                self.updateJobStatus(jobStatus: elem)
            }))
        }
        
        alert.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    func updateJobStatus(jobStatus: OptionData) {
        employmentFormView.employmentStatusTextField.textField.text = jobStatus.display
        employmentFormView.employmentStatusTextField.valueField.text = jobStatus.value
        
        if jobless.contains(jobStatus.value!) {
            updateInputLayout(hasCompany: false)
        } else {
            updateInputLayout(hasCompany: true)
        }
    }
    
    @objc func tapRevenueSource(_ sender: UIButton) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.view.subviews.first?.subviews.first?.subviews.first?.backgroundColor = .container
        alert.view.subviews.first?.subviews.first?.subviews.last?.backgroundColor = .container
        alert.view.tintColor = .grayColor
        
        for elem in revenueSources {
            
            alert.addAction(UIAlertAction(title: elem.display, style: .default , handler:{ (UIAlertAction) in
                self.updateRevenueSource(revenueSource: elem)
            }))
        }
        
        alert.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    func updateRevenueSource(revenueSource: OptionData) {
        
        employmentFormView.incomeSourcesTextField.valueField.text = revenueSource.value
        
        if revenueSource.value == "5" {
            
            employmentFormView.incomeSourcesTextField.button.isHidden = true
            employmentFormView.incomeSourcesTextField.textField.text = ""
            
            employmentFormView.incomeSourcesTextField.textField.becomeFirstResponder()
            
            return
        }
        
        employmentFormView.incomeSourcesTextField.button.isHidden = false
        
        employmentFormView.incomeSourcesTextField.textField.text = revenueSource.display
    }
    
    @objc func tapIndustry(_ sender: UIButton) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.view.subviews.first?.subviews.first?.subviews.first?.backgroundColor = .container
        alert.view.subviews.first?.subviews.first?.subviews.last?.backgroundColor = .container
        alert.view.tintColor = .grayColor
        
        for elem in industry {
            
            alert.addAction(UIAlertAction(title: elem.display, style: .default , handler:{ (UIAlertAction) in
                self.updateIndustry(industry: elem)
            }))
        }
        
        alert.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    @objc func tapSelectResidenceCountry(_ sender: UIButton) {
        isResidenceCountry = true
        isEmploymentCountry = false
        cpv.showCountriesList(from: self)
    }
    
    @objc func tapSelectEmploymentCountry(_ sender: UIButton) {
        isResidenceCountry = false
        isEmploymentCountry = true
        cpv.showCountriesList(from: self)
    }
    
    func updateIndustry(industry: OptionData) {
        employmentFormView.industryTextField.valueField.text = industry.value
        
        if industry.value == "20" {
            employmentFormView.industryTextField.button.isHidden = true
            employmentFormView.industryTextField.textField.text = ""
            employmentFormView.industryTextField.textField.becomeFirstResponder()
            return
        }
        
        employmentFormView.industryTextField.button.isHidden = false
        employmentFormView.industryTextField.textField.text = industry.display
    }
    
    func updateInputLayout(hasCompany: Bool) {
        
        if hasCompany == true {
            employmentFormView.industryTextField.isHidden = false
            employmentFormView.jobTitleTextField.isHidden = false
            employmentFormView.companyNameTextField.isHidden = false
            employmentFormView.buildingTextField.isHidden = false
            employmentFormView.streetTextField.isHidden = false
            employmentFormView.zipCodeTextField.isHidden = false
            employmentFormView.provinceTextField.isHidden = false
            employmentFormView.cityTextField.isHidden = false
            employmentFormView.districtTextField.isHidden = false
            employmentFormView.countryView.isHidden = false
            
            employmentFormView.industryTextField.snp.updateConstraints({ (make) in
                make.height.equalTo(54)
            })
            
            employmentFormView.jobTitleTextField.snp.updateConstraints({ (make) in
                make.height.equalTo(54)
            })
            
            employmentFormView.companyNameTextField.snp.updateConstraints({ (make) in
                make.height.equalTo(54)
            })
            
            employmentFormView.buildingTextField.snp.updateConstraints({ (make) in
                make.height.equalTo(54)
            })
            
            employmentFormView.streetTextField.snp.updateConstraints({ (make) in
                make.height.equalTo(54)
            })
            
            employmentFormView.zipCodeTextField.snp.updateConstraints({ (make) in
                make.height.equalTo(54)
            })
            
            employmentFormView.provinceTextField.snp.updateConstraints({ (make) in
                make.height.equalTo(54)
            })
            
            employmentFormView.cityTextField.snp.updateConstraints({ (make) in
                make.height.equalTo(54)
            })
            
            employmentFormView.districtTextField.snp.updateConstraints({ (make) in
                make.height.equalTo(54)
            })
            
            employmentFormView.countryView.snp.updateConstraints({ (make) in
                make.height.equalTo(54)
            })
            
            employmentFormView.snp.updateConstraints({ (make) in
                make.height.equalTo(900)
            })
            
            scrollViewHeight = scrollViewHeight + CGFloat(600)
            scrollView.contentSize = CGSize(width: screenWidth, height: scrollViewHeight)
            
            return
        }
        
        if hasCompany == false {
            employmentFormView.industryTextField.isHidden = true
            employmentFormView.jobTitleTextField.isHidden = true
            employmentFormView.companyNameTextField.isHidden = true
            employmentFormView.buildingTextField.isHidden = true
            employmentFormView.streetTextField.isHidden = true
            employmentFormView.zipCodeTextField.isHidden = true
            employmentFormView.provinceTextField.isHidden = true
            employmentFormView.cityTextField.isHidden = true
            employmentFormView.districtTextField.isHidden = true
            employmentFormView.countryView.isHidden = true
            
            employmentFormView.industryTextField.snp.updateConstraints({ (make) in
                make.height.equalTo(0)
            })
            
            employmentFormView.jobTitleTextField.snp.updateConstraints({ (make) in
                make.height.equalTo(0)
            })
            
            employmentFormView.companyNameTextField.snp.updateConstraints({ (make) in
                make.height.equalTo(0)
            })
            
            employmentFormView.buildingTextField.snp.updateConstraints({ (make) in
                make.height.equalTo(0)
            })
            
            employmentFormView.streetTextField.snp.updateConstraints({ (make) in
                make.height.equalTo(0)
            })
            
            employmentFormView.zipCodeTextField.snp.updateConstraints({ (make) in
                make.height.equalTo(0)
            })
            
            employmentFormView.provinceTextField.snp.updateConstraints({ (make) in
                make.height.equalTo(0)
            })
            
            employmentFormView.cityTextField.snp.updateConstraints({ (make) in
                make.height.equalTo(0)
            })
            
            employmentFormView.districtTextField.snp.updateConstraints({ (make) in
                make.height.equalTo(0)
            })
            
            employmentFormView.countryView.snp.updateConstraints({ (make) in
                make.height.equalTo(0)
            })
            
            return
        }
    }
}

// MARK: Action
extension ExtraDocumentViewController {
    
    @objc func tapSubmit(_ sender: UIBarButtonItem) {
        networking.postSubmitKYC(params: [:], controller: self) { response in
            print("response: \(response)")
            
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func tapResidential(_ sender: UIButton) {
        
        editResidentialButton.isHidden = true
        residentialDisplayView.isHidden = true
        residentialFormView.isHidden = false
        
        residentialFormView.buildingTextField.textField.text = advanceKYCInfo?.r?.building ?? ""
        residentialFormView.streetTextField.textField.text = advanceKYCInfo?.r?.street ?? ""
        residentialFormView.cityTextField.textField.text = advanceKYCInfo?.r?.city ?? ""
        residentialFormView.districtTextField.textField.text = advanceKYCInfo?.r?.district ?? ""
        residentialFormView.zipCodeTextField.textField.text = advanceKYCInfo?.r?.zipCode ?? ""
        residentialFormView.provinceTextField.textField.text = advanceKYCInfo?.r?.province ?? ""
        
        let authImgUrl1 = URL(string: advanceKYCInfo?.r?.authImgUrl1Display ?? "")
        residentialFormView.proofOfResidenceView.photoImageView.kf.setImage(with: authImgUrl1)
        
        residentialDisplayView.snp.remakeConstraints({ (make) in
            make.top.equalTo(residentialView.snp.bottom)
            make.left.right.equalTo(residentialView)
            make.height.equalTo(0)
        })
        
        residentialFormView.snp.remakeConstraints({ (make) in
            make.top.equalTo(residentialDisplayView.snp.bottom)
            make.left.right.equalTo(residentialView)
            make.height.equalTo(600)
        })
        
        scrollViewHeight = scrollViewHeight + CGFloat(600)
        scrollView.contentSize = CGSize(width: screenWidth, height: scrollViewHeight)
    }
    
    @objc func tapCancelResidential(_ sender: UIButton) {
        
        editResidentialButton.isHidden = false
        
        if advanceKYCInfo?.r != nil {
            residentialDisplayView.isHidden = false
            residentialDisplayView.snp.remakeConstraints({ (make) in
                make.top.equalTo(residentialView.snp.bottom)
                make.left.right.equalTo(residentialView)
                make.height.equalTo(250)
            })
        }
        
        residentialFormView.isHidden = true
        
        residentialFormView.snp.remakeConstraints({ (make) in
            make.top.equalTo(residentialDisplayView.snp.bottom)
            make.left.right.equalTo(residentialView)
            make.height.equalTo(0)
        })
    }
    
    @objc func tapEmployment(_ sender: UIButton) {
        
        editEmploymentButton.isHidden = true
        employmentDisplayView.isHidden = true
        employmentFormView.isHidden = false
        
        if jobless.contains(advanceKYCInfo?.e?.employStatus ?? "0") {
            employmentFormView.employmentStatusTextField.textField.text = advanceKYCInfo?.e?.employStatusName
            employmentFormView.employmentStatusTextField.valueField.text = advanceKYCInfo?.e?.employStatus
            employmentFormView.incomeSourcesTextField.textField.text = advanceKYCInfo?.e?.sourceIncomeName
            employmentFormView.incomeSourcesTextField.valueField.text = advanceKYCInfo?.e?.sourceIncome
            updateInputLayout(hasCompany: false)
        } else {
            employmentFormView.employmentStatusTextField.textField.text = advanceKYCInfo?.e?.employStatusName
            employmentFormView.employmentStatusTextField.valueField.text = advanceKYCInfo?.e?.employStatus
            employmentFormView.incomeSourcesTextField.textField.text = advanceKYCInfo?.e?.sourceIncomeName
            employmentFormView.incomeSourcesTextField.valueField.text = advanceKYCInfo?.e?.sourceIncome
            employmentFormView.industryTextField.textField.text = advanceKYCInfo?.e?.industryName
            employmentFormView.industryTextField.valueField.text = advanceKYCInfo?.e?.industry
            employmentFormView.jobTitleTextField.textField.text = advanceKYCInfo?.e?.jobTitle ?? ""
            employmentFormView.companyNameTextField.textField.text = advanceKYCInfo?.e?.companyName ?? ""
            employmentFormView.buildingTextField.textField.text = advanceKYCInfo?.e?.employerBuilding ?? ""
            employmentFormView.streetTextField.textField.text = advanceKYCInfo?.e?.employerStreet ?? ""
            employmentFormView.cityTextField.textField.text = advanceKYCInfo?.e?.employerCity ?? ""
            employmentFormView.districtTextField.textField.text = advanceKYCInfo?.e?.employerDistrict ?? ""
            employmentFormView.zipCodeTextField.textField.text = advanceKYCInfo?.e?.employerZipCode ?? ""
            employmentFormView.provinceTextField.textField.text = advanceKYCInfo?.e?.employerProvince ?? ""
            updateInputLayout(hasCompany: true)
        }
        
        let authImgUrl2 = URL(string: advanceKYCInfo?.e?.authImgUrl2Display ?? "")
        employmentFormView.proofOfEmploymentView.photoImageView.kf.setImage(with: authImgUrl2)
        
        employmentDisplayView.snp.remakeConstraints({ (make) in
            make.top.equalTo(employmentView.snp.bottom)
            make.left.right.equalTo(employmentView)
            make.height.equalTo(0)
        })
        
        if jobless.contains(advanceKYCInfo?.e?.employStatus ?? "0") {
            employmentFormView.snp.remakeConstraints({ (make) in
                make.top.equalTo(employmentDisplayView.snp.bottom)
                make.left.right.equalTo(residentialView)
                make.height.equalTo(450)
            })
            
            print("jobless")
            
            scrollViewHeight = scrollViewHeight - CGFloat(200)
            scrollView.contentSize = CGSize(width: screenWidth, height: scrollViewHeight)
        } else {
            employmentFormView.snp.remakeConstraints({ (make) in
                make.top.equalTo(employmentDisplayView.snp.bottom)
                make.left.right.equalTo(residentialView)
                make.height.equalTo(900)
            })
            
            print("jobful")
            
        }
    }
    
    @objc func tapCancelEmployment(_ sender: UIButton) {
        
        editEmploymentButton.isHidden = false
        
        if advanceKYCInfo?.e != nil {
            employmentDisplayView.isHidden = false
            employmentDisplayView.snp.remakeConstraints({ (make) in
                make.top.equalTo(employmentView.snp.bottom)
                make.left.right.equalTo(employmentView)
                make.height.equalTo(400)
            })
        }
        
        employmentFormView.isHidden = true
        
        employmentFormView.snp.remakeConstraints({ (make) in
            make.top.equalTo(employmentDisplayView.snp.bottom)
            make.left.right.equalTo(residentialView)
            make.height.equalTo(0)
        })
    }
}

// MARK: Capture Image
extension ExtraDocumentViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @objc func tapResidenceImage(_ sender: UIButton) {
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        
        isResidenceImage = true
        isEmploymentImage = false
        
        checkPermission()
    }
    
    @objc func tapEmploymentImage(_ sender: UIButton) {
        
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        
        isResidenceImage = false
        isEmploymentImage = true
        
        checkPermission()
    }
    
    func checkPermission() {
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized:
            print("authorize")
            present(imagePicker, animated: true, completion: nil)
            
        case .notDetermined:
            print("not determine")
            AVCaptureDevice.requestAccess(for: .video) { granted in
                if granted {
                    DispatchQueue.main.async {
                        self.present(self.imagePicker, animated: true, completion: nil)
                    }
                }
            }
            
        case .denied:
            print("denied")
            let alert = UIAlertController(title: "Camera", message: "Camera is needed to take photos".localized(), preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
                } else {
                    // Fallback on earlier versions
                }
            }))
            
            self.present(alert, animated: true)
            return
            
        case .restricted: // The user can't grant access due to restrictions.
            print("restrict")
            showSimpleToast("Camera is needed to take photos".localized())
            return
        }
    }
    
    //MARK: - Done image capture here
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        imagePicker.dismiss(animated: true, completion: nil)
        guard let selectedImage = info[.originalImage] as? UIImage else {
            print("Image not found!")
            return
        }
        
        proofImage = selectedImage
        
        if isResidenceImage == true {
            residentialFormView.proofOfResidenceView.photoImageView.image = proofImage
            residentialFormView.proofOfResidenceView.label.isHidden = true
        }
        
        if isEmploymentImage == true {
            employmentFormView.proofOfEmploymentView.photoImageView.image = proofImage
            employmentFormView.proofOfEmploymentView.label.isHidden = true
        }
    }
}

// MARK: Networking
extension ExtraDocumentViewController {

    func getAdvanceKYCInfo() {

        activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        networking.getAdvanceKycInfo(params: [:], controller: self) { advanceKYCInfo in
            print("advance kyc: \(advanceKYCInfo)")

            self.advanceKYCInfo = advanceKYCInfo
            self.displayContainer()
            
            self.activityIndicator.stopAnimating()
            UIApplication.shared.endIgnoringInteractionEvents()
        }
    }
    
    func getJobStatusOptions() {
        networking.getJobStatus(params: [:], controller: self) { jobStatus in
            self.jobStatus = jobStatus
        }
    }
    
    func getRevenueSourceOptions() {
        networking.getRevenueSource(params: [:], controller: self) { revenueSources in
            self.revenueSources = revenueSources
        }
    }
    
    func getIndustryOptions() {
        networking.getIndustry(params: [:], controller: self) { industry in
            self.industry = industry
        }
    }
    
    @objc func tapSaveResident(_ sender: UIButton) {
        getPreupload(type: "UPLOAD_RESIDENT")
    }
    
    @objc func tapSaveEmployment(_ sender: UIButton) {
        getPreupload(type: "UPLOAD_EMPLOYMENT")
    }
    
    func getPreupload(type: String) {
        var params: [String: String] = [:]
        params["type"] = type
        
        networking.getPreupload(params: params, controller: self) { preupload in
            self.postOSS(data: preupload, type: type)
        }
    }
    
    func postOSS(data: Preupload, type: String) {
        let url = data.host!
        
        var params: [String: String] = [:]
        params["OSSAccessKeyId"] = data.accessid
        params["policy"] = data.policy
        params["signature"] = data.signature
        params["name"] = data.signature
        params["key"] = "\(data.dir!).jpg"
        params["callback"] = data.callback
        params["expire"] = data.expire
        
        print("oss params")
        print(params)
        
        var imgData: Data?
        
        if type == "UPLOAD_RESIDENT" {
            imgData = residentialFormView.proofOfResidenceView.photoImageView.image!.jpegData(compressionQuality: 0.3)
        } else {
            imgData = employmentFormView.proofOfEmploymentView.photoImageView.image!.jpegData(compressionQuality: 0.3)
        }

        Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in params {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            } //Optional for extra parameters
                multipartFormData.append(imgData!, withName: "file",fileName: "upload.jpg", mimeType: "image/jpeg")
        }, to: url, method: .post)
        { (result) in
            switch result {
            case .success(let upload, _, _):

                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })

                upload.validate(statusCode: 200 ..< 300)
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        print("response: \(response)")
                        
                        let res = JSON(response.result.value!)
                        
                        print(res["filename"])
                        
                        switch type {
                        case "UPLOAD_RESIDENT":
                            print("upload reside")
                            self.postSetKYCResidence(imageUrl: res["filename"].stringValue)
                        case "UPLOAD_EMPLOYMENT":
                            print("upload employ")
                            self.postSetKYCEmployment(imageUrl: res["filename"].stringValue)
                        default:
                            return
                        }
                        
                    case .failure(let error):
                        print("error: \(error)")
                        
                        let alert = UIAlertController(title: "Upload Failed".localized(), message: "Do you want to retry?".localized(), preferredStyle: UIAlertController.Style.alert)
                        alert.view.subviews.first?.subviews.first?.subviews.first?.backgroundColor = .container
                        alert.view.subviews.first?.subviews.first?.subviews.last?.backgroundColor = .container
                        alert.view.tintColor = .grayColor
                        
                        alert.setValue(NSAttributedString(string: "Upload Failed".localized(), attributes: [NSAttributedString.Key.font : UIFont(name: bold, size: 15) as Any,NSAttributedString.Key.foregroundColor : UIColor.grayColor]), forKey: "attributedTitle")
                        alert.setValue(NSAttributedString(string: "Do you want to retry?".localized(), attributes: [NSAttributedString.Key.font: UIFont(name: regular, size: 13) as Any, NSAttributedString.Key.foregroundColor : UIColor.grayColor]), forKey: "attributedMessage")
                        
                        alert.addAction(UIAlertAction(title: "Later".localized(), style: .cancel, handler: nil))
                        alert.addAction(UIAlertAction(title: "Retry".localized(), style: .default, handler: { action in
                            self.getPreupload(type: type)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }

            case .failure(let encodingError):
                print("error:")
                // put up an alert box
                print(encodingError)
            }
        }
        
        
    }
    
    func postSetKYCResidence(imageUrl: String) {
        var params: [String: String] = [:]
        params["building"] = residentialFormView.buildingTextField.textField.text
        params["street"] = residentialFormView.streetTextField.textField.text
        params["district"] = residentialFormView.districtTextField.textField.text
        params["city"] = residentialFormView.cityTextField.textField.text
        params["province"] = residentialFormView.provinceTextField.textField.text
        params["zipCode"] = residentialFormView.zipCodeTextField.textField.text
        params["countryCode"] = cpv.selectedCountry.code
        params["authImgUrl1"] = imageUrl

        networking.postSetKYCResident(params: params, controller: self) { response in
            print("response")
            print(response)

            self.getAdvanceKYCInfo()
        }
    }
    
    func postSetKYCEmployment(imageUrl: String) {
        var params: [String: String] = [:]
        params["employStatus"] = employmentFormView.employmentStatusTextField.valueField.text
        params["employStatusName"] = employmentFormView.employmentStatusTextField.textField.text
        params["sourceIncome"] = employmentFormView.incomeSourcesTextField.valueField.text
        params["sourceIncomeName"] = employmentFormView.incomeSourcesTextField.textField.text
        params["industry"] = employmentFormView.industryTextField.valueField.text
        params["industryName"] = employmentFormView.industryTextField.textField.text
        params["jobTitle"] = employmentFormView.jobTitleTextField.textField.text
        params["companyName"] = employmentFormView.companyNameTextField.textField.text
        params["employerBuilding"] = employmentFormView.buildingTextField.textField.text
        params["employerStreet"] = employmentFormView.streetTextField.textField.text
        params["employerDistrict"] = employmentFormView.districtTextField.textField.text
        params["employerCity"] = employmentFormView.cityTextField.textField.text
        params["employerProvince"] = employmentFormView.provinceTextField.textField.text
        params["employerZipCode"] = employmentFormView.zipCodeTextField.textField.text
        params["employerCountryCode"] = cpv.selectedCountry.code
        params["authImgUrl2"] = imageUrl
        
        print("employ params")
        print(params)
        
        networking.postSetKYCEmployment(params: params, controller: self) { response in
            
            print("post set kyc employment")
            print(response)
            
            self.getAdvanceKYCInfo()
        }
    }
    
    func displayContainer() {

        if advanceKYCInfo!.r != nil {

//            residentialView.button.isHidden = true
            residentialDisplayView.isHidden = false
            editResidentialButton.isHidden = false

            residentialDisplayView.buildingContent.text = advanceKYCInfo?.r?.building
            residentialDisplayView.streetContent.text = advanceKYCInfo?.r?.street
            residentialDisplayView.cityContent.text = advanceKYCInfo?.r?.city
            residentialDisplayView.districtContent.text = advanceKYCInfo?.r?.district
            residentialDisplayView.zipCodeContent.text = advanceKYCInfo?.r?.zipCode
            residentialDisplayView.provinceContent.text = advanceKYCInfo?.r?.province
            residentialDisplayView.countryContent.text = countryName(from: advanceKYCInfo?.r?.countryCode ?? "")
            let authImgUrl1 = URL(string: advanceKYCInfo?.r?.authImgUrl1Display ?? "")
            
            residentialDisplayView.proofOfResidenceView.photoImageView.kf.setImage(with: authImgUrl1)

            residentialDisplayView.snp.updateConstraints({ (make) in
                make.height.equalTo(250)
            })
            
            residentialFormView.isHidden = true
            residentialFormView.snp.remakeConstraints({ (make) in
                make.top.equalTo(residentialDisplayView.snp.bottom)
                make.left.right.equalTo(residentialView)
                make.height.equalTo(0)
            })

            scrollViewHeight = scrollViewHeight + CGFloat(250)
            scrollView.contentSize = CGSize(width: screenWidth, height: scrollViewHeight)
        }
        
        if advanceKYCInfo!.e != nil {
            
            employmentDisplayView.isHidden = false
            editEmploymentButton.isHidden = false

            employmentDisplayView.employStatusContent.text = advanceKYCInfo?.e?.employStatusName
            employmentDisplayView.incomeContent.text = advanceKYCInfo?.e?.sourceIncomeName
            employmentDisplayView.industryContent.text = advanceKYCInfo?.e?.industryName
            employmentDisplayView.jobTitleContent.text = advanceKYCInfo?.e?.jobTitle
            employmentDisplayView.companyNameContent.text = advanceKYCInfo?.e?.companyName
            employmentDisplayView.buildingContent.text = advanceKYCInfo?.e?.employerBuilding
            employmentDisplayView.streetContent.text = advanceKYCInfo?.e?.employerStreet
            employmentDisplayView.cityContent.text = advanceKYCInfo?.e?.employerCity
            employmentDisplayView.districtContent.text = advanceKYCInfo?.e?.employerDistrict
            employmentDisplayView.zipCodeContent.text = advanceKYCInfo?.e?.employerZipCode
            employmentDisplayView.provinceContent.text = advanceKYCInfo?.e?.employerProvince
            employmentDisplayView.countryContent.text = countryName(from: advanceKYCInfo?.e?.employerCountryCode ?? "")
            let authImgUrl2 = URL(string: advanceKYCInfo?.e?.authImgUrl2Display ?? "")
            
            employmentDisplayView.proofOfEmploymentView.photoImageView.kf.setImage(with: authImgUrl2)

            employmentDisplayView.snp.updateConstraints({ (make) in
                make.height.equalTo(400)
            })
            
            employmentFormView.isHidden = true
            employmentFormView.snp.remakeConstraints({ (make) in
                make.top.equalTo(employmentDisplayView.snp.bottom)
                make.left.right.equalTo(residentialView)
                make.height.equalTo(0)
            })

            scrollViewHeight = scrollViewHeight + CGFloat(400)
            scrollView.contentSize = CGSize(width: screenWidth, height: scrollViewHeight)
        }
    }
}

extension ExtraDocumentViewController: CountryPickerViewDelegate, CountryPickerViewDataSource {
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        
        print("country: \(country)")
        
        residentialFormView.countryView.textField.text = cpv.selectedCountry.localizedName()
        employmentFormView.countryView.textField.text = cpv.selectedCountry.localizedName()
        return
    }
    
    private func preferredCountries(in countryPickerView: CountryPickerView) -> [Country]? {
        return nil
    }
    
    func sectionTitleForPreferredCountries(in countryPickerView: CountryPickerView) -> String? {
        return nil
    }
    
    private func showOnlyPreferredSection(in countryPickerView: CountryPickerView) -> Bool? {
        return false
    }
    
    func navigationTitle(in countryPickerView: CountryPickerView) -> String? {
        return "Select country".localized()
    }
    
    func closeButtonNavigationItem(in countryPickerView: CountryPickerView) -> UIBarButtonItem? {
        return nil
    }
    
    func searchBarPosition(in countryPickerView: CountryPickerView) -> SearchBarPosition {
        return .tableViewHeader
    }
    
    func showCountryCodeInList(in countryPickerView: CountryPickerView) -> Bool {
        return false
    }
    
    private func showPhoneCodeInList(in countryPickerView: CountryPickerView) -> Bool? {
        return false
    }
}
