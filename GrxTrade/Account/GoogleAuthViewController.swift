//
//  GoogleAuthViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 31/12/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit
import Presentr
import Toast_Swift

class GoogleAuthViewController: BaseViewController {
    
    var secret = String()
    
    lazy var containerView: DepositView = {
        let view = DepositView()
        view.depositAddressTitleLabel.text = "Authentication Key".localized()
        view.copyButton.setTitle("Copy Key".localized(), for: .normal)
        view.shareButton.isHidden = true
        view.saveButton.isHidden = true
        view.copyButton.addTarget(self, action: #selector(tapCopy(_:)), for: .touchUpInside)
        view.backgroundColor = .containerAlternative
        view.layer.cornerRadius = 2
        return view
    }()
    
    lazy var step1View: InstructionStepView = {
        let view = InstructionStepView()
        view.numberLabel.text = "1"
        view.remarkLabel.numberOfLines = 2
        return view
    }()
    
    lazy var step2View: InstructionStepView = {
        let view = InstructionStepView()
        view.remarkLabel.numberOfLines = 4
        view.numberLabel.text = "2"
        view.remarkLabel.text = "Open Google Authenticator, select the \"+\" at the top right corner and choose \"Manual entry\". Enter your GRX account and the authentication key.".localized()
        return view
    }()
    
    lazy var step3View: InstructionStepView = {
        let view = InstructionStepView()
        view.remarkLabel.numberOfLines = 2
        view.numberLabel.text = "3"
        view.remarkLabel.text = "Please enter 6 digits code displayed in the Google Authenticator app before it changes.".localized()
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addSubviews()
        adjustUI()
        addConstraints()
        
        getGoogleAuthData()
        
        NotificationCenter.default.addObserver(self, selector: #selector(popView), name:NSNotification.Name(rawValue: "popGoogleAuthView"), object: nil)
    }

}

// MARK: UI
extension GoogleAuthViewController {
    
    func addSubviews() {
        view.addSubview(containerView)
        view.addSubview(step1View)
        view.addSubview(step2View)
        view.addSubview(step3View)
    }
    
    func adjustUI() {
        title = "Google Authenticator".localized()
        view.backgroundColor = .backgroundAlternative
        
        let nextItem = UIBarButtonItem(title: "Next".localized(), style: .plain, target: self, action: #selector(tapNext(_:)))
        navigationItem.rightBarButtonItem = nextItem
    }
    
    func addConstraints() {
        containerView.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(14)
            make.left.equalToSuperview().offset(11)
            make.right.equalToSuperview().offset(-11)
            make.height.equalTo(284)
        })
        
        step1View.snp.makeConstraints({ (make) in
            make.top.equalTo(containerView.snp.bottom).offset(26)
            make.left.right.equalTo(containerView)
        })
        
        step2View.snp.makeConstraints({ (make) in
            make.top.equalTo(containerView.snp.bottom).offset(80)
            make.left.right.equalTo(containerView)
        })
        
        step3View.snp.makeConstraints({ (make) in
            make.top.equalTo(containerView.snp.bottom).offset(150)
            make.left.right.equalTo(containerView)
        })
    }
}

// MARK: Networking
extension GoogleAuthViewController {
    
    func getGoogleAuthData() {
        
        let networking = Networking()
        networking.getGoogleAuth(params: ["": ""], controller: self) { googleAuth in
            
            self.secret = googleAuth.secretKey!
            
            print("secret: \(self.secret)")
            
            self.containerView.depositAddressLabel.text = self.secret
            self.containerView.qrImageView.image = self.generateQRCode(from: googleAuth.otpAuthTotpURL!)
        }
    }
    
    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)

        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)

            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }

        return nil
    }
}

// MARK: Action
extension GoogleAuthViewController {
    
    @objc func tapNext(_ sender: UIBarButtonItem) {
        let vc = OneTimePasswordViewController()
        vc.type = "setGA"
        vc.secret = secret
        let presenter = Presentr(presentationType: .custom(width: .full, height: .custom(size: 680), center: .bottomCenter))
        self.customPresentViewController(presenter, viewController: vc, animated: true, completion: nil)
    }
    
    @objc func tapCopy(_ sender: UIButton) {
        UIPasteboard.general.string = containerView.depositAddressLabel.text
        showSimpleToast("Authentication key copied".localized())
    }
    
    @objc func popView() {
        BaseSingleton.shared.user.gaStatus = 1
        showSimpleToast("Google authenticator has been enabled".localized())
        navigationController?.popViewController(animated: true)
    }
}
