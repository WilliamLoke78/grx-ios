////
////  ExtraDocumentViewController.swift
////  GrxTrade
////
////  Created by Leon Mah Kean Loon on 30/12/2019.
////  Copyright © 2019 Leon Mah. All rights reserved.
////
//
//import UIKit
//import CountryPickerView
//
//private let residenceCellId = "residenceCellId"
//private let employmentCellId = "employmentCellId"
//
//class ExtraDocumentViewController: BaseViewController, CountryPickerViewDelegate, CountryPickerViewDataSource {
//    
//    var scrollViewHeight: CGFloat = screenHeight
//    
//    var advanceKYCInfo: AdvanceKYCInfo?
//    
//    var residenceCells: [AdvanceKYCCell] = []
//    var employmentCells: [AdvanceKYCCell] = []
//    
//    lazy var residentialView: InfoContainerView = {
//        let view = InfoContainerView()
//        view.iconImageView.image = UIImage(named: "residential")
//        view.titleLabel.text = "Residential Information".localized()
//        view.subtitleLabel.text = "Fill up your Residential Information for secure purpose".localized()
//        return view
//    }()
//    
//    lazy var residentialContentView: UIView = {
//        let view = UIView()
//        view.backgroundColor = .container
//        view.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 8)
//        return view
//    }()
//    
//    lazy var residentialDisplayView: ResidentialDisplayView = {
//        let view = ResidentialDisplayView()
//        return view
//    }()
//    
//    lazy var residentialFormView: ResidentialFormView = {
//        let view = ResidentialFormView()
//        view.isHidden = true
//        view.countryView.blockButton.addTarget(self, action: #selector(tapSelectCountry(_:)), for: .touchUpInside)
//        view.cancelButton.addTarget(self, action: #selector(tapCancelResidential(_:)), for: .touchUpInside)
//        return view
//    }()
//    
//    lazy var residentialButton: UIButton = {
//        let button = UIButton()
//        button.setTitleColor(.primary, for: .normal)
//        button.titleLabel?.font = UIFont(name: bold, size: 16)
//        button.setTitle("Edit Residential Info".localized(), for: .normal)
//        button.addTarget(self, action: #selector(tapResidential(_:)), for: .touchUpInside)
//        return button
//    }()
//    
//    /// Employment View
//
//    lazy var employmentView: InfoContainerView = {
//        let view = InfoContainerView()
//        view.iconImageView.image = UIImage(named: "employment")
//        view.titleLabel.text = "Employment Information".localized()
//        view.subtitleLabel.text = "Fill up your Employment Information for secure purpose".localized()
//        return view
//    }()
//    
//    lazy var employmentContentView: UIView = {
//        let view = UIView()
//        view.backgroundColor = .container
//        view.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 8)
//        return view
//    }()
//    
//    lazy var employmentDisplayView: UITableView = {
//        let tv = UITableView()
//        tv.allowsSelection = false
//        tv.isScrollEnabled = false
//        tv.separatorStyle = .none
//        return tv
//    }()
//    
//    lazy var employmentFormView: EmploymentFormView = {
//        let view = EmploymentFormView()
//        view.isHidden = true
//        view.countryView.blockButton.addTarget(self, action: #selector(tapSelectCountry(_:)), for: .touchUpInside)
//        view.cancelButton.addTarget(self, action: #selector(tapCancelEmployment(_:)), for: .touchUpInside)
//        return view
//    }()
//    
//    lazy var employmentButton: UIButton = {
//        let button = UIButton()
//        button.setTitleColor(.primary, for: .normal)
//        button.titleLabel?.font = UIFont(name: bold, size: 16)
//        button.setTitle("Edit Employment Info".localized(), for: .normal)
//        button.addTarget(self, action: #selector(tapEmployment(_:)), for: .touchUpInside)
//        return button
//    }()
//    
//    lazy var cpv = CountryPickerView(frame: CGRect(x: 0, y: 0, width: screenWidth-22, height: 20))
//    
//    lazy var scrollView: UIScrollView = {
//        let v = UIScrollView()
//        v.translatesAutoresizingMaskIntoConstraints = false
//        v.contentSize = CGSize(width: screenWidth, height: screenHeight)
//        return v
//    }()
//    
//    override func viewDidLoad() {
//        
//        addSubviews()
//        adjustUI()
//        addConstraints()
//        
//        cpv.delegate = self
//        cpv.dataSource = self
//        
//        cpv.showPhoneCodeInView = false
//        cpv.showCountryCodeInView = false
//        
//        residentialFormView.countryView.textField.leftView = cpv
//        residentialFormView.countryView.textField.leftViewMode = .always
//        residentialFormView.countryView.textField.text = cpv.selectedCountry.localizedName()
//        
//        
//        
//        getAdvanceKYCInfo()
//    }
//}
//
//// MARK: UI
//extension ExtraDocumentViewController {
//    
//    func addSubviews() {
//        view.addSubview(scrollView)
//        scrollView.addSubview(residentialView)
//        scrollView.addSubview(residentialContentView)
//        
//        residentialContentView.addSubview(residentialDisplayView)
//        residentialContentView.addSubview(residentialFormView)
//        residentialContentView.addSubview(residentialButton)
//        
//        scrollView.addSubview(employmentView)
//        scrollView.addSubview(employmentContentView)
//        
//        employmentContentView.addSubview(employmentDisplayView)
//        employmentContentView.addSubview(employmentFormView)
//        employmentContentView.addSubview(employmentButton)
//    }
//    
//    func adjustUI() {
//        title = "Residential Information".localized().uppercased()
//        view.backgroundColor = .background
//        
//        let submitItem = UIBarButtonItem(title: "Submit".localized(), style: .plain, target: self, action: #selector(tapSubmit(_:)))
//        navigationItem.rightBarButtonItem = submitItem
//    }
//    
//    func addConstraints() {
//        
//        var newFrame: CGRect = residentialDisplayView.frame
//        newFrame.size.height = 100;
//
//        residentialDisplayView.frame = newFrame
//        
//        scrollView.snp.makeConstraints({ (make) in
//            make.edges.equalToSuperview()
//        })
//        
//        residentialView.snp.makeConstraints({ (make) in
//            make.top.equalToSuperview().offset(20)
//            make.left.equalTo(view).offset(11)
//            make.right.equalTo(view).offset(-11)
//            make.height.equalTo(120)
//        })
//        
//        residentialContentView.snp.makeConstraints({ (make) in
//            make.top.equalTo(residentialView.snp.bottom)
//            make.left.right.equalTo(residentialView)
//            make.height.equalTo(40)
//        })
//        
//        residentialDisplayView.snp.makeConstraints({ (make) in
//            make.edges.equalToSuperview()
//            make.height.equalTo(0)
//        })
////
////        residentialFormView.snp.makeConstraints({ (make) in
////            make.edges.equalToSuperview()
////        })
////
//        residentialButton.snp.makeConstraints({ (make) in
//            make.bottom.equalToSuperview().offset(-5)
//            make.centerX.equalToSuperview()
//        })
//        
//        /// Employment
//
//        employmentView.snp.makeConstraints({ (make) in
//            make.top.equalTo(residentialContentView.snp.bottom).offset(20)
//            make.left.right.equalTo(residentialView)
//            make.height.equalTo(120)
//        })
//        
//        employmentContentView.snp.makeConstraints({ (make) in
//            make.top.equalTo(employmentView.snp.bottom)
//            make.left.right.equalTo(employmentView)
//            make.height.equalTo(40)
//        })
//
////        employmentDisplayView.snp.makeConstraints({ (make) in
////            make.edges.equalToSuperview()
////        })
////
////        employmentFormView.snp.makeConstraints({ (make) in
////            make.edges.equalToSuperview()
////        })
//        
//        employmentButton.snp.makeConstraints({ (make) in
//            make.bottom.equalToSuperview().offset(-5)
//            make.centerX.equalToSuperview()
//        })
//    }
//}
//
//// MARK: Action
//extension ExtraDocumentViewController {
//    
//    @objc func tapSelectCountry(_ sender: UIButton) {
//        cpv.showCountriesList(from: self)
//    }
//    
//    @objc func tapSubmit(_ sender: UIBarButtonItem) {
//        
//    }
//    
//    @objc func tapResidential(_ sender: UIButton) {
//        
////        residentialButton.isHidden = true
////        residentialFormView.isHidden = false
////
////        residentialContentView.snp.remakeConstraints({ (make) in
////            make.top.equalTo(residentialView.snp.bottom)
////            make.left.right.equalTo(residentialView)
////            make.height.equalTo(600)
////        })
////
////        residentialFormView.snp.remakeConstraints({ (make) in
////            make.top.equalTo(residentialView.snp.bottom)
////            make.left.right.equalTo(residentialView)
////            make.height.equalTo(600)
////        })
////
////        scrollViewHeight = scrollViewHeight + CGFloat(500)
////        scrollView.contentSize = CGSize(width: screenWidth, height: scrollViewHeight)
//    }
//    
//    @objc func tapCancelResidential(_ sender: UIButton) {
//        
////        residentialView.button.isHidden = false
////        residentialFormView.isHidden = true
////
////        residentialFormView.snp.remakeConstraints({ (make) in
////            make.top.equalTo(residentialView.snp.bottom)
////            make.left.right.equalTo(residentialView)
////            make.height.equalTo(0)
////        })
////
////        scrollViewHeight = scrollViewHeight - CGFloat(500)
////        scrollView.contentSize = CGSize(width: screenWidth, height: scrollViewHeight)
//    }
//    
//    @objc func tapEmployment(_ sender: UIButton) {
//
////        employmentView.button.isHidden = true
//        employmentFormView.isHidden = false
//
//        employmentFormView.snp.remakeConstraints({ (make) in
//            make.top.equalTo(employmentView.snp.bottom)
//            make.left.right.equalTo(residentialView)
//            make.height.equalTo(800)
//        })
//
//        scrollViewHeight = scrollViewHeight + CGFloat(500)
//        scrollView.contentSize = CGSize(width: screenWidth, height: scrollViewHeight)
//    }
//
//    @objc func tapCancelEmployment(_ sender: UIButton) {
//
////        employmentView.button.isHidden = false
//        employmentFormView.isHidden = true
//
//        employmentFormView.snp.remakeConstraints({ (make) in
//            make.top.equalTo(employmentView.snp.bottom)
//            make.left.right.equalTo(residentialView)
//            make.height.equalTo(0)
//        })
//
//        scrollViewHeight = scrollViewHeight - CGFloat(500)
//        scrollView.contentSize = CGSize(width: screenWidth, height: scrollViewHeight)
//    }
//}
//
//extension ExtraDocumentViewController {
//    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
//        residentialFormView.countryView.textField.text = cpv.selectedCountry.localizedName()
//        return
//    }
//    
//    private func preferredCountries(in countryPickerView: CountryPickerView) -> [Country]? {
//        return nil
//    }
//    
//    func sectionTitleForPreferredCountries(in countryPickerView: CountryPickerView) -> String? {
//        return nil
//    }
//    
//    private func showOnlyPreferredSection(in countryPickerView: CountryPickerView) -> Bool? {
//        return false
//    }
//    
//    func navigationTitle(in countryPickerView: CountryPickerView) -> String? {
//        return "Select country".localized()
//    }
//    
//    func closeButtonNavigationItem(in countryPickerView: CountryPickerView) -> UIBarButtonItem? {
//        return nil
//    }
//    
//    func searchBarPosition(in countryPickerView: CountryPickerView) -> SearchBarPosition {
//        return .tableViewHeader
//    }
//    
//    func showCountryCodeInList(in countryPickerView: CountryPickerView) -> Bool {
//        return false
//    }
//    
//    private func showPhoneCodeInList(in countryPickerView: CountryPickerView) -> Bool? {
//        return false
//    }
//}
//
//// MARK: Networking
//extension ExtraDocumentViewController {
//    
//    func getAdvanceKYCInfo() {
//        
//        let params: [String: String] = [:]
//        
//        let networking = Networking()
//        networking.getAdvanceKycInfo(params: params, controller: self) { advanceKYCInfo in
//            
//            print("advance kyc")
//            print(advanceKYCInfo)
//            
//            self.advanceKYCInfo = advanceKYCInfo
//            
//            self.displayContainer()
//        }
//    }
//    
//    func displayContainer() {
//        
//        if advanceKYCInfo!.r != nil {
//            
//            print("adv kyc")
//            print(advanceKYCInfo!.r?.countryCode)
//            
////            residentialView.button.isHidden = true
//            residentialFormView.isHidden = true
//            
//            residentialDisplayView.buildingContent.text = advanceKYCInfo?.r?.building
//            residentialDisplayView.streetContent.text = advanceKYCInfo?.r?.street
//            residentialDisplayView.cityContent.text = advanceKYCInfo?.r?.city
//            residentialDisplayView.districtContent.text = advanceKYCInfo?.r?.district
//            residentialDisplayView.zipCodeContent.text = advanceKYCInfo?.r?.zipCode
//            residentialDisplayView.provinceContent.text = advanceKYCInfo?.r?.province
//            residentialDisplayView.countryContent.text = advanceKYCInfo?.r?.countryCode
//            
//            residentialContentView.snp.updateConstraints({ (make) in
//                make.height.equalTo(400)
//            })
//            
////            residentialDisplayView.snp.updateConstraints({ (make) in
////                make.height.equalTo(400)
////            })
////
//            var newFrame: CGRect = residentialDisplayView.frame
//            
//            newFrame.size.height = 400;
//            residentialDisplayView.frame = newFrame
//            
//            scrollViewHeight = scrollViewHeight + CGFloat(400)
//            scrollView.contentSize = CGSize(width: screenWidth, height: scrollViewHeight)
//        }
//    }
//    
////    func setupResidenceView(advanceKYCInfo: AdvanceKYCInfo) {
////
////        let model0 = AdvanceKYCCell(label: "Building".localized(), content: advanceKYCInfo.r!.building ?? "- -")
////        let model1 = AdvanceKYCCell(label: "Street".localized(), content: advanceKYCInfo.r!.street ?? "- -")
////        let model2 = AdvanceKYCCell(label: "City".localized(), content: advanceKYCInfo.r!.city ?? "- -")
////        let model3 = AdvanceKYCCell(label: "District".localized(), content: advanceKYCInfo.r!.district ?? "- -")
////        let model4 = AdvanceKYCCell(label: "Zip Code".localized(), content: advanceKYCInfo.r!.zipCode ?? "- -")
////        let model5 = AdvanceKYCCell(label: "Province".localized(), content: advanceKYCInfo.r!.province ?? "- -")
////        let model6 = AdvanceKYCCell(label: "Country".localized(), content: advanceKYCInfo.r!.countryCode ?? "- -")
////
////        residenceCells = [model0, model1, model2, model3, model4, model5, model6]
////
////        residentialDisplayView.reloadData()
////    }
//}
