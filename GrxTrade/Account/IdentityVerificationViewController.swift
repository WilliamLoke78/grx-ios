//
//  IdentityVerificationViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 08/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import UIKit
import AVFoundation

private let accountCellId = "accountCellId"

class IdentityVerificationViewController: BaseTableViewController {

//    var labels: [Label] = []
    private var cellModelArray: [CellModel] = []
    var user: User = BaseSingleton.shared.user
    
    var emailVerified: String? = ""
    var phoneVerified: String? = ""
    var profileVerified: String? = ""
    var idDocumentVerified: String? = ""
    var livenessVerified: String? = ""
    var documentVerified: String? = ""
    
    lazy var tickImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "tick")
        return iv
    }()
    
    let activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addSubviews()
        adjustUI()
        addConstraints()
        
        setupData()
        
        tableView.register(AccountTableViewCell.self, forCellReuseIdentifier: accountCellId)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        getUserInfo()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
}

// MARK: User Interface
extension IdentityVerificationViewController {
    
    func addSubviews() {
        tableView.addSubview(activityIndicator)
    }
    
    func adjustUI() {
        title = "Identity Verification".localized()
        tableView.backgroundColor = .background
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView()
    }
    
    func addConstraints() {
        activityIndicator.snp.makeConstraints({ (make) in
            make.centerY.equalToSuperview()
            make.centerX.equalToSuperview()
        })
    }
}

// MARK: Table
extension IdentityVerificationViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2*2
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: accountCellId, for: indexPath) as! AccountTableViewCell
        
        let model = cellModelArray[indexPath.row]
        
        cell.iconImageView.image = UIImage(named: model.icon)
        cell.label.text = model.title
        cell.subLabel.text = model.subTitle.capitalized
        
        cell.iconImageView.snp.makeConstraints({ (make) in
            make.height.equalTo(20)
            make.width.equalTo(30)
        })
        
        print("kyc level: \(user.kycLevel!)")
        
        if model.tag == "level1" {
            
            var subLabel: String? = ""
            var allowAccess: Bool = false
            var subLabelColor: UIColor?
                        
            switch user.primaryAuthStatus {
            case 1:
                subLabel = "Verified".localized()
                allowAccess = false
                subLabelColor = .greenColor
                cell.tickImageView.isHidden = false
                cell.addSubview(cell.tickImageView)
                cell.tickImageView.snp.makeConstraints({ (make) in
                    make.right.equalTo(cell.subLabel.snp.left).offset(-4)
                    make.centerY.equalToSuperview()
                    make.height.equalTo(14)
                    make.width.equalTo(14)
                })
            case 2:
                subLabel = "Rejected".localized()
                if user.kycLevel! == 0 {
                    allowAccess = true
                } else {
                    allowAccess = false
                }
                subLabelColor = .redColor
//            case 3:
//                subLabel = "Retry".localized()
//                if user.kycLevel! == 0 {
//                    allowAccess = true
//                } else {
//                    allowAccess = false
//                }
//                subLabelColor = .primary
            case 8:
                subLabel = "Unverified".localized()
                if user.kycLevel! == 0 {
                    allowAccess = true
                } else {
                    allowAccess = false
                }
                subLabelColor = .grayColor
            case 9:
                subLabel = "Continue".localized()
                if user.kycLevel! == 0 {
                    allowAccess = true
                } else {
                    allowAccess = false
                }
                subLabelColor = .primary
            default: // 0
                subLabel = "Pending Review".localized()
                allowAccess = false
                subLabelColor = .grayColor
            }
            
            if allowAccess != true {
                cell.isUserInteractionEnabled = false
                cell.arrowImageView.isHidden = true
                cell.subLabel.snp.remakeConstraints({ (make) in
                    make.right.equalToSuperview().offset(-14)
                    make.centerY.equalToSuperview()
                })
            } else {
                cell.isUserInteractionEnabled = true
            }
            
            cell.subLabel.textColor = subLabelColor
            cell.subLabel.text = subLabel
        }
        
        if model.tag == "level2" {
            
            var subLabel: String? = ""
            var allowAccess: Bool = false
            var subLabelColor: UIColor?
                        
            switch user.advancedAuthStatus {
            case -1:
                subLabel = "Unverified".localized()
                if user.kycLevel! == 1 {
                    allowAccess = true
                } else {
                    allowAccess = false
                }
                subLabelColor = .grayColor
            case 1:
                subLabel = "Verified".localized()
                allowAccess = false
                subLabelColor = .greenColor
            case 2:
                subLabel = "Rejected".localized()
                if user.kycLevel! == 1 {
                    allowAccess = true
                } else {
                    allowAccess = false
                }
                subLabelColor = .redColor
//            case 3:
//                subLabel = "Retry".localized()
//                if user.kycLevel! == 1 {
//                    allowAccess = true
//                } else {
//                    allowAccess = false
//                }
//                subLabelColor = .primary
            case 9:
                subLabel = "Continue".localized()
                if user.kycLevel! == 1 {
                    allowAccess = true
                } else {
                    allowAccess = false
                }
                subLabelColor = .primary
            default:
                subLabel = "Pending Review".localized()
                allowAccess = false
                subLabelColor = .grayColor
            }
            
            if allowAccess != true {
                cell.isUserInteractionEnabled = false
                cell.arrowImageView.isHidden = true
                cell.subLabel.snp.remakeConstraints({ (make) in
                    make.right.equalToSuperview().offset(-14)
                    make.centerY.equalToSuperview()
                })
            } else {
                cell.isUserInteractionEnabled = true
                cell.tickImageView.isHidden = false
                cell.addSubview(cell.tickImageView)
                cell.tickImageView.snp.makeConstraints({ (make) in
                    make.right.equalTo(cell.subLabel.snp.left).offset(-4)
                    make.centerY.equalToSuperview()
                    make.height.equalTo(14)
                    make.width.equalTo(14)
                })
            }
            
            cell.subLabel.textColor = subLabelColor
            cell.subLabel.text = subLabel
        }
        
        if model.tag == "desc" {
            cell.selectionStyle = .none
            cell.backgroundColor = .background
            cell.iconImageView.isHidden = true
            cell.label.isHidden = true
            cell.subLabel.isHidden = true
            cell.arrowImageView.isHidden = true
            
            let descLabel: UILabel = {
                let label = UILabel()
                label.numberOfLines = 2
                label.text = model.title
                label.font = UIFont(name: regular, size: 11)
                label.textColor = .grayColor
                return label
            }()
            
            cell.addSubview(descLabel)
            
            descLabel.snp.makeConstraints({ (make) in
                make.top.equalToSuperview().offset(6)
                make.left.equalToSuperview().offset(16)
                make.right.equalToSuperview().offset(-16)
            })
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let model = cellModelArray[indexPath.row]
        
        if model.tag == "desc" {
            return 40
        }
        return 50
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let model = cellModelArray[indexPath.row]
        if model.tag == "level1" {
            getPackage()
        }
        
        if model.tag == "level2" {
            let vc = ExtraDocumentViewController()
            show(vc, sender: nil)
        }
    }
}

// Data
extension IdentityVerificationViewController {
    
    func setupData() {
        
        let model0 = CellModel(icon: "level_one", title: "Level 1".localized(), subTitle: "", accIcon: "", tag: "level1")
        let model1 = CellModel(icon: "", title: "Basic verification process with proof of Identity Card or Passport.".localized(), subTitle: "", accIcon: "", tag: "desc")
        let model2 = CellModel(icon: "level_two", title: "Level 2".localized(), subTitle: "", accIcon: "", tag: "level2")
        let model3 = CellModel(icon: "", title: "Advanced verification process with proof of Residence documents, Employment document and etc.".localized(), subTitle: "", accIcon: "", tag: "desc")
        
        cellModelArray = [model0, model1, model2, model3]
        
        tableView.reloadData()
    }
    
    func getPackage() {
        
        var params: [String: String] = [:]
        params["type"] = "MXW_WALLET_KYC"
        
        let networking = Networking()
        networking.getPackageInfo(params: params, controller: self) { response in
            
            let base64 = "\(response)".toBase64()
            
            let host = "com.mxw.wallet://maxRegister?scheme=com.grxtrade.app&type=kyc"
            var data = "&data=\(base64)"
            data = data.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
            
            guard let openMxwURL = URL(string: host+data) else {
              return //be safe
            }
            
            if UIApplication.shared.canOpenURL(openMxwURL) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(openMxwURL, options: [:], completionHandler: { (success) in
                        print("Successfully launched MaxWallet")
                    })
                } else {
                    UIApplication.shared.openURL(openMxwURL)
                    print("Successfully launched MaxWallet")
                }
            } else {
                guard let url = URL(string: networking.mxwHost) else {
                    return
                }
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    
    func postMxwKyc(data: Any) {
        var params: [String: Any] = [:]
        params["userId"] = user.id
        params["data"] = "\(data)".fromBase64()
        
        print("post mxw kyc")
        print(params)
        
        let networking = Networking()
        networking.postMxwKyc(params: params, controller: self) { response in
            
            print("mxw kyc response: \(response)")
            
            self.user.kycLevel! = 1
            self.getUserInfo()
            
            showSimpleToast("Identity successfully verified".localized())
        }
    }
    
    func getUserInfo() {
        
        let networking = Networking()
        networking.getUserInfo(params: [:], controller: self) { user in
            
            Storage.save(key: Storage.Name.user, value: user)
            BaseSingleton.shared.user = user
            self.user = user
            self.setupData()
        }
    }
    
//    func getUpdatedUserInfo() {
//
//        let networking = Networking()
//        networking.getUserInfo(params: [:], controller: self) { user in
//
//            print("user update again")
//
//            self.user.primaryAuthStatus = user.primaryAuthStatus
////            self.user.primaryAuthStatus = 8
//            self.user.advancedAuthStatus = user.advancedAuthStatus
//
//            self.tableView.reloadData()
//        }
//    }
    
    @objc func willEnterForeground() {
        print("foreground again")
//        navigationBack()
        restartApplication(pageIndex: 4)
    }
}
