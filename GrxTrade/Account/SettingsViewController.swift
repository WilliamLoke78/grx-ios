//
//  SettingsViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 24/12/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

private let settingsCellId = "settingsCellId"

class SettingsViewController: BaseTableViewController {

    private var cellModelArray: [CellModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.adjustUI()
        self.setupData()
        tableView.register(AccountSimpleTableViewCell.self, forCellReuseIdentifier: settingsCellId)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.setupData()
        tableView.reloadData()
    }
}

// MARK: User Interface
extension SettingsViewController: UI {
    func addEvents() {
        
    }
    
    func addSubviews() {
        
    }
    
    func addConstraints() {
        
    }
    
    func configure<T>(model: T) {
        
    }
    
    
    func adjustUI() {
        title = "Settings".localized()
        tableView.backgroundColor = .background
        tableView.separatorColor = .background
        tableView.tableFooterView = UIView()
    }
    
    @objc func tapCurrency(_ sender: UIButton) {
        let vc = SettingsCurrencyViewController()
        self.show(vc, sender: nil)
    }
    
    @objc func tapLanguage(_ sender: UIButton) {
        let vc = SettingsLanguageViewController()
        self.show(vc, sender: nil)
    }
}

// MARK: Table
extension SettingsViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellModelArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: settingsCellId, for: indexPath) as! AccountSimpleTableViewCell
        
        let model = cellModelArray[indexPath.row]
        cell.label.text = model.title
        cell.subLabel.text = model.subTitle
        
        if model.tag == "currency" {
            cell.button.addTarget(self, action: #selector(tapCurrency(_:)), for: .touchUpInside)
        }
        
        if model.tag == "language" {
            cell.button.addTarget(self, action: #selector(tapLanguage(_:)), for: .touchUpInside)
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

// MARK: Data
extension SettingsViewController {
    
    private func setupData() {
        
        let langs = ["en" : "English", "zh-Hans" : "简体中文", "zh-Hant" : "繁體中文"]
        let currencies = BaseSingleton.shared.currencies!
        let selectedCurrency = currencies.first(where: {$0.abbreviation == CurrencyManager.userCurrency()})
        
        let model0 = CellModel.init(icon: "", title: "Currency".localized(), subTitle: "\(selectedCurrency?.abbreviation ?? "") \(selectedCurrency?.symbol ?? "")", accIcon: "", tag: "currency")
        let model1 = CellModel.init(icon: "", title: "Language".localized(), subTitle: langs[LocalizationManager.userLang()] ?? "", accIcon: "", tag: "language")
        
        cellModelArray = [model0, model1]
    }
}
