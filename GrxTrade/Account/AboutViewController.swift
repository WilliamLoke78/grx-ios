//
//  AboutViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 16/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

private let accountCellId = "accountCellId"

class AboutViewController: BaseTableViewController {
    
    private var cellModels: [CellModel] = []
    
    let aboutHeaderView: AboutHeaderView = {
        let hv = AboutHeaderView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 162))
        return hv
    }()
    
    let aboutFooterView: AboutFooterView = {
        let fv = AboutFooterView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 100))
        fv.button.addTarget(self, action: #selector(tapTerms(_:)), for: .touchUpInside)
        return fv
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addSubviews()
        adjustUI()
        addConstraints()
        
        setupData()
        tableView.register(AccountTableViewCell.self, forCellReuseIdentifier: accountCellId)
    }
    
}

// MARK: User Interface
extension AboutViewController: UI {
    func addEvents() {
        
    }
    
    func configure<T>(model: T) {
        
    }
    
    
    func addSubviews() {
        tableView.addSubview(aboutHeaderView)
        tableView.addSubview(aboutFooterView)
        tableView.tableHeaderView = aboutHeaderView
        tableView.tableFooterView = aboutFooterView
    }
    
    func adjustUI() {
        title = "About GRXTrade".localized()
        view.backgroundColor = .background
        tableView.separatorColor = .background
    }
    
    func addConstraints() {
        aboutFooterView.snp.makeConstraints({ (make) in
            make.top.equalTo(tableView.snp.bottom).offset(500)
            make.centerX.equalToSuperview()
            make.height.equalTo(100)
            make.width.equalToSuperview()
        })
    }
}

// MARK: Action
extension AboutViewController {
    
    @objc func tapTerms(_ sender: UIButton) {
        let vc = WebViewController()
        vc.webViewTitle = "Terms of Use and Conditions".localized()
        vc.isScrollEnabled = true
        let networking = Networking()
        vc.urlString = "\(networking.staticHost)/legal/terms-of-use?isMobile=1&lang=\(LocalizationManager.serverLang())"
        print("\(networking.staticHost)/legal/terms-of-use?isMobile=1&lang=\(LocalizationManager.serverLang())")
        show(vc, sender: nil)
    }
}

// MARK: Table
extension AboutViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellModels.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: accountCellId, for: indexPath) as! AccountTableViewCell
        
        let model = cellModels[indexPath.row]
        
        cell.iconImageView.image = UIImage(named: model.icon)
        cell.label.text = model.title
        cell.subLabel.text = model.subTitle
        
        cell.subLabel.textColor = .primary
        
        cell.iconImageView.snp.makeConstraints({ (make) in
            make.height.equalTo(24)
            make.width.equalTo(24)
        })
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let model = cellModels[indexPath.row]
        
        if model.tag == "website" {
//            let vc = WebViewController()
//            vc.urlString = baseSite
//            show(vc, sender: nil)
        }
        
        if model.tag == "facebook" {
            let facebookURL = "https://www.facebook.com/GRXTrade-102217968010850"
            let openFacebookURL = NSURL(string: facebookURL)
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(openFacebookURL! as URL, options: [:], completionHandler: { (success) in
                    print("Successfully launched Facebook")
                })
            } else {
                UIApplication.shared.openURL(openFacebookURL! as URL)
            }
        }
        
        if model.tag == "twitter" {
            let twitterURL = "https://twitter.com/TradeGRX"
            let openTwitterURL = NSURL(string: twitterURL)
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(openTwitterURL! as URL, options: [:], completionHandler: { (success) in
                    print("Successfully launched Twitter")
                })
            } else {
                UIApplication.shared.openURL(openTwitterURL! as URL)
            }
        }
        
        if model.tag == "instagram" {
            let instagramURL = "https://www.instagram.com/GRXtrade"
            let openInstagramURL = NSURL(string: instagramURL)
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(openInstagramURL! as URL, options: [:], completionHandler: { (success) in
                    print("Successfully launched Instagram")
                })
            } else {
                UIApplication.shared.openURL(openInstagramURL! as URL)
            }
        }
        
        if model.tag == "telegram" {
            let telegramURL = "https://t.me/grxtrade"
            let openTelegramURL = NSURL(string: telegramURL)
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(openTelegramURL! as URL, options: [:], completionHandler: { (success) in
                    print("Successfully launched Telegram")
                })
            } else {
                UIApplication.shared.openURL(openTelegramURL! as URL)
            }
        }
        
        if model.tag == "email" {
            let email = "support@growdex.com"
            
            if let url = URL(string: "mailto:\(email)") {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

// MARK: Data
extension AboutViewController {
    
    private func setupData() {
        
        var email = String()
        let chineseLangs = ["zh-Hant", "zh-Hans"]
        if chineseLangs.contains(LocalizationManager.userLang()) {
            email = "support_cn@grxtrade.com"
        } else {
            email = "support@grxtrade.com"
        }
        
        let version = Bundle.main.infoDictionary!["CFBundleShortVersionString"]!
        aboutHeaderView.versionLabel.text = "Version".localized(with: [String(describing: version)])
        
        let model0 = CellModel.init(icon: "website", title: "Website".localized(), subTitle: "\(Networking().staticHost)", accIcon: "", tag: "website")
        let model1 = CellModel.init(icon: "facebook", title: "Facebook".localized(), subTitle: "/Growdex-1193440977483199", accIcon: "", tag: "facebook")
        let model2 = CellModel.init(icon: "twitter", title: "Twitter".localized(), subTitle: "/TradeGRX", accIcon: "", tag: "twitter")
        let model3 = CellModel.init(icon: "instagram", title: "Instagram".localized(), subTitle: "/GRXtrade", accIcon: "", tag: "instagram")
        let model6 = CellModel.init(icon: "telegram", title: "Telegram".localized(), subTitle: "/grxtrade", accIcon: "", tag: "telegram")
        let model11 = CellModel.init(icon: "email", title: "Email".localized(), subTitle: email, accIcon: "", tag: "email")
        
        cellModels = [model0, model1, model2, model3, model6, model11]
    }
}
