//
//  BeneficiaryTableViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 07/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import UIKit
import Presentr

private let beneficiaryCellId = "beneficiaryCellId"

protocol AddressDelegate {
    func backFromAddressSelection(id: Int, string: String)
}

class BeneficiaryTableViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    let networking = Networking()
    
    var delegate: AddressDelegate?
    
    var currency: Int!
    var beneficiaries: [Beneficiary] = []

    lazy var addButton: UIButton = {
        let button = UIButton()
        button.setTitle("Add New Beneficiary".localized(), for: .normal)
        button.titleLabel?.font = UIFont(name: bold, size: 14)
        button.setTitleColor(.textColor, for: .normal)
        button.backgroundColor = .containerAlternative
        button.layer.cornerRadius = 4
        button.addTarget(self, action: #selector(tapAddAddress(_:)), for: .touchUpInside)
        return button
    }()
    
    lazy var tableView: UITableView = {
        let tv = UITableView(frame: .zero, style: .grouped)
        tv.contentInset = UIEdgeInsets(top: -20, left: 0, bottom: 0, right: 0)
        tv.backgroundColor = .container
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addSubviews()
        adjustUI()
        addConstraints()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(BeneficiaryTableViewCell.self, forCellReuseIdentifier: beneficiaryCellId)
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadBeneficiaryTable), name:NSNotification.Name(rawValue: "reloadBeneficiaryTable"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getBeneficiaries()
        
    }
}

// MARK: User Interface
extension BeneficiaryTableViewController {
    
    func addSubviews() {
        
        view.addSubview(addButton)
        view.addSubview(tableView)
    }
    
    func adjustUI() {
        
        view.backgroundColor = .container
        tableView.separatorStyle = .none
    }
    
    func addConstraints() {
        
        addButton.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(15)
            make.left.equalToSuperview().offset(11)
            make.right.equalToSuperview().offset(-11)
            make.height.equalTo(54)
        })
        
        tableView.snp.makeConstraints({ (make) in
            make.top.equalTo(addButton.snp.bottom)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.equalToSuperview()
        })
    }
}

// MARK: Action
extension BeneficiaryTableViewController {
    
    @objc func tapAddAddress(_ sender: UIButton) {
        let vc = AddBeneficiaryViewController()
        vc.currency = currency
        let presenter = Presentr(presentationType: .custom(width: .custom(size: Float(screenWidth-22)), height: .custom(size: 270), center: .center))
        self.customPresentViewController(presenter, viewController: vc, animated: true, completion: nil)
    }
    
    @objc func tapInfo(_ sender: UIButton) {
        let memo = beneficiaries.filter{ $0.id!.contains("\(sender.tag)") }
        showSimpleToast(memo.first?.memo ?? "")
    }
    
    @objc func tapDelete(_ sender: UIButton) {
        var alert = UIAlertController(title: "Delete Beneficiary".localized(), message: "Are you sure you want to delete this beneficiary?".localized(), preferredStyle: UIAlertController.Style.alert)
        alert.view.subviews.first?.subviews.first?.subviews.first?.backgroundColor = .container
        alert.view.subviews.first?.subviews.first?.subviews.last?.backgroundColor = .container
        alert.view.tintColor = .grayColor
        
        alert.setValue(NSAttributedString(string: "Delete Beneficiary".localized(), attributes: [NSAttributedString.Key.font : UIFont(name: bold, size: 15),NSAttributedString.Key.foregroundColor : UIColor.grayColor]), forKey: "attributedTitle")
        alert.setValue(NSAttributedString(string: "Are you sure you want to delete this beneficiary?".localized(), attributes: [NSAttributedString.Key.font: UIFont(name: regular, size: 13), NSAttributedString.Key.foregroundColor : UIColor.grayColor]), forKey: "attributedMessage")
        
        alert.addAction(UIAlertAction(title: "No".localized(), style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Yes".localized(), style: .default, handler: { action in
            self.deleteBeneficiary(id: sender.tag)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func reloadBeneficiaryTable() {
        getBeneficiaries()
    }
}

// MARK: Table
extension BeneficiaryTableViewController {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return beneficiaries.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: beneficiaryCellId) as! BeneficiaryTableViewCell
        
        if beneficiaries[indexPath.row].memo == nil {
            cell.infoButton.isHidden = true
        }
        
        cell.configure(beneficiaries[indexPath.row])
        cell.infoButton.addTarget(self, action: #selector(tapInfo(_:)), for: .touchUpInside)
        cell.infoButton.tag = Int(beneficiaries[indexPath.row].id!)!
        cell.deleteButton.addTarget(self, action: #selector(tapDelete(_:)), for: .touchUpInside)
        cell.deleteButton.tag = Int(beneficiaries[indexPath.row].id!)!
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let beneficiary = beneficiaries[indexPath.row]
        self.delegate?.backFromAddressSelection(id: Int(beneficiary.id!)!, string: beneficiary.addr!)
        self.dismiss(animated: true, completion: nil)
    }
}

// MARK: Networking
extension BeneficiaryTableViewController {
    
    func getBeneficiaries() {
        
        var params: [String: String] = [:]
        params["coinId"] = "\(currency!)"
        
        
        networking.getBeneficiaries(params: params, controller: self) { beneficiaries in
            
            print("bene")
            for elem in beneficiaries {
                print(elem.id)
            }
            
            self.beneficiaries = beneficiaries
            self.tableView.reloadData()
        }
    }
    
    func deleteBeneficiary(id: Int) {
    
        var params: [String: String] = [:]
        params["addressId"] = "\(id)"
        
        let vc = OneTimePasswordViewController()
        vc.type = "deleteBeneficiary"
        vc.params = params
        let presenter = Presentr(presentationType: .custom(width: .full, height: .custom(size: 680), center: .bottomCenter))
        self.customPresentViewController(presenter, viewController: vc, animated: true, completion: nil)
    }
}

