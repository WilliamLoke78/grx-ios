//
//  WalletViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 01/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

private let walletCellId = "walletCellId"

class WalletViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    var wallets: [Wallet] = []
    var coins: [Coin] = BaseSingleton.shared.coins ?? []
    fileprivate var isVisible: Bool = false
    
    let activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)

    fileprivate lazy var tableHeader: UIView = {
        let th = UIView(frame: CGRect.init(x: 0, y: -1, width: screenWidth, height: 50))
        th.backgroundColor = .navBackgroundColor
        return th
    }()

    fileprivate lazy var subHeaderView: WalletSubHeaderView = {
        let hv = WalletSubHeaderView()
        return hv
    }()

    fileprivate lazy var walletHeaderView: WalletHeaderView = {
        let hv = WalletHeaderView(frame: CGRect(x: 0, y: 90, width: 400, height: 90))
        hv.layer.shadowColor = UIColor.black.cgColor
        hv.layer.shadowOpacity = 0.2
        hv.layer.shadowOffset = CGSize.zero
        hv.layer.shadowRadius = 4
        hv.depositButton.addTarget(self, action: #selector(tapDeposit(_:)), for: .touchUpInside)
        hv.withdrawButton.addTarget(self, action: #selector(tapWithdraw(_:)), for: .touchUpInside)
        hv.transactionButton.addTarget(self, action: #selector(tapTransaction(_:)), for: .touchUpInside)
        return hv
    }()

    lazy var tableView: UITableView = {
        let tv = UITableView.init(frame: CGRect.zero, style: .grouped)
        tv.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
        tv.backgroundColor = .background
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        addSubviews()
        addConstraints()
        adjustUI()

        tableView.delegate = self
        tableView.dataSource = self

        tableView.register(WalletTableViewCell.self, forCellReuseIdentifier: walletCellId)
        
        getAllCoin()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if BaseSingleton.shared.token == "" {
            wallets.removeAll()
            tableView.reloadData()
            
            tabBarController?.selectedIndex = 4
            showSimpleToast("Please login to access your wallets".localized())
            return
        } else {
            getWalletsData()
        }
    }
}

extension WalletViewController {
    
    func addSubviews() {

        view.addSubview(tableHeader)
        view.addSubview(activityIndicator)
        tableHeader.addSubview(subHeaderView)
        tableHeader.addSubview(walletHeaderView)
        view.addSubview(tableView)
    }
    
    func adjustUI() {
        let visibleItem = UIBarButtonItem(image: UIImage(named: "invisible"), style: .plain, target: self, action: #selector(tapVisible(_:)))
        navigationItem.rightBarButtonItem = visibleItem

        tableView.separatorColor = .background
        view.sendSubviewToBack(tableView)
    }

    func addConstraints() {
        
        activityIndicator.snp.makeConstraints({ (make) in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
        })

        walletHeaderView.snp.makeConstraints({ (make) in
            make.left.equalToSuperview().offset(11)
            make.right.equalToSuperview().offset(-11)
            make.height.equalTo(80)
            make.top.equalToSuperview()
        })

        tableView.snp.makeConstraints({ (make) in
            make.top.equalTo(walletHeaderView.snp.bottom).offset(-30)
            make.left.equalTo(view.snp.left)
            make.right.equalTo(view.snp.right)
            make.bottom.equalTo(view.snp.bottom)
        })

    }
}

// MARK: Table
extension WalletViewController {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return wallets.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: walletCellId, for: indexPath) as! WalletTableViewCell
        
        let coin = coins.filter({ $0.id == wallets[indexPath.row].coinId }).first
        cell.configure(wallets[indexPath.row], isVisible: isVisible, decimalPlace: coin?.round ?? 6)

        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
}

// MARK: Action
extension WalletViewController {

    @objc func tapVisible(_ sender: UIBarButtonItem) {
        if isVisible == true {
            isVisible = false
            let visibleItem = UIBarButtonItem(image: UIImage(named: "invisible"), style: .plain, target: self, action: #selector(tapVisible(_:)))
            navigationItem.rightBarButtonItem = visibleItem
        } else {
            isVisible = true
            let visibleItem = UIBarButtonItem(image: UIImage(named: "visible"), style: .plain, target: self, action: #selector(tapVisible(_:)))
            navigationItem.rightBarButtonItem = visibleItem
        }
        tableView.reloadData()
    }

    @objc func tapDeposit(_ sender: UIButton) {
        let vc = DepositViewController()
        vc.wallets = wallets
        show(vc, sender: nil)
    }

    @objc func tapWithdraw(_ sender: UIButton) {
        let vc = WithdrawViewController()
        vc.wallets = wallets
        show(vc, sender: nil)
    }

    @objc func tapTransaction(_ sender: UIButton) {
        let vc = AllTransactionViewController()
        vc.transactionType = "Deposit"
        show(vc, sender: nil)
    }
}

// MARK: Networking
extension WalletViewController {
    
    func getAllCoin() {
        let networking = Networking()
        networking.getAllCoin(params: [:], controller: self) { coins in
            BaseSingleton.shared.coins = coins
            self.coins = coins
        }
    }

    func getWalletsData() {

        wallets.removeAll()
        tableView.reloadData()
        
        activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()

        let networking = Networking()
        networking.getUserAssets(params: [:], controller: self) { wallet in
            self.wallets = wallet
            self.tableView.reloadData()
            
            self.activityIndicator.stopAnimating()
            UIApplication.shared.endIgnoringInteractionEvents()
        }
    }
}
