//
//  AccountDetailViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 09/02/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import UIKit

private let accountDetailsCellId = "accountDetailsCellId"

class AccountDetailsTableViewController: BaseTableViewController {
    
    var account = AccountDetail()
    var businessTypes: [BusinessType] = []
    var subTypes: [BusinessType] = []
    private var cellModelArray: [CellModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        adjustUI()
        setupData(account)
        
        tableView.register(AccountSimpleTableViewCell.self, forCellReuseIdentifier: accountDetailsCellId)
    }
}

// MARK: User Interface
extension AccountDetailsTableViewController {
    
    func adjustUI() {
        title = "Transaction Details".localized()
        tableView.backgroundColor = .background
        tableView.tableFooterView = UIView()
        tableView.separatorColor = .background
    }
}

// MARK: Table
extension AccountDetailsTableViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellModelArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: accountDetailsCellId, for: indexPath) as! AccountSimpleTableViewCell
        
        let model = cellModelArray[indexPath.row]
        cell.label.text = model.title
        cell.subLabel.text = model.subTitle
        cell.arrowImageView.isHidden = true
        
        cell.subLabel.snp.remakeConstraints({ (make) in
            make.right.equalToSuperview().offset(-21)
            make.centerY.equalToSuperview()
        })
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56
    }
}

// MARK: Networking
extension AccountDetailsTableViewController {
    
    func setupData(_ account: AccountDetail) {
        
        cellModelArray.removeAll()
        
        var businessType: BusinessType
        var subType: BusinessType
        
        businessType = businessTypes.filter({ $0.value == account.businessType}).first ?? BusinessType.init(aJSON: [])
        subType = subTypes.filter({ $0.value == account.businessSubType }).first ?? BusinessType.init(aJSON: [])

        let model0 = CellModel(icon: "", title: "Transaction Type".localized(), subTitle: businessType.display!, accIcon: "", tag: "")
        let model1 = CellModel(icon: "", title: "Action".localized(), subTitle: subType.display!, accIcon: "", tag: "")
        let model2 = CellModel(icon: "", title: "Time".localized(), subTitle: account.created!.serverToLocal(), accIcon: "", tag: "")
        let model3 = CellModel(icon: "", title: "Order ID".localized(), subTitle: account.orderId!, accIcon: "", tag: "")
        let model4 = CellModel(icon: "", title: "Account ID".localized(), subTitle: account.accountId!, accIcon: "", tag: "")
        let model5 = CellModel(icon: "", title: "Before Amount".localized(), subTitle: "\(account.beforeAmount!)".forceDecimalDigits(8) + " \(account.coinName ?? "")", accIcon: "", tag: "")
        let model6 = CellModel(icon: "", title: "Change Amount".localized(), subTitle: "\(account.changeAmount!)".forceDecimalDigits(8) + " \(account.coinName ?? "")", accIcon: "", tag: "")
        let model7 = CellModel(icon: "", title: "Fee".localized(), subTitle: "\(account.fee!)".forceDecimalDigits(8) + " \(account.coinName ?? "")", accIcon: "", tag: "")
        let model8 = CellModel(icon: "", title: "After Amount".localized(), subTitle: "\(account.afterAmount!)".forceDecimalDigits(8) + " \(account.coinName ?? "")", accIcon: "", tag: "")

        cellModelArray = [model0, model1, model2, model3, model4, model5, model6, model7, model8]
    }
}
