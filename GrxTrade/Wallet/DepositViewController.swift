//
//  DepositViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 30/12/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

class DepositViewController: BaseViewController {
    
    private var walletAddress: DepositResponse?
    var wallets: [Wallet] = []
    
    lazy var chooseCoinView: ChooseCoinView = {
        let view = ChooseCoinView()
        view.button.addTarget(self, action: #selector(tapChooseCoin(_:)), for: .touchUpInside)
        return view
    }()
    
    lazy var depositView: DepositView = {
        let view = DepositView()
        view.copyButton.addTarget(self, action: #selector(tapCopyAddress(_:)), for: .touchUpInside)
        return view
    }()
    
    lazy var remarkLabel: UILabel = {
        let label = UILabel()
        label.text = "Please make sure the wallet address and memo address provided are accurate and be sure to verify the wallet address of the type of token or coin. Any funds deposited in the wrong token/coin address cannot be recovered.".localized()
        label.textColor = .grayColor
        label.font = UIFont(name: regular, size: 12)
        label.numberOfLines = 5
        return label
    }()
    
    override func viewDidLoad() {
        
        addSubviews()
        addConstraints()
        adjustUI()
        
        getDepositAddress(coin: wallets.first!.coinName!)
    }
}

// MARK: UI
extension DepositViewController {
    
    func addSubviews() {
        view.addSubview(chooseCoinView)
        view.addSubview(depositView)
        view.addSubview(remarkLabel)
    }
    
    func adjustUI() {
        title = "Deposit".localized()
        view.backgroundColor = .backgroundAlternative
        
        let transactionItem = UIBarButtonItem(image: UIImage(named: "transaction"), style: .plain, target: self, action: #selector(tapTransaction(_:)))
        self.navigationItem.rightBarButtonItem = transactionItem
    }
    
    func addConstraints() {
        chooseCoinView.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(20)
            make.left.equalToSuperview().offset(11)
            make.right.equalToSuperview().offset(-11)
            make.height.equalTo(42)
        })
        
        depositView.snp.makeConstraints({ (make) in
            make.top.equalTo(chooseCoinView.snp.bottom).offset(10)
            make.left.equalToSuperview().offset(11)
            make.right.equalToSuperview().offset(-11)
            make.height.equalTo(327)
        })
        
        remarkLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(depositView.snp.bottom).offset(18)
            make.left.equalToSuperview().offset(11)
            make.right.equalToSuperview().offset(-11)
        })
    }
}

// MARK: Action
extension DepositViewController {
    
    @objc func tapTransaction(_ sender: UIBarButtonItem) {
        let vc = WalletTransactionViewController()
        vc.transactionType = "Deposit"
        self.show(vc, sender: nil)
    }
    
    @objc func tapChooseCoin(_ sender: UIButton) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.view.subviews.first?.subviews.first?.subviews.first?.backgroundColor = .container
        alert.view.subviews.first?.subviews.first?.subviews.last?.backgroundColor = .container
        alert.view.tintColor = .grayColor
        
        for wallet in wallets {
            
            alert.addAction(UIAlertAction(title: wallet.coinName, style: .default , handler:{ (UIAlertAction) in
                self.updateSelectedCoin(coin: wallet.coinName!)
            }))
        }
        
        alert.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    @objc func tapCopyAddress(_ sender: UIButton) {
        UIPasteboard.general.string = depositView.depositAddressLabel.text
        showSimpleToast("Deposit address has been copied to clipboard".localized())
    }
    
    func updateSelectedCoin(coin: String) {
        chooseCoinView.currencyLabel.text = coin.uppercased()
        getDepositAddress(coin: coin)
    }
}

// MARK: Networking
extension DepositViewController {
    
    func getDepositAddress(coin: String) {
        
        var params: [String: String] = [:]
        params["symbol"] = coin
        
        print("params: \(params)")
        
        let networking = Networking()
        networking.getDepositAddress(params: params, controller: self) { depositResponse in
            
            print("response:")
            print(depositResponse)
            
            let wallet = depositResponse
            self.depositView.depositAddressLabel.text = wallet.address
            self.depositView.qrImageView.isHidden = false
            self.depositView.qrImageView.image = wallet.address!.generateQRCode()
            
            if wallet.memo == nil {
                self.depositView.memoTitleLabel.isHidden = true
                self.depositView.memoValueLabel.isHidden = true
            } else {
                self.depositView.memoTitleLabel.isHidden = false
                self.depositView.memoValueLabel.isHidden = false
                self.depositView.memoValueLabel.text = wallet.memo
            }
        }
    }
}
