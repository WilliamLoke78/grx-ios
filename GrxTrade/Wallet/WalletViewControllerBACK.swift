////
////  WalletViewController.swift
////  GrxTrade
////
////  Created by Leon Mah Kean Loon on 01/10/2019.
////  Copyright © 2019 Leon Mah. All rights reserved.
////
//
//import UIKit
//
//private let walletCellId = "walletCellId"
//
//class WalletViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
//
//    fileprivate var wallets: [Wallet] = []
//    fileprivate var isVisible: Bool = false
//    
//    fileprivate lazy var tableHeader: UIView = {
//        let th = UIView(frame: CGRect.init(x: 0, y: -1, width: screenWidth, height: 50))
//        th.backgroundColor = .navBackgroundColor
//        return th
//    }()
//    
//    fileprivate lazy var subHeaderView: WalletSubHeaderView = {
//        let hv = WalletSubHeaderView()
//        return hv
//    }()
//    
//    fileprivate lazy var walletHeaderView: WalletHeaderView = {
//        let hv = WalletHeaderView(frame: CGRect(x: 0, y: 90, width: 400, height: 90))
//        hv.layer.shadowColor = UIColor.black.cgColor
//        hv.layer.shadowOpacity = 0.2
//        hv.layer.shadowOffset = CGSize.zero
//        hv.layer.shadowRadius = 4
//        hv.depositButton.addTarget(self, action: #selector(tapDeposit(_:)), for: .touchUpInside)
//        hv.withdrawButton.addTarget(self, action: #selector(tapWithdraw(_:)), for: .touchUpInside)
//        hv.transactionButton.addTarget(self, action: #selector(tapTransaction(_:)), for: .touchUpInside)
//        return hv
//    }()
//    
//    lazy var tableView: UITableView = {
//        let tv = UITableView.init(frame: CGRect.zero, style: .grouped)
//        tv.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
//        tv.backgroundColor = .background
//        tv.translatesAutoresizingMaskIntoConstraints = false
//        return tv
//    }()
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        addSubviews()
//        addConstraints()
//        adjustUI()
//        
//        tableView.delegate = self
//        tableView.dataSource = self
//        
//        tableView.register(WalletTableViewCell.self, forCellReuseIdentifier: walletCellId)
//        
//        getWalletsData()
//    }
//    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        
//        getWalletsData()
//    }
//}
//
//extension WalletViewController {
//    func adjustUI() {
//    
//        let visibleItem = UIBarButtonItem(image: UIImage(named: "invisible"), style: .plain, target: self, action: #selector(tapVisible(_:)))
//        navigationItem.rightBarButtonItem = visibleItem
//        
//        view.sendSubviewToBack(tableView)
//        
//        tableView.separatorColor = .background
//    }
//    
//    func addEvents() {
//        
//    }
//    
//    func addSubviews() {
//        let visibleItem = UIBarButtonItem(image: UIImage(named: "invisible"), style: .plain, target: self, action: #selector(tapVisible(_:)))
//        navigationItem.rightBarButtonItem = visibleItem
//        
//        view.addSubview(tableHeader)
//        tableHeader.addSubview(subHeaderView)
//        tableHeader.addSubview(walletHeaderView)
//        
//        view.addSubview(tableView)
//    }
//    
//    func addConstraints() {
//        
//        walletHeaderView.snp.makeConstraints({ (make) in
//            make.left.equalToSuperview().offset(11)
//            make.right.equalToSuperview().offset(-11)
//            make.height.equalTo(80)
//            make.top.equalToSuperview()
//        })
//        
//        tableView.snp.makeConstraints({ (make) in
//            make.top.equalTo(walletHeaderView.snp.bottom).offset(-30)
//            make.left.equalTo(view.snp.left)
//            make.right.equalTo(view.snp.right)
//            make.bottom.equalTo(view.snp.bottom)
//        })
//        
//    }
//}
//
//// MARK: Table
//extension WalletViewController {
//    
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
// 
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return wallets.count
//    }
//    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: walletCellId, for: indexPath) as! WalletTableViewCell
//        
//        cell.configure(wallets[indexPath.row], isVisible: isVisible)
//        
//        return cell
//    }
//    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 80
//    }
//    
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return 20
//    }
//}
//
//// MARK: Action
//extension WalletViewController {
//    
//    @objc func tapVisible(_ sender: UIBarButtonItem) {
//        if isVisible == true {
//            isVisible = false
//            let visibleItem = UIBarButtonItem(image: UIImage(named: "invisible"), style: .plain, target: self, action: #selector(tapVisible(_:)))
//            navigationItem.rightBarButtonItem = visibleItem
//        } else {
//            isVisible = true
//            let visibleItem = UIBarButtonItem(image: UIImage(named: "visible"), style: .plain, target: self, action: #selector(tapVisible(_:)))
//            navigationItem.rightBarButtonItem = visibleItem
//        }
//        tableView.reloadData()
//    }
//    
//    @objc func tapDeposit(_ sender: UIButton) {
//        let vc = DepositViewController()
//        vc.wallets = wallets
//        show(vc, sender: nil)
//    }
//    
//    @objc func tapWithdraw(_ sender: UIButton) {
//        let vc = WithdrawViewController()
//        vc.wallets = wallets
//        show(vc, sender: nil)
//    }
//    
//    @objc func tapTransaction(_ sender: UIButton) {
//        let vc = AllTransactionViewController()
//        vc.transactionType = "Deposit"
//        show(vc, sender: nil)
//    }
//}
//
//// MARK: Networking
//extension WalletViewController {
//    
//    func getWalletsData() {
//        
//        wallets.removeAll()
//        tableView.reloadData()
//        
//        print("Get wallets")
//        print("Token: \(BaseSingleton.shared.user.accessToken)")
//        
//        let networking = Networking()
//        networking.getWalletAddress(params: [:], controller: self) { wallet in
//            
//            print("wallets")
//            print(wallet)
//            
//            self.wallets = wallet
//            self.tableView.reloadData()
//        }
//    }
//}
