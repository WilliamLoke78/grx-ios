//
//  TransactionTableViewCell.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 30/12/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

class TransactionTableViewCell: BaseTableViewCell {
    
    lazy var iconImageView: UIImageView = {
        let iv = UIImageView()
        return iv
    }()
    
    lazy var coinLabel: UILabel = {
        let label = UILabel()
        label.text = "BTC"
        label.font = UIFont(name: regular, size: 12)
        label.textColor = .grayColor
        return label
    }()
    
    lazy var stateLabel: UILabel = {
        let label = UILabel()
        label.text = "Accepted"
        label.font = UIFont(name: bold, size: 14)
        label.textColor = .grayColor
        return label
    }()
    
    lazy var datetimeLabel: UILabel = {
        let label = UILabel()
        label.text = "Oct 12, 2019"
        label.font = UIFont(name: regular, size: 12)
        label.textColor = .grayColor
        return label
    }()
    
    lazy var amountLabel: UILabel = {
        let label = UILabel()
        label.text = "12.0004"
        label.font = UIFont(name: bold, size: 14)
        label.textColor = .textColor
        return label
    }()
    
    lazy var bottomLine: UIView = {
        let view = UIView()
        view.backgroundColor = .background
        return view
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .container
        
        addSubview(iconImageView)
        addSubview(coinLabel)
        addSubview(stateLabel)
        addSubview(datetimeLabel)
        addSubview(amountLabel)
        addSubview(bottomLine)
        
        iconImageView.snp.makeConstraints({ (make) in
            make.left.equalToSuperview().offset(10)
            make.centerY.equalToSuperview()
            make.height.equalTo(42)
            make.width.equalTo(42)
        })
        
        coinLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(iconImageView.snp.top)
            make.left.equalTo(iconImageView.snp.right).offset(10)
        })
        
        stateLabel.snp.makeConstraints({ (make) in
            make.left.equalTo(coinLabel)
            make.top.equalTo(coinLabel.snp.bottom)
        })
        
        datetimeLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(iconImageView.snp.top)
            make.right.equalToSuperview().offset(-21)
        })
        
        amountLabel.snp.makeConstraints({ (make) in
            make.right.equalToSuperview().offset(-21)
            make.top.equalTo(datetimeLabel.snp.bottom)
        })
        
        bottomLine.snp.makeConstraints({ (make) in
            make.bottom.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalTo(1)
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(_ transaction: Transaction) {
        
        coinLabel.text = transaction.coinName?.uppercased()
        datetimeLabel.text = transaction.created
        amountLabel.text = "\(transaction.amount ?? 0.0)"
        
        switch transaction.status {
        case 0:
            stateLabel.text = "Verifying".localized()
        case 1:
            stateLabel.text = "Success".localized()
        case 2:
            stateLabel.text = "Rejected".localized()
        case 3:
            stateLabel.text = "Revoked".localized()
        case 4:
            stateLabel.text = "Passed Verification".localized()
        case 5:
            stateLabel.text = "Transferring".localized()
        case 6:
            stateLabel.text = "Abnormal Status".localized()
        default:
            stateLabel.text = "- -"
        }
    }
    
//    override func resetTheme() {
//        backgroundColor = .container
//        bottomLine.backgroundColor = .background
//    }
}
