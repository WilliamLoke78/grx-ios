//
//  WalletTableViewCell.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 17/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

class WalletTableViewCell: BaseTableViewCell {
    
    lazy var button: UIButton = {
        let button = UIButton()
        button.layer.cornerRadius = 4
        button.backgroundColor = .container
        return button
    }()
    
    lazy var currencyLabel: UILabel = {
        let label = UILabel()
        label.text = "BTC"
        label.textColor = .primary
        label.font = UIFont(name: bold, size: 16)
        return label
    }()
    
    lazy var availableLabel: UILabel = {
        let label = UILabel()
        label.text = "Available".localized()
        label.textColor = .grayColor
        label.font = UIFont(name: regular, size: 11)
        return label
    }()
    
    lazy var currencyValueLabel: UILabel = {
        let label = UILabel()
        label.text = "******"
        label.textColor = .textColor
        label.font = UIFont(name: bold, size: 14)
        return label
    }()
    
    lazy var arrowImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "arrow_right")
        iv.isHidden = true
        return iv
    }()
    
    lazy var lockedLabel: UILabel = {
        let label = UILabel()
        label.text = "Locked".localized()
        label.textColor = .grayColor
        label.font = UIFont(name: regular, size: 11)
        return label
    }()
    
    lazy var lockedValueLabel: UILabel = {
        let label = UILabel()
        label.text = "0.00"
        label.textColor = .grayColor
        label.font = UIFont(name: regular, size: 14)
        return label
    }()
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .darkTheme, object: nil)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .background
        
        addSubview(button)
        
        button.snp.makeConstraints({ (make) in
            make.top.bottom.equalToSuperview()
            make.left.equalToSuperview().offset(11)
            make.right.equalToSuperview().offset(-11)
        })
        
        button.addSubview(currencyLabel)
        button.addSubview(availableLabel)
        button.addSubview(currencyValueLabel)
        button.addSubview(arrowImageView)
        button.addSubview(lockedLabel)
        button.addSubview(lockedValueLabel)
        
        currencyLabel.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(7)
            make.left.equalToSuperview().offset(11)
        })
        
        availableLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(currencyLabel.snp.bottom).offset(9)
            make.left.equalTo(currencyLabel.snp.left)
        })
        
        currencyValueLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(availableLabel.snp.bottom)
            make.left.equalTo(currencyLabel.snp.left)
        })
        
        arrowImageView.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(7)
            make.right.equalToSuperview().offset(-11)
        })
        
        lockedLabel.snp.makeConstraints({ (make) in
//            make.top.equalTo(arrowImageView.snp.bottom).offset(9)
            make.top.equalToSuperview().offset(9)
            make.right.equalTo(arrowImageView.snp.right)
        })
        
        lockedValueLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(lockedLabel.snp.bottom)
            make.right.equalTo(arrowImageView.snp.right)
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(_ wallets: WalletProtocol, isVisible: Bool, decimalPlace: Int) {
        currencyLabel.text = wallets.coinName?.uppercased()
        
        if isVisible == true {
            currencyValueLabel.text = "\(wallets.available ?? 0)".forceDecimalDigits(decimalPlace)
            lockedValueLabel.text = "\(wallets.freeze ?? 0)".forceDecimalDigits(decimalPlace)
        } else {
            currencyValueLabel.text = "******"
            lockedValueLabel.text = "******"
        }
    }
}
