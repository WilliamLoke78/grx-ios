//
//  WalletHeaderView.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 17/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

class WalletHeaderView: UIView {
    
    lazy var depositButton: ImageButton = {
        let button = ImageButton()
        button.setImage(UIImage(named: "deposit"), for: .normal)
        button.label.text = "Deposit".localized()
        return button
    }()
    
    lazy var withdrawButton: ImageButton = {
        let button = ImageButton()
        button.setImage(UIImage(named: "withdrawal"), for: .normal)
        button.label.text = "Withdraw".localized()
        return button
    }()
    
    lazy var transactionButton: ImageButton = {
        let button = ImageButton()
        button.setImage(UIImage(named: "transaction"), for: .normal)
        button.label.text = "Transaction".localized()
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .container
        layer.cornerRadius = 4
        
        addSubview(depositButton)
        addSubview(withdrawButton)
        addSubview(transactionButton)
        
        let blockWidth = (screenWidth-22)/3
        
        depositButton.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(5)
            make.bottom.equalToSuperview().offset(-5)
            make.left.equalToSuperview()
            make.width.equalTo(blockWidth)
        })
        withdrawButton.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(5)
            make.bottom.equalToSuperview().offset(-5)
            make.centerX.equalToSuperview()
            make.width.equalTo(blockWidth)
        })
        transactionButton.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(5)
            make.bottom.equalToSuperview().offset(-5)
            make.right.equalToSuperview()
            make.width.equalTo(blockWidth)
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
