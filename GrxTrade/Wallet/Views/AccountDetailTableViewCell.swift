//
//  AccountDetailTableViewCell.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 09/02/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import UIKit

class AccountDetailTableViewCell: BaseTableViewCell {
    
    lazy var orderIdLabel: UILabel = {
        let label = UILabel()
        label.textColor = .grayColor
        label.font = UIFont(name: regular, size: 12)
        return label
    }()
    
    lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .container
        view.layer.cornerRadius = 4
        return view
    }()
    
    lazy var typeLabel: UILabel = {
        let label = UILabel()
        label.textColor = .textColor
        label.adjustsFontSizeToFitWidth = true
        label.font = UIFont(name: bold, size: 14)
        return label
    }()
    
    lazy var datetimeLabel: UILabel = {
        let label = UILabel()
        label.textColor = .grayColor
        label.font = UIFont(name: regular, size: 12)
        return label
    }()
    
    lazy var separatorLine: UIView = {
        let view = UIView()
        view.backgroundColor = .background
        return view
    }()
    
    lazy var actionLabel: UILabel = {
        let label = UILabel()
        label.textColor = .grayColor
        label.font = UIFont(name: regular, size: 14)
        return label
    }()
    
    lazy var amountLabel: UILabel = {
        let label = UILabel()
        label.text = "Amount".localized()
        label.font = UIFont(name: regular, size: 12)
        return label
    }()
    
    lazy var changeAmountLabel: UILabel = {
        let label = UILabel()
        label.textColor = .grayColor
        label.font = UIFont(name: bold, size: 14)
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .background
        
        addSubview(orderIdLabel)
        addSubview(containerView)
        containerView.addSubview(typeLabel)
        containerView.addSubview(datetimeLabel)
        containerView.addSubview(separatorLine)
        containerView.addSubview(actionLabel)
        containerView.addSubview(amountLabel)
        containerView.addSubview(changeAmountLabel)
        
        orderIdLabel.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(11)
            make.left.equalToSuperview().offset(12)
        })
        
        containerView.snp.makeConstraints({ (make) in
            make.top.equalTo(orderIdLabel.snp.bottom).offset(5)
            make.left.equalToSuperview().offset(12)
            make.right.equalToSuperview().offset(-12)
            make.height.equalTo(100)
        })
        
        typeLabel.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(11)
            make.left.equalToSuperview().offset(11)
            make.width.equalTo(containerView.snp.width).offset(-150)
        })
        
        datetimeLabel.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(13)
            make.right.equalToSuperview().offset(-11)
        })
        
        separatorLine.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(40)
            make.left.equalToSuperview().offset(11)
            make.right.equalToSuperview().offset(-11)
            make.height.equalTo(1)
        })
        
        amountLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(separatorLine.snp.bottom).offset(10)
            make.right.equalToSuperview().offset(-11)
        })
        
        actionLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(amountLabel.snp.top).offset(5)
            make.left.equalToSuperview().offset(11)
        })
        
        changeAmountLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(amountLabel.snp.bottom)
            make.right.equalToSuperview().offset(-11)
        })
    }
    
    func configure(_ account: AccountDetail, businessTypes: [BusinessType], subTypes: [BusinessType]) {
        
        orderIdLabel.text = "Order ID".localized() + ": \(account.orderId ?? "")"
        
        let businessType = businessTypes.filter({ $0.value == account.businessType}).first
        typeLabel.text = businessType?.display?.capitalized
        
        let subType = subTypes.filter({ $0.value == account.businessSubType}).first
        actionLabel.text = subType?.display?.capitalized
        
        datetimeLabel.text = account.created?.serverToLocal()
        changeAmountLabel.text = "\(account.changeAmount ?? 0.0)".forceDecimalDigits(8) + " \(account.coinName ?? "")"
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
