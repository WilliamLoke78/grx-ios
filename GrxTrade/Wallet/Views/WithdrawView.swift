//
//  WithdrawalView.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 30/12/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

class WithdrawView: BaseView {

    lazy var topLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.containerAlternative.withAlphaComponent(0.2)
        return view
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: regular, size: 12)
        label.textColor = .grayColor
        label.text = "Receive Amount".localized()
        return label
    }()
    
    lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: bold, size: 20)
        label.textColor = .primary
        label.adjustsFontSizeToFitWidth = true
//        label.text = "0.0000001 BTC"
        return label
    }()
    
    lazy var button: UIButton = {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 128, height: 44))
        button.titleLabel?.font = UIFont(name: bold, size: 16)
        button.setTitle("Withdraw".localized(), for: .normal)
        button.layer.cornerRadius = 22
        button.applyGradient(with: ["#5686F4".hex(), "#385CAD".hex()], gradient: .topRightBottomLeft)
        button.clipsToBounds = true
        
        return button
    }()
    
    override func setupViews() {
     
        addSubview(topLine)
        addSubview(titleLabel)
        addSubview(valueLabel)
        addSubview(button)
        
        topLine.snp.makeConstraints({ (make) in
            make.top.equalToSuperview()
            make.height.equalTo(0.5)
            make.width.equalToSuperview()
        })
        
        titleLabel.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(10)
            make.left.equalToSuperview().offset(22)
        })
        
        valueLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(titleLabel.snp.bottom)
            make.left.equalToSuperview().offset(22)
            make.right.equalTo(button.snp.left).offset(-30)
        })
        
        button.snp.makeConstraints({ (make) in
            make.top.equalTo(titleLabel.snp.top)
            make.right.equalToSuperview().offset(-20)
            make.height.equalTo(44)
            make.width.equalTo(128)
        })
    }

}

typealias GradientPoints = (startPoint: CGPoint, endPoint: CGPoint)

enum GradientOrientation {
    case topRightBottomLeft
    case topLeftBottomRight
    case horizontal
    case vertical
    
    var startPoint : CGPoint {
        return points.startPoint
    }
    
    var endPoint : CGPoint {
        return points.endPoint
    }
    
    var points : GradientPoints {
        switch self {
        case .topRightBottomLeft:
            return (CGPoint(x: 0.0,y: 1.0), CGPoint(x: 1.0,y: 0.0))
        case .topLeftBottomRight:
            return (CGPoint(x: 0.0,y: 0.0), CGPoint(x: 1,y: 1))
        case .horizontal:
            return (CGPoint(x: 0.0,y: 0.5), CGPoint(x: 1.0,y: 0.5))
        case .vertical:
            return (CGPoint(x: 0.0,y: 0.0), CGPoint(x: 0.0,y: 1.0))
        }
    }
}
