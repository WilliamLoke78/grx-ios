//
//  ChooseCoinView.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 30/12/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

class ChooseCoinView: BaseView {
    
    lazy var button: UIButton = {
        let button = UIButton()
        return button
    }()
    
    lazy var currencyLabel: UILabel = {
        let label = UILabel()
        label.text = "BTC"
        label.font = UIFont(name: bold, size: 14)
        label.textColor = .textColor
        return label
    }()
    
    lazy var chooseLabel: UILabel = {
        let label = UILabel()
        label.text = "Choose coin".localized()
        label.font = UIFont(name: regular, size: 12)
        label.textColor = .primary
        return label
    }()
    
    override func setupViews() {
        
        backgroundColor = .containerAlternative
        layer.cornerRadius = 4
        
        addSubview(button)
        button.addSubview(currencyLabel)
        button.addSubview(chooseLabel)
        
        button.snp.makeConstraints({ (make) in
            make.height.equalToSuperview()
            make.width.equalToSuperview()
        })
        
        currencyLabel.snp.makeConstraints({ (make) in
            make.left.equalToSuperview().offset(11)
            make.centerY.equalToSuperview()
        })
        
        chooseLabel.snp.makeConstraints({ (make) in
            make.right.equalToSuperview().offset(-11)
            make.centerY.equalToSuperview()
        })
    }
}
