//
//  DepositView.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 30/12/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

class DepositView: BaseView {
    
    lazy var depositAddressTitleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: regular, size: 12)
        label.textColor = .grayColor
        label.text = "Deposit Address".localized()
        return label
    }()
    
    lazy var depositAddressLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: regular, size: 12)
        label.textColor = .textColor
        label.text = "- -"
        return label
    }()
    
    lazy var copyButton: UIButton = {
        let button = UIButton()
        button.setTitle("Copy Address".localized(), for: .normal)
        button.backgroundColor = UIColor.primary.withAlphaComponent(0.2)
        button.setTitleColor(.primary, for: .normal)
        button.titleLabel?.font = UIFont(name: bold, size: 12)
        button.frame = CGRect(x: 0, y: 0, width: 120, height: 32)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 4
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.primary.cgColor
        return button
    }()
    
    lazy var qrImageView: UIImageView = {
        let iv = UIImageView()
        return iv
    }()
    
    lazy var memoTitleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: regular, size: 12)
        label.textColor = .grayColor
        label.text = "Memo".localized()
        return label
    }()
    
    lazy var memoValueLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: regular, size: 12)
        label.textColor = .textColor
        label.text = "- -"
        return label
    }()
    
    lazy var saveButton: UIButton = {
        let button = UIButton()
        button.setTitle("Save QR Code".localized(), for: .normal)
        button.backgroundColor = .primary
        button.titleLabel?.font = UIFont(name: bold, size: 16)
        button.layer.cornerRadius = 4
        button.isHidden = true
        return button
    }()
    
    lazy var shareButton: UIButton = {
        let button = UIButton()
        button.setTitle("Share".localized(), for: .normal)
        button.setTitleColor(.primary, for: .normal)
        button.titleLabel?.font = UIFont(name: bold, size: 16)
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.primary.cgColor
        button.layer.cornerRadius = 4
        button.isHidden = true
        return button
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .containerAlternative
        layer.cornerRadius = 2
        
        addSubview(depositAddressTitleLabel)
        addSubview(depositAddressLabel)
        addSubview(copyButton)
        addSubview(qrImageView)
        addSubview(memoTitleLabel)
        addSubview(memoValueLabel)
        addSubview(saveButton)
        addSubview(shareButton)
        
        depositAddressTitleLabel.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(15)
            make.centerX.equalToSuperview()
        })
        
        depositAddressLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(depositAddressTitleLabel.snp.bottom).offset(4)
            make.centerX.equalToSuperview()
        })
        
        copyButton.snp.makeConstraints({ (make) in
            make.top.equalTo(depositAddressLabel.snp.bottom).offset(10)
            make.centerX.equalToSuperview()
            make.height.equalTo(32)
            make.width.equalTo(120)
        })
        
        qrImageView.snp.makeConstraints({ (make) in
            make.top.equalTo(copyButton.snp.bottom).offset(25)
            make.centerX.equalToSuperview()
            make.height.equalTo(117)
            make.width.equalTo(117)
        })
        
        memoTitleLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(qrImageView.snp.bottom).offset(25)
            make.centerX.equalToSuperview()
        })
        
        memoValueLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(memoTitleLabel.snp.bottom).offset(4)
            make.centerX.equalToSuperview()
        })
        
        saveButton.snp.makeConstraints({ (make) in
            make.bottom.equalToSuperview().offset(-29)
            make.height.equalTo(44)
            make.width.equalTo(150)
            make.left.equalToSuperview().offset(20)
        })
        
        shareButton.snp.makeConstraints({ (make) in
            make.bottom.equalToSuperview().offset(-29)
            make.height.equalTo(44)
            make.width.equalTo(150)
            make.right.equalToSuperview().offset(-20)
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
