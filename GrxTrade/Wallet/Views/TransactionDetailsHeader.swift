//
//  TransactionDetailsHeader.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 30/12/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

class TransactionDetailsHeader: BaseView {

    lazy var iconImageView: UIImageView = {
        let iv = UIImageView()
        return iv
    }()
    
    lazy var currencyLabel: UILabel = {
        let label = UILabel()
        label.textColor = .textColor
        label.font = UIFont(name: regular, size: 14)
        return label
    }()
    
    lazy var amountLabel: UILabel = {
        let label = UILabel()
        label.textColor = .textColor
        label.font = UIFont(name: bold, size: 20)
        return label
    }()
    
    lazy var feeLabel: UILabel = {
        let label = UILabel()
        label.textColor = .grayColor
        label.font = UIFont(name: regular, size: 12)
        return label
    }()
    
    override func setupViews() {
        
        addSubview(iconImageView)
        addSubview(currencyLabel)
        addSubview(amountLabel)
        addSubview(feeLabel)
        
        iconImageView.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(25)
            make.centerX.equalToSuperview()
            make.height.equalTo(42)
            make.width.equalTo(42)
        })
        
        currencyLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(iconImageView.snp.bottom).offset(9)
            make.centerX.equalToSuperview()
        })
        
        amountLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(currencyLabel.snp.bottom)
            make.centerX.equalToSuperview()
        })
        
        feeLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(amountLabel.snp.bottom).offset(6)
            make.centerX.equalToSuperview()
        })
    }

}
