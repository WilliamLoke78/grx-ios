//
//  BeneficiaryTableViewCell.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 07/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import UIKit
import SwiftyJSON

class BeneficiaryTableViewCell: BaseTableViewCell {

    lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.text = "Babayaga"
        label.textColor = .textColor
        label.font = UIFont(name: bold, size: 16)
        return label
    }()
    
    lazy var addressLabel: UILabel = {
        let label = UILabel()
        label.text = "123123123"
        label.textColor = .grayColor
        label.font = UIFont(name: regular, size: 14)
        return label
    }()
    
    lazy var infoButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "info"), for: .normal)
        return button
    }()
    
    lazy var deleteButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "trash"), for: .normal)
        return button
    }()
    
    lazy var separatorLine: UIView = {
        let view = UIView()
        view.backgroundColor = .grayColor
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(nameLabel)
        addSubview(addressLabel)
        addSubview(infoButton)
        addSubview(deleteButton)
        addSubview(separatorLine)
        
        nameLabel.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(17)
            make.left.equalToSuperview().offset(10)
        })
        
        addressLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(nameLabel.snp.bottom)
            make.left.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-40)
        })
        
        deleteButton.snp.makeConstraints({ (make) in
            make.right.equalToSuperview().offset(-11)
            make.centerY.equalToSuperview()
            make.height.equalTo(30)
            make.width.equalTo(30)
        })
        
        infoButton.snp.makeConstraints({ (make) in
            make.right.equalTo(deleteButton.snp.left).offset(-11)
            make.centerY.equalToSuperview()
            make.height.equalTo(30)
            make.width.equalTo(30)
        })
        
        separatorLine.snp.makeConstraints({ (make) in
            make.height.equalTo(0.5)
            make.bottom.equalToSuperview()
            make.left.right.equalToSuperview()
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(_ beneficiary: Beneficiary) {
        
        nameLabel.text = beneficiary.name
        addressLabel.text = beneficiary.addr
    }    
}
