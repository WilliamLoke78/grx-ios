//
//  WalletSubHeaderView.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 23/12/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

class WalletSubHeaderView: UIView {
    
    lazy var titlelabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(14)
        label.textColor = .grayColor
        label.text = "Total Balance"
        label.font = UIFont(name: "Montserrat Bold", size: 20)
        return label
    }()
    
    lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: bold, size: 22)
        label.textColor = .white
        label.text = "1,000.00"
        return label
    }()
    
    lazy var visibleButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "profile"), for: .normal)
//        button.addTarget(self, action: #selector(<#T##@objc method#>), for: .touchUpInside)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
//        addSubview(titlelabel)
//        addSubview(subtitleLabel)
//        addSubview(visibleButton)
//
//        addConstraintsWithFormat(format: "V:|-11-[v0]", views: titlelabel)
//        addConstraintsWithFormat(format: "V:|-11-[v0]", views: visibleButton)
//        addConstraintsWithFormat(format: "H:|-13-[v0][v1]-13-|", views: titlelabel, visibleButton)
//
//        subtitleLabel.snp.makeConstraints({ (make) in
//            make.top.equalTo(titlelabel.snp.bottom)
//            make.left.equalTo(titlelabel.snp.left)
//        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

