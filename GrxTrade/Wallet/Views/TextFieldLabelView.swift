//
//  TextFieldLabelView.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 30/12/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

class TextFieldLabelView: BaseView {
    
    lazy var button: UIButton = {
        let button = UIButton()
        return button
    }()
    
    lazy var label: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: regular, size: 12)
        label.textColor = .grayColor
        return label
    }()
    
    lazy var textFieldLabel: UILabel = {
        let tf = UILabel()
        tf.backgroundColor = .clear
        tf.textColor = .textColor
        return tf
    }()
    
    lazy var firstButton: UIButton = {
        let button = UIButton()
        button.isHidden = true
        return button
    }()
    
    lazy var lastButton: UIButton = {
        let button = UIButton()
        let image = UIImage.init(named: "invisible")
        button.setImage(image, for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        button.isHidden = true
        return button
    }()
    
    override func setupViews() {
        
        self.backgroundColor = .containerAlternative
        self.layer.cornerRadius = 4
        
        self.addSubview(button)
        button.addSubview(label)
        button.addSubview(textFieldLabel)
        button.addSubview(firstButton)
        button.addSubview(lastButton)
        
        button.snp.makeConstraints({ (make) in
            make.height.equalToSuperview()
            make.width.equalToSuperview()
        })
        
        label.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(5)
            make.left.equalToSuperview().offset(10)
        })
        
        textFieldLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(label.snp.bottom).offset(5)
            make.left.equalToSuperview().offset(10)
            make.right.equalTo(firstButton.snp.left)
        })
        
        firstButton.snp.makeConstraints({ (make) in
            make.centerY.equalToSuperview()
            make.right.equalTo(lastButton.snp.left).offset(-15)
            make.height.equalTo(20)
            make.width.equalTo(20)
        })
        
        lastButton.snp.makeConstraints({ (make) in
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(-11)
            make.height.equalTo(20)
            make.width.equalTo(20)
        })
    }
}
