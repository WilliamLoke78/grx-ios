//
//  AddBeneficiaryViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 07/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import UIKit
import AVFoundation
import Presentr

class AddBeneficiaryViewController: BaseViewController, QrDelegate {
    
    var currency: Int?

    lazy var addressView: TextFieldView = {
        let view = TextFieldView()
        view.label.text = "Withdrawal Address".localized()
        view.textField.autocorrectionType = .no
        view.firstButton.frame = CGRect(x: 0, y: 0, width: 60, height: 32)
        view.firstButton.isHidden = false
        view.firstButton.layer.cornerRadius = 4
        view.firstButton.layer.borderWidth = 1
        view.firstButton.titleLabel?.font = UIFont(name: bold, size: 12)
        view.firstButton.setTitleColor(.primary, for: .normal)
        view.firstButton.setTitle("Paste".localized(), for: .normal)
        view.firstButton.backgroundColor = UIColor.primary.withAlphaComponent(0.2)
        view.firstButton.layer.borderColor = UIColor.primary.cgColor
        view.firstButton.snp.remakeConstraints({ (make) in
            make.height.equalTo(32)
            make.width.equalTo(60)
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(-47)
        })
        view.firstButton.addTarget(self, action: #selector(tapPaste(_:)), for: .touchUpInside)
        view.lastButton.isHidden = false
        view.lastButton.setImage(UIImage(named: "scan"), for: .normal)
        view.lastButton.addTarget(self, action: #selector(tapScan(_:)), for: .touchUpInside)
        return view
    }()
    
    lazy var nameView: TextFieldView = {
        let view = TextFieldView()
        view.label.text = "Beneficiary Name".localized()
        return view
    }()
    
    lazy var descView: TextFieldView = {
        let view = TextFieldView()
        view.label.text = "Memo (optional)".localized()
        return view
    }()
    
    lazy var submitButton: UIButton = {
        let button = UIButton()
        button.setTitle("Submit".localized(), for: .normal)
        button.backgroundColor = .primary
        button.layer.cornerRadius = 4
        button.addTarget(self, action: #selector(tapSubmit(_:)), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addSubviews()
        addConstraints()
        adjustUI()
        
        NotificationCenter.default.addObserver(self, selector: #selector(popAddBeneficiaryView), name:NSNotification.Name(rawValue: "popAddBeneficiaryView"), object: nil)
    }
}

// MARK: User Interface
extension AddBeneficiaryViewController {
    
    func addSubviews() {
        view.addSubview(addressView)
        view.addSubview(nameView)
        view.addSubview(descView)
        view.addSubview(submitButton)
    }
    
    func addConstraints() {
        
        addressView.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(20)
            make.height.equalTo(50)
            make.width.equalToSuperview().offset(-22)
            make.centerX.equalToSuperview()
        })
        
        nameView.snp.makeConstraints({ (make) in
            make.top.equalTo(addressView.snp.bottom).offset(10)
            make.height.equalTo(50)
            make.width.equalToSuperview().offset(-22)
            make.centerX.equalToSuperview()
        })
        
        descView.snp.makeConstraints({ (make) in
            make.top.equalTo(nameView.snp.bottom).offset(10)
            make.height.equalTo(50)
            make.width.equalToSuperview().offset(-22)
            make.centerX.equalToSuperview()
        })
        
        submitButton.snp.makeConstraints({ (make) in
            make.top.equalTo(descView.snp.bottom).offset(10)
            make.height.equalTo(50)
            make.width.equalToSuperview().offset(-22)
            make.centerX.equalToSuperview()
        })
    }
    
    func adjustUI() {
        view.backgroundColor = .container
        view.layer.cornerRadius = 8
    }
}

// MARK: Action
extension AddBeneficiaryViewController {
 
    @objc func tapSubmit(_ sender: UIButton) {
        
        guard (addressView.textField.text != "") else {
            showSimpleToast("Address cannot be empty".localized())
            return
        }
        
        guard (nameView.textField.text != "") else {
            showSimpleToast("Name cannot be empty".localized())
            return
        }
        
        postBeneficiary()
    }
    
    @objc func tapScan(_ sender: UIButton) {
        let vc = ScanViewController()
        vc.modalPresentationStyle = .fullScreen
        vc.delegate = self

        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized:
            present(vc, animated: true, completion: nil)

        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { granted in
                if granted {
                    DispatchQueue.main.async {
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            }

        case .denied:
            let alert = UIAlertController(title: "Camera".localized(), message: "Camera is needed to scan QR code".localized(), preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
                } else {
                    // Fallback on earlier versions
                }
            }))

            self.present(alert, animated: true)
            return

        case .restricted:
            showSimpleToast("Camera is needed to scan QR code".localized())
            return
        }
    }

    @objc func tapPaste(_ sender: UIButton) {
        let pb = UIPasteboard.general
        addressView.textField.text = pb.string
    }
    
    func backFromCamera(string: String) {
        print("string: \(string)")
        addressView.textField.text = string
    }
    
    @objc func popAddBeneficiaryView() {
        showSimpleToast("Beneficiary has been added".localized())
        dismiss(animated: true, completion: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadBeneficiaryTable"), object: nil)
    }
}

// MARK: Networking
extension AddBeneficiaryViewController {
    
    func postBeneficiary() {
        
        var params: [String: String] = [:]
        params["name"] = nameView.textField.text
        params["addr"] = addressView.textField.text
        params["coinId"] = "\(currency ?? 0)"
        params["memo"] = descView.textField.text
        
        let vc = OneTimePasswordViewController()
        vc.type = "addBeneficiary"
        vc.params = params
        let presenter = Presentr(presentationType: .custom(width: .full, height: .custom(size: 680), center: .bottomCenter))
        self.customPresentViewController(presenter, viewController: vc, animated: true, completion: nil)
        
        
    }
}

