//
//  TransactionDetailsViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 30/12/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

private let detailsCellId = "detailsCellId"

class TransactionDetailsViewController: BaseTableViewController {
    
    var transactionType = String()
    var transaction: Transaction?
    private var cellModels: [CellModel] = []
    
    lazy var tableHeader: TransactionDetailsHeader = {
        let th = TransactionDetailsHeader(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 166))
        return th
    }()
    
    override func viewDidLoad() {
        
        addSubviews()
        adjustUI()
        
        setupData(transaction!)
        tableView.register(AccountSimpleTableViewCell.self, forCellReuseIdentifier: detailsCellId)
    }
}

// MARK: UI
extension TransactionDetailsViewController {
    
    func addSubviews() {
        tableView.addSubview(tableHeader)
        tableView.tableHeaderView = tableHeader
        tableView.tableFooterView = UIView()
    }
    
    func adjustUI() {
        title = "Transaction Details".localized()
        tableView.backgroundColor = .background
        tableView.separatorColor = .background
    }
}

// MARK: Table
extension TransactionDetailsViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellModels.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: detailsCellId, for: indexPath) as! AccountSimpleTableViewCell
        
        let model = cellModels[indexPath.row]
        cell.label.text = model.title
        cell.subLabel.text = model.subTitle
        cell.subLabel.textColor = .textColor
        cell.arrowImageView.isHidden = true
        
        cell.subLabel.snp.remakeConstraints({ (make) in
            make.right.equalToSuperview().offset(-21)
            make.centerY.equalToSuperview()
        })
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56
    }
    
    func setupData(_ transaction: Transaction) {

        tableHeader.iconImageView.image = UIImage(named: transactionType == "Withdraws" ? "withdraw_round" : "deposit_round")
        tableHeader.currencyLabel.text = "\(transaction.coinName ?? "")"
        tableHeader.amountLabel.text = "\(transaction.amount ?? 0.0)"
        
        if transaction.fee != nil {
            tableHeader.feeLabel.text = "You paid a fee.".localized(with: [String(describing: "\(transaction.fee!)" + " \(transaction.coinName ?? "")")])
        }

        var statusLabel = String()
        switch transaction.status {
        case 0:
            statusLabel = "Verifying".localized()
        case 1:
            statusLabel = "Success".localized()
        case 2:
            statusLabel = "Rejected".localized()
        case 3:
            statusLabel = "Revoked".localized()
        case 4:
            statusLabel = "Passed Verification".localized()
        case 5:
            statusLabel = "Transferring".localized()
        case 6:
            statusLabel = "Abnormal Status".localized()
        default:
            statusLabel = "- -"
        }
        
        let model0 = CellModel(icon: "", title: "Transaction type".localized(), subTitle: transactionType.localized(), accIcon: "", tag: "")
        let model1 = CellModel(icon: "", title: "Transaction status".localized(), subTitle: statusLabel, accIcon: "", tag: "")
        let model2 = CellModel(icon: "", title: "Date", subTitle: transaction.created!, accIcon: "", tag: "")

        cellModels = [model0, model1, model2]
    }
}
