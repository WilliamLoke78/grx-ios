//
//  AllTransactionViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 30/12/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

private let transactionCellId = "transactionCellId"
private let accountCellId = "accountCellId"

class AllTransactionViewController: BaseTableViewController {

    var transactionType = String()
    var transactions: [Transaction] = []
    var accounts: [AccountDetail] = []
    var businessTypes: [BusinessType] = []
    var businessSubTypes: [BusinessType] = []
    
    let networking = Networking()

    lazy var topBarView: TradeTopBarView = {
        let view = TradeTopBarView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 40))
        view.buyTab.setTitle("Deposits".localized(), for: .normal)
        view.buyTab.addTarget(self, action: #selector(tapDeposit(_:)), for: .touchUpInside)
        view.sellTab.setTitle("Withdraws".localized(), for: .normal)
        view.sellTab.addTarget(self, action: #selector(tapWithdraw(_:)), for: .touchUpInside)
        view.orderTab.setTitle("All".localized(), for: .normal)
        view.orderTab.addTarget(self, action: #selector(tapAll(_:)), for: .touchUpInside)
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.backgroundColor = .white

        addSubviews()
        addConstraints()
        adjustUI()

        tableView.register(TransactionTableViewCell.self, forCellReuseIdentifier: transactionCellId)
        tableView.register(AccountDetailTableViewCell.self, forCellReuseIdentifier: accountCellId)
        
        getBusinessTypes()
        getBusinessSubTypes()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        getTransactionData(transactionType: "\(transactionType)")
    }
}

// MARK: User Interface
extension AllTransactionViewController {

    func addSubviews() {
        view.addSubview(topBarView)
        tableView.tableHeaderView = topBarView
    }

    func adjustUI() {
//        title = transactionType.localized()
        title = "Transaction History".localized()
        tableView.backgroundColor = .background
        tableView.separatorStyle = .none
        tableView.alwaysBounceVertical = false
    }

    func addConstraints() {
//        topBarView.snp.makeConstraints({ (make) in
//            make.top.equalToSuperview()
//            make.height.equalTo(40)
//            make.width.equalToSuperview()
//        })
//
//        tableView.snp.makeConstraints({ (make) in
//            make.top.equalTo(topBarView.snp.bottom).offset(10)
//        })
    }
}

// MARK: Table
extension AllTransactionViewController {

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if accounts.count > 0 {
            return accounts.count
        }
        
        if transactions.count > 0 {
            return transactions.count
        }
        
        return 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if accounts.count > 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: accountCellId, for: indexPath) as! AccountDetailTableViewCell
            cell.configure(accounts[indexPath.row], businessTypes: businessTypes, subTypes: businessSubTypes)
            
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: transactionCellId, for: indexPath) as! TransactionTableViewCell

        cell.iconImageView.image = UIImage(named: transactionType == "Withdraw" ? "withdrawal_round" : "deposit_round")
        cell.configure(transactions[indexPath.row])

        return cell
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if accounts.count > 0 {
            return 130
        }
        
        return 70
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if accounts.count > 0 {
            let vc = AccountDetailsTableViewController()
            vc.account = accounts[indexPath.row]
            vc.businessTypes = businessTypes
            vc.subTypes = businessSubTypes
            show(vc, sender: nil)
            return
        }
        
        let vc = TransactionDetailsViewController()
        vc.transactionType = transactionType
        vc.transaction = transactions[indexPath.row]
        show(vc, sender: nil)
    }
}

// MARK: Networking
extension AllTransactionViewController {

    func getTransactionData(transactionType: String) {
        
        if transactionType == "Deposit" {
            getDepositRecords()
            return
        }
        
        if transactionType == "Withdraw" {
            getWithdrawRecords()
            return
        }
        
        if transactionType == "All" {
            getAllTransactions()
            return
        }
    }
    
    func getDepositRecords() {
        self.transactions.removeAll()
        self.accounts.removeAll()
        networking.getDepositRecord(params: [:], controller: self) { transactions in
            self.transactions = transactions
            self.tableView.reloadData()
        }
    }

    func getWithdrawRecords() {
        self.transactions.removeAll()
        self.accounts.removeAll()
        networking.getWithdrawRecord(params: [:], controller: self) { transactions in
            self.transactions = transactions
            self.tableView.reloadData()
        }
    }
    
    func getBusinessTypes() {
        networking.getAllBusinessTypes(params: [:], controller: self) { businessType in
            self.businessTypes = businessType
        }
    }
    
    func getBusinessSubTypes() {
        networking.getAllBusinessSubTypes(params: [:], controller: self) { businessType in
            self.businessSubTypes = businessType
        }
    }
    
    func getAllTransactions() {
        
        self.transactions.removeAll()
        self.accounts.removeAll()
        
        var params: [String: Any] = [:]
        params["current"] = "1"
        params["size"] = "50"
        
        networking.getAllTransactions(params: params, controller: self) { account in
            self.accounts = account.records!
            self.tableView.reloadData()
        }
    }
}

// MARK: Action
extension AllTransactionViewController {

    @objc func tapDeposit(_ sender: UIButton) {
        transactionType = "Deposit"
//        title = transactionType.localized()
        topBarView.buyBottomLine.backgroundColor = .primary
        topBarView.buyTab.setTitleColor(.primary, for: .normal)
        topBarView.sellBottomLine.backgroundColor = .clear
        topBarView.sellTab.setTitleColor(.grayColor, for: .normal)
        topBarView.orderBottomLine.backgroundColor = .clear
        topBarView.orderTab.setTitleColor(.grayColor, for: .normal)
        getDepositRecords()
    }

    @objc func tapWithdraw(_ sender: UIButton) {
        transactionType = "Withdraw"
//        title = transactionType.localized()
        topBarView.buyBottomLine.backgroundColor = .clear
        topBarView.buyTab.setTitleColor(.grayColor, for: .normal)
        topBarView.sellBottomLine.backgroundColor = .primary
        topBarView.sellTab.setTitleColor(.primary, for: .normal)
        topBarView.orderBottomLine.backgroundColor = .clear
        topBarView.orderTab.setTitleColor(.grayColor, for: .normal)
        getWithdrawRecords()
    }
    
    @objc func tapAll(_ sender: UIButton) {
        transactionType = "All"
//        title = transactionType.localized()
        topBarView.buyBottomLine.backgroundColor = .clear
        topBarView.buyTab.setTitleColor(.grayColor, for: .normal)
        topBarView.sellBottomLine.backgroundColor = .clear
        topBarView.sellTab.setTitleColor(.grayColor, for: .normal)
        topBarView.orderBottomLine.backgroundColor = .primary
        topBarView.orderTab.setTitleColor(.primary, for: .normal)
        getAllTransactions()
    }
}
