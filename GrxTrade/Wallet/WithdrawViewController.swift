//
//  WithdrawViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 30/12/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit
import Presentr

class WithdrawViewController: BaseViewController, SuccessDelegate, AddressDelegate, UITextFieldDelegate {

    var coins: [Coin] = BaseSingleton.shared.coins ?? []
    var wallets: [Wallet] = []
    var currency: Int?
    
    var feeType: Int?
    var feeRate: Double?
    
    var beneficiaryId: Int?
    let networking = Networking()
    
    lazy var chooseCoinView: ChooseCoinView = {
        let view = ChooseCoinView()
        view.button.addTarget(self, action: #selector(tapChooseCoin(_:)), for: .touchUpInside)
        return view
    }()
    
    lazy var addressView: TextFieldLabelView = {
        let view = TextFieldLabelView()
        view.label.text = "Withdraw Address".localized()
        view.button.addTarget(self, action: #selector(tapAddress(_:)), for: .touchUpInside)
        return view
    }()
    
    lazy var amountView: TextFieldView = {
        let view = TextFieldView()
        view.textField.keyboardType = .decimalPad
        view.label.text = "Amount".localized()
        view.textField.delegate = self
        view.textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        return view
    }()
    
    lazy var feeView: TextFieldView = {
        let view = TextFieldView()
        view.label.text = "Fee".localized()
        view.textField.isEnabled = false
        view.textField.text = ""
        return view
    }()
    
    lazy var passwordView: TextFieldView = {
        let view = TextFieldView()
        view.label.text = "Transaction Password".localized()
        view.backgroundColor = .background
        view.lastButton.isHidden = false
        view.lastButton.addTarget(self, action: #selector(togglePassword(_:)), for: .touchUpInside)
        view.textField.isSecureTextEntry = true
        view.tag = 2
        view.isUserInteractionEnabled = true
        return view
    }()
    
    lazy var availableLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: regular, size: 11)
        label.textColor = .grayColor
        return label
    }()
    
    lazy var remarkLabel1: UILabel = {
        let label = UILabel()
        label.text = "Please make sure whether the wallet address and memo address are accurate and also confirm wallet address of various type of token or coin. Any funds deposited in non-corresponding token/coin address will not be recovered.".localized()
        label.numberOfLines = 5
        label.font = UIFont(name: regular, size: 12)
        label.textColor = .grayColor
        return label
    }()
    
    lazy var remarkLabel2: UILabel = {
        let label = UILabel()
        label.text = "Once you have submitted your withdrawal request, we will send a confirmation email. Please clock on the confirmation link in your email.".localized()
        label.numberOfLines = 3
        label.font = UIFont(name: regular, size: 12)
        label.textColor = .grayColor
        return label
    }()
    
    lazy var withdrawView: WithdrawView = {
        let view = WithdrawView()
        view.backgroundColor = .containerAlternative
        view.button.addTarget(self, action: #selector(tapWithdraw(_:)), for: .touchUpInside)
        return view
    }()
    
    lazy var scrollView: UIScrollView = {
        let v = UIScrollView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.contentSize = CGSize(width: screenWidth, height: screenHeight + 40)
        return v
    }()
    
    override func viewDidLoad() {
        
        addSubviews()
        adjustUI()
        addConstraints()
        
        currency = wallets.first?.coinId
        availableLabel.text = "Available".localized() + " \(wallets.first?.available ?? 0) \(wallets.first?.coinName ?? "")"
        withdrawView.valueLabel.text = "0.000000 \(wallets.first?.coinName ?? "")"
        
        hideKeyboardWhenTappedAround()
    }
}

// MARK: UI
extension WithdrawViewController {
    
    func addSubviews() {
        
        view.addSubview(scrollView)
        scrollView.addSubview(chooseCoinView)
        scrollView.addSubview(addressView)
        scrollView.addSubview(amountView)
        scrollView.addSubview(availableLabel)
        scrollView.addSubview(feeView)
        scrollView.addSubview(passwordView)
        scrollView.addSubview(remarkLabel1)
        scrollView.addSubview(remarkLabel2)
        scrollView.addSubview(withdrawView)
    }
    
    func adjustUI() {
        title = "Withdraw".localized()
        view.backgroundColor = .backgroundAlternative
        
        let transactionItem = UIBarButtonItem(image: UIImage(named: "transaction"), style: .plain, target: self, action: #selector(tapTransaction(_:)))
        self.navigationItem.rightBarButtonItem = transactionItem
    }
    
    func addConstraints() {
        
        scrollView.snp.makeConstraints({ (make) in
            make.edges.equalToSuperview()
        })
        
        chooseCoinView.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(20)
            make.left.equalTo(view).offset(11)
            make.right.equalTo(view).offset(-11)
            make.height.equalTo(42)
        })
        
        addressView.snp.makeConstraints({ (make) in
            make.top.equalTo(chooseCoinView.snp.bottom).offset(20)
            make.left.right.equalTo(chooseCoinView)
            make.height.equalTo(54)
            make.width.equalTo(50)
        })

        amountView.snp.makeConstraints({ (make) in
            make.top.equalTo(addressView.snp.bottom).offset(10)
            make.left.right.equalTo(chooseCoinView)
            make.height.equalTo(54)
        })
        
        availableLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(amountView.snp.bottom).offset(3)
            make.left.right.equalTo(chooseCoinView)
        })
        
        feeView.snp.makeConstraints({ (make) in
            make.top.equalTo(availableLabel.snp.bottom).offset(16)
            make.left.right.equalTo(chooseCoinView)
            make.height.equalTo(54)
        })
        
        passwordView.snp.makeConstraints({ (make) in
            make.top.equalTo(feeView.snp.bottom).offset(10)
            make.left.right.equalTo(chooseCoinView)
            make.height.equalTo(54)
        })
        
        remarkLabel1.snp.makeConstraints({ (make) in
            make.top.equalTo(passwordView.snp.bottom).offset(62)
            make.left.right.equalTo(chooseCoinView)
        })
        
        remarkLabel2.snp.makeConstraints({ (make) in
            make.top.equalTo(remarkLabel1.snp.bottom).offset(10)
            make.left.right.equalTo(chooseCoinView)
        })
        
        withdrawView.snp.makeConstraints({ (make) in
            make.bottom.equalTo(view)
            make.left.right.equalTo(view)
            make.height.equalTo(80)
        })
    }
}

// MARK: Action
extension WithdrawViewController {
     
    @objc func tapChooseCoin(_ sender: UIButton) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.view.subviews.first?.subviews.first?.subviews.first?.backgroundColor = .container
        alert.view.subviews.first?.subviews.first?.subviews.last?.backgroundColor = .container
        alert.view.tintColor = .grayColor
        
        for wallet in wallets {
            
            alert.addAction(UIAlertAction(title: wallet.coinName, style: .default , handler:{ (UIAlertAction) in
                self.updateSelectedCoin(wallet: wallet)
            }))
        }
        
        alert.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    func updateSelectedCoin(wallet: Wallet) {
        chooseCoinView.currencyLabel.text = wallet.coinName?.uppercased()
        currency = wallet.coinId
        beneficiaryId = 0
        addressView.textFieldLabel.text = ""
        amountView.textField.text = ""
        availableLabel.text = "Available \(wallet.available ?? 0) \(wallet.coinName ?? "")"
        feeView.textField.text = ""
        withdrawView.valueLabel.text = "0.00000000 \(wallet.coinName?.uppercased() ?? "")"
    }
    
    @objc func tapAddress(_ sender: UIButton) {
        displayBeneficiaryTable()
    }
    
    @objc func tapTransaction(_ sender: UIBarButtonItem) {
        let vc = WalletTransactionViewController()
        vc.transactionType = "Withdraw"
        self.show(vc, sender: nil)
    }
    
    @objc func tapWithdraw(_ sender: UIButton) {
        postWithdraw(addressId: beneficiaryId ?? 0, coinId: currency ?? 0)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        let amount = Double(amountView.textField.text!) ?? 0
        let fee = Double(feeView.textField.text!) ?? 0
        
        print("feetype: \(feeType)")
        
        if feeType == 2 {
            print("amount: \(amount*feeRate!)")
        }
        
        var receiveAmount = amount - fee
        
        if receiveAmount < 0 {
            receiveAmount = 0
        }
        
        let coin = coins.filter({ $0.id == currency }).first
        var decimalPlace = Int()
        if textField.tag == 0 {
            decimalPlace = coin!.round ?? 6
        } else if textField.tag == 1 {
            decimalPlace = coin!.round ?? 6
        }
        
        withdrawView.valueLabel.text = "\(receiveAmount)".forceDecimalDigits(decimalPlace) + " BTC"
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
        let coin = coins.filter({ $0.id == currency }).first
        var decimalPlace = Int()
        if textField.tag == 0 {
            decimalPlace = coin!.round ?? 6
        } else if textField.tag == 1 {
            decimalPlace = coin!.round ?? 6
        }
        
        let dotString = "."
        if let text = textField.text {
           let isDeleteKey = string.isEmpty

           if !isDeleteKey {
               if text.contains(dotString) {
                   if text.components(separatedBy: dotString)[1].count == decimalPlace {
                       return false
                   }
               }
           }
        }
        return true
    }
    
    @objc private func togglePassword(_ sender: UIButton) {
        passwordView.textField.isSecureTextEntry.toggle()
        if passwordView.textField.isSecureTextEntry == true {
            passwordView.lastButton.setImage(UIImage(named: "invisible"), for: .normal)
        } else {
            passwordView.lastButton.setImage(UIImage(named: "visible"), for: .normal)
        }
    }
    
    func displayBeneficiaryTable() {
        
        let vc = BeneficiaryTableViewController()
        vc.delegate = self
        vc.currency = currency
        let presenter = Presentr(presentationType: .bottomHalf)
        self.customPresentViewController(presenter, viewController: vc, animated: true, completion: nil)
    }
    
    func displaySuccess() {
        let vc = SuccessViewController()
        vc.subtitleLabel.text = "Your withdrawal request has been submitted.".localized()
        let presenter = Presentr(presentationType: .custom(width: .full, height: .custom(size: 350), center: .bottomCenter))
        presenter.presentationType = .bottomHalf
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.customPresentViewController(presenter, viewController: vc, animated: true, completion: nil)
        }
    }
    
    func backFromAddressSelection(id: Int, string: String) {
        beneficiaryId = id
        addressView.textFieldLabel.text = string
        
        getWithdrawFee(addressId: "\(id)", coinId: "\(currency!)")
    }
}

extension WithdrawViewController {
    
    func getWithdrawFee(addressId: String, coinId: String) {
        
        var params: [String: String] = [:]
        params["addressId"] = addressId
        params["coinId"] = coinId
        
        print("withdraw fee")
        print(params)

        networking.getWithdrawFee(params: params, controller: self) { withdrawFee in
            
            var fee = Double()
            let type = withdrawFee.feeType
            
            print("network feetype: \(type)")
            print("withdraw fee")
            print(withdrawFee.feeType)
            print(withdrawFee.feeRate)
            
            self.feeType = type!
            self.feeRate = withdrawFee.feeRate
            
            if type == 1 {
                fee = withdrawFee.feeRate!
                self.feeView.textField.text = "\(fee)"
                return
            }
        }
    }
    
    func postWithdraw(addressId: Int, coinId: Int) {
        
        var params: [String: String] = [:]
        params["address"] = addressView.textFieldLabel.text
        params["addressId"] = "\(addressId)"
        params["amount"] = amountView.textField.text
        params["coinId"] = "\(coinId)"
        params["payPassword"] = passwordView.textField.text?.md5()
            
        networking.postWithdraw(params: params, controller: self) { response in
            print("withdraw fee")
            print(response)
            
            self.navigationController?.popViewController(animated: true)
        }
    }
}
