//
//  WalletTransactionViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 13/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import UIKit

private let transactionCellId = "transactionCellId"

class WalletTransactionViewController: BaseTableViewController {

    var transactionType = String()
    var transactions: [Transaction] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        adjustUI()
        
        tableView.register(TransactionTableViewCell.self, forCellReuseIdentifier: transactionCellId)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        getTransactionData(transactionType: transactionType)
    }
}

// MARK: User Interface
extension WalletTransactionViewController {
    
    func adjustUI() {
        title = transactionType.localized()
        tableView.backgroundColor = .background
        tableView.separatorStyle = .none
    }
}

// MARK: Table
extension WalletTransactionViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transactions.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: transactionCellId, for: indexPath) as! TransactionTableViewCell
        
        cell.iconImageView.image = UIImage(named: transactionType == "Withdraw" ? "withdrawal_round" : "deposit_round")
        cell.configure(transactions[indexPath.row])
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = TransactionDetailsViewController()
        vc.transactionType = transactionType
        vc.transaction = transactions[indexPath.row]
        show(vc, sender: nil)
    }
}

// MARK: Networking
extension WalletTransactionViewController {
    
    func getTransactionData(transactionType: String) {
    
        let networking = Networking()
        
        
        
        print("transac type")
        print(transactionType)
        
        if transactionType == "Deposit" {
            
            networking.getDepositRecord(params: [:], controller: self) { transactions in
                
                self.transactions.removeAll()
                
                print("deposit")
                
                self.transactions.removeAll()
                self.transactions = transactions
                self.tableView.reloadData()
            }
        }
        
        if transactionType == "Withdraw" {
            
            networking.getWithdrawRecord(params: [:], controller: self) { transactions in
                
                print("withdraw")
                print(transactions)
                
                self.transactions.removeAll()
                self.transactions = transactions
                self.tableView.reloadData()
            }
        }
        
        
    }
}
