//
//  Order.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 17/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import SwiftyJSON

protocol OrderProtocol: JSONParse {
    
    var id: String? { get set }
    var symbol: String? { get set }
    var price: String? { get set }
    var volume: String? { get set }
    var amount: String? { get set }
    var dealVolume: String? { get set }
    var dealAmount: String? { get set }
    var orderType: Int? { get set }
    var category: Int? { get set }
    var hidden: Int? { get set }
    var status: Int? { get set } // 0:pending, 1:done, 2:canceled
    var created: String? { get set }
}

class Order: OrderProtocol, Codable {
    
    var id: String?
    var symbol: String?
    var price: String?
    var volume: String?
    var amount: String?
    var dealVolume: String?
    var dealAmount: String?
    var orderType: Int?
    var category: Int?
    var hidden: Int?
    var status: Int?
    var created: String?
    
    init() {
        
    }
    
    required convenience init(aJSON: JSON) {
        self.init(fromDictionary: aJSON.dictionaryObject ?? [:])
    }
    
    required init(fromDictionary dictionary: [String: Any]) {
        
        id = dictionary["id"] as? String ?? ""
        symbol = dictionary["symbol"] as? String ?? ""
        price = dictionary["price"] as? String ?? ""
        volume = dictionary["volume"] as? String ?? ""
        amount = dictionary["amount"] as? String ?? ""
        dealVolume = dictionary["dealVolume"] as? String ?? ""
        dealAmount = dictionary["dealAmount"] as? String ?? ""
        orderType = dictionary["orderType"] as? Int ?? 1
        category = dictionary["category"] as? Int ?? 1
        hidden = dictionary["hidden"] as? Int ?? 0
        status = dictionary["status"] as? Int ?? 0
        created = dictionary["created"] as? String ?? ""
    }
    
    func toDictionary() -> [String : Any] {
        var dictionary = [String: Any]()
        
        if id != nil {
            dictionary["id"] = id
        }
        if symbol != nil {
            dictionary["symbol"] = symbol
        }
        if price != nil {
            dictionary["price"] = price
        }
        if volume != nil {
            dictionary["volume"] = volume
        }
        if amount != nil {
            dictionary["amount"] = amount
        }
        if dealVolume != nil {
            dictionary["dealVolume"] = dealVolume
        }
        if dealAmount != nil {
            dictionary["dealAmount"] = dealAmount
        }
        if orderType != nil {
            dictionary["orderType"] = orderType
        }
        if category != nil {
            dictionary["category"] = category
        }
        if hidden != nil {
            dictionary["hidden"] = hidden
        }
        if status != nil {
            dictionary["status"] = status
        }
        if created != nil {
            dictionary["created"] = created
        }
        
        return dictionary
    }
}
