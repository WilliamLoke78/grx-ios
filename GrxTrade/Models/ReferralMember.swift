//
//  ReferralMember.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 09/02/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import SwiftyJSON

//protocol ReferralMemberProtocol: JSONParse {
//
//    var id: Int? { get set }
//    var usrLogin: String? { get set }
//    var created: String? { get set }
//    var status: Int? { get set }
//    var coin: String? { get set }
//    var earning: String? { get set }
//    var afLevel: Int? { get set }
//    var ptLevel: Int? { get set }
//}
//
//class ReferralMember: ReferralMemberProtocol {
//
//    var id: Int?
//    var usrLogin: String?
//    var created: String?
//    var status: Int?
//    var coin: String?
//    var earning: String?
//    var afLevel: Int?
//    var ptLevel: Int?
//
//    init() {
//
//    }
//
//    required convenience init(aJSON: JSON) {
//        self.init(fromDictionary: aJSON.dictionaryObject ?? [:])
//    }
//
//    required init(fromDictionary dictionary: [String: Any]) {
//
//        id = dictionary["id"] as? Int
//        usrLogin = dictionary["usrLogin"] as? String ?? ""
//        created = dictionary["created"] as? String ?? ""
//        status = dictionary["status"] as? Int ?? 0
//        coin = dictionary["coin"] as? String ?? ""
//        earning = dictionary["earning"] as? String ?? ""
//        afLevel = dictionary["afLevel"] as? Int ?? 0
//        ptLevel = dictionary["ptLevel"] as? Int ?? 0
//    }
//
//    func toDictionary() -> [String : Any] {
//        var dictionary = [String: Any]()
//
//        if id != nil {
//            dictionary["id"] = id
//        }
//        if usrLogin != nil {
//            dictionary["usrLogin"] = usrLogin
//        }
//        if created != nil {
//            dictionary["created"] = created
//        }
//        if status != nil {
//            dictionary["status"] = status
//        }
//        if coin != nil {
//            dictionary["coin"] = coin
//        }
//        if earning != nil {
//            dictionary["earning"] = earning
//        }
//        if afLevel != nil {
//            dictionary["afLevel"] = afLevel
//        }
//        if ptLevel != nil {
//            dictionary["ptLevel"] = ptLevel
//        }
//
//        return dictionary
//    }
//}

import Foundation
import SwiftyJSON

protocol ReferralMemberProtocol: JSONParse {
    
    var id: Int? { get set }
    var usrLogin: String? { get set }
    var created: String? { get set }
    var status: Int? { get set }
    var coin: String? { get set }
    var earning: String? { get set }
    var afLevel: Int? { get set }
    var ptLevel: Int? { get set }
}

class ReferralMember: ReferralMemberProtocol {
    
    var id: Int?
    var usrLogin: String?
    var created: String?
    var status: Int?
    var coin: String?
    var earning: String?
    var afLevel: Int?
    var ptLevel: Int?
    
    required convenience init(aJSON: JSON) {
        self.init(fromDictionary: aJSON.dictionaryObject ?? [:])
    }
    
    required init(fromDictionary dictionary: [String: Any]) {

        id = dictionary["id"] as? Int
        usrLogin = dictionary["usrLogin"] as? String ?? ""
        created = dictionary["created"] as? String ?? ""
        status = dictionary["status"] as? Int ?? 0
        coin = dictionary["coin"] as? String ?? ""
        earning = dictionary["earning"] as? String ?? ""
        afLevel = dictionary["afLevel"] as? Int ?? 0
        ptLevel = dictionary["ptLevel"] as? Int ?? 0
    }
    
    func toDictionary() -> [String : Any] {
        var dictionary = [String: Any]()

        if id != nil {
            dictionary["id"] = id
        }
        if usrLogin != nil {
            dictionary["usrLogin"] = usrLogin
        }
        if created != nil {
            dictionary["created"] = created
        }
        if status != nil {
            dictionary["status"] = status
        }
        if coin != nil {
            dictionary["coin"] = coin
        }
        if earning != nil {
            dictionary["earning"] = earning
        }
        if afLevel != nil {
            dictionary["afLevel"] = afLevel
        }
        if ptLevel != nil {
            dictionary["ptLevel"] = ptLevel
        }

        return dictionary
    }
}
