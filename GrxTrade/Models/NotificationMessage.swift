//
//  Notification.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 19/02/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import SwiftyJSON

protocol NotificationProtocol: JSONParse {
    
    var id: String? { get set }
    var userId: Int? { get set }
    var title: String? { get set }
    var content: String? { get set }
    var status: Int? { get set }
    var created: String? { get set }
    var lastUpdateTime: String? { get set }
}

class NotificationMessage: NotificationProtocol, Codable {
    
    var id: String?
    var userId: Int?
    var title: String?
    var content: String?
    var status: Int?
    var created: String?
    var lastUpdateTime: String?
    
    init() {
        
    }
    
    required convenience init(aJSON: JSON) {
        self.init(fromDictionary: aJSON.dictionaryObject ?? [:])
    }
    
    required init(fromDictionary dictionary: [String: Any]) {
        
        id = dictionary["id"] as? String ?? ""
        userId = dictionary["userId"] as? Int ?? 0
        title = dictionary["title"] as? String ?? ""
        content = dictionary["content"] as? String ?? ""
        status = dictionary["status"] as? Int ?? 0
        created = dictionary["created"] as? String ?? ""
        lastUpdateTime = dictionary["lastUpdateTime"] as? String ?? ""
    }
    
    func toDictionary() -> [String : Any] {
        var dictionary = [String: Any]()
        
        if id != nil {
            dictionary["id"] = id
        }
        if userId != nil {
            dictionary["userId"] = userId
        }
        if title != nil {
            dictionary["title"] = title
        }
        if content != nil {
            dictionary["content"] = content
        }
        if status != nil {
            dictionary["status"] = status
        }
        if created != nil {
            dictionary["created"] = created
        }
        if lastUpdateTime != nil {
            dictionary["lastUpdateTime"] = lastUpdateTime
        }
        
        return dictionary
    }
}

