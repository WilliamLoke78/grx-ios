//
//  OrderData.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 17/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol OrderDataProtocol: JSONParse {
    
    var record: [Order]? { get set }
}

class OrderData: OrderDataProtocol {
    
    var record: [Order]?
    
    required convenience init(aJSON: JSON) {
        self.init(fromDictionary: aJSON.dictionaryObject ?? [:])
    }
    
    required init(fromDictionary dictionary: [String : Any]) {
        var orders = [Order]()
        if let records = dictionary["record"] as? [[String:Any]]{
            for elem in records{
                let value = Order(fromDictionary: elem)
                orders.append(value)
            }
        }
        self.record = orders
    }
    
    func toDictionary() -> [String : Any] {
        
        var dictionary = [String: Any]()
        
        if record != nil{
            var dictionaryElements = [[String:Any]]()
            for elem in record! {
                dictionaryElements.append(elem.toDictionary())
            }
            dictionary["record"] = dictionaryElements
        }
        
        return dictionary
    }
}
