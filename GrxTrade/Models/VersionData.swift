//
//  VersionData.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 12/02/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import SwiftyJSON

protocol VersionDataProtocol: JSONParse {
    
    var id: String? { get set }
    var build: Int? { get set }
    var version: String? { get set }
    var title: String? { get set }
    var did: String? { get set }
    var isForceUpdate: Int? { get set }
    var requestUrl: String? { get set }
    var lastUpdateTime: String? { get set }
    var created: String? { get set }
    var app_id: String? { get set }
    var mobi_app: String? { get set }
}

class VersionData: VersionDataProtocol {
    
    var id: String?
    var build: Int?
    var version: String?
    var title: String?
    var did: String?
    var isForceUpdate: Int?
    var requestUrl: String?
    var lastUpdateTime: String?
    var created: String?
    var app_id: String?
    var mobi_app: String?
    
    init() {
        
    }
    
    required convenience init(aJSON: JSON) {
        self.init(fromDictionary: aJSON.dictionaryObject ?? [:])
    }
    
    required init(fromDictionary dictionary: [String: Any]) {
        
        id = dictionary["id"] as? String
        build = dictionary["build"] as? Int ?? 0
        version = dictionary["version"] as? String ?? ""
        title = dictionary["title"] as? String
        did = dictionary["did"] as? String
        isForceUpdate = dictionary["isForceUpdate"] as? Int ?? 0
        requestUrl = dictionary["requestUrl"] as? String
        lastUpdateTime = dictionary["lastUpdateTime"] as? String
        created = dictionary["created"] as? String
        app_id = dictionary["app_id"] as? String
        mobi_app = dictionary["mobi_app"] as? String
    }
    
    func toDictionary() -> [String : Any] {
        var dictionary = [String: Any]()
        
        if id != nil {
            dictionary["id"] = id
        }
        if build != nil {
            dictionary["build"] = build
        }
        if version != nil {
            dictionary["version"] = version
        }
        if title != nil {
            dictionary["title"] = title
        }
        if did != nil {
            dictionary["did"] = did
        }
        if isForceUpdate != nil {
            dictionary["isForceUpdate"] = isForceUpdate
        }
        if requestUrl != nil {
            dictionary["requestUrl"] = requestUrl
        }
        if lastUpdateTime != nil {
            dictionary["lastUpdateTime"] = lastUpdateTime
        }
        if created != nil {
            dictionary["created"] = created
        }
        if app_id != nil {
            dictionary["app_id"] = app_id
        }
        if mobi_app != nil {
            dictionary["mobi_app"] = mobi_app
        }
                
        return dictionary
    }
}
