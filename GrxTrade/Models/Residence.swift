//
//  Residence.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 09/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol ResidenceProtocol: JSONParse {
    
    var building: String? { get set }
    var street: String? { get set }
    var district: String? { get set }
    var city: String? { get set }
    var province: String? { get set }
    var zipCode: String? { get set }
    var countryCode: String? { get set }
    var authImgUrl1: String? { get set }
    var authImgUrl1Display: String? { get set }
}

class Residence: ResidenceProtocol {
    
    var building: String?
    var street: String?
    var district: String?
    var city: String?
    var province: String?
    var zipCode: String?
    var countryCode: String?
    var authImgUrl1: String?
    var authImgUrl1Display: String?
    
    required convenience init(aJSON: JSON) {
        self.init(fromDictionary: aJSON.dictionaryObject ?? [:])
    }
    
    required init(fromDictionary dictionary: [String : Any]) {
        building = dictionary["building"] as? String
        street = dictionary["street"] as? String
        district = dictionary["district"] as? String
        city = dictionary["city"] as? String
        province = dictionary["province"] as? String
        zipCode = dictionary["zipCode"] as? String
        countryCode = dictionary["countryCode"] as? String
        authImgUrl1 = dictionary["authImgUrl1"] as? String
        authImgUrl1Display = dictionary["authImgUrl1Display"] as? String
    }
    
    func toDictionary() -> [String : Any] {
        var dictionary = [String: Any]()
        
        if building != nil{
            dictionary["building"] = building
        }
        
        if street != nil{
            dictionary["street"] = street
        }
        
        if district != nil{
            dictionary["district"] = district
        }
        
        if city != nil{
            dictionary["city"] = city
        }
        
        if province != nil {
            dictionary["province"] = province
        }
        
        if zipCode != nil {
            dictionary["zipCode"] = zipCode
        }
        
        if countryCode != nil {
            dictionary["countryCode"] = countryCode
        }
        
        if authImgUrl1 != nil {
            dictionary["authImgUrl1"] = authImgUrl1
        }
        
        if authImgUrl1Display != nil {
            dictionary["authImgUrl1Display"] = authImgUrl1Display
        }

        return dictionary
    }
    
    
}

