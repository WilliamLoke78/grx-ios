//
//  User.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 27/12/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import SwiftyJSON
import SwiftKeychainWrapper

class User: NSObject, JSONParse {
    
    var accessToken: String?
    var id: String?
    var registerType: String?
    var userName: String?
    var nickName: String?
    var country: String?
    var regWalletStatus: Int?
    var paymentPasswordStatus: Int?
    var mobileStatus: Int?
    var email: String?
    var kycLevel: Int?
    var pwdStatus: Int?
    var gaStatus: Int?
    var status: Int?
    var lang: String?
    var primaryAuthStatus: Int? // -1: Unverified, 0: Pending, 1: Verified, 2: Rejected, 3: Retry, 9: Continue
    var advancedAuthStatus: Int?
    var phoneCode: String?
    var mobile: String?
    var created: String?
    
    static let shared: User = {
        return Storage.load(key: Storage.Name.user, className: User.self) as? User ?? User.init()
    }()
    
    static let token: String = {
        return Storage.stringValue(key: Storage.Name.token)
    }()
    
    private override init() {
        
    }
    
    required convenience init(aJSON: JSON) {
        self.init(fromDictionary: aJSON.dictionaryObject ?? [:])
    }
    
    required init(fromDictionary dictionary: [String: Any]) {
        
        accessToken = dictionary["accessToken"] as? String ?? ""
        id = dictionary["id"] as? String
        registerType = dictionary["registerType"] as? String ?? ""
        userName = dictionary["userName"] as? String ?? ""
        nickName = dictionary["nickName"] as? String
        country = dictionary["country"] as? String
        regWalletStatus = dictionary["regWalletStatus"] as? Int ?? 0
        paymentPasswordStatus = dictionary["paymentPasswordStatus"] as? Int ?? 0
        mobileStatus = dictionary["mobileStatus"] as? Int ?? 0
        email = dictionary["email"] as? String ?? ""
        kycLevel = dictionary["kycLevel"] as? Int ?? 0
        pwdStatus = dictionary["pwdStatus"] as? Int ?? 0
        gaStatus = dictionary["gaStatus"] as? Int ?? 0
        status = dictionary["status"] as? Int ?? 0
        lang = dictionary["lang"] as? String ?? ""
        primaryAuthStatus = dictionary["primaryAuthStatus"] as? Int ?? 0
        advancedAuthStatus = dictionary["advancedAuthStatus"] as? Int ?? 0
        phoneCode = dictionary["phoneCode"] as? String
        mobile = dictionary["mobile"] as? String
        created = dictionary["created"] as? String ?? ""
    }
    
    func toDictionary() -> [String : Any] {
        var dictionary = [String: Any]()
        
        if accessToken != nil {
            dictionary["accessToken"] = accessToken
        }
        if id != nil {
            dictionary["id"] = id
        }
        if registerType != nil {
            dictionary["registerType"] = registerType
        }
        if userName != nil {
            dictionary["userName"] = userName
        }
        if nickName != nil {
            dictionary["nickName"] = nickName
        }
        if country != nil {
            dictionary["country"] = country
        }
        if regWalletStatus != nil {
            dictionary["regWalletStatus"] = regWalletStatus
        }
        if paymentPasswordStatus != nil {
            dictionary["paymentPasswordStatus"] = paymentPasswordStatus
        }
        if mobileStatus != nil {
            dictionary["mobileStatus"] = mobileStatus
        }
        if email != nil {
            dictionary["email"] = email
        }
        if kycLevel != nil {
            dictionary["kycLevel"] = kycLevel
        }
        if pwdStatus != nil {
            dictionary["pwdStatus"] = pwdStatus
        }
        if gaStatus != nil {
            dictionary["gaStatus"] = gaStatus
        }
        if status != nil {
            dictionary["status"] = status
        }
        if lang != nil {
            dictionary["lang"] = lang
        }
        if primaryAuthStatus != nil {
            dictionary["primaryAuthStatus"] = primaryAuthStatus
        }
        if advancedAuthStatus != nil {
            dictionary["advancedAuthStatus"] = advancedAuthStatus
        }
        if phoneCode != nil {
            dictionary["phoneCode"] = phoneCode
        }
        if mobile != nil {
            dictionary["mobile"] = mobile
        }
        if created != nil {
            dictionary["created"] = created
        }
        
        return dictionary
    }
    
    func saveSharedInstance() {
        Storage.save(key: Storage.Name.user, value: self)
    }
    
    func reset() {
        accessToken = nil
        id = nil
        registerType = nil
        userName = nil
        nickName = nil
        country = nil
        regWalletStatus = nil
        paymentPasswordStatus = nil
        mobileStatus = nil
        email = nil
        kycLevel = nil
        pwdStatus = nil
        gaStatus = nil
        status = nil
        lang = nil
        primaryAuthStatus = nil
        advancedAuthStatus = nil
        phoneCode = nil
        mobile = nil
        created = nil
        
        KeychainWrapper.standard.remove(key: "transactionPassword")
        Storage.delete(key: Storage.Name.user)
        Storage.delete(key: Storage.Name.token)
        BaseSingleton.shared.token = ""
    }
    
}

extension User: Modelable {
    func properties() -> Properties {
        return self.toDictionary()
    }
    
    static func model(properties: Properties) -> Modelable {
        return User.init(aJSON: JSON.init(properties))
    }
}
