//
//  Asset.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 31/12/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import SwiftyJSON

protocol AssetProtocol: JSONParse {
    
    var assertList: [Wallet]? { get set }
    var amount: Double? { get set }
}

class Asset: AssetProtocol {
    
    var assertList: [Wallet]?
    var amount: Double?
    
    required convenience init(aJSON: JSON) {
        self.init(fromDictionary: aJSON.dictionaryObject ?? [:])
    }
    
    required init(fromDictionary dictionary: [String : Any]) {
        var assertList = [Wallet]()
        if let assertListArray = dictionary["assertList"] as? [[String:Any]]{
            for elem in assertListArray{
                let value = Wallet(fromDictionary: elem)
                assertList.append(value)
            }
        }
        self.assertList = assertList
        
        amount = dictionary["amount"] as? Double ?? 0.0
    }
    
    func toDictionary() -> [String : Any] {
        
        var dictionary = [String: Any]()
        
        if assertList != nil{
            var dictionaryElements = [[String:Any]]()
            for elem in assertList! {
                dictionaryElements.append(elem.toDictionary())
            }
            dictionary["assertList"] = dictionaryElements
        }
        
        if amount != nil{
            dictionary["amount"] = amount
        }
        
        return dictionary
    }
}

