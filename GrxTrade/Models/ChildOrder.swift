//
//  ChildOrder.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 07/02/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import SwiftyJSON

protocol ChildOrderProtocol: JSONParse {
    
    var id: String? { get set }
    var symbol: String? { get set }
    var price: String? { get set }
    var vol: String? { get set }
    var amount: String? { get set }
    var created: String? { get set }
    var fee: String? { get set }
}

class ChildOrder: ChildOrderProtocol, Codable {
    
    var id: String?
    var symbol: String?
    var price: String?
    var vol: String?
    var amount: String?
    var created: String?
    var fee: String?
    
    init() {
        
    }
    
    required convenience init(aJSON: JSON) {
        self.init(fromDictionary: aJSON.dictionaryObject ?? [:])
    }
    
    required init(fromDictionary dictionary: [String: Any]) {
        
        id = dictionary["id"] as? String ?? ""
        symbol = dictionary["symbol"] as? String ?? ""
        price = dictionary["price"] as? String ?? ""
        vol = dictionary["vol"] as? String ?? ""
        amount = dictionary["amount"] as? String ?? ""
        created = dictionary["created"] as? String ?? ""
        fee = dictionary["fee"] as? String ?? ""
    }
    
    func toDictionary() -> [String : Any] {
        var dictionary = [String: Any]()
        
        if id != nil {
            dictionary["id"] = id
        }
        if symbol != nil {
            dictionary["symbol"] = symbol
        }
        if price != nil {
            dictionary["price"] = price
        }
        if vol != nil {
            dictionary["vol"] = vol
        }
        if amount != nil {
            dictionary["amount"] = amount
        }
        if created != nil {
            dictionary["created"] = created
        }
        if fee != nil {
            dictionary["fee"] = fee
        }
        
        return dictionary
    }
}
