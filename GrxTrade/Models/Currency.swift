//
//  Currency.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 15/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol CurrencyProtocol {
    
    var id: String? { get set }
    var abbreviation: String? { get set }
    var symbol: String? { get set }
    var rate: Double? { get set }
    var status: Int? { get set }
    var created: String? { get set }
    var lastUpdateTime: String? { get set }
}

class Currency: CurrencyProtocol {
    
    var id: String?
    var abbreviation: String?
    var symbol: String?
    var rate: Double?
    var status: Int?
    var created: String?
    var lastUpdateTime: String?
    
    required convenience init(aJSON: JSON) {
        self.init(fromDictionary: aJSON.dictionaryObject ?? [:])
    }
    
    required init(fromDictionary dictionary: [String : Any]) {
        id = dictionary["id"] as? String
        abbreviation = dictionary["abbreviation"] as? String
        symbol = dictionary["symbol"] as? String
        rate = dictionary["rate"] as? Double
        status = dictionary["status"] as? Int
        created = dictionary["created"] as? String
        lastUpdateTime = dictionary["lastUpdateTime"] as? String
    }
    
    func toDictionary() -> [String : Any] {
        
        var dictionary = [String: Any]()
        
        if id != nil {
            dictionary["id"] = id
        }
        
        if abbreviation != nil {
            dictionary["abbreviation"] = abbreviation
        }
        
        if symbol != nil {
            dictionary["symbol"] = symbol
        }
        
        if rate != nil {
            dictionary["rate"] = rate
        }
        
        if status != nil {
            dictionary["status"] = status
        }
        
        if created != nil {
            dictionary["created"] = created
        }
        
        if lastUpdateTime != nil {
            dictionary["lastUpdateTime"] = lastUpdateTime
        }
        
        return dictionary
    }
}
