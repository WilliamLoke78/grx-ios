//
//  Preupload.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 10/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol PreuploadProtocol: JSONParse {
    
    var accessid: String? { get set }
    var policy: String? { get set }
    var signature: String? { get set }
    var dir: String? { get set }
    var host: String? { get set }
    var callback: String? { get set }
    var expire: String? { get set }
}

class Preupload: PreuploadProtocol {
    
    var accessid: String?
    var policy: String?
    var signature: String?
    var dir: String?
    var host: String?
    var callback: String?
    var expire: String?
    
    required convenience init(aJSON: JSON) {
        self.init(fromDictionary: aJSON.dictionaryObject ?? [:])
    }
    
    required init(fromDictionary dictionary: [String : Any]) {
        accessid = dictionary["accessid"] as? String
        policy = dictionary["policy"] as? String
        signature = dictionary["signature"] as? String
        dir = dictionary["dir"] as? String
        host = dictionary["host"] as? String
        callback = dictionary["callback"] as? String
        expire = dictionary["expire"] as? String
    }
    
    func toDictionary() -> [String : Any] {
        var dictionary = [String: Any]()
        
        if accessid != nil {
            dictionary["accessid"] = accessid
        }
        
        if policy != nil {
            dictionary["policy"] = policy
        }
        
        if signature != nil {
            dictionary["signature"] = signature
        }
        
        if dir != nil {
            dictionary["dir"] = dir
        }
        
        if host != nil{
            dictionary["host"] = host
        }
        
        if callback != nil{
            dictionary["callback"] = callback
        }
        
        if expire != nil{
            dictionary["expire"] = expire
        }
        
        return dictionary
    }
}
