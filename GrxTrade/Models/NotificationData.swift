//
//  NotificationData.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 19/02/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import SwiftyJSON

protocol NotificationDataProtocol: JSONParse {
    
    var records: [NotificationMessage]? { get set }
}

class NotificationData: NotificationDataProtocol {
    
    var records: [NotificationMessage]?
    
    required convenience init(aJSON: JSON) {
        self.init(fromDictionary: aJSON.dictionaryObject ?? [:])
    }
    
    required init(fromDictionary dictionary: [String : Any]) {
        var notifications = [NotificationMessage]()
        if let records = dictionary["records"] as? [[String:Any]]{
            for elem in records{
                let value = NotificationMessage(fromDictionary: elem)
                notifications.append(value)
            }
        }
        self.records = notifications
    }
    
    func toDictionary() -> [String : Any] {
        
        var dictionary = [String: Any]()
        
        if records != nil{
            var dictionaryElements = [[String:Any]]()
            for elem in records! {
                dictionaryElements.append(elem.toDictionary())
            }
            dictionary["records"] = dictionaryElements
        }
        
        return dictionary
    }
}

