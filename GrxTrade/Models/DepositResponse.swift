//
//  DepositResponse.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 06/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol DepositProtocol {
    
    var id: String? { get set }
    var userId: Int? { get set }
    var coinId: Int? { get set }
    var coinName: String? { get set }
    var address: String? { get set }
    var memo: String? { get set }
    var created: String? { get set }
    var lastUpdateTime: String? { get set }
}

class DepositResponse: DepositProtocol {
    
    var userId: Int?
    var coinId: Int?
    var coinName: String?
    var address: String?
    var memo: String?
    var created: String?
    var lastUpdateTime: String?
    var id: String?
    
    required convenience init(aJSON: JSON) {
        self.init(fromDictionary: aJSON.dictionaryObject ?? [:])
    }
    
    required init(fromDictionary dictionary: [String : Any]) {
        id = dictionary["id"] as? String
        userId = dictionary["userId"] as? Int
        coinId = dictionary["coinId"] as? Int
        coinName = dictionary["coinName"] as? String
        address = dictionary["address"] as? String
        memo = dictionary["memo"] as? String
        created = dictionary["created"] as? String
        lastUpdateTime = dictionary["lastUpdateTime"] as? String
    }
    
    func toDictionary() -> [String : Any] {
        
        var dictionary = [String: Any]()
        
        if id != nil {
            dictionary["id"] = id
        }
        
        if userId != nil {
            dictionary["userId"] = userId
        }
        
        if coinId != nil {
            dictionary["coinId"] = coinId
        }
        
        if coinName != nil {
            dictionary["coinName"] = coinName
        }
        
        if address != nil {
            dictionary["address"] = address
        }
        
        if memo != nil {
            dictionary["memo"] = memo
        }
        
        if created != nil {
            dictionary["created"] = created
        }
        
        if lastUpdateTime != nil {
            dictionary["lastUpdateTime"] = lastUpdateTime
        }
        
        return dictionary
    }
}
