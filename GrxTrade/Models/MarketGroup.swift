////
////  MarketGroup.swift
////  GrxTrade
////
////  Created by Leon Mah Kean Loon on 31/12/2019.
////  Copyright © 2019 Leon Mah. All rights reserved.
////
//
//import SwiftyJSON
//
//protocol MarketGroupProtocol: JSONParse {
//    
//    var coins: [String]? { get set }
//}
//
//class MarketGroup: MarketGroupProtocol {
//    
//    var coins: [String]?
//    
//    required convenience init(aJSON: JSON) {
//        self.init(fromDictionary: aJSON.dictionaryObject ?? [:])
//    }
//    
//    required init(fromDictionary dictionary: [String : Any]) {
//        coins = dictionary["coins"] as? [String]
//    }
//    
//    func toDictionary() -> [String : Any] {
//        
//        var dictionary = [String: Any]()
//        
//        if coins != nil {
//            dictionary["coins"] = coins
//        }
//        
//        return dictionary
//    }
//}
