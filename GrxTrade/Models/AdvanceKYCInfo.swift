//
//  AdvanceKYCInfo.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 09/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol AdvanceKYCInfoProtocol: JSONParse {
    
    var kycLevel: Int? { get set }
    var primaryAuthStatus: Int? { get set }
    var advancedAuthStatus: Int? { get set }
    var r: Residence? { get set }
    var e: Employment? { get set }
}

class AdvanceKYCInfo: AdvanceKYCInfoProtocol {
    
    var kycLevel: Int?
    var primaryAuthStatus: Int?
    var advancedAuthStatus: Int?
    var r: Residence?
    var e: Employment?
    
    required convenience init(aJSON: JSON) {
        self.init(fromDictionary: aJSON.dictionaryObject ?? [:])
    }
    
    required init(fromDictionary dictionary: [String : Any]) {
        kycLevel = dictionary["kycLevel"] as? Int
        primaryAuthStatus = dictionary["primaryAuthStatus"] as? Int
        advancedAuthStatus = dictionary["advancedAuthStatus"] as? Int
        
        if let r = dictionary["r"] as? [String: Any] {
            let value = Residence(fromDictionary: r)
            self.r = value
        }
        
        if let e = dictionary["e"] as? [String: Any] {
            let value = Employment(fromDictionary: e)
            self.e = value
        }
    }
    
    func toDictionary() -> [String : Any] {
        var dictionary = [String: Any]()
        
        if kycLevel != nil {
            dictionary["kycLevel"] = kycLevel
        }
        
        if primaryAuthStatus != nil {
            dictionary["primaryAuthStatus"] = primaryAuthStatus
        }
        
        if advancedAuthStatus != nil {
            dictionary["advancedAuthStatus"] = advancedAuthStatus
        }
        
        if r != nil {
            dictionary["r"] = r
        }
        
        if e != nil{
            dictionary["e"] = e
        }
        
        return dictionary
    }
}

