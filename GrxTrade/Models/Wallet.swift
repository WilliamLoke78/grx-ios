//
//  Wallet.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 17/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import SwiftyJSON

protocol WalletProtocol: JSONParse {
    
    var userId: Int? { get set }
    var coinId: Int? { get set }
    var coinName: String? { get set }
    var available: Double? { get set }
    var freeze: Double? { get set }
    var total: Double? { get set }
    var coinImgUrl: String? { get set }
    var allowWithdraw: Int? { get set }
    var allowDeposit: Int? { get set }
    var marketValue: Double? { get set }
    var amountUnit: String? { get set }
}

class Wallet: WalletProtocol {
    
    var userId: Int?
    var coinId: Int?
    var coinName: String?
    var available: Double?
    var freeze: Double?
    var total: Double?
    var coinImgUrl: String?
    var allowWithdraw: Int?
    var allowDeposit: Int?
    var marketValue: Double?
    var amountUnit: String?
    
    required convenience init(aJSON: JSON) {
        self.init(fromDictionary: aJSON.dictionaryObject ?? [:])
    }
    
    required init(fromDictionary dictionary: [String : Any]) {
        userId = dictionary["userId"] as? Int ?? 0
        coinId = dictionary["coinId"] as? Int ?? 0
        coinName = dictionary["coinName"] as? String ?? ""
        available = dictionary["available"] as? Double ?? 0.0
        freeze = dictionary["freeze"] as? Double ?? 0.0
        total = dictionary["total"] as? Double ?? 0.0
        coinImgUrl = dictionary["coinImgUrl"] as? String ?? ""
        allowWithdraw = dictionary["allowWithdraw"] as? Int ?? 0
        allowDeposit = dictionary["allowDeposit"] as? Int ?? 0
        marketValue = dictionary["marketValue"] as? Double ?? 0.0
        amountUnit = dictionary["amountUnit"] as? String ?? ""
    }
    
    func toDictionary() -> [String : Any] {
        
        var dictionary = [String: Any]()
        
        if userId != nil {
            dictionary["userId"] = userId
        }
        
        if coinId != nil {
            dictionary["coinId"] = coinId
        }
        
        if coinName != nil {
            dictionary["coinName"] = coinName
        }
        
        if available != nil {
            dictionary["available"] = available
        }
        
        if freeze != nil {
            dictionary["freeze"] = freeze
        }
        
        if total != nil {
            dictionary["total"] = total
        }
        
        if coinImgUrl != nil {
            dictionary["coinImgUrl"] = coinImgUrl
        }
        
        if allowWithdraw != nil {
            dictionary["allowWithdraw"] = allowWithdraw
        }
        
        if allowDeposit != nil {
            dictionary["allowDeposit"] = allowDeposit
        }
        
        if marketValue != nil {
            dictionary["marketValue"] = marketValue
        }
        
        if amountUnit != nil {
            dictionary["amountUnit"] = amountUnit
        }
        return dictionary
    }
}
