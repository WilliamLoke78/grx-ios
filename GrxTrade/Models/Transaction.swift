//
//  Transaction.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 12/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol TransactionProtocol: JSONParse {
    var id: String? { get set }
    var userId: Int? { get set }
    var coinId: Int? { get set }
    var coinName: String? { get set }
    var addressId: String? { get set }
    var toAddress: String? { get set }
    var volume: Double? { get set }
    var feeType: Double? { get set }
    var feeRate: Double? { get set }
    var fee: Double? { get set }
    var amount: Double? { get set }
    var blockNum: Int? { get set }
    var step: Int? { get set }
    var status: Int? { get set }
    var lastUpdateTime: String? { get set }
    var created: String? { get set }
}

class Transaction: TransactionProtocol {
    
    var id: String?
    var userId: Int?
    var coinId: Int?
    var coinName: String?
    var addressId: String?
    var toAddress: String?
    var volume: Double?
    var feeType: Double?
    var feeRate: Double?
    var fee: Double?
    var amount: Double?
    var blockNum: Int?
    var step: Int?
    var status: Int?
    var lastUpdateTime: String?
    var created: String?
    
    required convenience init(aJSON: JSON) {
        self.init(fromDictionary: aJSON.dictionaryObject ?? [:])
    }
    
    required init(fromDictionary dictionary: [String : Any]) {
        id = dictionary["id"] as? String
        userId = dictionary["userId"] as? Int
        coinId = dictionary["coinId"] as? Int
        coinName = dictionary["coinName"] as? String
        addressId = dictionary["addressId"] as? String
        toAddress = dictionary["toAddress"] as? String
        volume = dictionary["volume"] as? Double
        feeType = dictionary["feeType"] as? Double
        feeRate = dictionary["feeRate"] as? Double
        fee = dictionary["fee"] as? Double
        amount = dictionary["amount"] as? Double
        blockNum = dictionary["blockNum"] as? Int
        step = dictionary["step"] as? Int
        status = dictionary["status"] as? Int
        lastUpdateTime = dictionary["lastUpdateTime"] as? String
        created = dictionary["created"] as? String
    }
    
    func toDictionary() -> [String : Any] {
        var dictionary = [String: Any]()
        
        if id != nil {
            dictionary["id"] = id
        }
        
        if userId != nil {
            dictionary["userId"] = userId
        }
        
        if coinId != nil {
            dictionary["coinId"] = coinId
        }
        
        if coinName != nil {
            dictionary["coinName"] = coinName
        }
        
        if addressId != nil {
            dictionary["addressId"] = addressId
        }
        
        if toAddress != nil {
            dictionary["toAddress"] = toAddress
        }
        
        if volume != nil {
            dictionary["volume"] = volume
        }
        
        if feeType != nil {
            dictionary["feeType"] = feeType
        }
        
        if feeRate != nil {
            dictionary["feeRate"] = feeRate
        }
        
        if fee != nil {
            dictionary["fee"] = fee
        }
        
        if amount != nil {
            dictionary["amount"] = amount
        }
        
        if blockNum != nil {
            dictionary["blockNum"] = blockNum
        }
        
        if step != nil {
            dictionary["step"] = step
        }
        
        if status != nil {
            dictionary["status"] = status
        }
        
        if lastUpdateTime != nil {
            dictionary["lastUpdateTime"] = lastUpdateTime
        }
        
        if created != nil {
            dictionary["created"] = created
        }
        
        return dictionary
    }
}
