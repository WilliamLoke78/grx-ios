//
//  Coin.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 06/02/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol CoinProtocol: JSONParse {
    
    var id: Int? { get set }
    var name: String? { get set }
    var symbol: String? { get set }
    var round: Int? { get set }
    var status: Int? { get set }
    var created: String? { get set }
    var lastUpdateTime: String? { get set }
}

class Coin: CoinProtocol {
    
    var id: Int?
    var name: String?
    var symbol: String?
    var round: Int?
    var status: Int?
    var created: String?
    var lastUpdateTime: String?
    
    required convenience init(aJSON: JSON) {
        self.init(fromDictionary: aJSON.dictionaryObject ?? [:])
    }
    
    required init(fromDictionary dictionary: [String : Any]) {
        id = dictionary["id"] as? Int
        name = dictionary["name"] as? String
        symbol = dictionary["symbol"] as? String
        round = dictionary["round"] as? Int
        status = dictionary["status"] as? Int
        created = dictionary["created"] as? String
        lastUpdateTime = dictionary["lastUpdateTime"] as? String
    }
    
    func toDictionary() -> [String : Any] {
        
        var dictionary = [String: Any]()
        
        if id != nil {
            dictionary["id"] = id
        }
        
        if name != nil {
            dictionary["name"] = name
        }
        
        if symbol != nil {
            dictionary["symbol"] = symbol
        }
        
        if round != nil {
            dictionary["round"] = round
        }
        
        if status != nil {
            dictionary["status"] = status
        }
        
        if created != nil {
            dictionary["created"] = created
        }
        
        if lastUpdateTime != nil {
            dictionary["lastUpdateTime"] = lastUpdateTime
        }
        
        return dictionary
    }
}
