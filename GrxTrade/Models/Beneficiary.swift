//
//  Beneficiary.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 07/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol BeneficiaryProtocol: JSONParse {
    
    var id: String? { get set }
    var name: String? { get set }
    var addr: String? { get set }
    var memo: String? { get set }
    var status: Int? { get set }
}

class Beneficiary: BeneficiaryProtocol {
    
    var id: String?
    var name: String?
    var addr: String?
    var memo: String?
    var status: Int?
    
    required convenience init(aJSON: JSON) {
        self.init(fromDictionary: aJSON.dictionaryObject ?? [:])
    }
    
    required init(fromDictionary dictionary: [String : Any]) {
        id = dictionary["id"] as? String
        name = dictionary["name"] as? String
        addr = dictionary["addr"] as? String
        memo = dictionary["memo"] as? String
        status = dictionary["status"] as? Int
    }
    
    func toDictionary() -> [String : Any] {
        var dictionary = [String: Any]()
        
        if id != nil {
            dictionary["id"] = id
        }
        
        if name != nil{
            dictionary["name"] = name
        }
        
        if addr != nil{
            dictionary["addr"] = addr
        }
        
        if memo != nil{
            dictionary["memo"] = memo
        }
        
        if status != nil{
            dictionary["status"] = status
        }

        return dictionary
    }
    
    
}

