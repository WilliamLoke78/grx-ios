//
//  AccountDetail.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 09/02/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import SwiftyJSON

protocol AccountDetailProtocol: JSONParse {
    
    var id: String? { get set }
    var coinId: Int? { get set }
    var coinName: String? { get set }
    var userId: Int? { get set }
    var accountId: String? { get set }
    var refAccountId: String? { get set }
    var orderId: String? { get set }
    var assetType: Int? { get set }
    var paymentType: Int? { get set }
    var businessType: String? { get set }
    var beforeAmount: Double? { get set }
    var changeAmount: Double? { get set }
    var afterAmount: Double? { get set }
    var fee: Int? { get set }
    var remark: String? { get set }
    var created: String? { get set }
}

class AccountDetail: AccountDetailProtocol, Codable {
    
    var id: String?
    var coinId: Int?
    var coinName: String?
    var userId: Int?
    var accountId: String?
    var refAccountId: String?
    var orderId: String?
    var assetType: Int?
    var paymentType: Int?
    var businessType: String?
    var businessSubType: String?
    var beforeAmount: Double?
    var changeAmount: Double?
    var afterAmount: Double?
    var fee: Int?
    var remark: String?
    var created: String?
    
    init() {
        
    }
    
    required convenience init(aJSON: JSON) {
        self.init(fromDictionary: aJSON.dictionaryObject ?? [:])
    }
    
    required init(fromDictionary dictionary: [String: Any]) {
        
        id = dictionary["id"] as? String ?? ""
        coinId = dictionary["coinId"] as? Int ?? 0
        coinName = dictionary["coinName"] as? String ?? ""
        userId = dictionary["userId"] as? Int ?? 0
        accountId = dictionary["accountId"] as? String ?? ""
        refAccountId = dictionary["refAccountId"] as? String ?? ""
        orderId = dictionary["orderId"] as? String ?? ""
        assetType = dictionary["assetType"] as? Int ?? 0
        paymentType = dictionary["paymentType"] as? Int ?? 0
        businessType = dictionary["businessType"] as? String ?? ""
        businessSubType = dictionary["businessSubType"] as? String ?? ""
        beforeAmount = dictionary["beforeAmount"] as? Double ?? 0.0
        changeAmount = dictionary["changeAmount"] as? Double ?? 0.0
        afterAmount = dictionary["afterAmount"] as? Double ?? 0.0
        fee = dictionary["fee"] as? Int ?? 0
        remark = dictionary["remark"] as? String ?? ""
        created = dictionary["created"] as? String ?? ""
    }
    
    func toDictionary() -> [String : Any] {
        var dictionary = [String: Any]()
        
        if id != nil {
            dictionary["id"] = id
        }
        if coinId != nil {
            dictionary["coinId"] = coinId
        }
        if coinName != nil {
            dictionary["coinName"] = coinName
        }
        if userId != nil {
            dictionary["userId"] = userId
        }
        if accountId != nil {
            dictionary["accountId"] = accountId
        }
        if refAccountId != nil {
            dictionary["refAccountId"] = refAccountId
        }
        if orderId != nil {
            dictionary["orderId"] = orderId
        }
        if assetType != nil {
            dictionary["assetType"] = assetType
        }
        if paymentType != nil {
            dictionary["paymentType"] = paymentType
        }
        if businessType != nil {
            dictionary["businessType"] = businessType
        }
        if businessSubType != nil {
            dictionary["businessSubType"] = businessSubType
        }
        if beforeAmount != nil {
            dictionary["beforeAmount"] = beforeAmount
        }
        if changeAmount != nil {
            dictionary["changeAmount"] = changeAmount
        }
        if afterAmount != nil {
            dictionary["afterAmount"] = afterAmount
        }
        if fee != nil {
            dictionary["fee"] = fee
        }
        if remark != nil {
            dictionary["remark"] = remark
        }
        if created != nil {
            dictionary["created"] = created
        }
        
        return dictionary
    }
}

