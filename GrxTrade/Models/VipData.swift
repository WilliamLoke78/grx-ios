//
//  VipData.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 12/02/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import SwiftyJSON

protocol VipDataProtocol: JSONParse {
    
    var vipId: Int? { get set }
    var vipName: String? { get set }
    var tradeVolume: Double? { get set }
    var tradeCoinSymbol: String? { get set }
}

class VipData: VipDataProtocol {
    
    var vipId: Int?
    var vipName: String?
    var tradeVolume: Double?
    var tradeCoinSymbol: String?
    
    init() {
        
    }
    
    required convenience init(aJSON: JSON) {
        self.init(fromDictionary: aJSON.dictionaryObject ?? [:])
    }
    
    required init(fromDictionary dictionary: [String: Any]) {
        
        vipId = dictionary["vipId"] as? Int ?? 0
        vipName = dictionary["vipName"] as? String ?? ""
        tradeVolume = dictionary["tradeVolume"] as? Double ?? 0.0
        tradeCoinSymbol = dictionary["tradeCoinSymbol"] as? String ?? ""
    }
    
    func toDictionary() -> [String : Any] {
        var dictionary = [String: Any]()
        
        if vipId != nil {
            dictionary["vipId"] = vipId
        }
        if vipName != nil {
            dictionary["vipName"] = vipName
        }
        if tradeVolume != nil {
            dictionary["tradeVolume"] = tradeVolume
        }
        if tradeCoinSymbol != nil {
            dictionary["tradeCoinSymbol"] = tradeCoinSymbol
        }
                
        return dictionary
    }
}

