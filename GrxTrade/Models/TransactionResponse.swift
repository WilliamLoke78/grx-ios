//
//  TransactionResponse.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 13/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol TransactionReponseProtocol: JSONParse {
    
    var records: [Transaction]? { get set }
    var total: Int? { get set }
    var size: Int? { get set }
    var current: Int? { get set }
    var pages: Int? { get set }
}

class TransactionResponse: TransactionReponseProtocol {
    
    var records: [Transaction]?
    var total: Int?
    var size: Int?
    var current: Int?
    var pages: Int?
    
    required convenience init(aJSON: JSON) {
        self.init(fromDictionary: aJSON.dictionaryObject ?? [:])
    }
    
    required init(fromDictionary dictionary: [String : Any]) {
        
        var records = [Transaction]()
        if let recordsArray = dictionary["records"] as? [[String: Any]] {
            for elem in recordsArray {
                let value = Transaction(fromDictionary: elem)
                records.append(value)
            }
        }
        self.records = records
        
        total = dictionary["total"] as? Int
        size = dictionary["size"] as? Int
        current = dictionary["current"] as? Int
        pages = dictionary["pages"] as? Int
    }
    
    func toDictionary() -> [String : Any] {
        var dictionary = [String: Any]()
        
        if records != nil {
            var dictionaryElements = [[String: Any]]()
            for elem in records! {
                dictionaryElements.append(elem.toDictionary())
            }
            dictionary["records"] = records
        }
        
        if total != nil {
            dictionary["total"] = total
        }
        
        if size != nil {
            dictionary["size"] = size
        }
        
        if current != nil {
            dictionary["current"] = current
        }
        
        if pages != nil{
            dictionary["pages"] = pages
        }
        
        return dictionary
    }

}
