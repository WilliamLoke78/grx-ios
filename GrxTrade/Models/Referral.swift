//
//  Referral.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 03/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol ReferralProtocol {
    
    var referral: String? { get set }
    var invite: String? { get set }
}

class Referral: ReferralProtocol {
    
    var referral: String?
    var invite: String?
    
    required convenience init(aJSON: JSON) {
        self.init(fromDictionary: aJSON.dictionaryObject ?? [:])
    }
    
    required init(fromDictionary dictionary: [String : Any]) {
        referral = dictionary["referral"] as? String
        invite = dictionary["invite"] as? String ?? ""
    }
    
    func toDictionary() -> [String : Any] {
        
        var dictionary = [String: Any]()
        
        if referral != nil {
            dictionary["referral"] = referral
        }
        
        if invite != nil {
            dictionary["invite"] = invite
        }
        
        return dictionary
    }
}
