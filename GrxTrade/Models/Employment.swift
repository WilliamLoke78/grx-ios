//
//  Employment.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 09/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol EmploymentProtocol: JSONParse {
    
    var employStatus: String? { get set }
    var employStatusName: String? { get set }
    var industry: String? { get set }
    var industryName: String? { get set }
    var jobTitle: String? { get set }
    var companyName: String? { get set }
    var employerBuilding: String? { get set }
    var employerStreet: String? { get set }
    var employerDistrict: String? { get set }
    var employerCity: String? { get set }
    var employerProvince: String? { get set }
    var employerZipCode: String? { get set }
    var employerCountryCode: String? { get set }
    var authImgUrl2: String? { get set }
    var authImgUrl2Display: String? { get set }
    var sourceIncome: String? { get set }
    var sourceIncomeName: String? { get set }
}

class Employment: EmploymentProtocol {
    var employStatus: String?
    var employStatusName: String?
    var industry: String?
    var industryName: String?
    var jobTitle: String?
    var companyName: String?
    var employerBuilding: String?
    var employerStreet: String?
    var employerDistrict: String?
    var employerCity: String?
    var employerProvince: String?
    var employerZipCode: String?
    var employerCountryCode: String?
    var authImgUrl2: String?
    var authImgUrl2Display: String?
    var sourceIncome: String?
    var sourceIncomeName: String?
    
    required convenience init(aJSON: JSON) {
        self.init(fromDictionary: aJSON.dictionaryObject ?? [:])
    }
    
    required init(fromDictionary dictionary: [String : Any]) {
        employStatus = dictionary["employStatus"] as? String
        employStatusName = dictionary["employStatusName"] as? String
        industry = dictionary["industry"] as? String
        industryName = dictionary["industryName"] as? String
        jobTitle = dictionary["jobTitle"] as? String
        companyName = dictionary["companyName"] as? String
        employerBuilding = dictionary["employerBuilding"] as? String
        employerStreet = dictionary["employerStreet"] as? String
        employerDistrict = dictionary["employerDistrict"] as? String
        employerCity = dictionary["employerCity"] as? String
        employerProvince = dictionary["employerProvince"] as? String
        employerZipCode = dictionary["employerCountryCode"] as? String
        employerCountryCode = dictionary["employerCountryCode"] as? String
        authImgUrl2 = dictionary["authImgUrl2"] as? String
        authImgUrl2Display = dictionary["authImgUrl2Display"] as? String
        sourceIncome = dictionary["sourceIncome"] as? String
        sourceIncomeName = dictionary["sourceIncomeName"] as? String
    }
    
    func toDictionary() -> [String : Any] {
        var dictionary = [String: Any]()
        
        if employStatus != nil {
            dictionary["employStatus"] = employStatus
        }
        
        if employStatusName != nil {
            dictionary["employStatusName"] = employStatusName
        }
        
        if industry != nil{
            dictionary["industry"] = industry
        }
        
        if industryName != nil{
            dictionary["industryName"] = industryName
        }
        
        if jobTitle != nil{
            dictionary["jobTitle"] = jobTitle
        }
        
        if companyName != nil{
            dictionary["companyName"] = companyName
        }
        
        if employerBuilding != nil {
            dictionary["employerBuilding"] = employerBuilding
        }
        
        if employerStreet != nil {
            dictionary["employerStreet"] = employerStreet
        }
        
        if employerDistrict != nil {
            dictionary["employerDistrict"] = employerDistrict
        }
        
        if employerCity != nil {
            dictionary["employerCity"] = employerCity
        }
        
        if employerProvince != nil {
            dictionary["employerProvince"] = employerProvince
        }
        
        if employerZipCode != nil {
            dictionary["employerZipCode"] = employerZipCode
        }
        
        if employerCountryCode != nil {
            dictionary["employerCountryCode"] = employerCountryCode
        }
        
        if authImgUrl2 != nil {
            dictionary["authImgUrl2"] = authImgUrl2
        }
        
        if authImgUrl2Display != nil {
            dictionary["authImgUrl2Display"] = authImgUrl2Display
        }
        
        if sourceIncome != nil {
            dictionary["sourceIncome"] = sourceIncome
        }
        
        if sourceIncomeName != nil {
            dictionary["sourceIncomeName"] = sourceIncomeName
        }

        return dictionary
    }
}
