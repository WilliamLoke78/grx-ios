//
//  MarketData.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 15/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol MarketDataProtocol: JSONParse {
    
    var kline: [KlineChartData]? { get set }
    var depth: Depth? { get set }
}

class MarketData: MarketDataProtocol {
    
    var kline: [KlineChartData]?
    var depth: Depth?
    
    required convenience init(aJSON: JSON) {
        self.init(fromDictionary: aJSON.dictionaryObject ?? [:])
    }
    
    required init(fromDictionary dictionary: [String : Any]) {
        
        var klineList = [KlineChartData]()
        if let klineListArray = dictionary["kline"] as? [[String]]{
            for elem in klineListArray{
                let value = KlineChartData.init(json: elem)
                klineList.append(value)
            }
        }
        self.kline = klineList
        
        if let depth = dictionary["depth"] as? [String: Any] {
            let value = Depth(fromDictionary: depth)
            self.depth = value
        }
    }
    
    func toDictionary() -> [String : Any] {
        
        var dictionary = [String: Any]()
        
        if kline != nil {
            dictionary["kline"] = kline
        }
        
        if depth != nil{
            dictionary["depth"] = depth
        }
        
        return dictionary
    }
}
