//
//  GoogleAuth.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 31/12/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol GoogleAuthProtocol {
    
    var secretKey: String? { get set }
    var otpAuthTotpURL: String? { get set }
}

class GoogleAuth: GoogleAuthProtocol {
    
    var secretKey: String?
    var otpAuthTotpURL: String?
    
    required convenience init(aJSON: JSON) {
        self.init(fromDictionary: aJSON.dictionaryObject ?? [:])
    }
    
    required init(fromDictionary dictionary: [String : Any]) {
        secretKey = dictionary["secretKey"] as? String ?? ""
        otpAuthTotpURL = dictionary["otpAuthTotpURL"] as? String ?? ""
    }
    
    func toDictionary() -> [String : Any] {
        
        var dictionary = [String: Any]()
        
        if secretKey != nil {
            dictionary["secretKey"] = secretKey
        }
        
        if otpAuthTotpURL != nil {
            dictionary["otpAuthTotpURL"] = otpAuthTotpURL
        }
        
        return dictionary
    }
}

