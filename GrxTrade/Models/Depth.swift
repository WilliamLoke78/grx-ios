//
//  Depth.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 15/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol DepthProtocol: JSONParse {
    
    var buy: [[String]]? { get set }
    var sell: [[String]]? { get set }
}

class Depth: DepthProtocol {
    
    var buy: [[String]]?
    var sell: [[String]]?
    
    required convenience init(aJSON: JSON) {
        self.init(fromDictionary: aJSON.dictionaryObject ?? [:])
    }
    
    required init(fromDictionary dictionary: [String : Any]) {
        buy = dictionary["buy"] as? [[String]]
        sell = dictionary["sell"] as? [[String]]
    }
    
    func toDictionary() -> [String : Any] {
        
        var dictionary = [String: Any]()
        
        if buy != nil{
            dictionary["buy"] = buy
        }
        
        if sell != nil{
            dictionary["sell"] = sell
        }
        
        return dictionary
    }
}
