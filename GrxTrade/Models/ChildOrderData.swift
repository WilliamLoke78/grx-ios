//
//  ChildOrderData.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 07/02/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol ChildOrderDataProtocol: JSONParse {
    
    var record: [ChildOrder]? { get set }
}

class ChildOrderData: ChildOrderDataProtocol {
    
    var record: [ChildOrder]?
    
    required convenience init(aJSON: JSON) {
        self.init(fromDictionary: aJSON.dictionaryObject ?? [:])
    }
    
    required init(fromDictionary dictionary: [String : Any]) {
        var orders = [ChildOrder]()
        if let records = dictionary["record"] as? [[String:Any]]{
            for elem in records{
                let value = ChildOrder(fromDictionary: elem)
                orders.append(value)
            }
        }
        self.record = orders
    }
    
    func toDictionary() -> [String : Any] {
        
        var dictionary = [String: Any]()
        
        if record != nil{
            var dictionaryElements = [[String:Any]]()
            for elem in record! {
                dictionaryElements.append(elem.toDictionary())
            }
            dictionary["record"] = dictionaryElements
        }
        
        return dictionary
    }
}
