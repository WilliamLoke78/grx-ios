//
//  WithdrawFee.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 06/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol WithdrawFeeProtocol {
    
    var coinId: Int? { get set }
    var addressId: String? { get set }
    var feeType: Int? { get set }
    var feeRate: Double? { get set }
}

class WithdrawFee: WithdrawFeeProtocol {
    
    var coinId: Int?
    var addressId: String?
    var feeType: Int?
    var feeRate: Double?

    required convenience init(aJSON: JSON) {
        self.init(fromDictionary: aJSON.dictionaryObject ?? [:])
    }
    
    required init(fromDictionary dictionary: [String : Any]) {
        coinId = dictionary["coinId"] as? Int ?? 0
        addressId = dictionary["addressId"] as? String ?? ""
        feeType = dictionary["feeType"] as? Int ?? 0
        feeRate = dictionary["feeRate"] as? Double ?? 0.0
    }
    
    func toDictionary() -> [String : Any] {
        
        var dictionary = [String: Any]()
        
        if coinId != nil {
            dictionary["coinId"] = coinId
        }
        
        if addressId != nil {
            dictionary["addressId"] = addressId
        }
        
        if feeType != nil {
            dictionary["feeType"] = feeType
        }
        
        if feeRate != nil {
            dictionary["feeRate"] = feeRate
        }
        
        return dictionary
    }
}
