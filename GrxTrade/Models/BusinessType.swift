//
//  BusinessType.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 10/02/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import SwiftyJSON

protocol BusinessTypeProtocol: JSONParse {
    
    var id: String? { get set }
    var category: String? { get set }
    var display: String? { get set }
    var value: String? { get set }
    var weight: Int? { get set }
    var status: Int? { get set }
    var created: String? { get set }
}

class BusinessType: BusinessTypeProtocol {
    
    var id: String?
    var category: String?
    var display: String?
    var value: String?
    var weight: Int?
    var status: Int?
    var created: String?
    
    init() {
        
    }
    
    required convenience init(aJSON: JSON) {
        self.init(fromDictionary: aJSON.dictionaryObject ?? [:])
    }
    
    required init(fromDictionary dictionary: [String: Any]) {
        
        id = dictionary["id"] as? String ?? ""
        category = dictionary["category"] as? String ?? ""
        display = dictionary["display"] as? String ?? ""
        value = dictionary["value"] as? String ?? ""
        weight = dictionary["weight"] as? Int ?? 0
        status = dictionary["status"] as? Int ?? 0
        created = dictionary["created"] as? String ?? ""
    }
    
    func toDictionary() -> [String : Any] {
        var dictionary = [String: Any]()
        
        if id != nil {
            dictionary["id"] = id
        }
        if category != nil {
            dictionary["category"] = category
        }
        if display != nil {
            dictionary["display"] = display
        }
        if value != nil {
            dictionary["value"] = value
        }
        if weight != nil {
            dictionary["weight"] = weight
        }
        if status != nil {
            dictionary["status"] = status
        }
        if created != nil {
            dictionary["created"] = created
        }
                
        return dictionary
    }
}
