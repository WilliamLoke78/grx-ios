//
//  AccountDetailData.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 09/02/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol AccountDetailDataProtocol: JSONParse {
    
    var records: [AccountDetail]? { get set }
}

class AccountDetailData: AccountDetailDataProtocol {
    
    var records: [AccountDetail]?
    
    required convenience init(aJSON: JSON) {
        self.init(fromDictionary: aJSON.dictionaryObject ?? [:])
    }
    
    required init(fromDictionary dictionary: [String : Any]) {
        var accounts = [AccountDetail]()
        if let records = dictionary["records"] as? [[String:Any]]{
            for elem in records{
                let value = AccountDetail(fromDictionary: elem)
                accounts.append(value)
            }
        }
        self.records = accounts
    }
    
    func toDictionary() -> [String : Any] {
        
        var dictionary = [String: Any]()
        
        if records != nil{
            var dictionaryElements = [[String:Any]]()
            for elem in records! {
                dictionaryElements.append(elem.toDictionary())
            }
            dictionary["records"] = dictionaryElements
        }
        
        return dictionary
    }
}

