//
//  Market.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 17/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import SwiftyJSON

protocol MarketProtocol: JSONParse {

    var change: String? { get set }
    var close: String? { get set }
    var lastUpdate: String? { get set }
    var high: String? { get set }
    var open: String? { get set }
    var low: String? { get set }
    var lowTime: String? { get set }
    var volume: String? { get set }
    var amount: String? { get set }
    var usd: String? { get set }
    var highTime: String? { get set }
    var symbol: String? { get set }
    var coin: String? { get set }
    var marketCoin: String? { get set }
    var priceDec: Int? { get set }
    var volDec: Int? { get set }
    var tradeStatus: Int? { get set }
    var status: Int? { get set }
    var mergeDepth: String? { get set }
    var priceMin: String? { get set }
    var priceMax: String? { get set }
}

struct Market: MarketProtocol {
    
    var change: String?
    var close: String?
    var lastUpdate: String?
    var high: String?
    var open: String?
    var low: String?
    var lowTime: String?
    var volume: String?
    var amount: String?
    var usd: String?
    var highTime: String?
    var symbol: String?
    var coin: String?
    var marketCoin: String?
    var priceDec: Int?
    var volDec: Int?
    var tradeStatus: Int?
    var status: Int?
    var mergeDepth: String?
    var priceMin: String?
    var priceMax: String?
    
    init() {
        
    }
    
    init(aJSON: JSON) {
        self.init(fromDictionary: aJSON.dictionaryObject ?? [:])
    }
    
    init(fromDictionary dictionary: [String: Any]) {
        change = dictionary["change"] as? String ?? ""
        close = dictionary["close"] as? String ?? ""
        lastUpdate = dictionary["lastUpdate"] as? String ?? ""
        high = dictionary["high"] as? String ?? ""
        open = dictionary["open"] as? String ?? ""
        low = dictionary["low"] as? String ?? ""
        lowTime = dictionary["lowTime"] as? String ?? ""
        volume = dictionary["volume"] as? String ?? ""
        amount = dictionary["amount"] as? String ?? ""
        usd = dictionary["usd"] as? String ?? ""
        highTime = dictionary["highTime"] as? String ?? ""
        symbol = dictionary["symbol"] as? String ?? ""
        coin = dictionary["coin"] as? String ?? ""
        marketCoin = dictionary["marketCoin"] as? String ?? ""
        priceDec = dictionary["priceDec"] as? Int ?? 0
        volDec = dictionary["volDec"] as? Int ?? 0
        tradeStatus = dictionary["tradeStatus"] as? Int ?? 0
        status = dictionary["status"] as? Int ?? 1
        mergeDepth = dictionary["mergeDepth"] as? String ?? ""
        priceMin = dictionary["priceMin"] as? String ?? ""
        priceMax = dictionary["priceMax"] as? String ?? ""
    }
    
    func toDictionary() -> [String : Any] {
        var dictionary = [String: Any]()
        if change != nil {
            dictionary["change"] = change
        }
        if close != nil {
            dictionary["close"] = close
        }
        if lastUpdate != nil {
            dictionary["lastUpdate"] = lastUpdate
        }
        if high != nil {
            dictionary["high"] = high
        }
        if open != nil {
            dictionary["open"] = open
        }
        if low != nil {
            dictionary["low"] = low
        }
        if lowTime != nil {
            dictionary["lowTime"] = lowTime
        }
        if volume != nil {
            dictionary["volume"] = volume
        }
        if amount != nil {
            dictionary["amount"] = amount
        }
        if usd != nil {
            dictionary["usd"] = usd
        }
        if highTime != nil {
            dictionary["highTime"] = highTime
        }
        if symbol != nil {
            dictionary["symbol"] = symbol
        }
        if coin != nil {
            dictionary["coin"] = coin
        }
        if marketCoin != nil {
            dictionary["marketCoin"] = marketCoin
        }
        if priceDec != nil {
            dictionary["priceDec"] = priceDec
        }
        if volDec != nil {
            dictionary["volDec"] = volDec
        }
        if tradeStatus != nil {
            dictionary["tradeStatus"] = tradeStatus
        }
        if status != nil {
            dictionary["status"] = status
        }
        if mergeDepth != nil {
            dictionary["mergeDepth"] = mergeDepth
        }
        if priceMin != nil {
            dictionary["priceMin"] = priceMin
        }
        if priceMax != nil {
            dictionary["priceMax"] = priceMax
        }
        return dictionary
    }
}
