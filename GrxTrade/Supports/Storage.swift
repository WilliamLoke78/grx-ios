//
//  Storage.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 01/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import Foundation

final class Storage {
    struct Name {
        private(set)var rawValue: String
        
        init(_ rawValue: String) {
            self.rawValue = rawValue
        }
        
        init(rawValue: String) {
            self.rawValue = rawValue
        }
    }
}

// MARK: - Storage
extension Storage {
    static func save(key: Storage.Name, value: Modelable) -> Void {
        UserDefaults.standard.set(value.properties(), forKey: key.rawValue)
        UserDefaults.standard.synchronize()
    }
    static func load(key: Storage.Name, className: Modelable.Type) -> Modelable? {
        if let properties = UserDefaults.standard.value(forKey: key.rawValue) as? Properties {
            return className.model(properties: properties)
        }
        return nil
    }
}

// MARK: - Any
extension Storage {
    static func save(key: Storage.Name, value: Any) -> Void {
        UserDefaults.standard.set(value, forKey: key.rawValue)
        UserDefaults.standard.synchronize()
    }
    static func load(key: Storage.Name) -> Any? {
        return UserDefaults.standard.value(forKey: key.rawValue)
    }
    static func delete(key: Storage.Name) -> Void {
        UserDefaults.standard.removeObject(forKey: key.rawValue)
    }
}

// MARK: - String
extension Storage {
    static func string(key: Storage.Name) -> String? {
        return UserDefaults.standard.string(forKey: key.rawValue)
    }
    static func stringValue(key: Storage.Name) -> String {
        return UserDefaults.standard.string(forKey: key.rawValue) ?? ""
    }
}

// MARK: - Bool
extension Storage {
    static func bool(key: Storage.Name) -> Bool? {
        return UserDefaults.standard.value(forKey: key.rawValue) as? Bool
    }
    static func boolValue(key: Storage.Name) -> Bool {
        return UserDefaults.standard.bool(forKey: key.rawValue)
    }
}

// MARK: - Number
extension Storage {
    //Int
    static func int(key: Storage.Name) -> Int? {
        return UserDefaults.standard.value(forKey: key.rawValue) as? Int
    }
    static func intValue(key: Storage.Name) -> Int {
        return UserDefaults.standard.integer(forKey: key.rawValue)
    }
    //Double
    static func double(key: Storage.Name) -> Double? {
        return UserDefaults.standard.value(forKey: key.rawValue) as? Double
    }
    static func doubleValue(key: Storage.Name) -> Double {
        return UserDefaults.standard.double(forKey: key.rawValue)
    }
    //Float
    static func float(key: Storage.Name) -> Float? {
        return UserDefaults.standard.value(forKey: key.rawValue) as? Float
    }
    static func floatValue(key: Storage.Name) -> Float {
        return UserDefaults.standard.float(forKey: key.rawValue)
    }
}
