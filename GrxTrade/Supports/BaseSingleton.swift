//
//  BaseSingleton.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 27/12/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import Foundation

class BaseSingleton {
    
    static let shared = BaseSingleton()
    
    lazy var user: User = User.shared
    lazy var token: String = User.token
    var wallets: [Wallet]?
    
    func save() {
        user.saveSharedInstance()
    }
    
    var markets: [Market]? = []
    var currencies: [Currency]? = []
    var coins: [Coin]?
}
