//
//  GrxTrade-Bridging-Header.h
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 14/02/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

#ifndef GrxTrade_Bridging_Header_h
#define GrxTrade_Bridging_Header_h

//导入UMCommon的OC的头文件
#import <UMCommon/UMCommon.h>

//导入UMAnalytics的OC的头文件
#import <UMAnalytics/MobClick.h>

#endif /* GrxTrade_Bridging_Header_h */
