//
//  Utility.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 02/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import Foundation
import UIKit

func showSimpleToast(_ msg: String) {
    let windows = UIApplication.shared.windows
    windows.last?.makeToast(msg, duration: 3.0, position: .bottom)
//    UIApplication.shared.keyWindow?.makeToast(msg, duration: 3.0, position: .bottom)
}

func countryName(from countryCode: String) -> String {
    if let name = (Locale.current as NSLocale).displayName(forKey: .countryCode, value: countryCode) {
        // Country name was found
        return name
    } else {
        // Country name cannot be found
        return countryCode
    }
}

func showTimeoutAlertView(_ title: String, _ message: String, controller: UIViewController) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { action in
        BaseSingleton.shared.user.reset()
        
        let vc = TabBarViewController()
        vc.selectedIndex = 4

        guard
        let window = UIApplication.shared.keyWindow,
        let rootViewController = window.rootViewController
        else {
            return
        }

        vc.view.frame = rootViewController.view.frame
        vc.view.layoutIfNeeded()

        window.rootViewController = vc
    }))
    controller.present(alert, animated: true, completion: nil)
    return
}

func showAlertView(_ title: String, _ message: String, controller: UIViewController) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
    controller.present(alert, animated: true, completion: nil)
    return
}

func isValidPassword(_ password : String) -> Bool {
    let passwordRegex = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{6,20}$"
    return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: password)
}

func restartApplication(pageIndex: Int) {
    let vc = TabBarViewController()
    vc.selectedIndex = pageIndex

    guard
    let window = UIApplication.shared.keyWindow,
    let rootViewController = window.rootViewController
    else {
        return
    }

    vc.view.frame = rootViewController.view.frame
    vc.view.layoutIfNeeded()

//        UIView.transition(with: window, duration: 0.5, options: .curveEaseIn, animations: {
        window.rootViewController = vc
//        })
}
