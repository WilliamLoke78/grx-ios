//
//  Extensions.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 02/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit
import CommonCrypto

//extension UIViewController {
//    func hideKeyboardWhenTappedAround() {
//        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
//        tap.cancelsTouchesInView = false
//        view.addGestureRecognizer(tap)
//    }
//    
//    @objc func dismissKeyboard() {
//        view.endEditing(true)
//    }
//}

extension UIView {
    
    func addConstraintsWithFormat(format: String, views: UIView...) {
        var viewsDictionary = [String: UIView]()
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            viewsDictionary[key] = view
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewsDictionary))
    }
    
    func roundCorners(corners:UIRectCorner, radius: CGFloat) {
        DispatchQueue.main.async {
            let path = UIBezierPath(roundedRect: self.bounds,
                                    byRoundingCorners: corners,
                                    cornerRadii: CGSize(width: radius, height: radius))
            let maskLayer = CAShapeLayer()
            maskLayer.frame = self.bounds
            maskLayer.path = path.cgPath
            self.layer.mask = maskLayer
        }
    }
    
    func dropShadow() {
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.2
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 2
    }
    
    func pushTransition(_ duration:CFTimeInterval) {
        let animation:CATransition = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name:
            CAMediaTimingFunctionName.easeInEaseOut)
        animation.type = CATransitionType.push
        animation.subtype = CATransitionSubtype.fromTop
        animation.duration = duration
        layer.add(animation, forKey: CATransitionType.push.rawValue)
    }
}

extension UILabel {
    
    func pairBeautifier(_ primaryColor: UIColor) {
        let string = self.text?.split(separator: "/")
        
        let firstAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: UIFont(name: bold, size: 16)!, NSAttributedString.Key.foregroundColor: primaryColor]
        let secondAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: UIFont(name: regular, size: 11)!, NSAttributedString.Key.foregroundColor: UIColor.grayColor]
        
        let firstString = NSMutableAttributedString(string: String(string![0]), attributes: firstAttributes)
        let secondString = NSAttributedString(string: String(" /\(string![1])"), attributes: secondAttributes)
        
        firstString.append(secondString)
        
        self.attributedText = firstString
    }
    
    func currencyBeautifier() {
        let string = self.text?.split(separator: " ")
        
        let firstAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: UIFont(name: bold, size: 20)!, NSAttributedString.Key.foregroundColor: darkTheme ? UIColor.white : UIColor.primary]
        let secondAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: UIFont(name: bold, size: 12)!, NSAttributedString.Key.foregroundColor: darkTheme ? UIColor.white : UIColor.primary]
        
        let firstString = NSMutableAttributedString(string: String(string![0]), attributes: firstAttributes)
        let secondString = NSAttributedString(string: String(" \(string![1])"), attributes: secondAttributes)
        
        firstString.append(secondString)
        
        self.attributedText = firstString
    }
}

extension UIImageView {
    func setImageColor(color: UIColor) {
        let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
}

extension UITextField {
    func leftPadding(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func rightPadding(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

extension String {
    public var length: Int {return self.count}
    
    // MARK: - 强制指定需要多少位小数，不够小数位的补0（方法支持到20位小数，常用够用了，如需更多位小数请自行修改）
    public func forceDecimalDigits(_ digits: Int) -> String {
        guard digits >= 0, digits <= 20 else {
            return self
        }
        let max = "00000000000000000000"
        if !self.contains(".") {
            return self + "." + max.subString(start: 0, length: digits)
        }
        let arr = self.components(separatedBy: ".")
        
        return arr[0] + "." + (arr[1] + max).subString(start: 0, length: digits)
    }
    
    // MARK: - String类型转Double，1、避免Double(str)失败，2、NumberFormatter处理
    public func toDouble() -> Double {
        guard self.length > 0 else {
            return 0
        }
        let formater = NumberFormatter.init()
        formater.numberStyle = .decimal
        
        return formater.number(from: self)?.doubleValue ?? 0
    }
    
    // MARK: - 根据开始位置和长度截取字符串
    public func subString(start:Int, length:Int = -1) -> String {
        var len = length
        if len == -1 {
            len = self.count - start
        }
        let st = self.index(startIndex, offsetBy:start)
        let en = self.index(st, offsetBy:len)
        return String(self[st ..< en])
    }
    
    func md5() -> String {
        let data = Data(utf8) as NSData
        var hash = [UInt8](repeating: 0, count: Int(CC_MD5_DIGEST_LENGTH))
        CC_MD5(data.bytes, CC_LONG(data.length), &hash)
        return hash.map { String(format: "%02hhx", $0) }.joined()
    }
    
    func generateQRCode() -> UIImage? {
        let data = self.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        
        return nil
    }
    
    func isValidEmail() -> Bool {
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
    
    public func localized(with arguments: [CVarArg]) -> String {
        return String(format: self.localized(), locale: nil, arguments: arguments)
    }
    
    func serverToLocal() -> String {
        let serverDatetime = self
        let localDatetime = UTCToLocal(date: serverDatetime)
        
        return localDatetime
    }
    
    func UTCToLocal(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: LocalizationManager.serverLang())
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "MMM dd, yyyy HH:mm"
        
        return dateFormatter.string(from: dt!)
    }

    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else {
            return nil
        }

        return String(data: data, encoding: .utf8)
    }

    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
}

extension UINavigationBar {
    // MARK: - 修改navbar背景颜色
    public func setBackground(_ color: UIColor) {
        self.setBackgroundImage(color.image(), for: .any, barMetrics: .default)
    }
    // MARK: - 是否显示navbar的底部阴影效果
    public func setShadowVisible(_ visible: Bool) {
        self.shadowImage = visible ? nil : UIImage()
    }
}

extension UIColor {
    // MARK: - 由颜色填充生成一张图片
    public func image() -> UIImage? {
        let rect = CGRect.init(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context?.setFillColor(self.cgColor)
        context?.fill(rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}

extension UIDevice{
    
    var isBeforeIOS11: Bool{
        return UIDevice.current.systemVersion.toDouble() < 11
    }
    
    var isAfterIOS11: Bool{
        return UIDevice.current.systemVersion.toDouble() >= 11
    }
    
    public var isSafeAreaDevice: Bool {
        if #available(iOS 11, *) {
            guard let w = UIApplication.shared.delegate?.window, let unwrapedWindow = w else {
                return false
            }
            
            if unwrapedWindow.safeAreaInsets.bottom > 0 {
                print(unwrapedWindow.safeAreaInsets)
                return true
            }
        }
        return false
    }
}

extension UIDevice {
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        return identifier
    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    var className: String {
        return NSStringFromClass(self.classForCoder).components(separatedBy: ".").last!
    }
}

extension UIView {
    
    var width: CGFloat {
        get {
            return self.frame.width
        }
        set {
            var frame = self.frame
            frame.size.width = newValue
            self.frame = frame
        }
    }
    
    func applyGradient(with colours: [UIColor], locations: [NSNumber]? = nil) {
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func applyGradient(with colours: [UIColor], gradient orientation: GradientOrientation) {
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.startPoint = orientation.startPoint
        gradient.endPoint = orientation.endPoint
        self.layer.insertSublayer(gradient, at: 0)
    }
}
