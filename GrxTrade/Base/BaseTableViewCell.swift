//
//  BaseTableViewCell.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 01/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit
import SnapKit

class BaseTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = .background
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Outcome is okay, but not desirable
        let view = UIView()
        view.backgroundColor = .background
        selectedBackgroundView = view
    }

//    func resetTheme() {
//        backgroundColor = .container
//    }
}

//extension BaseTableViewCell: ResetTheme {
//    @objc func receivedResetThemeNotification(_ notification: Notification) {
//        self.resetTheme()
//    }
//}
