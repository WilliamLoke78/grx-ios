//
//  BasePasswordViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 02/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import UIKit

class BasePasscodeViewController: UIViewController {
    
    lazy var backgroundView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 10
        view.backgroundColor = .container
        return view
    }()
    
    lazy var cancelButton: UIButton = {
        let button = UIButton()
        let image = UIImage.init(named: "cross")
        button.setImage(image, for: .normal)
        button.addTarget(self, action: #selector(tapCancel(_:)), for: .touchUpInside)
        return button
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: bold, size: 20)
        label.textColor = .textColor
        label.text = "Verify Your Email".localized()
        return label
    }()
    
    lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: regular, size: 14)
        label.textColor = .grayColor
        label.numberOfLines = 2
        label.text = "Enter the 6-digit code we’ve sent it to 123@abc.com.".localized()
        return label
    }()
    
    lazy var codeTextField: CustomTextField = {
        let tf = CustomTextField()
        tf.font = UIFont.init(name: "Courier", size: 30)
        tf.keyboardType = .numberPad
        tf.defaultTextAttributes.updateValue(24.2, forKey: NSAttributedString.Key.kern)
        tf.tintColor = .clear
        tf.textColor = .textColor
        tf.keyboardAppearance = darkTheme ? .dark : .light
        tf.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        return tf
    }()
    
    lazy var bottomLineView: UIView = {
        let view = UIView()
        return view
    }()
    
    lazy var bottomLine1: UIView = {
        let view = UIView()
        view.backgroundColor = .primary
        return view
    }()
    
    lazy var bottomLine2: UIView = {
        let view = UIView()
        view.backgroundColor = .primary
        return view
    }()
    
    lazy var bottomLine3: UIView = {
        let view = UIView()
        view.backgroundColor = .primary
        return view
    }()
    
    lazy var bottomLine4: UIView = {
        let view = UIView()
        view.backgroundColor = .primary
        return view
    }()
    
    lazy var bottomLine5: UIView = {
        let view = UIView()
        view.backgroundColor = .primary
        return view
    }()
    
    lazy var bottomLine6: UIView = {
        let view = UIView()
        view.backgroundColor = .primary
        return view
    }()
    
    lazy var remarkLabel: UILabel = {
        let label = UILabel()
        
        let firstAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: UIFont(name: regular, size: 14) as Any, NSAttributedString.Key.foregroundColor: UIColor.textColor]
        let secondAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: UIFont(name: bold, size: 14) as Any, NSAttributedString.Key.foregroundColor: UIColor.primary]
        
        let firstString = NSMutableAttributedString(string: "If you have not received the email, please check your spam. ".localized(), attributes: firstAttributes)
        let secondString = NSAttributedString(string: "Re-send".localized(), attributes: secondAttributes)
        
        firstString.append(secondString)
        
        label.numberOfLines = 2
        label.attributedText = firstString
        label.lineBreakMode = .byWordWrapping
        
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addSubviews()
        adjustUI()
        addConstraints()
        addEvents()
    }
    
}

// MARK: User Interface
extension BasePasscodeViewController {
    
    func addSubviews() {
        view.addSubview(backgroundView)
        backgroundView.addSubview(cancelButton)
        backgroundView.addSubview(titleLabel)
        backgroundView.addSubview(subtitleLabel)
        backgroundView.addSubview(codeTextField)
        backgroundView.addSubview(bottomLineView)
        bottomLineView.addSubview(bottomLine1)
        bottomLineView.addSubview(bottomLine2)
        bottomLineView.addSubview(bottomLine3)
        bottomLineView.addSubview(bottomLine4)
        bottomLineView.addSubview(bottomLine5)
        bottomLineView.addSubview(bottomLine6)
        backgroundView.addSubview(remarkLabel)
    }
    
    func adjustUI() {
        view.backgroundColor = .clear
    }
    
    func addConstraints() {
        view.addConstraintsWithFormat(format: "H:|-0-[v0]-0-|", views: backgroundView)
        view.addConstraintsWithFormat(format: "V:|-0-[v0]-0-|", views: backgroundView)
        
        backgroundView.addConstraintsWithFormat(format: "H:[v0(22)]-13-|", views: cancelButton)
        backgroundView.addConstraintsWithFormat(format: "V:|-14-[v0(22)]", views: cancelButton)
        
        backgroundView.addConstraintsWithFormat(format: "H:|-21-[v0]-21-|", views: titleLabel)
        backgroundView.addConstraintsWithFormat(format: "H:|-21-[v0]-21-|", views: subtitleLabel)
        backgroundView.addConstraintsWithFormat(format: "H:|-28-[v0]-21-|", views: codeTextField)
        backgroundView.addConstraintsWithFormat(format: "H:|-21-[v0]-21-|", views: bottomLineView)
        backgroundView.addConstraintsWithFormat(format: "H:|-21-[v0]-21-|", views: remarkLabel)
        backgroundView.addConstraintsWithFormat(format: "V:|-49-[v0]-14-[v1]-43-[v2]-2-[v3(1)]-14-[v4]", views: titleLabel, subtitleLabel, codeTextField, bottomLineView, remarkLabel)
        
        bottomLineView.addConstraintsWithFormat(format: "V:|[v0(1)]|", views: bottomLine1)
        bottomLineView.addConstraintsWithFormat(format: "V:|[v0(1)]|", views: bottomLine2)
        bottomLineView.addConstraintsWithFormat(format: "V:|[v0(1)]|", views: bottomLine3)
        bottomLineView.addConstraintsWithFormat(format: "V:|[v0(1)]|", views: bottomLine4)
        bottomLineView.addConstraintsWithFormat(format: "V:|[v0(1)]|", views: bottomLine5)
        bottomLineView.addConstraintsWithFormat(format: "V:|[v0(1)]|", views: bottomLine6)
        bottomLineView.addConstraintsWithFormat(format: "H:|[v0(34)]-8-[v1(34)]-8-[v2(34)]-8-[v3(34)]-8-[v4(34)]-8-[v5(34)]", views: bottomLine1, bottomLine2, bottomLine3, bottomLine4, bottomLine5, bottomLine6)
    }
    
    func addEvents() {
        
        codeTextField.delegate = self
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
            self.codeTextField.becomeFirstResponder()
        }
    }
    
    @objc private func tapCancel(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        let currentText = textField.text ?? ""
        
//        if currentText.count == 6 {
//            let vc = SuccessViewController()
//            let presenter = Presentr(presentationType: .custom(width: .full, height: .custom(size: 600), center: .bottomCenter))
//            customPresentViewController(presenter, viewController: vc, animated: true, completion: nil)
//        }
    }
}

extension BasePasscodeViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = textField.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        
        return updatedText.count <= 6
    }
}

class CustomTextField: UITextField {
    override public func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
}

