//
//  BaseViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 01/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit
import SnapKit

class BaseViewController: UIViewController {

    deinit {
//        NotificationCenter.default.removeObserver(self, name: .darkTheme, object: nil)
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(receivedResetThemeNotification(_:)), name: .darkTheme, object: nil)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
//        NotificationCenter.default.addObserver(self, selector: #selector(receivedResetThemeNotification(_:)), name: .darkTheme, object: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
//        NotificationCenter.default.addObserver(self, selector: #selector(receivedResetThemeNotification(_:)), name: .darkTheme, object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        hideKeyboardWhenTappedAround()
    }
    
    func setupSubview() {
        
    }
    
//    func resetTheme() {
//        view.backgroundColor = .background
//    }
}

//extension BaseViewController: ResetTheme {
//    @objc func receivedResetThemeNotification(_ notification: Notification) {
//        self.resetTheme()
//    }
//}
