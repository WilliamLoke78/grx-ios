//
//  BaseView.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 01/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit
import SnapKit

class BaseView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        
    }
}

//extension BaseView: ResetTheme {
//    @objc func receivedResetThemeNotification(_ notification: Notification) {
//        print("Received reset theme notification")
//        self.resetTheme()
//    }
//}
