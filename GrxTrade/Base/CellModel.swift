//
//  CellModel.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 16/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

struct CellModel {
    var icon = ""
    var title = ""
    var subTitle = ""
    var accIcon = ""
    var button = ""
    var tag = ""
}

struct AdvanceKYCCell {
    var label = ""
    var content = ""
}
