//
//  BaseTableViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 01/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit
import EmptyDataSet_Swift

class BaseTableViewController: UITableViewController {
    
    var isHomeView: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if isHomeView != true {
            tableView.emptyDataSetView { [weak self] view in
                view.customView(EmptyView(frame: CGRect(x: 0, y: 0, width: 150, height: 150)))
            }
        }
    }
}
