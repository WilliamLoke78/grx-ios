//
//  SignupViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 02/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import UIKit
import AVFoundation
import Presentr
import CountryPickerView

class SignupViewController: BaseViewController, CountryPickerViewDelegate, CountryPickerViewDataSource, QrDelegate, SuccessDelegate, UITextFieldDelegate {

    var delegate: SuccessDelegate?
    
    lazy var emailView: TextFieldView = {
        let view = TextFieldView()
        view.label.text = "Your Email".localized()
        view.textField.keyboardType = .emailAddress
        view.textField.autocorrectionType = .no
        view.textField.autocapitalizationType = .none
        return view
    }()
    
    lazy var passwordView: TextFieldView = {
        let view = TextFieldView()
        view.label.text = "Password".localized()
        view.textField.isSecureTextEntry = true
        view.lastButton.isHidden = false
        view.lastButton.addTarget(self, action: #selector(togglePassword(_:)), for: .touchUpInside)
//        view.textField.text = "abc123"
        return view
    }()
    
    lazy var retypePasswordView: TextFieldView = {
        let view = TextFieldView()
        view.label.text = "Re-type Password".localized()
        view.textField.isSecureTextEntry = true
        view.lastButton.isHidden = false
        view.lastButton.addTarget(self, action: #selector(toggleRetypePassword(_:)), for: .touchUpInside)
//        view.textField.text = "abc123"
        view.textField.tag = 2
        return view
    }()
    
    lazy var countryView: SelectFieldView = {
        let view = SelectFieldView()
        view.label.text = "Country".localized()
        view.blockButton.addTarget(self, action: #selector(tapSelectCountry(_:)), for: .touchUpInside)
        return view
    }()
    
    lazy var cpv = CountryPickerView(frame: CGRect(x: 0, y: 0, width: screenWidth-22, height: 20))
    
    lazy var referralLabel: UIButton = {
        let button = UIButton()
        button.setTitle("Referral ID?".localized(), for: .normal)
        button.titleLabel?.font = UIFont(name: bold, size: 14)
        button.setTitleColor(.primary, for: .normal)
        button.addTarget(self, action: #selector(tapReferral(_:)), for: .touchUpInside)
        return button
    }()
    
    lazy var referralView: TextFieldView = {
        let view = TextFieldView()
        view.label.text = "Referral ID".localized()
        view.textField.autocorrectionType = .no
        view.isHidden = true
        view.lastButton.isHidden = false
        view.lastButton.setImage(UIImage(named: "scan"), for: .normal)
        view.lastButton.addTarget(self, action: #selector(tapScan(_:)), for: .touchUpInside)
        return view
    }()
    
    lazy var createButton: PrimaryButton = {
        let button = PrimaryButton()
        button.setTitle("Create Account".localized(), for: .normal)
        button.addTarget(self, action: #selector(tapCreateAccounButton(_:)), for: .touchUpInside)
        button.titleLabel?.font = UIFont(name: bold, size: 18)
        return button
    }()
    
    lazy var separatorLine: UIView = {
        let view = UIView()
        view.backgroundColor = .grayColor
        return view
    }()
    
    lazy var orLabel: UILabel = {
        let label = UILabel()
        label.text = "OR".localized()
        label.font = UIFont(name: bold, size: 14)
        label.textColor = .textColor
        label.backgroundColor = .backgroundAlternative
        label.textAlignment = .center
        return label
    }()
    
    lazy var maxRegister: PrimaryButton = {
        let button = PrimaryButton()
        button.setTitle("Register with MAX-Wallet".localized(), for: .normal)
        button.addTarget(self, action: #selector(tapWallet(_:)), for: .touchUpInside)
        button.backgroundColor = .mxwGold
        button.titleLabel?.font = UIFont(name: bold, size: 18)
        return button
    }()
    
    lazy var termsLabel: UILabel = {
        let label = UILabel()
        
        let firstAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: UIFont(name: regular, size: 12) as Any, NSAttributedString.Key.foregroundColor: UIColor.textColor]
        let secondAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: UIFont(name: bold, size: 12) as Any, NSAttributedString.Key.foregroundColor: UIColor.textColor]
        
        let firstString = NSMutableAttributedString(string: "By creating an account, I agree to GRXTrade’s ".localized(), attributes: firstAttributes)
        let secondString = NSAttributedString(string: "Terms of Use".localized(), attributes: secondAttributes)
        let thirdString = NSAttributedString(string: " and ".localized(), attributes: firstAttributes)
        let fourthString = NSAttributedString(string: "Privacy Policy".localized(), attributes: secondAttributes)
        
        firstString.append(secondString)
        firstString.append(thirdString)
        firstString.append(fourthString)
        
        label.attributedText = firstString
        label.numberOfLines = 2
        label.isUserInteractionEnabled = true
        label.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapLabel(_:))))
        
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.addSubviews()
        self.adjustUI()
        self.addConstraints()
        self.addDeepLinkObserver()
        
        cpv.delegate = self
        cpv.dataSource = self
        
        cpv.showPhoneCodeInView = false
        cpv.showCountryCodeInView = false
        
//        countryView.textField.leftView = cpv
//        countryView.textField.leftViewMode = .always
        countryView.textField.text = cpv.selectedCountry.localizedName()
    }
    
    var activeTextField = UITextField()
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 2 {
            guard isValidPassword(passwordView.textField.text!) else {
                print(123)
                showSimpleToast("Password must contain at least 6 alphanumerical characters with uppercase and lowercase".localized())
                return
            }
        }
        
        
    }
        

        
    func anotherMethod() {

        // self.activeTextField.text is an optional, we safely unwrap it here
        if let activeTextFieldText = self.activeTextField.text {
            
            guard isValidPassword(passwordView.textField.text!) else {
                showSimpleToast("Password must contain at least 6 alphanumerical characters with uppercase and lowercase".localized())
                return
            }
            
              print("Active text field's text: \(activeTextFieldText)")
              return;
        }

        print("Active text field is empty")
    }

//        if textField == retypePasswordView.textField {
//
//            guard isValidPassword(passwordView.textField.text!) else {
//                showSimpleToast("Password must contain at least 6 alphanumerical characters with uppercase and lowercase".localized())
//                return
//            }
//        }
//
//
//
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        navigationController?.navigationBar.barTintColor = .darkBackground
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

// MARK: User Interface
extension SignupViewController {
    func addDeepLinkObserver() {
//        NotificationCenter.default.addObserver(
//        self,
//        selector: #selector(grabData),
//        name: NSNotification.Name.UIApplication.didBecomeActiveNotification,
//        object: nil)
    }
    
    func grabData() {
        
    }
    
    func addSubviews() {
        view.addSubview(emailView)
        view.addSubview(passwordView)
        view.addSubview(retypePasswordView)
        view.addSubview(countryView)
        view.addSubview(referralLabel)
        view.addSubview(referralView)
        view.addSubview(createButton)
        view.addSubview(separatorLine)
        view.addSubview(orLabel)
        view.addSubview(maxRegister)
        view.addSubview(termsLabel)
    }
    
    func adjustUI() {
        title = "Register".localized()
        view.backgroundColor = .backgroundAlternative
    }
    
    func addConstraints() {
        view.addConstraintsWithFormat(format: "H:|-22-[v0]-22-|", views: countryView)
        view.addConstraintsWithFormat(format: "H:|-22-[v0]-22-|", views: emailView)
        view.addConstraintsWithFormat(format: "H:|-22-[v0]-22-|", views: passwordView)
        view.addConstraintsWithFormat(format: "H:|-22-[v0]-22-|", views: retypePasswordView)
        view.addConstraintsWithFormat(format: "H:|-22-[v0]-22-|", views: createButton)
        view.addConstraintsWithFormat(format: "H:|-22-[v0]-22-|", views: maxRegister)
        
        referralLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(retypePasswordView.snp.bottom).offset(10)
            make.left.equalToSuperview().offset(22)
        })
        
        createButton.snp.makeConstraints({ (make) in
            make.top.equalTo(referralLabel.snp.bottom).offset(22)
            make.left.equalTo(referralLabel)
            make.height.equalTo(54)
        })
        
        separatorLine.snp.makeConstraints({ (make) in
            make.top.equalTo(createButton.snp.bottom).offset(20)
            make.left.right.equalToSuperview()
            make.height.equalTo(0.3)
        })
        
        orLabel.snp.makeConstraints({ (make) in
            make.centerX.centerY.equalTo(separatorLine)
            make.width.equalTo(50)
        })
        
        maxRegister.snp.makeConstraints({ (make) in
            make.top.equalTo(separatorLine.snp.bottom).offset(20)
            make.left.equalTo(referralLabel)
            make.height.equalTo(54)
        })
        
        termsLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(maxRegister.snp.bottom).offset(4)
            make.left.equalTo(referralLabel)
            make.right.equalToSuperview().offset(-22)
        })
        
        view.addConstraintsWithFormat(format: "V:|-13-[v0(54)]-10-[v1(54)]-10-[v2(54)]-10-[v3(54)]", views: countryView, emailView, passwordView, retypePasswordView)
    }
    
    @objc private func togglePassword(_ sender: UIButton) {
        passwordView.textField.isSecureTextEntry.toggle()
        retypePasswordView.textField.isSecureTextEntry.toggle()
        if passwordView.textField.isSecureTextEntry == true {
            passwordView.lastButton.setImage(UIImage(named: "invisible"), for: .normal)
            retypePasswordView.lastButton.setImage(UIImage(named: "invisible"), for: .normal)
        } else {
            passwordView.lastButton.setImage(UIImage(named: "visible"), for: .normal)
             retypePasswordView.lastButton.setImage(UIImage(named: "visible"), for: .normal)
        }
    }
    
    @objc private func toggleRetypePassword(_ sender: UIButton) {
        passwordView.textField.isSecureTextEntry.toggle()
        retypePasswordView.textField.isSecureTextEntry.toggle()
        if retypePasswordView.textField.isSecureTextEntry == true {
            passwordView.lastButton.setImage(UIImage(named: "invisible"), for: .normal)
            retypePasswordView.lastButton.setImage(UIImage(named: "invisible"), for: .normal)
        } else {
            passwordView.lastButton.setImage(UIImage(named: "visible"), for: .normal)
            retypePasswordView.lastButton.setImage(UIImage(named: "visible"), for: .normal)
        }
    }
    
    @objc func tapSelectCountry(_ sender: UIButton) {
        cpv.showCountriesList(from: self)
    }
    
    @objc private func tapCreateAccounButton(_ sender: UIButton) {
        launchOTP()
    }
    
    @objc private func tapWallet(_ sender: UIButton) {
        setupDialog()
    }
    
    @objc private func tapReferral(_ sender: UIButton) {
        referralLabel.isHidden = true
        referralView.isHidden = false
        
        referralView.snp.makeConstraints({ (make) in
            make.top.equalTo(retypePasswordView.snp.bottom).offset(10)
            make.left.equalToSuperview().offset(22)
            make.right.equalToSuperview().offset(-22)
            make.height.equalTo(54)
        })
        
        createButton.snp.remakeConstraints({ (make) in
            make.top.equalTo(referralView.snp.bottom).offset(20)
            make.height.equalTo(54)
        })
    }
    
    @objc private func tapScan(_ sender: UIButton) {
        let vc = ScanViewController()
        vc.modalPresentationStyle = .fullScreen
        vc.delegate = self
        
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized:
            present(vc, animated: true, completion: nil)
            
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { granted in
                if granted {
                    DispatchQueue.main.async {
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            }
            
        case .denied:
            let alert = UIAlertController(title: "Camera".localized(), message: "Camera is needed to scan QR code".localized(), preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
                } else {
                    // Fallback on earlier versions
                }
            }))
            
            self.present(alert, animated: true)
            return
            
        case .restricted:
            showSimpleToast("Camera is needed to scan QR code".localized())
            return
        }
    }
    
    @objc func tapLabel(_ recognizer: UITapGestureRecognizer) {
        guard let text = termsLabel.attributedText?.string else {
            return
        }

        if let range = text.range(of: NSLocalizedString("Terms of Use".localized(), comment: "terms")),
            recognizer.didTapAttributedTextInLabel(label: termsLabel, inRange: NSRange(range, in: text)) {
            showTerms()
        } else if let range = text.range(of: NSLocalizedString("Privacy Policy".localized(), comment: "privacy")),
            recognizer.didTapAttributedTextInLabel(label: termsLabel, inRange: NSRange(range, in: text)) {
            showPolicy()
        }
    }
    
    func showTerms() {
        let vc = WebViewController()
        vc.webViewTitle = "Terms of Use".localized()
        vc.isScrollEnabled = true
        let networking = Networking()
        vc.urlString = "\(networking.staticHost)/legal/terms-of-use?isMobile=1&lang=\(LocalizationManager.serverLang())"
        print("\(networking.staticHost)/legal/terms-of-use?isMobile=1&lang=\(LocalizationManager.serverLang())")
        show(vc, sender: nil)
    }
    
    func showPolicy() {
        let vc = WebViewController()
        vc.webViewTitle = "Privacy Policy".localized()
        vc.isScrollEnabled = true
        let networking = Networking()
        vc.urlString = "\(networking.staticHost)/legal/privacy-policy?isMobile=1&lang=\(LocalizationManager.serverLang())"
        print("\(networking.staticHost)/legal/privacy-policy?isMobile=1&lang=\(LocalizationManager.serverLang())")
        show(vc, sender: nil)
    }

    
    func backFromCamera(string: String) {
        referralView.textField.text = string
    }
    
//    @objc private func showVerifyCode() {
//        let vc = VerifyEmailViewController()
//        let presenter = Presentr(presentationType: .custom(width: .full, height: .custom(size: 680), center: .bottomCenter))
//        customPresentViewController(presenter, viewController: vc, animated: true, completion: nil)
//    }
}

extension SignupViewController {
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        countryView.textField.text = cpv.selectedCountry.localizedName()
        return
    }
    
    private func preferredCountries(in countryPickerView: CountryPickerView) -> [Country]? {
        return nil
    }
    
    func sectionTitleForPreferredCountries(in countryPickerView: CountryPickerView) -> String? {
        return nil
    }
    
    private func showOnlyPreferredSection(in countryPickerView: CountryPickerView) -> Bool? {
        return false
    }
    
    func navigationTitle(in countryPickerView: CountryPickerView) -> String? {
        return "Select country".localized()
    }
    
    func closeButtonNavigationItem(in countryPickerView: CountryPickerView) -> UIBarButtonItem? {
        return nil
    }
    
    func searchBarPosition(in countryPickerView: CountryPickerView) -> SearchBarPosition {
        return .tableViewHeader
    }
    
    func showCountryCodeInList(in countryPickerView: CountryPickerView) -> Bool {
        return false
    }
    
    private func showPhoneCodeInList(in countryPickerView: CountryPickerView) -> Bool? {
        return false
    }
}

// MARK: Networking
extension SignupViewController {
    
    func displaySuccess() {
        restartApplication(pageIndex: 4)
    }
    
    func getPackage(kyc: Bool) {
    
        var params: [String: String] = [:]
        if kyc == true {
            params["type"] = "MXW_WALLET_REGISTER_KYC"
        } else {
            params["type"] = "MXW_WALLET_REGISTER"
        }
        
        let networking = Networking()
        networking.getPackageInfo(params: params, controller: self) { response in
            
            print("response:")
            print(response)
            
            let base64 = "\(response)".toBase64()
            
            let host = "com.mxw.wallet://maxRegister?scheme=com.grxtrade.app&type=register"
            var data = "&data=\(base64)"
            data = data.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
            
            guard let openMxwURL = URL(string: host+data) else {
                print("open MXW URL failed")
                return //be safe
            }
            
            if UIApplication.shared.canOpenURL(openMxwURL) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(openMxwURL, options: [:], completionHandler: { (success) in
                        print("Successfully launched MaxWallet")
                    })
                } else {
                    UIApplication.shared.openURL(openMxwURL)
                    print("Successfully launched MaxWallet")
                }
            } else {
                guard let url = URL(string: networking.mxwHost) else {
                    return
                }
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    
    func postMXWRegister(data: Any) {
        
        var params: [String: Any] = [:]
        params["data"] = "\(data)".fromBase64()
        
        print("post mxw register")
        print(params)
        
        let networking = Networking()
        networking.postMxwRegister(params: params as! [String : String], controller: self) { user in
            
            Storage.save(key: Storage.Name.user, value: user)
            Storage.save(key: Storage.Name.token, value: user.accessToken)
            BaseSingleton.shared.user = user
            BaseSingleton.shared.token = user.accessToken!
            
            restartApplication(pageIndex: 4)
        }
    }
    
    func setupDialog() {
        let alert = UIAlertController(title: "KYC".localized(), message: "Do you wish to perform identity verification upon registration?".localized(), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Later".localized(), style: .cancel, handler: { action in
            self.getPackage(kyc: false)
        }))
        alert.addAction(UIAlertAction(title: "Sure".localized(), style: .default, handler: { action in
            self.getPackage(kyc: true)
        }))
        
        self.present(alert, animated: true) {
            alert.view.superview?.isUserInteractionEnabled = true
            alert.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertControllerBackgroundTapped)))
        }
    }
    
    func launchOTP() {

        guard (emailView.textField.text != "") else {
            showSimpleToast("Email cannot be empty".localized())
            return
        }

        guard emailView.textField.text!.isValidEmail() else {
            showSimpleToast("Invalid email address".localized())
            return
        }

        guard passwordView.textField.text != "" else {
            showSimpleToast("Password cannot be empty".localized())
            return
        }

        if passwordView.textField.text != retypePasswordView.textField.text {
            showSimpleToast("Password does not match".localized())
            return
        }
        
        guard isValidPassword(passwordView.textField.text!) else {
            showSimpleToast("Password must contain at least 6 alphanumerical characters with uppercase and lowercase".localized())
            return
        }

        var params: [String:String] = [:]
        params["email"] = emailView.textField.text!
        params["password"] = passwordView.textField.text!.md5()
        params["registerType"] = "APP_EMAIL"
        params["country"] = cpv.selectedCountry.code

        if referralView.textField.text != "" {
            params["refCode"] = referralView.textField.text!
        }
        
        print("params: \(params)")
        
        let networking = Networking()
        networking.requestOTP(params: ["email": emailView.textField.text!], controller: self) { response in
            print(response)
        }

        let vc = OneTimePasswordViewController()
        vc.type = "signup"
        vc.params = params
        let presenter = Presentr(presentationType: .custom(width: .full, height: .custom(size: 680), center: .bottomCenter))
        self.customPresentViewController(presenter, viewController: vc, animated: true, completion: nil)
    }
    
    @objc func alertControllerBackgroundTapped() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension UITapGestureRecognizer {
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        guard let attrString = label.attributedText else {
            return false
        }

        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: .zero)
        let textStorage = NSTextStorage(attributedString: attrString)

        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)

        textContainer.lineFragmentPadding = 0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize

        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y: locationOfTouchInLabel.y - textContainerOffset.y)
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
}

public extension UIResponder {

    private struct Static {
        static weak var responder: UIResponder?
    }

    public static func currentFirst() -> UIResponder? {
        Static.responder = nil
        UIApplication.shared.sendAction(#selector(UIResponder._trap), to: nil, from: nil, for: nil)
        return Static.responder
    }

    @objc private func _trap() {
        Static.responder = self
    }
}
