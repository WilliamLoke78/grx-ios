//
//  OneTimePasswordViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 02/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

protocol SuccessDelegate {
    func displaySuccess()
}

class OneTimePasswordViewController: BasePasscodeViewController {
    
    var delegate: SuccessDelegate?
    
    let networking = Networking()
    
    var secret = String()
    var params: [String: String] = [:]
    var type = String() // "signup", "loginPassword", "transactionPassword", "setGA", "cancelGA", "cancelSMS"
    var isSet: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch type {
        case "signup":
            titleLabel.text = "Email - Security Verification".localized()
        case "loginPassword":
            titleLabel.text = "Google Authenticator".localized()
        case "transactionPassword":
            titleLabel.text = "Google Authenticator".localized()
        case "forgotPassword":
            titleLabel.text = "Email - Security Verification".localized()
        case "addBeneficiary":
            titleLabel.text = "Google Authenticator".localized()
        case "deleteBeneficiary":
            titleLabel.text = "Google Authenticator".localized()
        case "setGA":
            titleLabel.text = "Set Google Authenticator".localized()
        case "cancelGA":
            titleLabel.text = "Cancel Google Authenticator".localized()
        case "cancelSMS":
            titleLabel.text = "Cancel SMS Authenticator".localized()
        default:
            return
        }
        
        subtitleLabel.text = "Please enter the 6-digit security verification code.".localized()
        remarkLabel.isHidden = true
    }
    
    @objc override func textFieldDidChange(_ textField: UITextField) {
        let code = textField.text ?? ""
        
        if code.count == 6 {
            
            if !secret.isEmpty { // Setup Google Authenticator
            
                params["gaCode"] = code
                params["gaSecret"] = secret
                
                print("params:")
                print(params)
                
                networking.setGoogleAuth(params: params, controller: self) { result in
                    self.dismiss(animated: true, completion: {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "popGoogleAuthView"), object: nil)
                    })
                }
                
            } else {
                
                switch type {
                case "signup":
                    signup(otp: code)
                case "loginPassword":
                    changeLoginPassword(gaCode: code)
                case "transactionPassword":
                    changeTransactionPassword(gaCode: code)
                case "forgotPassword":
                    forgotPassword(otp: code)
                case "addBeneficiary":
                    addBeneficiary(gaCode: code)
                case "deleteBeneficiary":
                    deleteBeneficiary(gaCode: code)
                case "cancelGA":
                    cancelGA(gaCode: code)
                case "cancelSMS":
                    cancelSMS(otp: code)
                default:
                    return
                }
            }
        }
    }
    
    func signup(otp: String) {
        
        params["otp"] = otp
        
        networking.postSignup(params: params, controller: self) { user in
            
            Storage.save(key: Storage.Name.user, value: user)
            Storage.save(key: Storage.Name.token, value: user.accessToken)
            BaseSingleton.shared.user = user
            BaseSingleton.shared.token = user.accessToken!
            
            self.dismiss(animated: true, completion: {
                restartApplication(pageIndex: 4)
            })
        }
    }
    
    func changeLoginPassword(gaCode: String) {
        
        params["gaCode"] = gaCode
        
        if isSet == true {
            networking.putLoginPassword(params: params, controller: self) { response in
                self.dismiss(animated: true, completion: {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "popChangeLoginPasswordView"), object: nil)
                })
            }
            return
        }
        
        networking.postLoginPassword(params: params, controller: self) { response in
            self.dismiss(animated: true, completion: {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "popChangeLoginPasswordView"), object: nil)
            })
        }
    }
    
    func changeTransactionPassword(gaCode: String) {
     
        params["gaCode"] = gaCode
        
        if isSet == true {
            networking.putTransactionPassword(params: params, controller: self) { response in
                KeychainWrapper.standard.remove(key: "transactionPassword")
                
                self.dismiss(animated: true, completion: {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "popChangeTransactionPasswordView"), object: nil)
                })
            }
            return
        }
        
        networking.postTransactionPassword(params: params, controller: self) { response in
            self.dismiss(animated: true, completion: {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "popChangeTransactionPasswordView"), object: nil)
            })
        }
    }
    
    func forgotPassword(otp: String) {
        
        params["otp"] = otp
        
        networking.putForgotPassword(params: params, controller: self) { user in
            
            Storage.save(key: Storage.Name.user, value: user)
            Storage.save(key: Storage.Name.token, value: user.accessToken)
            BaseSingleton.shared.user = user
            BaseSingleton.shared.token = user.accessToken!
            
            self.dismiss(animated: true, completion: {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "popForgotPasswordView"), object: nil)
            })
        }
    }
    
    func addBeneficiary(gaCode: String) {
        
        params["ga_code"] = gaCode
        
        networking.addBeneficiary(params: params, controller: self) { beneficiary in
            
            print("beneficiary:")
            print(beneficiary)
            
            self.dismiss(animated: true, completion: nil)
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "popAddBeneficiaryView"), object: nil)
        }
    }
    
    func deleteBeneficiary(gaCode: String) {
        
        params["gaCode"] = gaCode
        
        networking.deleteBeneficiary(params: params, controller: self) { response in
            
            print("delete bene: \(response)")
            
            self.dismiss(animated: true, completion: nil)
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadBeneficiaryTable"), object: nil)
        }
    }
    
    func cancelGA(gaCode: String) {
        
        params["gaCode"] = gaCode
        
        networking.cancelGoogleAuth(params: params, controller: self) { response in
            self.dismiss(animated: true, completion: {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadSecurityCenter"), object: nil)
            })
        }
    }
    
    func cancelSMS(otp: String) {
        
        params["phoneCode"] = BaseSingleton.shared.user.phoneCode ?? "+6011"
        params["mobile"] = BaseSingleton.shared.user.mobile ?? "17000917"
        params["otp"] = otp
        
        print("cancel sms otp: \(params)")
        
        networking.cancelSmsAuth(params: params, controller: self) { response in
            self.dismiss(animated: true, completion: {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadSecurityCenter"), object: nil)
            })
        }
    }
}
