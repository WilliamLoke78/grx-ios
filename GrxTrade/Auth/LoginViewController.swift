//
//  LoginViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 02/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController {
    
    lazy var emailView: TextFieldView = {
        let view = TextFieldView()
        view.label.text = "Your Email".localized()
        view.textField.keyboardType = .emailAddress
        view.backgroundColor = .background
        view.tag = 1
        view.isUserInteractionEnabled = true
//        view.textField.text = "ae@ae.ae"
        return view
    }()
    
    lazy var passwordView: TextFieldView = {
        let view = TextFieldView()
        view.label.text = "Your Password".localized()
        view.backgroundColor = .background
        view.label.text = "Password".localized()
        view.lastButton.isHidden = false
        view.lastButton.addTarget(self, action: #selector(togglePassword(_:)), for: .touchUpInside)
        view.textField.isSecureTextEntry = true
        view.tag = 2
        view.isUserInteractionEnabled = true
//        view.textField.text = "abc123"
        return view
    }()
    
    lazy var loginButton: PrimaryButton = {
        let button = PrimaryButton()
        button.titleLabel?.font = UIFont(name: bold, size: 18)
        button.setTitle("Log In".localized(), for: .normal)
        button.addTarget(self, action: #selector(tapLogin(_:)), for: .touchUpInside)
        return button
    }()
    
    lazy var separatorLine: UIView = {
        let view = UIView()
        view.backgroundColor = .grayColor
        return view
    }()
    
    lazy var orLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: bold, size: 14)
        label.text = "OR".localized()
        label.textColor = .textColor
        label.backgroundColor = .backgroundAlternative
        label.textAlignment = .center
        return label
    }()
    
    lazy var maxWalletLogin: PrimaryButton = {
        let button = PrimaryButton()
        button.titleLabel?.font = UIFont(name: bold, size: 18)
        button.setTitle("Login with MAX-Wallet".localized(), for: .normal)
        button.backgroundColor = .mxwGold
        button.addTarget(self, action: #selector(tapWallet(_:)), for: .touchUpInside)
        return button
    }()
    
    lazy var forgotPasswordLabel: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = UIFont(name: bold, size: 14)
        button.setTitle("Forgot Password?".localized(), for: .normal)
        button.setTitleColor(.primary, for: .normal)
        button.addTarget(self, action: #selector(tapForgotPassword(_:)), for: .touchUpInside)
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .backgroundAlternative
        
        emailView.addGestureRecognizer(setGesture())
        passwordView.addGestureRecognizer(setGesture())
        
        addSubviews()
        adjustUI()
        addConstraints()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(popLoginView), name:NSNotification.Name(rawValue: "popLoginView"), object: nil)
    }
    
    func setGesture() -> UITapGestureRecognizer {
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(textFieldFocus(_:)))
        
        return tap
    }
    
    @objc func textFieldFocus(_ sender: UITapGestureRecognizer) {
        
        switch sender.view?.tag {
        case 1:
            emailView.textField.becomeFirstResponder()
        case 2:
            passwordView.textField.becomeFirstResponder()
        default:
            return
        }
    }

}

extension LoginViewController {
    
    func addSubviews() {
        view.addSubview(emailView)
        view.addSubview(passwordView)
        view.addSubview(loginButton)
        view.addSubview(separatorLine)
        view.addSubview(orLabel)
        view.addSubview(maxWalletLogin)
        view.addSubview(forgotPasswordLabel)
    }
    
    func addConstraints() {
        emailView.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(13)
            make.left.equalToSuperview().offset(22)
            make.right.equalToSuperview().offset(-22)
            make.height.equalTo(54)
            make.width.equalToSuperview().offset(-44)
        })
        
        passwordView.snp.makeConstraints({ (make) in
            make.top.equalTo(emailView.snp.bottom).offset(10)
            make.left.equalToSuperview().offset(22)
            make.right.equalToSuperview().offset(-22)
            make.height.equalTo(54)
            make.width.equalToSuperview().offset(-44)
        })
        
        forgotPasswordLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(passwordView.snp.bottom).offset(14)
            make.left.equalTo(passwordView)
        })
        
        loginButton.snp.makeConstraints({ (make) in
            make.top.equalTo(forgotPasswordLabel.snp.bottom).offset(16)
            make.left.right.equalTo(emailView)
            make.height.equalTo(54)
        })
        
        separatorLine.snp.makeConstraints({ (make) in
            make.top.equalTo(loginButton.snp.bottom).offset(20)
            make.left.right.equalToSuperview()
            make.height.equalTo(0.3)
        })
        
        orLabel.snp.makeConstraints({ (make) in
            make.centerX.centerY.equalTo(separatorLine)
            make.width.equalTo(50)
        })
        
        maxWalletLogin.snp.makeConstraints({ (make) in
            make.top.equalTo(separatorLine.snp.bottom).offset(20)
            make.left.right.equalTo(emailView)
            make.height.equalTo(54)
        })
    }
    
    func adjustUI() {
        title = "Log In".localized()
        view.backgroundColor = .backgroundAlternative
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel".localized(), style: .plain, target: self, action: #selector(tapCancel(_:)))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Register".localized(), style: .plain, target: self, action: #selector(tapRegister(_:)))
    }
}

extension LoginViewController {
    
    @objc private func tapCancel(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc private func tapRegister(_ sender: UIBarButtonItem) {
        let vc = SignupViewController()
        show(vc, sender: nil)
    }
    
    @objc private func tapLogin(_ sender: UIButton) {
        
        var params: [String: String] = [:]
        params["username"] = emailView.textField.text
        params["password"] = passwordView.textField.text?.md5()
        
        guard (emailView.textField.text != "") else {
            showSimpleToast("Email cannot be empty".localized())
            return
        }

        guard emailView.textField.text!.isValidEmail() else {
            showSimpleToast("Invalid email address".localized())
            return
        }

        guard passwordView.textField.text != "" else {
            showSimpleToast("Password cannot be empty".localized())
            return
        }
        
        let networking = Networking()
        networking.postLogin(params: params, controller: self) { user in
            
            Storage.save(key: Storage.Name.user, value: user)
            Storage.save(key: Storage.Name.token, value: user.accessToken)
            
            print("access token")
            print(user.accessToken)
            
            BaseSingleton.shared.user = user
            BaseSingleton.shared.token = user.accessToken!
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc private func tapWallet(_ sender: UIButton) {
        getPackage()
    }
    
    @objc private func togglePassword(_ sender: UIButton) {
        passwordView.textField.isSecureTextEntry.toggle()
        if passwordView.textField.isSecureTextEntry == true {
            passwordView.lastButton.setImage(UIImage(named: "invisible"), for: .normal)
        } else {
            passwordView.lastButton.setImage(UIImage(named: "visible"), for: .normal)
        }
    }
    
    @objc private func tapForgotPassword(_ sender: UIButton) {
        
        let vc = ForgotPasswordViewController()
        self.show(vc, sender: nil)
    }
    
    @objc func popLoginView() {
        dismiss(animated: true, completion: nil)
    }
    
    func getPackage() {
        
        var params: [String: String] = [:]
        params["type"] = "MXW_WALLET_LOGIN"
        
        let networking = Networking()
        networking.getPackageInfo(params: params, controller: self) { response in
            
            let base64 = "\(response)".toBase64()
            
            let host = "com.mxw.wallet://maxRegister?scheme=com.grxtrade.app&type=login"
            var data = "&data=\(base64)"
            data = data.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
            
            guard let openMxwURL = URL(string: host+data) else {
                print("open MXW URL failed")
                return //be safe
            }
            
            if UIApplication.shared.canOpenURL(openMxwURL) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(openMxwURL, options: [:], completionHandler: { (success) in
                        print("Successfully launched MaxWallet")
                    })
                } else {
                    UIApplication.shared.openURL(openMxwURL)
                    print("Successfully launched MaxWallet")
                }
            } else {
                guard let url = URL(string: networking.mxwHost) else {
                    return
                }
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    
    func postMXWLogin(data: Any) {
        
        var params: [String: Any] = [:]
        params["data"] = "\(data)".fromBase64()
        
        print("post mxw login")
        print(params)
        
        let networking = Networking()
        networking.postMxwLogin(params: params, controller: self) { user in
            
            Storage.save(key: Storage.Name.user, value: user)
            Storage.save(key: Storage.Name.token, value: user.accessToken)
            BaseSingleton.shared.user = user
            BaseSingleton.shared.token = user.accessToken!
            
            print("user: \(user)")
            
            restartApplication(pageIndex: 4)
        }
    }
}
