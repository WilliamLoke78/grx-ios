//
//  ForgotPasswordViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 24/12/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import Presentr

class ForgotPasswordViewController: UIViewController {

    lazy var emailView: TextFieldView = {
        let view = TextFieldView()
        view.label.text = "Your Email".localized()
        view.textField.keyboardType = .emailAddress
        view.textField.autocorrectionType = .no
        let button = UIButton()
        button.setTitle("Send code".localized(), for: .normal)
        button.titleLabel?.font = UIFont(name: bold, size: 12)
        button.setTitleColor(.primary, for: .normal)
        button.addTarget(self, action: #selector(tapSendCode(_:)), for: .touchUpInside)
        button.backgroundColor = UIColor.primary.withAlphaComponent(0.2)
        button.layer.cornerRadius = 4
        button.layer.borderColor = UIColor.primary.cgColor
        button.layer.borderWidth = 1
        view.addSubview(button)
        
        button.snp.makeConstraints({ (make) in
            make.right.equalToSuperview().offset(-10)
            make.centerY.equalToSuperview()
            make.width.equalTo(84)
        })
        return view
    }()
    
    lazy var passwordView: TextFieldView = {
        let view = TextFieldView()
        view.label.text = "New Password".localized()
        view.textField.isSecureTextEntry = true
        view.lastButton.isHidden = false
        view.lastButton.addTarget(self, action: #selector(togglePassword(_:)), for: .touchUpInside)
        view.isHidden = true
        return view
    }()
    
    lazy var retypePasswordView: TextFieldView = {
        let view = TextFieldView()
        view.label.text = "Re-type Password".localized()
        view.textField.isSecureTextEntry = true
        view.lastButton.isHidden = false
        view.lastButton.addTarget(self, action: #selector(toggleRetypePassword(_:)), for: .touchUpInside)
        view.isHidden = true
        return view
    }()
    
    lazy var sendButton: PrimaryButton = {
        let button = PrimaryButton()
        button.setTitle("Reset".localized(), for: .normal)
        button.addTarget(self, action: #selector(tapSend(_:)), for: .touchUpInside)
        button.isHidden = true
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addSubviews()
        adjustUI()
        addConstraints()
        
        NotificationCenter.default.addObserver(self, selector: #selector(popForgotPasswordView), name:NSNotification.Name(rawValue: "popForgotPasswordView"), object: nil)
    }

}

// MARK: User Interface
extension ForgotPasswordViewController {
    
    func addSubviews() {
        view.addSubview(emailView)
        view.addSubview(passwordView)
        view.addSubview(retypePasswordView)
        view.addSubview(sendButton)
    }
    
    func adjustUI() {
        self.title = "Forgot Password".localized().uppercased()
        view.backgroundColor = .backgroundAlternative
    }
    
    func addConstraints() {
        emailView.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(13)
            make.left.equalToSuperview().offset(22)
            make.right.equalToSuperview().offset(-22)
            make.height.equalTo(54)
        })
        
        passwordView.snp.makeConstraints({ (make) in
            make.top.equalTo(emailView.snp.bottom).offset(16)
            make.left.right.equalTo(emailView)
            make.height.equalTo(54)
        })
        
        retypePasswordView.snp.makeConstraints({ (make) in
            make.top.equalTo(passwordView.snp.bottom).offset(16)
            make.left.right.equalTo(emailView)
            make.height.equalTo(54)
        })
        
        sendButton.snp.makeConstraints({ (make) in
            make.top.equalTo(retypePasswordView.snp.bottom).offset(16)
            make.left.right.equalTo(emailView)
            make.height.equalTo(54)
        })
    }
    
    @objc func tapSendCode(_ sender: UIButton) {
        postSendCode()
    }
    
    @objc private func tapSend(_ sender: UIButton) {
        resetPassword()
    }
}

// MARK: Networking
extension ForgotPasswordViewController {
    
    func postSendCode() {
        
        var params: [String: String] = [:]
        params["email"] = emailView.textField.text
    
        let networking = Networking()
        networking.postForgotPassword(params: params, controller: self) { response in
            
            print("response: \(response)")
            
            self.passwordView.isHidden = false
            self.retypePasswordView.isHidden = false
            self.sendButton.isHidden = false
        }
    }
    
    func resetPassword() {
        
        if passwordView.textField.text!.md5() != retypePasswordView.textField.text!.md5() {
            showSimpleToast("Password does not match".localized())
            return
        }
        
        if !isValidPassword(passwordView.textField.text!) {
            showSimpleToast("Password must contain at least 6 alphanumerical characters with uppercase and lowercase".localized())
            return
        }
        
        var params: [String: String] = [:]
        params["username"] = emailView.textField.text
        params["password"] = passwordView.textField.text!.md5()
        
        let vc = OneTimePasswordViewController()
        vc.type = "forgotPassword"
        vc.params = params
        let presenter = Presentr(presentationType: .custom(width: .full, height: .custom(size: 680), center: .bottomCenter))
        self.customPresentViewController(presenter, viewController: vc, animated: true, completion: nil)
    }
}

// MARK: Action
extension ForgotPasswordViewController {
    
    @objc private func togglePassword(_ sender: UIButton) {
        passwordView.textField.isSecureTextEntry.toggle()
        retypePasswordView.textField.isSecureTextEntry.toggle()
        if passwordView.textField.isSecureTextEntry == true {
            passwordView.lastButton.setImage(UIImage(named: "invisible"), for: .normal)
            retypePasswordView.lastButton.setImage(UIImage(named: "invisible"), for: .normal)
        } else {
            passwordView.lastButton.setImage(UIImage(named: "visible"), for: .normal)
             retypePasswordView.lastButton.setImage(UIImage(named: "visible"), for: .normal)
        }
    }
    
    @objc private func toggleRetypePassword(_ sender: UIButton) {
        passwordView.textField.isSecureTextEntry.toggle()
        retypePasswordView.textField.isSecureTextEntry.toggle()
        if retypePasswordView.textField.isSecureTextEntry == true {
            passwordView.lastButton.setImage(UIImage(named: "invisible"), for: .normal)
            retypePasswordView.lastButton.setImage(UIImage(named: "invisible"), for: .normal)
        } else {
            passwordView.lastButton.setImage(UIImage(named: "visible"), for: .normal)
            retypePasswordView.lastButton.setImage(UIImage(named: "visible"), for: .normal)
        }
    }
    
    @objc private func popForgotPasswordView() {
        showSimpleToast("Password has been reset".localized())
        navigationController?.popViewController(animated: true)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "popLoginView"), object: nil)
    }
}
