////
////  AYKLineLatestPriceIndicator.swift
////  Growdex
////
////  Created by YUXI on 2019/11/7.
////  Copyright © 2019 YUXI. All rights reserved.
////
//
//import UIKit
//
//class AYKLineLatestPriceIndicator: UIView {
//    
////    var dashColor = UIColor.KLine_Text {
////        didSet {
////            self.setNeedsDisplay()
////        }
////    }
//    var dashColor = UIColor.textColor {
//        didSet {
//            self.setNeedsDisplay()
//        }
//    }
//    var lineWidth: CGFloat = 1 {
//        didSet {
//            self.setNeedsDisplay()
//        }
//    }
//    var lineDashPattern = [NSNumber(value: 5), NSNumber(value: 2)] {
//        didSet {
//            self.setNeedsDisplay()
//        }
//    }
//    var frameHeight: CGFloat = 24
//    
//    lazy var btn: UIButton = {
//        let btn = UIButton()
////        btn.backgroundColor = UIColor.KLine_Main_BG
//        btn.setTitle("0.0000 >", for: .normal)
//        btn.setTitleColor(dashColor, for: .normal)
//        btn.titleLabel?.font = UIFont.systemFont(ofSize: 10)
//        btn.layer.cornerRadius = frameHeight/2
//        btn.layer.borderWidth = 1
//        btn.layer.borderColor = dashColor.cgColor
//        
//        return btn
//    }()
//
//    override func draw(_ rect: CGRect) {
//        super.draw(rect)
//        
//        //绘画虚线
//        let shapeLayer = CAShapeLayer()
//        shapeLayer.bounds = self.bounds
//        shapeLayer.frame = self.bounds
//        shapeLayer.strokeColor = dashColor.cgColor
//        shapeLayer.lineWidth = lineWidth
//        shapeLayer.lineDashPattern = lineDashPattern
//        
//        //路径
//        let path = CGMutablePath()
//        let p1 = CGPoint.init(x: 0, y: (bounds.height-lineWidth)/2)
//        let p2 = CGPoint.init(x: bounds.width, y: p1.y)
//        path.move(to: p1)
//        path.addLines(between: [p1, p2])
//        
//        //设置路径
//        shapeLayer.path = path
//        
//        //添加到self.layer
//        self.layer.addSublayer(shapeLayer)
//        
//        self.addSubview(btn)
//        var btnWidth = (btn.currentTitle?.ch_sizeWithConstrained(btn.titleLabel!.font!).width ?? 0) + 12
//        btnWidth = max(btnWidth, 64)
//        btn.frame = CGRect.init(x: bounds.width-70-btnWidth, y: 0, width: btnWidth, height: bounds.height)
//    }
//    
//    func reDraw(_ frame: CGRect, latestPrice: String) {
//        self.btn.setTitle(latestPrice+" ▷", for: .normal)
//        UIView.animate(withDuration: 0.1) {
//            self.frame = frame
//            self.layoutIfNeeded()
//        }
//        
//        self.setNeedsDisplay()
//    }
//
//}
