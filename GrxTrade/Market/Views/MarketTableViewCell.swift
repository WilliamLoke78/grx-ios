//
//  MarketTableViewCell.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 26/12/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

private let marketCellHeight: CGFloat = 60

class MarketTableViewCell: BaseTableViewCell {
    
    lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .container
        return view
    }()

    lazy var currencyPairLabel: UILabel = {
        let label = UILabel()
        label.text = "BTC/USDT"
        label.textColor = .grayColor
        return label
    }()
    
    lazy var volumeLabel: UILabel = {
        let label = UILabel()
        label.text = "24H Vol 332391"
        label.font = UIFont(name: regular, size: 11)
        label.adjustsFontSizeToFitWidth = true
        label.textColor = .grayColor
        return label
    }()
    
    lazy var priceLabel: UILabel = {
        let label = UILabel()
        label.text = "9380.23"
        label.font = UIFont(name: bold, size: 16)
        label.adjustsFontSizeToFitWidth = true
        label.textColor = .textColor
        return label
    }()
    
    lazy var convertedPriceLabel: UILabel = {
        let label = UILabel()
        label.text = "117.34"
        label.font = UIFont(name: regular, size: 11)
        label.textColor = .grayColor
        return label
    }()
    
    lazy var percentageLabel: UIButton = {
        let button = UIButton()
        button.backgroundColor = .greenColor
        button.layer.cornerRadius = 2
        button.titleLabel!.font = UIFont(name: bold, size: 14)
        button.titleLabel!.adjustsFontSizeToFitWidth = true
        button.setTitle("+8.00%", for: .normal)
        return button
    }()
    
    lazy var bottomLine: UIView = {
        let view = UIView()
        view.backgroundColor = .background
        return view
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .background
        
        addSubview(containerView)
        containerView.addSubview(currencyPairLabel)
        containerView.addSubview(volumeLabel)
        containerView.addSubview(priceLabel)
        containerView.addSubview(convertedPriceLabel)
        containerView.addSubview(percentageLabel)
        containerView.addSubview(bottomLine)
        
        let columnWidth = (screenWidth-22)/3
        
        containerView.snp.makeConstraints({ (make) in
            make.edges.equalToSuperview()
        })
        
        currencyPairLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(percentageLabel.snp.top)
            make.left.equalToSuperview().offset(11)
            make.width.equalTo(columnWidth)
        })
        
        volumeLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(currencyPairLabel.snp.bottom)
            make.left.equalTo(currencyPairLabel.snp.left)
            make.width.equalTo((screenWidth/3)-22)
        })
        
        priceLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(currencyPairLabel)
            make.left.equalTo(currencyPairLabel.snp.right)
            make.width.equalTo(columnWidth)
        })
        
        convertedPriceLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(volumeLabel)
            make.left.equalTo(priceLabel.snp.left)
        })
        
        percentageLabel.snp.makeConstraints({ (make) in
            make.height.equalTo(40)
            make.width.equalTo(100)
            make.right.equalToSuperview().offset(-11)
            make.centerY.equalToSuperview()
        })
        
        bottomLine.snp.makeConstraints({ (make) in
            make.height.equalTo(1)
            make.width.equalToSuperview()
            make.bottom.equalToSuperview()
        })
    }

    func configure(_ market: MarketProtocol) {
        
        let currencies = BaseSingleton.shared.currencies
        let selectedCurrency = currencies?.first(where: {$0.abbreviation == CurrencyManager.userCurrency()})
        let marketUsd = market.usd!.toDouble()
        let convertedPrice = marketUsd * (selectedCurrency?.rate ?? 0.0)
        let convertedPriceText = (convertedPrice == 0.0) ? "" : "\(convertedPrice)".forceDecimalDigits(2)
        
        let marketSymbol = "\(market.coin!)/\(market.marketCoin!)"
        currencyPairLabel.text = marketSymbol
        currencyPairLabel.pairBeautifier(.textColor)
        priceLabel.text = market.close != "" ? market.close?.forceDecimalDigits(market.priceDec!) : "--"
        convertedPriceLabel.text = market.close != "" ? "\(selectedCurrency?.symbol ?? "") \(convertedPriceText)" : ""
        volumeLabel.text = "24H Vol".localized() + " \(market.volume!)"
        if market.change!.contains("-") {
            percentageLabel.setTitle(market.change != "" ? (market.change?.forceDecimalDigits(2))! + "%" : "--", for: .normal)
            percentageLabel.backgroundColor = .redColor
        } else {
            percentageLabel.setTitle(market.change != "" ? "+" + (market.change?.forceDecimalDigits(2))! + "%" : "--", for: .normal)
            percentageLabel.backgroundColor = .greenColor
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
