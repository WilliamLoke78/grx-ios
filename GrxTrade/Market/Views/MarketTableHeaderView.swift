//
//  MarketTableHeaderView.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 26/12/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

class MarketTableHeaderView: BaseView {
    
    lazy var tableHeaderLabel: UILabel = {
        let label = UILabel()
        label.text = "Popular Trades".localized()
        label.font = UIFont(name: bold, size: 16)
        label.textColor = .textColor
        return label
    }()

    lazy var nameTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Coin Name".localized()
        label.font = UIFont(name: regular, size: 11)
        label.textColor = .grayColor
        return label
    }()
    
    lazy var priceTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Coin Price".localized()
        label.font = UIFont(name: regular, size: 11)
        label.textColor = .grayColor
        return label
    }()
    
    lazy var changeTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Change %".localized()
        label.font = UIFont(name: regular, size: 11)
        label.textAlignment = .right
        label.textColor = .grayColor
        return label
    }()
    
    lazy var bottomLine: UIView = {
        let view = UIView()
        view.backgroundColor = .background
        return view
    }()
    
    override func setupViews() {
        
        addSubview(tableHeaderLabel)
        addSubview(nameTitleLabel)
        addSubview(priceTitleLabel)
        addSubview(changeTitleLabel)
        addSubview(bottomLine)
        
        tableHeaderLabel.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(14)
            make.left.equalToSuperview().offset(11)
            make.right.equalToSuperview().offset(-11)
        })
        
        let columnWidth = (screenWidth-22)/3
        
        nameTitleLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(tableHeaderLabel.snp.bottom).offset(10)
            make.left.equalToSuperview().offset(11)
            make.width.equalTo(columnWidth)
        })
        
        priceTitleLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(tableHeaderLabel.snp.bottom).offset(10)
            make.left.equalTo(nameTitleLabel.snp.right)
            make.width.equalTo(columnWidth)
        })
        
        changeTitleLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(tableHeaderLabel.snp.bottom).offset(10)
            make.right.equalToSuperview().offset(-11)
            make.width.equalTo(columnWidth)
        })
        
        bottomLine.snp.makeConstraints({ (make) in
            make.bottom.equalToSuperview()
            make.height.equalTo(1)
            make.width.equalToSuperview()
        })
    }

}
