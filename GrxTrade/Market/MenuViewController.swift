//
//  MenuViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 01/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit
import SideMenu
import WMPageController

class MenuViewController: BaseViewController {
    
    var marketGroup: [String]?
    
    private var pageList : [DCWMPage] = []
    
    lazy var pageVc: WMPageController = {
        let page = WMPageController()
        page.delegate = self
        page.dataSource = self
        page.menuViewStyle = .line
        page.titleSizeNormal = 14
        page.automaticallyCalculatesItemWidths = true
        page.titleSizeSelected = 14
        page.itemMargin = 20
        page.titleFontName = bold
        page.titleColorSelected = .primary
        page.titleColorNormal = .grayColor
        page.menuViewLayoutMode = .left
        return page
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addSubviews()
        adjustUI()
        addConstraints()
        
        getMarketGroup()
    }
}

// MARK: UI
extension MenuViewController {
    func addSubviews() {
        self.view.addSubview(pageVc.view)
    }
    
    func adjustUI() {
        navigationController?.navigationBar.barTintColor = .background
        navigationController?.navigationBar.isTranslucent = false
        view.subviews[0].backgroundColor = .background
    }
    
    func addConstraints() {
        pageVc.view.snp.makeConstraints { (make) in
             if #available(iOS 11.0, *) {
                 //Bottom guide
                 make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottomMargin)
                 //Top guide
                 make.top.equalTo(view.safeAreaLayoutGuide.snp.topMargin)
                 //Leading guide
                 make.leading.equalTo(view.safeAreaLayoutGuide.snp.leadingMargin)
                 //Trailing guide
               make.trailing.equalTo(self.view.frame.size.width * 2 / 3)

              } else {
                 make.edges.equalToSuperview()
                 make.trailing.equalTo(self.view.frame.size.width * 2 / 3)
              }
         }
    }
}

// MARK: WM
extension MenuViewController: WMPageControllerDataSource, WMPageControllerDelegate {
    
    func numbersOfChildControllers(in pageController: WMPageController) -> Int {
        return pageList.count
    }
    
    func pageController(_ pageController: WMPageController, titleAt index: Int) -> String {
        return pageList[index].title
    }
    
    func pageController(_ pageController: WMPageController, viewControllerAt index: Int) -> UIViewController {
        return pageList[index].controller
    }
    
    func pageController(_ pageController: WMPageController, preferredFrameForContentView contentView: WMScrollView) -> CGRect {
        var top: CGFloat = 116
        let menuHeight: CGFloat = 44
        var bottom: CGFloat = 49
        
        if isSafeAreaDevice {
            top = top + 44
            bottom = bottom+34
        } else {
            top = top + 20
        }
        
//        let height = screenHeight - top - bottom - menuHeight
        let height = screenHeight - menuHeight
        return CGRect(x: 0, y: 44, width: screenWidth * 2 / 3, height: height)
    }
    
    
    func pageController(_ pageController: WMPageController, preferredFrameFor menuView: WMMenuView) -> CGRect {
//        menuView.backgroundColor = .background
        return CGRect(x: 0, y: 0, width: screenWidth * 2 / 3, height: 44)
    }
}

// MARK: Networking
extension MenuViewController {
    
    func getMarketGroup() {
        let networking = Networking()
        networking.getMarketGroup(params: [:], controller: self) { response in
            
            let list: [String] = response
            
            self.pageList = list.compactMap{
                let vc = MarketListViewController()
                vc.isSimpleCell = true
                vc.marketCoin = $0
                return DCWMPage(title: $0, controller: vc)
            }
            
            self.pageVc.reloadData()
        }
    }
}
