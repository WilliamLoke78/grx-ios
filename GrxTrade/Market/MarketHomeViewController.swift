//
//  MarketHomeViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 14/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import UIKit

private let marketCellId = "marketCellId"

class MarketHomeViewController: BaseViewController {
    
    fileprivate lazy var searchController = UISearchController(searchResultsController: nil)
    
    lazy var searchBarView: SearchBarView = {
        let sbv = SearchBarView()
        return sbv
    }()
    
    lazy var contentView: UIView = {
        let cv = UIView()
        return cv
    }()
    
    lazy var marketView: MarketViewController = {
        let mv = MarketViewController()
        return mv
    }()
    
    lazy var tableView: UITableView = {
        let tv = UITableView()
        tv.backgroundColor = .background
        tv.isHidden = true
        return tv
    }()
    
    var markets: [Market] = []
    var filteredMarkets: [Market] = []
    var displayMarkets: [Market] = []
    
    override func viewDidLoad() {
        
        addSubviews()
        adjustUI()
        addConstraints()
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        
        searchBarView.addSubview(searchController.searchBar)
        configureSearchController()
        
        tableView.register(MarketTableViewCell.self, forCellReuseIdentifier: marketCellId)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        getAllMarketData()
    }
}

extension MarketHomeViewController {
    
    func addSubviews() {
        view.addSubview(searchBarView)
        view.addSubview(contentView)
        
        view.addSubview(tableView)
        
        contentView.addSubview(marketView.view)
        self.addChild(marketView)
        marketView.view.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    func adjustUI() {
        title = "Markets".localized()
    }
    
    func addConstraints() {
        
        searchBarView.snp.makeConstraints({ (make) in
            make.top.left.right.equalToSuperview()
            make.height.equalTo(50)
        })
        
        contentView.snp.makeConstraints({ (make) in
            make.top.equalTo(searchBarView.snp.bottom)
            make.left.right.bottom.equalToSuperview()
        })
        
        tableView.snp.makeConstraints({ (make) in
            make.top.equalTo(searchBarView.snp.bottom).offset(50)
            make.left.right.bottom.equalToSuperview()
        })
    }
}

// MARK: Search Configuration
extension MarketHomeViewController {
    
    func configureSearchController() {
        searchController.searchResultsUpdater = self
//        searchController.dimsBackgroundDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = true
        definesPresentationContext = true
        searchController.searchBar.delegate = self
        searchController.searchBar.backgroundColor = .container
        searchController.searchBar.searchTextField.backgroundColor = .background
        searchController.searchBar.searchTextField.textColor = .textColor
        searchController.searchBar.barTintColor = .container
        searchController.searchBar.placeholder = "Search".localized()
    }
    
    func filterSearchController(_ searchBar: UISearchBar) {
        
        let searchText = searchBar.text ?? ""
        
        filteredMarkets.removeAll()
        
        filteredMarkets = markets.filter { market in
            let isMatchingSearchText = market.coin!.lowercased().contains(searchText.lowercased())
            return isMatchingSearchText
        }
        
        displayMarkets = filteredMarkets
        
        tableView.reloadData()
        
        if searchText == "" {
            filteredMarkets = markets
            
            tableView.isHidden = true
            return
        }
        tableView.isHidden = false
    }
}


extension MarketHomeViewController : UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        filterSearchController(searchController.searchBar)
    }
}

extension MarketHomeViewController : UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        filterSearchController(searchBar)
    }
}

extension MarketHomeViewController: UITableViewDataSource, UITableViewDelegate {
 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredMarkets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: marketCellId, for: indexPath) as! MarketTableViewCell
        
        cell.configure(filteredMarkets[indexPath.item])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        if let tabBar = appDelegate!.window!.rootViewController as? UITabBarController {
            
            let secondTab = tabBar.viewControllers![2].children[0] as! TradeViewController
            secondTab.market = self.filteredMarkets[indexPath.row]
            tabBar.selectedIndex = 2
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

extension MarketHomeViewController {
    
    func getAllMarketData() {
        let networking = Networking()
        networking.getMarketList(params: [:], controller: self) { markets in
            self.markets = markets
        }
    }
}
