//
//  MarketListViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 31/12/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit
import SwiftyJSON
import SocketIO

private let marketCellId = "marketCellId"

class MarketListViewController: BaseTableViewController {

    var markets: [Market] = []
    var marketCoin: String?
    var isSimpleCell: Bool = false
    
    var manager: SocketManager!
    var socket: SocketIOClient!
    
    var socketSingleton = Socket.shared
    
    lazy var nameTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Coin Name".localized()
        label.font = UIFont(name: regular, size: 11)
        label.textColor = .grayColor
        return label
    }()
    
    lazy var priceTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Coin Price".localized()
        label.font = UIFont(name: regular, size: 11)
        label.textColor = .grayColor
        return label
    }()
    
    lazy var changeTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Change %".localized()
        label.font = UIFont(name: regular, size: 11)
        label.textAlignment = .right
        label.textColor = .grayColor
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        adjustUI()
        
        tableView.dataSource = self
        tableView.delegate = self
//        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        tableView.sectionHeaderHeight = 35
        
        if isSimpleCell == true {
            tableView.register(SimpleMarketTableViewCell.self, forCellReuseIdentifier: marketCellId)
        } else {
            tableView.register(MarketTableViewCell.self, forCellReuseIdentifier: marketCellId)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getMarkets(marketCoin: marketCoin!)
        socketSingleton.connectToSocket(token: "")
        connectToSocket()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        disconnectSocket()
    }
    
    func getMarkets(marketCoin: String) {
        
        var params: [String: String] = [:]
        params["marketCoin"] = marketCoin
        
        let networking = Networking()
        networking.getMarketList(params: params, controller: self) { markets in
            
            var activeMarkets: [Market] = []
            for elem in markets {
                if elem.status != 0 {
                    activeMarkets.append(elem)
                }
            }
            
            self.markets = activeMarkets            
            self.tableView.reloadData()
        }
    }
    
    func connectToSocket() {
    
        manager = socketSingleton.manager
        socket = manager.defaultSocket

        self.socket.on(clientEvent: .connect) {data, ack in
            print(data)
            print("message socket connected")

            self.socket.on("marketList") { (data, eck) in
                print("market list socket data: \(data)")

                let result = JSON(data.first!)
                let market = Market.init(aJSON: result["data"])

                if let index = self.markets.firstIndex(where: {$0.symbol == market.symbol}) {
                    
                    if market.status == 0 {
                        self.markets.remove(at: index)
                    } else {
                        self.markets[index] = market
                    }
                    
//                    self.markets[index] = market
                    self.tableView.reloadData()
                } else if market.marketCoin == self.marketCoin {
                    
                    print("join market")
                    print(market)
                    
                    self.markets.append(market)
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    func disconnectSocket() {
        
        manager = socketSingleton.manager
        socket = manager.defaultSocket
        
        socket.on(clientEvent: .disconnect) { (data, eck) in
            print(data)
            print("socket disconnect")
        }
    }

}

// MARK: UI
extension MarketListViewController {
    
    func adjustUI() {
        view.backgroundColor = .background
    }
}

// MARK: Table
extension MarketListViewController {
 
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return markets.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isSimpleCell == true {
            let cell = tableView.dequeueReusableCell(withIdentifier: marketCellId, for: indexPath) as! SimpleMarketTableViewCell
            cell.configure(markets[indexPath.item])
            return cell
        }
            
        let cell = tableView.dequeueReusableCell(withIdentifier: marketCellId, for: indexPath) as! MarketTableViewCell
        cell.configure(markets[indexPath.item])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        restartApplication(pageIndex: 2)
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        if let tabBar = appDelegate!.window!.rootViewController as? UITabBarController {
            
            let secondTab = tabBar.viewControllers![2].children[0] as! TradeViewController
            secondTab.market = self.markets[indexPath.row]
            tabBar.selectedIndex = 2
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        headerView.backgroundColor = .background
        
        headerView.addSubview(nameTitleLabel)
        headerView.addSubview(priceTitleLabel)
        headerView.addSubview(changeTitleLabel)
        
        let columnWidth = (view.superview!.width-22)/3
        
        nameTitleLabel.snp.makeConstraints({ (make) in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(11)
            make.width.equalTo(columnWidth)
        })
        
        priceTitleLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(nameTitleLabel)
            make.left.equalTo(nameTitleLabel.snp.right).offset(5)
        })
        
        changeTitleLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(nameTitleLabel)
            make.right.equalToSuperview().offset(-11)
        })

        return headerView
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
