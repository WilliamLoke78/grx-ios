//
//  MarketViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 01/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit
import SideMenu
import WMPageController

struct DCWMPage {
    var title: String
    var controller: UIViewController
}

class MarketViewController: BaseViewController {

    var marketGroup: [String]?
    var list: [String]?
    
    private var pageList : [DCWMPage] = []
    
    lazy var pageVc: WMPageController = {
        let page = WMPageController()
        page.delegate = self
        page.dataSource = self
        page.menuViewStyle = .line
        page.titleSizeNormal = 14
        page.automaticallyCalculatesItemWidths = true
        page.titleSizeSelected = 14
        page.titleFontName = bold
        page.titleColorSelected = .primary
        page.titleColorNormal = .grayColor
        page.itemMargin = 20
        page.menuViewLayoutMode = .left
        return page
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getMarketGroup()
        
        self.view.addSubview(pageVc.view)
        self.addChild(pageVc)
        pageVc.view.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        view.subviews[0].backgroundColor = .background
    }
    
    func generate() {
        pageList = list!.compactMap{
            let vc = MarketListViewController()
            vc.marketCoin = $0
            return DCWMPage(title: $0, controller: vc)
        }
        
        pageVc.reloadData()
    }
}

// MARK: WM
extension MarketViewController: WMPageControllerDataSource, WMPageControllerDelegate {
    
    func numbersOfChildControllers(in pageController: WMPageController) -> Int {
        return pageList.count
    }
    
    func pageController(_ pageController: WMPageController, titleAt index: Int) -> String {
        return pageList[index].title
    }
    
    func pageController(_ pageController: WMPageController, viewControllerAt index: Int) -> UIViewController {
        return pageList[index].controller
    }
    
    func pageController(_ pageController: WMPageController, preferredFrameForContentView contentView: WMScrollView) -> CGRect {
        var top: CGFloat = 116
        let menuHeight: CGFloat = 44
        var bottom: CGFloat = 49
        
        if isSafeAreaDevice {
//            top = top + 44
            top = top + 24
            bottom = bottom+34
        } else {
//            top = top + 20
            top = top + 0 - 2
        }
        
        let height = screenHeight - top - bottom - menuHeight
        return CGRect(x: 0, y: 44, width: screenWidth, height: height)
    }
    
    func pageController(_ pageController: WMPageController, preferredFrameFor menuView: WMMenuView) -> CGRect {
        menuView.backgroundColor = .backgroundAlternative
        return CGRect(x: 0, y: 0, width: screenWidth, height: 44)
    }
}

// MARK: Networking
extension MarketViewController {
    
    func getMarketGroup() {
        let networking = Networking()
        networking.getMarketGroup(params: [:], controller: self) { response in
            
            let list: [String] = response
            
            self.list = list
            self.generate()
        }
    }
}
