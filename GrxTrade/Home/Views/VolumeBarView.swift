//
//  VolumeBarView.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 23/12/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

class VolumeBarView: BaseView {

    lazy var imageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: darkTheme ? "loudspeaker_dark" : "loudspeaker_light")
        return iv
    }()
    
    lazy var label: UILabel = {
        let label = UILabel()
        label.text = "Digital Asset".localized()
        label.font = UIFont(name: bold, size: 12)
        label.textColor = darkTheme ? .grayColor : .blackTextColor
        return label
    }()
    
    lazy var button: UIButton = {
        let button = UIButton()
        return button
    }()
    
    override func setupViews() {
        
        layer.cornerRadius = 2
        backgroundColor = .container
        
        self.addSubview(button)
        button.addSubview(imageView)
        button.addSubview(label)
        
        button.snp.makeConstraints({ (make) in
            make.edges.equalToSuperview()
        })
        
        imageView.snp.makeConstraints({ (make) in
            make.centerY.equalTo(self.snp.centerY)
            make.left.equalToSuperview().offset(11)
            make.height.equalTo(20)
            make.width.equalTo(20)
        })
        
        label.snp.makeConstraints({ (make) in
            make.centerY.equalTo(self.snp.centerY)
            make.left.equalTo(imageView.snp.right).offset(10)
        })
    }
}

