//
//  BalanceView.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 23/12/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit

class BalanceView: BaseView {

    let imageView: UIImageView = {
        let iv = UIImageView()
        iv.image = darkTheme ? UIImage(named: "balance_dark") : UIImage(named: "balance_light")
        return iv
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "My Balance".localized()
        label.textColor = darkTheme ? .grayColor : .primary
        label.font = UIFont(name: bold, size: 14)
        return label
    }()
    
    let subtitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Balance of Exchange Account".localized()
        label.textColor = .grayColor
        label.font = UIFont(name: regular, size: 11)
        return label
    }()
    
    let visibleButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "invisible"), for: .normal)
        return button
    }()
    
    let loginButton: UIButton = {
        let button = UIButton()
        button.setTitle("Log In".localized(), for: .normal)
        button.backgroundColor = .primary
        button.titleLabel?.font = UIFont(name: bold, size: 16)
        button.layer.cornerRadius = 4
        return button
    }()
    
    let balanceLabel: UILabel = {
        let label = UILabel()
        label.textColor = darkTheme ? .white : .primary
        label.font = UIFont(name: bold, size: 20)
        label.text = "****** USD"
//        label.labelStyling()
        return label
    }()
    
    override func setupViews() {
        
        backgroundColor = .container
        layer.cornerRadius = 4
        
        addSubview(imageView)
        addSubview(titleLabel)
        addSubview(subtitleLabel)
        addSubview(visibleButton)
        addSubview(loginButton)
        addSubview(balanceLabel)
        
        imageView.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(10)
            make.left.equalToSuperview().offset(10)
            make.height.equalTo(40)
            make.width.equalTo(40)
        })
        
        titleLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(imageView.snp.top)
            make.left.equalTo(imageView.snp.right).offset(10)
        })
        
        subtitleLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(titleLabel.snp.bottom)
            make.left.equalTo(titleLabel.snp.left)
        })
        
        visibleButton.snp.makeConstraints({ (make) in
            make.top.equalTo(imageView.snp.top)
            make.right.equalToSuperview().offset(-20)
            make.height.equalTo(16)
            make.width.equalTo(24)
        })
        
        loginButton.snp.makeConstraints({ (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(5)
            make.left.equalTo(titleLabel.snp.left)
            make.height.equalTo(44)
            make.width.equalTo(154)
        })
        
        balanceLabel.snp.makeConstraints({ (make) in
            make.left.equalTo(titleLabel.snp.left)
            make.bottom.equalToSuperview().offset(-11)
        })
    }
}

