//
//  NotificationTableViewCell.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 19/02/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import UIKit

class NotificationTableViewCell: BaseTableViewCell {
    
    lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .container
        return view
    }()
    
    lazy var iconImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "message")
        return iv
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .textColor
        label.numberOfLines = 2
        label.font = UIFont(name: regular, size: 13)
        return label
    }()
    
    lazy var subLabel: UILabel = {
        let label = UILabel()
        label.textColor = .textColor
        label.font = UIFont(name: regular, size: 12)
        return label
    }()
    
    lazy var dateLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: regular, size: 11)
        label.textColor = .grayColor
        return label
    }()
    
    lazy var arrowImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "arrow_right")
        return iv
    }()
    
    lazy var bottomLine: UIView = {
        let view = UIView()
        view.backgroundColor = .background
        return view
    }()
    
    lazy var unreadLabel: UILabel = {
        let label = UILabel(frame: CGRect(x: 20, y: 0, width: 10, height: 10))
        label.layer.borderColor = UIColor.white.cgColor
        label.layer.borderWidth = 2
        label.layer.cornerRadius = label.bounds.size.height / 2
        label.textAlignment = .center
        label.layer.masksToBounds = true
        label.font = UIFont(name: regular, size: 11)
        label.textColor = .white
        label.backgroundColor = .red
        label.isHidden = true
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
     
        backgroundColor = .background
        
        addSubview(containerView)
        containerView.snp.makeConstraints({ (make) in
            make.left.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-10)
            make.height.equalToSuperview()
        })
        
        containerView.addSubview(iconImageView)
        iconImageView.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(11)
            make.left.equalToSuperview().offset(11)
            make.height.equalTo(30)
            make.width.equalTo(30)
        })
        
        iconImageView.addSubview(unreadLabel)
        
        containerView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(iconImageView.snp.top)
            make.left.equalTo(iconImageView.snp.right).offset(10)
            make.right.equalToSuperview().offset(-40)
        })
        
        containerView.addSubview(subLabel)
        subLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(titleLabel.snp.bottom)
            make.left.equalTo(titleLabel)
            make.right.equalToSuperview().offset(-40)
        })
        
        containerView.addSubview(dateLabel)
        dateLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(subLabel.snp.bottom).offset(5)
            make.left.equalTo(titleLabel)
        })
        
        containerView.addSubview(arrowImageView)
        arrowImageView.snp.makeConstraints({ (make) in
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(-20)
            make.height.equalTo(40)
            make.width.equalTo(10)
        })
        
        containerView.addSubview(bottomLine)
        bottomLine.snp.makeConstraints({ (make) in
            make.height.equalTo(1)
            make.width.equalToSuperview()
            make.bottom.equalToSuperview()
        })
    }

    func configure(_ notification: NotificationMessage) {
        
        titleLabel.text = notification.title
        subLabel.text = notification.content
        dateLabel.text = notification.created?.serverToLocal()
        
        if notification.status == 0 {
            titleLabel.font = UIFont(name: bold, size: 13)
            unreadLabel.isHidden = false
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
