//
//  MessageViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 19/02/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import Foundation

class MessageViewController: BaseViewController {
    
    var message: NotificationMessage?
    
    lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .container
        return view
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .textColor
        label.numberOfLines = 8
        label.font = UIFont(name: bold, size: 14)
        return label
    }()
    
    lazy var dateLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: regular, size: 11)
        label.textColor = .grayColor
        return label
    }()
    
    lazy var contentTextView: UITextView = {
        let tv = UITextView()
        tv.textColor = .textColor
        tv.backgroundColor = .container
        tv.isEditable = false
        tv.font = UIFont(name: regular, size: 14)
        return tv
    }()
    
    lazy var scrollView: UIScrollView = {
        let v = UIScrollView()
        v.backgroundColor = .background
        v.translatesAutoresizingMaskIntoConstraints = false
        v.contentSize = CGSize(width: screenWidth, height: screenHeight)
        return v
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let trashItem = UIBarButtonItem(image: UIImage(named: "trash"), style: .plain, target: self, action: #selector(tapTrash(_:)))
        navigationItem.rightBarButtonItem = trashItem
        
        view.addSubview(scrollView)
        scrollView.snp.makeConstraints({ (make) in
            make.edges.equalToSuperview()
        })
        
        scrollView.addSubview(containerView)
        containerView.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(13)
            make.left.equalToSuperview().offset(11)
            make.right.equalToSuperview().offset(-11)
            make.bottom.equalToSuperview().offset(-13)
            make.height.equalToSuperview().offset(-26)
            make.width.equalTo(scrollView.snp.width).offset(-22)
        })
        
        containerView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(18)
            make.left.equalToSuperview().offset(11)
            make.right.equalToSuperview().offset(-11)
        })
        
        containerView.addSubview(dateLabel)
        dateLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(titleLabel.snp.bottom)
            make.left.equalTo(titleLabel)
        })
        
        containerView.addSubview(contentTextView)
        contentTextView.snp.makeConstraints({ (make) in
            make.top.equalTo(dateLabel.snp.bottom).offset(15)
            make.left.equalTo(titleLabel)
            make.right.equalTo(titleLabel)
            make.bottom.equalToSuperview().offset(-11)
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        getMessage()
        
        titleLabel.text = message?.title
        contentTextView.text = message?.content
        dateLabel.text = message?.created
    }
}

// MARK: Action
extension MessageViewController {
    
    @objc func tapTrash(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "Delete".localized(), message: "Are you sure you want to delete this notification?".localized(), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "No".localized(), style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Yes".localized(), style: .default, handler: { action in
            self.deleteMessage()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func getMessage() {
        
        var params: [String: Any] = [:]
        params["messageId"] = message?.id
        
        let networking = Networking()
        networking.getReadNotificationMessage(params: params, controller: self) { response in

        }
    }
    
    func deleteMessage() {
        
        var params: [String: Any] = [:]
        params["messageId"] = message?.id
        
        let networking = Networking()
        networking.deleteNotificationMessage(params: params, controller: self) { response in
            self.navigationBack()
        }
    }
}
