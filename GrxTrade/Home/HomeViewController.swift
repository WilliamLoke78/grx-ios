//
//  HomeViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 01/10/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import UIKit
import SocketIO
import SwiftyJSON

private let marketCellId = "marketCellId"

class HomeViewController: BaseTableViewController {

    var unreadCount: String?
    var coins: [Coin] = BaseSingleton.shared.coins ?? []
    var wallets: [Wallet] = []
    var markets = [Market]()
    var isVisible = false
    var walletIndex: Int = 0
    let networking = Networking()
    
    var manager: SocketManager!
    var socket: SocketIOClient!
    
    var socketSingleton = Socket.shared
    
    lazy var unreadCountLabel: UILabel = {
        let label = UILabel(frame: CGRect(x: 10, y: -10, width: 20, height: 20))
        label.layer.borderColor = UIColor.clear.cgColor
        label.layer.borderWidth = 2
        label.layer.cornerRadius = label.bounds.size.height / 2
        label.textAlignment = .center
        label.layer.masksToBounds = true
        label.font = UIFont(name: regular, size: 11)
        label.textColor = .white
        label.backgroundColor = .red
        label.isHidden = true
        return label
    }()
    
    private lazy var tableHeader: UIView = {
        let th = UIView.init(frame: CGRect.init(x: 0, y: 0, width: screenWidth, height: 405 - 70)) // 70 is for hidden duo buttons
        return th
    }()
    
    private lazy var backgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = .primary
        return view
    }()
    
    private lazy var volumeBarView: VolumeBarView = {
        let view = VolumeBarView()
        view.button.addTarget(self, action: #selector(tapMarket(_:)), for: .touchUpInside)
        return view
    }()
    
    private lazy var balanceView: BalanceView = {
        let bv = BalanceView()
        bv.visibleButton.addTarget(self, action: #selector(tapVisible(_:)), for: .touchUpInside)
        bv.loginButton.addTarget(self, action: #selector(tapLogin(_:)), for: .touchUpInside)
        return bv
    }()
    
    private lazy var referFriendView: BannerView = {
        let bv = BannerView()
        bv.dropShadow()
        bv.imageView.image = UIImage(named: "refer_friend")
        bv.titleLabel.text = "Refer a Friend".localized()
        bv.subtitleLabel.text = "Gain additional benefit.".localized()
        return bv
    }()
    
    private lazy var sectionHeaderView: MarketTableHeaderView = {
        let view = MarketTableHeaderView()
        view.backgroundColor = .container
        view.roundCorners(corners: [.topLeft, .topRight], radius: 4)
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addSubviews()
        adjustUI()
        addConstraints()
        
        tableView.register(MarketTableViewCell.self, forCellReuseIdentifier: marketCellId)
        self.isHomeView = true
        
        getCurrenciesData()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if BaseSingleton.shared.token != "" {
            balanceView.loginButton.isHidden = true
            balanceView.visibleButton.isHidden = false
            balanceView.subtitleLabel.isHidden = false
            balanceView.balanceLabel.isHidden = false
            getWalletData()
            getUnreadCount()
        } else {
            balanceView.loginButton.isHidden = false
            balanceView.visibleButton.isHidden = true
            balanceView.subtitleLabel.isHidden = true
            balanceView.balanceLabel.isHidden = true
        }
        
        getHotMarketData()
        socketSingleton.connectToSocket(token: "")
        connectToSocket()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        disconnectSocket()
    }
}

extension HomeViewController {
       
    func adjustUI() {
        
        let logo = UIImage(named: "logo")
        let imageView = UIImageView(image: logo)
        self.navigationItem.titleView = imageView
        
        self.view.backgroundColor = .background
        self.tableView.separatorStyle = .none
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
        self.view.bringSubviewToFront(backgroundView)
        self.view.bringSubviewToFront(tableView)
        
        referFriendView.isUserInteractionEnabled = true
        let gesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapReferFriend(_:)))
        gesture.numberOfTapsRequired = 1
        gesture.numberOfTouchesRequired = 1
        referFriendView.addGestureRecognizer(gesture)
    }
    
    func addSubviews() {
        view.addSubview(tableHeader)
        tableView.tableHeaderView = tableHeader
        tableHeader.addSubview(backgroundView)
        tableHeader.addSubview(volumeBarView)
        tableHeader.addSubview(balanceView)
        tableHeader.addSubview(referFriendView)
        tableHeader.addSubview(sectionHeaderView)
    }
    
    func addConstraints() {
        backgroundView.snp.makeConstraints({ (make) in
            make.top.equalTo(self.topLayoutGuide.snp.top)
            make.left.right.equalToSuperview()
            make.height.equalTo(117)
        })
        
        volumeBarView.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(10)
            make.left.equalToSuperview().offset(11)
            make.right.equalToSuperview().offset(-11)
            make.height.equalTo(38)
        })
        
        balanceView.snp.makeConstraints({ (make) in
            make.top.equalTo(volumeBarView.snp.bottom).offset(12)
            make.left.equalTo(volumeBarView)
            make.right.equalTo(volumeBarView)
            make.height.equalTo(90)
        })
        
        referFriendView.snp.makeConstraints({ (make) in
            make.top.equalTo(balanceView.snp.bottom).offset(10)
            make.width.equalToSuperview()
            make.height.equalTo(90)
        })
        
        sectionHeaderView.snp.makeConstraints({ (make) in
            make.top.equalTo(referFriendView.snp.bottom).offset(10)
            make.left.equalToSuperview().offset(11)
            make.right.equalToSuperview().offset(-11)
            make.height.equalTo(75)
        })
    }
}

// MARK: Table
extension HomeViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return markets.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: marketCellId, for: indexPath) as! MarketTableViewCell
        
        cell.configure(markets[indexPath.row])
        
        cell.containerView.snp.remakeConstraints({ (make) in
            make.top.equalToSuperview()
            make.bottom.equalToSuperview()
            make.left.equalToSuperview().offset(11)
            make.right.equalToSuperview().offset(-11)
        })
        
        cell.currencyPairLabel.pairBeautifier(.textColor)
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        if let tabBar = appDelegate!.window!.rootViewController as? UITabBarController {
            
            let secondTab = tabBar.viewControllers![2].children[0] as! TradeViewController
            secondTab.market = self.markets[indexPath.row]
            tabBar.selectedIndex = 2
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 48
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
}

// MARK: Action
extension HomeViewController {
    
    @objc func tapInbox(_ sender: UIButton) {
        let vc = NotificationTableViewController()
        show(vc, sender: nil)
    }
    
    @objc func tapVisible(_ sender: UIButton) {
        
        let runningIndex = walletIndex == 0 ? wallets.count-1 : walletIndex-1
        
        if isVisible == true {
            isVisible = false
            balanceView.visibleButton.setImage(UIImage(named: "invisible"), for: .normal)
            balanceView.balanceLabel.text = "****** \(self.wallets[runningIndex].coinName!.uppercased())"
            balanceView.balanceLabel.currencyBeautifier()
        } else {
            isVisible = true
            balanceView.visibleButton.setImage(UIImage(named: "visible"), for: .normal)
            
            balanceView.balanceLabel.text = "\(String(describing: self.wallets[runningIndex].available!)) \(String(describing: self.wallets[runningIndex].coinName!.uppercased()))"
            balanceView.balanceLabel.currencyBeautifier()
        }
        tableView.reloadData()
    }
    
    @objc func tapLogin(_ sender: UIButton) {
        let vc = NavViewController(rootViewController: LoginViewController())
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func tapReferFriend(_ sender: UITapGestureRecognizer) {
        if BaseSingleton.shared.token != "" {
            let vc = ReferralViewController()
            show(vc, sender: nil)
        }
    }
    
    @objc func tapMarket(_ sender: UIButton) {
        tabBarController?.selectedIndex = 1
    }
    
    func repWalletBalance() {
        
        let coin = coins.filter({ $0.id == self.wallets[self.walletIndex].coinId }).first
        
        if self.isVisible == true {
            self.balanceView.balanceLabel.text = "\(self.wallets[self.walletIndex].available!)".forceDecimalDigits(coin?.round ?? 6) + " \(self.wallets[self.walletIndex].coinName!.uppercased())"
        } else {
            self.balanceView.balanceLabel.text = "****** \(self.wallets[self.walletIndex].coinName!.uppercased())"
        }
        balanceView.balanceLabel.pushTransition(0.4)
        balanceView.balanceLabel.currencyBeautifier()
        self.walletIndex = self.walletIndex + 1
        if self.walletIndex >= self.wallets.count {
            self.walletIndex = 0
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 4.0) {
            self.repWalletBalance()
        }
    }
    
    func updateBalanceView() {
        if BaseSingleton.shared.token != "" {
            balanceView.loginButton.isHidden = true
            balanceView.balanceLabel.isHidden = false
        } else {
            balanceView.loginButton.isHidden = false
            balanceView.balanceLabel.isHidden = true
        }
    }
    
    func getWalletData() {
        networking.getUserAssets(params: [:], controller: self) { wallets in
            self.wallets = wallets
            self.balanceView.subtitleLabel.isHidden = false
            self.balanceView.visibleButton.isHidden = false
            self.repWalletBalance()
            self.updateBalanceView()
        }
    }
}

// MARK: Networking
extension HomeViewController {
    
    func getUnreadCount() {
        
        let rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: 18, height: 16))
        rightButton.setBackgroundImage(UIImage(named: "bell"), for: .normal)
        rightButton.addTarget(self, action: #selector(tapInbox(_:)), for: .touchUpInside)
        rightButton.addSubview(unreadCountLabel)

        // Bar button item
        let rightBarButtomItem = UIBarButtonItem(customView: rightButton)
        navigationItem.rightBarButtonItem = rightBarButtomItem
        
        networking.getNotificationUnreadCount(params: [:], controller: self) { response in
            self.unreadCount = "\(response)"

            if "\(response)" != "0" {
                self.unreadCountLabel.isHidden = false
                self.unreadCountLabel.text = "\(response)"
            }
        }
    }
    
    func getCurrenciesData() {
        
        networking.getAllCoin(params: [:], controller: self) { coins in
            BaseSingleton.shared.coins = coins
            self.coins = coins
        }
        
        networking.getCurrenciesList(params: [:], controller: self) { currencies in
            BaseSingleton.shared.currencies = currencies
            self.getHotMarketData()
        }
    }
    
    func getHotMarketData() {
        networking.getHotMarket(params: [:], controller: self) { markets in
            
            var activeMarkets: [Market] = []
            for elem in markets {
                if elem.status != 0 {
                    activeMarkets.append(elem)
                }
            }
            
            if activeMarkets.count == 0 {
                self.sectionHeaderView.isHidden = true
            } else {
                self.sectionHeaderView.isHidden = false
            }
            
            self.markets = activeMarkets
            BaseSingleton.shared.markets = activeMarkets
            self.tableView.reloadData()
        }
    }
    
    func connectToSocket() {
        
        manager = socketSingleton.manager
        socket = manager.defaultSocket
        
        self.socket.on(clientEvent: .connect) {data, ack in
            print(data)
            print("message socket connected")
            
            self.socket.on("marketList") { (data, eck) in
//                print("show home market list data: \(data)")

                let result = JSON(data.first!)
                
                let market = Market.init(aJSON: result["data"])
                
                if let index = self.markets.index(where: {$0.symbol == market.symbol}) {
                    
                    if market.status == 0 {
                        self.markets.remove(at: index)
                    } else {
                        self.markets[index] = market
                    }
                    self.tableView.reloadData()
                }
            }
        }
        
    }
    
    func disconnectSocket() {
        
        socketSingleton.disconnectSocket()
    }
}
