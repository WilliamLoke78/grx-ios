//
//  NotificationViewController.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 19/02/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import UIKit

private let notificationViewCellId = "notificationViewCellId"

class NotificationTableViewController: BaseTableViewController {

    private var notifications: [NotificationMessage] = []
    
    var isFetching = false
    var isLast = false
    var pagination: Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        adjustUI()
        tableView.register(NotificationTableViewCell.self, forCellReuseIdentifier: notificationViewCellId)
        tableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
//        getOrderHistoryData(page: pagination)
        getNotifications()
    }
}

// MARK: User Interface
extension NotificationTableViewController {
    
    func adjustUI() {
        
        title = "Notification".localized()
        
        tableView.backgroundColor = .background
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView()
    }
}

// MARK: Table
extension NotificationTableViewController {

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: notificationViewCellId, for: indexPath) as! NotificationTableViewCell
        
        cell.configure(notifications[indexPath.row])
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = notifications.count - 1
//        if indexPath.row == lastElement - 5 {
//            getOrderHistoryData(page: pagination)
//
//        }
    }
    
    // Estimated height for row at is needed to avoid jumping cells
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    
        let vc = MessageViewController()
        vc.message = notifications[indexPath.row]
        show(vc, sender: nil)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}

// MARK: Networking
extension NotificationTableViewController {
 
    func getNotifications() {
        
        let networking = Networking()
        networking.getNotificationMessages(params: [:], controller: self) { notificationData in

            self.notifications = notificationData.records!
            self.tableView.reloadData()
        }
    }
}

