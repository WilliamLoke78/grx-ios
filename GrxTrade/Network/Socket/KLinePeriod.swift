//
//  KLinePeriod.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 29/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import Foundation
//import SocketRocket
//import Gzip

let favAreaID = "19900802"

enum KLinePeriod: String {
    
    case timeline = "timeline"
    case p1min = "1min"
    case p5min = "5min"
    case p15min = "15min"
    case p30min = "30min"
    case p1hour = "1hour"
    case p2hour = "2hour"
    case p4hour = "4hour"
    case p6hour = "6hour"
    case p12hour = "12hour"
    case p1day = "1day"
    case p1week = "1week"
    case p1mon = "1mon"
    case p1year = "1year"
    
    func timeInterval() -> TimeInterval {
        switch self {
        case .timeline:
            return 60*1000
        case .p1min:
            return 60*1000
        case .p5min:
            return 5*60*1000
        case .p15min:
            return 15*60*1000
        case .p30min:
            return 30*60*1000
        case .p1hour:
            return 1*3600*1000
        case .p2hour:
            return 2*3600*1000
        case .p4hour:
            return 4*3600*1000
        case .p6hour:
            return 6*3600*1000
        case .p12hour:
            return 12*3600*1000
        case .p1day:
            return 24*3600*1000
        case .p1week:
            return 7*24*3600*1000
        case .p1mon:
            return 30*24*3600*1000
        case .p1year:
            return 365*24*3600*1000
        }
    }
    func getRawValue() -> String {
        if self.rawValue == "timeline" {
            return KLinePeriod.p1min.rawValue
        }
        
        return self.rawValue
    }
    
    static func defaultValue() -> KLinePeriod {
        return .p15min
    }
}
enum DepthType: String {
    case step0
    case step1
    case step2
    case step3
    case step4
    case step5
    
}
struct AYWSTopic {
    
    static func kLine(symbol: String, period: KLinePeriod) -> String {
        return "market.\(symbol).kline.\(period.rawValue)"
    }
    static func depth(symbol: String, type: DepthType) -> String {
        return "market.\(symbol).depth.\(type.rawValue)"
    }
    static func tradeDetail(symbol: String) -> String {
        return "market.\(symbol).trade.detail"
    }
    static func marketDetail(symbol: String) -> String {
        return "market.\(symbol).detail"
    }
    static func marketTicker(symbol: String) -> String {
        if symbol == favAreaID {
            return "market.ticker"
        }
        return "market.\(symbol).ticker"
    }
}


protocol AYWebSocketDelegate {
    func receivedMessage(_ message: String)
}


class AYWebSocketUtility: NSObject {
//    static let shared = AYWebSocketUtility.init()
//
//    var webSocket : SRWebSocket?
//    var delegate: AYWebSocketDelegate?
//    var url: URL? = URL.init(string: ws)
////    var url: URL? = URL.init(string: ws)
//    let reconnectMax = 15
//    var reconnectCounter = 0
//
//    var status: Status = .initial
//
//    fileprivate var heartBeatTimer: Timer?
//    fileprivate var taskArray: [[String : String]] = []
    
    public enum Status{
        case initial
        case opening
        case open
        case closing
        case closed
        case cancel
    }

    override init() {
        super.init()
//        InternetConnectionManager.shared.observeInternetConnection()
    }
    
//    func connect() {
////        NotificationCenter.default.addObserver(self, selector: #selector(internetAvailable), name: NSNotification.Name(rawValue: NOTIFICATION_INTERNET_CONNECTION_AVAILABLE), object: nil)
//        open()
//    }
//
//    func reConnect() {
//        self.webSocket?.close()
//        self.open()
//        print("重连。。。")
//    }
//
//    func open() {
//        guard url != nil else {
//            return
//        }
////        webSocket = SRWebSocket(url: url!)
//        webSocket?.delegate = self
//        status = .opening
//        webSocket?.open()
//
//        heartBeatTimer?.invalidate()
//        heartBeatTimer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(AYWebSocketUtility.heartBeat), userInfo: nil, repeats: true)
//        heartBeatTimer?.fire()
//
//    }
//
//
//    func close() {
////        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_INTERNET_CONNECTION_AVAILABLE), object: nil)
//        if webSocket != nil {
//            status = .closing
//            webSocket?.close()
//        }
//    }
    
//    @objc fileprivate func heartBeat() {
//        if status == .open {
//            self.sendMessage(message: "ping")
////            AYNetworking.updateAccessToken()
//        }
//        //BaseSingleton.share.hosts.checkAndPullHostsInfo()
//    }
//
//    fileprivate func sendMessage(message: String) {
//        self.webSocket?.send(message)
//    }
//
//    fileprivate func sendParams(params: [String : String]) {
//        var dic = params
//        if (BaseSingleton.share.loginer.isLogin ?? false), BaseSingleton.share.loginer.token != nil {
//            dic["authorization"] = BaseSingleton.share.loginer.token!
//        }
//        if let data = try? JSONSerialization.data(withJSONObject: dic, options: []) {
//            //self.webSocket?.send(data)
//            let jsonString = String.init(data: data, encoding: String.Encoding.utf8)
//            self.webSocket?.send(jsonString)
//        }
    }
    
//    func sub(topic: String, id: String = "") {
//        let info = ["sub" : topic.lowercased(), "id" : id]
//        if status == .open {
//            self.sendParams(params: info)
//        }else{
//            if (status == .cancel || status == .closed || status == .closing) && InternetConnectionManager.shared.reachability.connection != .none {
//                self.reConnect()
//            }
//            if taskArray.firstIndex(where: { (elem: [String : String]) -> Bool in
//                return topic.lowercased() == elem["sub"]
//            }) != nil {
//                //如果已经存在就不需要做什么
//            }else{
//                taskArray.append(info)
//            }
//
//            if let index = taskArray.firstIndex(where: { (elem: [String : String]) -> Bool in
//                return topic.lowercased() == elem["cancel"]
//            }) {
//                taskArray.remove(at: index)
//            }
//        }
//    }
    
//    func unSub(topic: String, id: String = "") {
//        let info = ["cancel" : topic.lowercased(), "id" : id]
//        if status == .open {
//            self.sendParams(params: info)
//        }else{
//            if (status == .cancel || status == .closed || status == .closing) && InternetConnectionManager.shared.reachability.connection != .none {
//                self.reConnect()
//            }
//
//            if taskArray.firstIndex(where: { (elem: [String : String]) -> Bool in
//                return topic.lowercased() == elem["cancel"]
//            }) != nil {
//                //已经存在，不需要操作
//            }else{
//                taskArray.append(info)
//            }
//
//            if let index = taskArray.firstIndex(where: { (elem: [String : String]) -> Bool in
//                return topic.lowercased() == elem["sub"]
//            }) {
//                taskArray.remove(at: index)
//            }
//        }
//
//    }
    
//    fileprivate func executeTasksList() {
//        guard status == .open else {
//            return
//        }
//        if taskArray.count > 0 {
//            taskArray.forEach { (elem: [String : String]) in
//                self.sendParams(params: elem)
//            }
//        }
//    }
//
//    fileprivate func subbedOrCanceledTask(_ info: (String, String)) {
//        if let index = taskArray.firstIndex(where: { (elem: [String : String]) -> Bool in
//            return info.1 == elem[info.0]
//        }) {
//            taskArray.remove(at: index)
//        }
//        switch info.0 {
//        case "sub":
//            if let index = taskArray.index(where: { (elem: [String : String]) -> Bool in
//                return info.1 == elem["sub"]
//            }) {
//                taskArray.remove(at: index)
//            }
//        case "cancel":
//            if let index = taskArray.index(where: { (elem: [String : String]) -> Bool in
//                return info.1 == elem["cancel"]
//            }) {
//                taskArray.remove(at: index)
//            }
//        default:
//            break
//        }
//    }
    
    
    // MARK: Notification center
//    @objc func internetAvailable() {
//        if status == .cancel || status == .closed || status == .closing {
//            self.reConnect()
//        }
//    }
//}


// MARK: SRWebSocketDelegate
//extension AYWebSocketUtility: SRWebSocketDelegate {
//    
//    func webSocket(_ webSocket: SRWebSocket!, didReceiveMessage message: Any!){
//        guard message != nil else {
//            print("didReceiveMessage--nil--")
//            return
//        }
//        
//        var jdata: Data? = nil
//        var jString = message as? String
//        if message is Data {
//            print("didReceiveMessage--Data--")
//            jdata = message as? Data
//            if (jdata?.isGzipped ?? false) {
//                jdata = try! jdata?.gunzipped()
//                if jdata != nil {
//                    jString = (String.init(data: jdata!, encoding: .utf8) ?? "")
//                    debugPrint("didReceiveMessage:" + jString!)
//                }
//            }
//        }else if message is String {
//            print("didReceiveMessage--String--")
//            jdata = (message as! String).data(using: String.Encoding.utf8)
//            debugPrint("didReceiveMessage:" + (message as! String))
//        }else{
//            print("didReceiveMessage: 接收的数据类型不是string也不是data类型，请开发人员排查原因")
//        }
//        
//        if let data = jdata {
//            if let obj = try? JSONSerialization.jsonObject(with: data, options: []) {
//                if let dic = obj as? [String : Any] {
//                    if let subbedTopic = dic["subbed"] as? String {
//                        self.subbedOrCanceledTask(("sub", subbedTopic))
//                        return
//                    }else if let canceledTopic = dic["canceled"] as? String {
//                        self.subbedOrCanceledTask(("cancel", canceledTopic))
//                        return
//                    }
//                    var notiName: String?
//                    if let ch = (dic["ch"] as? String) {
//                        notiName = ch
//                    }else if let event = dic["event"] as? String {
//                        notiName = event
//                    }
//                    if notiName != nil {
//                        let notification = Notification.init(name: Notification.Name.init(notiName!), object: nil, userInfo: dic)
//                        NotificationCenter.default.post(notification)
//                    }
//                }
//            }
//        }
//        if jString != nil {
//            delegate?.receivedMessage(jString!)
//        }
//    }
//    
//    
//    func webSocketDidOpen(_ webSocket: SRWebSocket!) {
//        print("webSocketDidOpen")
//        status = .open
//        reconnectCounter = 1
//        
//        self.executeTasksList()
//        self.sub(topic: Notification.Name.handsel_activity.rawValue)
//        self.sub(topic: Notification.Name.ieo_handsel_activity.rawValue)
//        self.sub(topic: Notification.Name.anniversary_event.rawValue)
//        self.sub(topic: Notification.Name.otc_match_order.rawValue)
//    }
//    
//    func webSocket(_ webSocket: SRWebSocket!, didFailWithError error: Swift.Error!) {
//        print("didFailWithError:\(error.localizedDescription)")
//        if reconnectCounter == 1 {
//            print("Reconnecting...")
//        }
//        status = .cancel
//        // reconnect in 1s, 2s, 3s... 15s
//        let tryAgainIn = DispatchTime.now() + DispatchTimeInterval.seconds(reconnectCounter)
//        DispatchQueue.main.asyncAfter(deadline: tryAgainIn) {
//            if(self.reconnectCounter <= self.reconnectMax) {
//                self.reconnectCounter += 1
//                self.reConnect()
//                //print("重连。。。")
//            }
//        }
//    }
//    
//    func webSocket(_ webSocket: SRWebSocket!, didCloseWithCode code: Int, reason: String!, wasClean: Bool) {
//        status = .closed
//        print("didCloseWithCode, code:\(code),reason:\(reason ?? "")")
//    }
//    func webSocket(_ webSocket: SRWebSocket!, didReceivePong pongPayload: Data!) {
//        print("didReceivePong")
//    }
//}
