//
//  Socket.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 19/01/2020.
//  Copyright © 2020 Leon Mah. All rights reserved.
//

import SwiftyJSON
import Alamofire
import SocketIO

class Socket {
    
    let networking = Networking()

    var manager: SocketManager!
    var socket: SocketIOClient!
    
    static let shared = Socket()
    
    func connectToSocket(token: String) {
        
        manager = SocketManager(socketURL: URL(string: networking.wsHost)!, config: [.path("/trade-ws/socket.io"), .log(false), .forceNew(true), .connectParams([:]), .forceWebsockets(true), .compress])
        socket = manager.defaultSocket
        
        
        socket.on(clientEvent: .connect) {data, ack in
            print(data)
            print("socket connected")
        }
        
        socket.on(clientEvent: .error) { (data, eck) in
//            print(data)
//            print("socket error")
        }
        
        socket.on(clientEvent: .disconnect) { (data, eck) in
//            print(data)
//            print("socket disconnect")
        }
        
        socket.on(clientEvent: SocketClientEvent.reconnect) { (data, eck) in
//            print(data)
//            print("socket reconnect")
        }
        
//        socket?.onAny { (data) in
//            if data.event == "reconnectAttempt" {
//                self.manager?.disconnect()
//                self.socket.connect()
//            }
//        }
        
        socket.connect()
    }
    
    func disconnectSocket() {
        
        manager = SocketManager(socketURL: URL(string: networking.host)!, config: [.path("/trade-ws/socket.io"), .log(false), .forceNew(true), .connectParams([:]), .forceWebsockets(true), .compress])
        socket = manager.defaultSocket
        
        socket.on(clientEvent: .disconnect) { (data, eck) in
            print(data)
            print("socket disconnect")
        }
    }
}
