//
//  Networking.swift
//  GrxTrade
//
//  Created by Leon Mah Kean Loon on 26/12/2019.
//  Copyright © 2019 Leon Mah. All rights reserved.
//

import SwiftyJSON
import Alamofire

class Networking {
    
    #if Dev
        let host = "https://dev-grx-api.usdp.io"
        let staticHost = "https://dev-grx.usdp.io"
        let wsHost = "https://dev-grx-api.usdp.io"
    #elseif UAT
        let host = "https://uat-grx-api.usdp.io"
        let staticHost = "https://uat-grx.usdp.io"
        let wsHost = "https://uat-grx-api.usdp.io"
    #elseif Production
        let host = "https://api.grxtrade.com"
        let staticHost = "https://grxtrade.com"
        let wsHost = "https://ws.grxtrade.com"
    #endif
    
    let mxwHost = "https://www.maxonrow.com/mxw-products/#max-wallet"
    
    static let shared = Networking()
    
    func alamofire(endpoint: String, method: HTTPMethod, token: Bool, params: [String: Any], controller: UIViewController, completion: @escaping (_ : Any) -> Void) -> Void {

        guard let url = URL(string: endpoint) else { return }
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(UIDevice.current.systemName, forHTTPHeaderField: "source-terminal")
        request.setValue(UIDevice.current.modelName, forHTTPHeaderField: "device-model")
        request.setValue(UIDevice.current.systemVersion, forHTTPHeaderField: "os-version")
        request.setValue(LocalizationManager.serverLang(), forHTTPHeaderField: "Lang")
        if token == true {
            request.setValue(BaseSingleton.shared.token as! String, forHTTPHeaderField: "Authorization")
        }
        
        print("header: \(request.value(forHTTPHeaderField: "Authorization"))")
        
        if method == .post || method == .put {
            do {
                request.httpBody   = try JSONSerialization.data(withJSONObject: params)
            } catch let error {
                print("Error : \(error.localizedDescription)")
            }
        }
        print("endpoint: \(endpoint)")

        Alamofire.request(request).validate().responseJSON { response in
            print("show response code: \(response.result) (\(response.response?.statusCode))")
            switch response.result {
            case .success:

                let result = JSON(response.result.value!)
                
                if result["errcode"].stringValue == "3507" {
                    // Not working yet
                    print("max already registered")
                    showAlertView("Error".localized(), "You're already an existing user, please try to login.".localized(), controller: controller)
                    return
                }
                
                if result["errcode"].stringValue == "40001" {
                    print("session timeout: \(result["errmsg"].stringValue)")
                    showTimeoutAlertView("Session Timeout".localized(), "Your session has expired, please log in to continue.".localized(), controller: controller)
                    
                    return
                }
                
                if result["errcode"].stringValue == "1904" {
                    
                    // Do nothing here
                    
                } else if result["errcode"].stringValue != "0" {
                    
                    let mxwViews = ["SignupViewController", "LoginViewController", "IdentityVerificationViewController"]
                    if mxwViews.contains(controller.className) {
                        showSimpleToast("\(result["errmsg"].stringValue)")
                    } else {
                        showAlertView("Error".localized(), result["errmsg"].stringValue, controller: controller)
                    }
                    print("error message: \(result["errmsg"].stringValue)")
                    return
                }

                DispatchQueue.main.async {
                    completion(result["data"])
                }
                
            case .failure:
//                let result = JSON(response.data)
                showAlertView("Error".localized(), "", controller: controller)
            }
        }
    }
    
    func getVersionLookup(params: [String: Any], controller: UIViewController, completion: @escaping (_: VersionData) -> Void) -> Void {
        alamofire(endpoint: "\(host)/gw/w/version/lookup", method: .post, token: false, params: params, controller: controller) { result in
            
            let versionData = VersionData.init(aJSON: result as! JSON)
            
            DispatchQueue.main.async {
                completion(versionData)
            }
        }
    }
    
    func getUserAssets(params: [String: String], controller: UIViewController, completion: @escaping (_ : [Wallet]) -> Void) -> Void {        
        alamofire(endpoint: "\(host)/gw/w/assets/info", method: .get, token: true, params: params, controller: controller) { result in
            
            let asset = Asset.init(aJSON: result as! JSON)
            
            DispatchQueue.main.async {
                completion(asset.assertList!)
            }
        }
    }
    
    func postLogin(params: [String: String], controller: UIViewController, completion: @escaping (_ : User) -> Void) -> Void {
        alamofire(endpoint: "\(host)/auth/login", method: .post, token: false, params: params, controller: controller) { result in
            
            let user = User.init(aJSON: result as! JSON)
            
            DispatchQueue.main.async {
                completion(user)
            }
        }
    }
    
    func getGoogleAuth(params: [String: String], controller: UIViewController, completion: @escaping (_ : GoogleAuth) -> Void) -> Void {
        alamofire(endpoint: "\(host)/user/get/ga", method: .get, token: true, params: params, controller: controller) { result in
            
            let googleAuth = GoogleAuth.init(aJSON: result as! JSON)
            
            DispatchQueue.main.async {
                completion(googleAuth)
            }
        }
    }
    
    func setGoogleAuth(params: [String: String], controller: UIViewController, completion: @escaping (_ : Any) -> Void) -> Void {
        alamofire(endpoint: "\(host)/user/set/ga", method: .post, token: true, params: params, controller: controller) { result in
            
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    func cancelGoogleAuth(params: [String: String], controller: UIViewController, completion: @escaping (_ : Any) -> Void) -> Void {
        alamofire(endpoint: "\(host)/user/reset/ga", method: .post, token: true, params: params, controller: controller) { result in
            
            print("ga cancel:")
            print(result)
            
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    func postSmsAuthOTP(params: [String: String], controller: UIViewController, completion: @escaping (_ : Any) -> Void) -> Void {
        alamofire(endpoint: "\(host)/user/request/bind-phone-otp", method: .post, token: true, params: params, controller: controller) { result in
            
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    func setSmsAuth(params: [String: String], controller: UIViewController, completion: @escaping (_ : Any) -> Void) -> Void {
        alamofire(endpoint: "\(host)/user/bind/phone", method: .post, token: true, params: params, controller: controller) { result in
            
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    func cancelSmsAuth(params: [String: String], controller: UIViewController, completion: @escaping (_ : Any) -> Void) -> Void {
        alamofire(endpoint: "\(host)/user/unbind/phone", method: .put, token: true, params: params, controller: controller) { result in
            
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    func requestOTP(params: [String: String], controller: UIViewController, completion: @escaping (_ : Any) -> Void) -> Void {
        alamofire(endpoint: "\(host)/user/request/reg-otp", method: .post, token: false, params: params, controller: controller) { result in
            
            print("request otp: \(result)")
            
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    func postSignup(params: [String: String], controller: UIViewController, completion: @escaping (_ : User) -> Void) -> Void {
        alamofire(endpoint: "\(host)/user/register", method: .post, token: false, params: params, controller: controller) { result in
            
            let user = User.init(aJSON: result as! JSON)
            
            DispatchQueue.main.async {
                completion(user)
            }
        }
    }
    
    func postLoginPassword(params: [String: String], controller: UIViewController, completion: @escaping (_ : Any) -> Void) -> Void {
        alamofire(endpoint: "\(host)/user/set/pwd", method: .post, token: true, params: params, controller: controller) { result in
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    func putLoginPassword(params: [String: String], controller: UIViewController, completion: @escaping (_ : Any) -> Void) -> Void {
        alamofire(endpoint: "\(host)/user/upd/pwd", method: .put, token: true, params: params, controller: controller) { result in
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    func postTransactionPassword(params: [String: String], controller: UIViewController, completion: @escaping (_ : Any) -> Void) -> Void {
        alamofire(endpoint: "\(host)/user/set/txn-pwd", method: .post, token: true, params: params, controller: controller) { result in
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    func putTransactionPassword(params: [String: String], controller: UIViewController, completion: @escaping (_ : Any) -> Void) -> Void {
        alamofire(endpoint: "\(host)/user/upd/txn-pwd", method: .put, token: true, params: params, controller: controller) { result in
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    func getReferralCode(params: [String: String], controller: UIViewController, completion: @escaping (_ : Referral) -> Void) -> Void {
        alamofire(endpoint: "\(host)/user/get/ref", method: .get, token: true, params: params, controller: controller) { result in
            
            let referral = Referral.init(aJSON: result as! JSON)
            
            DispatchQueue.main.async {
                completion(referral)
            }
        }
    }
    
    func postSetDefaultReferral(params: [String: String], controller: UIViewController, completion: @escaping (_ : Any) -> Void) -> Void {
        alamofire(endpoint: "\(host)/user/set/def-ref", method: .post, token: true, params: [:], controller: controller) { result in
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    func postReferralCode(params: [String: String], controller: UIViewController, completion: @escaping (_ : Any) -> Void) -> Void {
        alamofire(endpoint: "\(host)/user/set/ref", method: .post, token: true, params: params, controller: controller) { result in
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    func getReferralMembers(params: [String: String], controller: UIViewController, completion: @escaping (_ : [ReferralMember]) -> Void) -> Void {
        alamofire(endpoint: "\(host)/user/get/ref-list", method: .get, token: true, params: [:], controller: controller) { result in
                        
            var members: [ReferralMember] = []
            
            let response = JSON(result)
            for elem in response.arrayValue {
                members.append(ReferralMember.init(aJSON: elem))
            }
            
            DispatchQueue.main.async {
                completion(members)
            }
        }
    }
    
    func postForgotPassword(params: [String: String], controller: UIViewController, completion: @escaping (_ : Any) -> Void) -> Void {
        alamofire(endpoint: "\(host)/user/request/fp-otp", method: .post, token: false, params: params, controller: controller) { result in
            
            print("result: \(result)")
            
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    func putForgotPassword(params: [String: String], controller: UIViewController, completion: @escaping (_ : User) -> Void) -> Void {
        alamofire(endpoint: "\(host)/user/forget/pwd", method: .put, token: false, params: params, controller: controller) { result in
            
            print("put forgot pwd: \(result)")
            
            let user = User.init(aJSON: result as! JSON)
        
            DispatchQueue.main.async {
                completion(user)
            }
        }
    }
    
    func getPackageInfo(params: [String: String], controller: UIViewController, completion: @escaping (_ : Any) -> Void) -> Void {
        alamofire(endpoint: "\(host)/auth/get/mxw-package?type=\(params["type"]!)", method: .get, token: false, params: [:], controller: controller) { result in
         
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    func postMxwRegister(params: [String: String], controller: UIViewController, completion: @escaping (_ : User) -> Void) -> Void {
        alamofire(endpoint: "\(host)/max/register", method: .post, token: false, params: params, controller: controller) { result in
            
            print("mxw register")
            print(result)
            
            let user = User.init(aJSON: result as! JSON)
            
            print("mxw user")
            print(user)
            
            DispatchQueue.main.async {
                completion(user)
            }
        }
    }
    
    func postBindMaxWallet(params: [String: String], controller: UIViewController, completion: @escaping (_ : Any) -> Void) -> Void {
        alamofire(endpoint: "\(host)/user/bind/max-wallet", method: .post, token: true, params: params, controller: controller) { result in
            
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    func postMxwLogin(params: [String: Any], controller: UIViewController, completion: @escaping (_ : User) -> Void) -> Void {
        
        print("mxw login param")
        print(params)
        
        alamofire(endpoint: "\(host)/auth/max-wallet/login", method: .post, token: false, params: params, controller: controller) { result in
            
            print("mxw login result")
            print(result)
            
            let user = User.init(aJSON: result as! JSON)
            
            DispatchQueue.main.async {
                completion(user)
            }
        }
    }
    
    func postMxwKyc(params: [String: Any], controller: UIViewController, completion: @escaping (_ : Any) -> Void) -> Void {
        alamofire(endpoint: "\(host)/kyc/set/primary", method: .post, token: true, params: params, controller: controller) { result in
            
            print("mxw kyc")
            print(result)
            
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    func getDepositAddress(params: [String: String], controller: UIViewController, completion: @escaping (_ : DepositResponse) -> Void) -> Void {
        alamofire(endpoint: "\(host)/gw/w/deposit/address?symbol=\(params["symbol"]!)", method: .get, token: true, params: [:], controller: controller) { result in
            
            print("result: \(result)")
            
            let deposit = DepositResponse.init(aJSON: result as! JSON)
            
            DispatchQueue.main.async {
                completion(deposit)
            }
        }
    }
    
    func getWithdrawFee(params: [String: String], controller: UIViewController, completion: @escaping (_ : WithdrawFee) -> Void) -> Void {
        alamofire(endpoint: "\(host)/gw/w/withdraw/fee?coinId=\(params["coinId"]!)&addressId=\(params["addressId"]!)", method: .get, token: true, params: [:], controller: controller) { result in
            
            let withdrawFee = WithdrawFee.init(aJSON: result as! JSON)
            
            DispatchQueue.main.async {
                completion(withdrawFee)
            }
        }
    }
    
    func getAllCoin(params: [String: String], controller: UIViewController, completion: @escaping (_ : [Coin]) -> Void) -> Void {
        alamofire(endpoint: "\(host)/gw/w/coin/allCoin", method: .get, token: true, params: [:], controller: controller) { result in
            
            var coins: [Coin] = []
            
            let response = JSON(result)
            for elem in response.arrayValue {
                coins.append(Coin.init(aJSON: elem))
            }
            
            DispatchQueue.main.async {
                completion(coins)
            }
        }
    }
    
    func getAllTransactions(params: [String: Any], controller: UIViewController, completion: @escaping (_ : AccountDetailData) -> Void) -> Void {
        alamofire(endpoint: "\(host)/gw/w/account/detail?current=\(params["current"]!)&size=\(params["size"]!)", method: .get, token: true, params: params, controller: controller) { result in
            
            let transactions = AccountDetailData.init(aJSON: result as! JSON)
            
            DispatchQueue.main.async {
                completion(transactions)
            }
        }
    }
    
    func getAllBusinessTypes(params: [String: Any], controller: UIViewController, completion: @escaping (_ : [BusinessType]) -> Void) -> Void {
        alamofire(endpoint: "\(host)/gw/w/res/business_types", method: .get, token: false, params: [:], controller: controller) { result in
            
            var businessTypes: [BusinessType] = []

            let response = JSON(result)

            for elem in response.arrayValue {
                businessTypes.append(BusinessType.init(aJSON: elem))
            }
            
            DispatchQueue.main.async {
                completion(businessTypes)
            }
        }
    }
    
    func getAllBusinessSubTypes(params: [String: Any], controller: UIViewController, completion: @escaping (_ : [BusinessType]) -> Void) -> Void {
        alamofire(endpoint: "\(host)/gw/w/res/business_sub_types", method: .get, token: false, params: [:], controller: controller) { result in
            
            print("sub types result: \(result)")
            
            var businessSubTypes: [BusinessType] = []
            
            let response = JSON(result)
            
            for elem in response.arrayValue {
                businessSubTypes.append(BusinessType.init(aJSON: elem))
            }
            
            DispatchQueue.main.async {
                completion(businessSubTypes)
            }
        }
    }
    
    func getBeneficiaries(params: [String: String], controller: UIViewController, completion: @escaping (_ : [Beneficiary]) -> Void) -> Void {
        alamofire(endpoint: "\(host)/gw/p/withdraw/address/\(params["coinId"] ?? "0")", method: .get, token: true, params: [:], controller: controller) { result in
            
            var beneficiaries: [Beneficiary] = []

            let response = JSON(result)

            for elem in response.arrayValue {
                beneficiaries.append(Beneficiary.init(aJSON: elem))
            }
            
            DispatchQueue.main.async {
                completion(beneficiaries)
            }
        }
    }
    
    func addBeneficiary(params: [String: String], controller: UIViewController, completion: @escaping (_ : Beneficiary) -> Void) -> Void {
        
        print("bene params: \(params)")
        
        alamofire(endpoint: "\(host)/gw/p/withdraw/address", method: .post, token: true, params: params, controller: controller) { result in
            
            let beneficiary = Beneficiary.init(aJSON: result as! JSON)
            
            DispatchQueue.main.async {
                completion(beneficiary)
            }
        }
    }
    
    func deleteBeneficiary(params: [String: String], controller: UIViewController, completion: @escaping (_ : Any) -> Void) -> Void {
        alamofire(endpoint: "\(host)/gw/p/withdraw/address?addressId=\(params["addressId"]!)&ga_code=\(params["gaCode"]!)", method: .delete, token: true, params: [:], controller: controller) { result in
            
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    func postWithdraw(params: [String: String], controller: UIViewController, completion: @escaping (_ : Any) -> Void) -> Void {
        alamofire(endpoint: "\(host)/gw/w/withdraw", method: .post, token: true, params: params, controller: controller) { result in
            
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    func getAdvanceKycInfo(params: [String: String], controller: UIViewController, completion: @escaping (_ : AdvanceKYCInfo) -> Void) -> Void {
        alamofire(endpoint: "\(host)/kyc/get/advance/info", method: .get, token: true, params: [:], controller: controller) { result in
            
            print("advance kyc result:")
            print(result)
            
            
            let advanceKYCInfo = AdvanceKYCInfo.init(aJSON: result as! JSON)
            
            print(advanceKYCInfo.primaryAuthStatus)
            print(advanceKYCInfo.r)
            
            DispatchQueue.main.async {
                completion(advanceKYCInfo)
            }
        }
    }
    
    func getJobStatus(params: [String: String], controller: UIViewController, completion: @escaping (_ : [OptionData]) -> Void) -> Void {
        alamofire(endpoint: "\(host)/gw/w/res/job_status", method: .get, token: false, params: [:], controller: controller) { result in
            
            var jobStatusArray: [OptionData] = []
            
            let response = JSON(result)
        
            for elem in response.arrayValue {
                jobStatusArray.append(OptionData.init(aJSON: elem))
            }
            
            let sorted = jobStatusArray.sorted(by: { $0.weight ?? 0 < $1.weight ?? 0 })
            
            DispatchQueue.main.async {
                completion(sorted)
            }
        }
    }
    
    func getRevenueSource(params: [String: String], controller: UIViewController, completion: @escaping (_ : [OptionData]) -> Void) -> Void {
        alamofire(endpoint: "\(host)/gw/w/res/revenue_sources", method: .get, token: false, params: [:], controller: controller) { result in
            
            var revenueSourcesArray: [OptionData] = []
                
            let response = JSON(result)
        
            for elem in response.arrayValue {
                revenueSourcesArray.append(OptionData.init(aJSON: elem))
            }
            
            let sorted = revenueSourcesArray.sorted(by: { $0.weight ?? 0 < $1.weight ?? 0 })
            
            DispatchQueue.main.async {
                completion(sorted)
            }
        }
    }
    
    func getIndustry(params: [String: String], controller: UIViewController, completion: @escaping (_ : [OptionData]) -> Void) -> Void {
        alamofire(endpoint: "\(host)/gw/w/res/industry", method: .get, token: false, params: [:], controller: controller) { result in
        
            var industryArray: [OptionData] = []
                
            let response = JSON(result)
        
            for elem in response.arrayValue {
                industryArray.append(OptionData.init(aJSON: elem))
            }
            
            let sorted = industryArray.sorted(by: { $0.weight ?? 0 < $1.weight ?? 0 })
            
            DispatchQueue.main.async {
                completion(sorted)
            }
        }
    }
    
    func postSetKYCResident(params: [String: String], controller: UIViewController, completion: @escaping (_ : Any) -> Void) -> Void {
        alamofire(endpoint: "\(host)/kyc/set/advance/resident", method: .post, token: true, params: params, controller: controller) { result in
            
            print("post kyc resident:")
            print(result)
            
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    func postSetKYCEmployment(params: [String: String], controller: UIViewController, completion: @escaping (_ : Any) -> Void) -> Void {
        alamofire(endpoint: "\(host)/kyc/set/advance/employment", method: .post, token: true, params: params, controller: controller) { result in
            
            print("post kyc employment:")
            print(result)
            
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    func getPreupload(params: [String: String], controller: UIViewController, completion: @escaping (_ : Preupload) -> Void) -> Void {
        
        print("preupload")
        print(params)
        
        alamofire(endpoint: "\(host)/kyc/get/preupload?type=\(params["type"]!)", method: .get, token: true, params: [:], controller: controller) { result in
            
            let preupload = Preupload.init(aJSON: result as! JSON)

            DispatchQueue.main.async {
                completion(preupload)
            }
        }
    }
    
    func getDepositRecord(params: [String: String], controller: UIViewController, completion: @escaping (_ : [Transaction]) -> Void) -> Void {
        alamofire(endpoint: "\(host)/gw/w/deposit/records?current=\(1)&size=\(50)", method: .get, token: true, params: [:], controller: controller) { result in
            
            let transactionResponse = TransactionResponse.init(aJSON: result as! JSON)
            
            DispatchQueue.main.async {
                completion(transactionResponse.records!)
            }
        }
    }
    
    func getWithdrawRecord(params: [String: String], controller: UIViewController, completion: @escaping (_ : [Transaction]) -> Void) -> Void {
        alamofire(endpoint: "\(host)/gw/w/withdraw/records?current=\(1)&size=\(50)", method: .get, token: true, params: [:], controller: controller) { result in

            let transactionResponse = TransactionResponse.init(aJSON: result as! JSON)
            
            DispatchQueue.main.async {
                completion(transactionResponse.records!)
            }
        }
    }
    
    func postSubmitKYC(params: [String: String], controller: UIViewController, completion: @escaping (_ : Any) -> Void) -> Void  {
        alamofire(endpoint: "\(host)/kyc/submit/advance", method: .post, token: true, params: [:], controller: controller) { result in
            
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    // MARK: Market
    
    func getHotMarket(params: [String: String], controller: UIViewController, completion: @escaping (_ : [Market]) -> Void) -> Void {
        alamofire(endpoint: "\(host)/trade/market/hot", method: .get, token: false, params: [:], controller: controller) { result in
            
            var markets: [Market] = []
            let response = JSON(result)

            for elem in response.arrayValue {
                markets.append(Market.init(aJSON: elem))
            }
            
            DispatchQueue.main.async {
                completion(markets)
            }
        }
    }
    
    func getMarketGroup(params: [String: String], controller: UIViewController, completion: @escaping (_ : [String]) -> Void) -> Void {
        alamofire(endpoint: "\(host)/trade/market/group", method: .get, token: false, params: [:], controller: controller) { result in
            
            var marketGroups: [String] = []
            let response = JSON(result)
            
            for elem in response.arrayValue {
                marketGroups.append("\(elem)")
            }

            DispatchQueue.main.async {
                completion(marketGroups)
            }
        }
    }
    
    func getMarketList(params: [String: String], controller: UIViewController, completion: @escaping (_ : [Market]) -> Void) -> Void {
        alamofire(endpoint: "\(host)/trade/market/list?marketCoin=\(params["marketCoin"] ?? "")", method: .get, token: false, params: [:], controller: controller) { result in
            
            print("market result")
            print(result)
            
            var markets: [Market] = []
            let response = JSON(result)
            
            for elem in response.arrayValue {
                markets.append(Market.init(aJSON: elem))
            }
            
            DispatchQueue.main.async {
                completion(markets)
            }
        }
    }
    
    func getUserInfo(params: [String: String], controller: UIViewController, completion: @escaping (_ : User) -> Void) -> Void {
        alamofire(endpoint: "\(host)/user/user/info", method: .get, token: true, params: params, controller: controller) { result in
            
            print("user result:")
            print(result)
            
            let user = User.init(aJSON: result as! JSON)
            
            DispatchQueue.main.async {
                completion(user)
            }
        }
    }
    
    func getVipInfo(params: [String: Any], controller: UIViewController, completion: @escaping (_ : VipData) -> Void) -> Void {
        alamofire(endpoint: "\(host)/gw/w/vip", method: .get, token: true, params: params, controller: controller) { result in
            
            let vipData = VipData.init(aJSON: result as! JSON)
            
            DispatchQueue.main.async {
                completion(vipData)
            }
        }
    }
    
    func getCurrenciesList(params: [String: String], controller: UIViewController, completion: @escaping (_ : [Currency]) -> Void) -> Void {
        alamofire(endpoint: "\(host)/gw/w/exchange/rate", method: .get, token: false, params: params, controller: controller) { result in
            
            var currencies: [Currency] = []
            let response = JSON(result)
            
            for elem in response.arrayValue {
                currencies.append(Currency.init(aJSON: elem))
            }
            
            DispatchQueue.main.async {
                completion(currencies)
            }
        }
    }
    
    func getMarketData(params: [String: String], controller: UIViewController, completion: @escaping (_ : MarketData) -> Void) -> Void {
        alamofire(endpoint: "\(host)/trade/list?pair=\(params["pair"] ?? "")&depthGroup=\(params["depthGroup"] ?? "")&filter=\(params["filter"] ?? "")&interval=\(params["interval"] ?? "1m")", method: .get, token: false, params: params, controller: controller) { result in
            
            let marketData = MarketData.init(aJSON: result as! JSON)
            
            print("marketData")
            print(marketData)
            
            DispatchQueue.main.async {
                completion(marketData)
            }
        }
    }
    
    // MARK: Order
    func getOrderList(params: [String: Any], controller: UIViewController, completion: @escaping (_ : OrderData) -> Void) -> Void {
        alamofire(endpoint: "\(host)/order/list?current=\(params["current"]!)&size=\(params["size"]!)&status=\(params["status"]!)&pair=\(params["pair"]!)", method: .get, token: true, params: params, controller: controller) { result in
            
            print("order result:")
            print(result)
            
            let orderData = OrderData.init(aJSON: result as! JSON)
            
            DispatchQueue.main.async {
                completion(orderData)
            }
        }
    }
    
    func getChildOrderList(params: [String: String], controller: UIViewController, completion: @escaping (_ : ChildOrderData) -> Void) -> Void {
        // Category, 1: Buy, 2: Sell
        let childOrderUrl = (params["category"] == "1") ? "\(host)/trade/detail?buyOrderId=\(params["orderId"]!)" : "\(host)/trade/detail?sellOrderId=\(params["orderId"]!)"
        alamofire(endpoint: childOrderUrl, method: .get, token: true, params: [:], controller: controller) { result in
            
            let childOrderData = ChildOrderData.init(aJSON: result as! JSON)
            
            DispatchQueue.main.async {
                completion(childOrderData)
            }
        }
    }
    
    func postOrderPlacing(params: [String: Any], controller: UIViewController, completion: @escaping (_ : Any) -> Void) -> Void {
        alamofire(endpoint: "\(host)/order/place", method: .post, token: true, params: params, controller: controller) { result in
            
            print("order result")
            print(result)
            
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    func cancelOrder(params: [String: Any], controller: UIViewController, completion: @escaping (_ : Any) -> Void) -> Void {
        alamofire(endpoint: "\(host)/order/cancel", method: .put, token: true, params: params, controller: controller) { result in       
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    func postErrorLog(params: [String: Any], controller: UIViewController, completion: @escaping (_ : Any) -> Void) -> Void {
        alamofire(endpoint: "\(host)/app/preLoginErrorLog", method: .post, token: false, params: params, controller: controller) { result in
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    // Notifications
    func getNotificationMessages(params: [String: Any], controller: UIViewController, completion: @escaping (_ : NotificationData) -> Void) -> Void {
        alamofire(endpoint: "\(host)/gw/w/notifications", method: .get, token: true, params: params, controller: controller) { result in
            
            let notificationData = NotificationData.init(aJSON: result as! JSON)
            
            DispatchQueue.main.async {
                completion(notificationData)
            }
        }
    }
    
    func getReadNotificationMessage(params: [String: Any], controller: UIViewController, completion: @escaping (_ : Any) -> Void) -> Void {
        alamofire(endpoint: "\(host)/gw/w/notifications/\(params["messageId"]!)", method: .get, token: true, params: [:], controller: controller) { result in
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    func deleteNotificationMessage(params: [String: Any], controller: UIViewController, completion: @escaping (_ : Any) -> Void) -> Void {
        alamofire(endpoint: "\(host)/gw/w/notifications/\(params["messageId"]!)", method: .delete, token: true, params: [:], controller: controller) { result in
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
//    func getNotificationReadMessages(params: [String: Any], controller: UIViewController, completion: @escaping (_ : Any) -> Void) -> Void {
//        alamofire(endpoint: "\(host)/gw/w/notifications/read_msgs)", method: .get, token: true, params: [:], controller: controller) { result in
//            DispatchQueue.main.async {
//                completion(result)
//            }
//        }
//    }
    
    func getNotificationUnreadCount(params: [String: Any], controller: UIViewController, completion: @escaping (_ : Any) -> Void) -> Void {
        alamofire(endpoint: "\(host)/gw/w/notifications/unread_msg_count", method: .get, token: true, params: [:], controller: controller) { result in
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
}
